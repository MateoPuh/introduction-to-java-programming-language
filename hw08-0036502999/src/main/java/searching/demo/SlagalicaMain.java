package searching.demo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import searching.algorithms.Node;
import searching.algorithms.SearchUtil;
import searching.slagalica.KonfiguracijaSlagalice;
import searching.slagalica.Slagalica;
import searching.slagalica.gui.SlagalicaViewer;

/**
 * This class demonstrates tree search algorithms on
 * puzzle.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class SlagalicaMain {
	
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("Wrong number of arguments");
			System.exit(1);
		}
		
		if(args[0].length() != 9) {
			System.out.println("Invalid number of elements in array");
			System.exit(1);
		}
		
		int[] polje = new int[9];
		
		for(int i = 0; i < 9; i++) {
			if(!args[0].contains(String.valueOf(i))) {
				System.out.println("Array does not contain value: " + i);
				System.exit(1);
			}
			
			polje[i] = Integer.parseInt(String.valueOf(args[0].charAt(i)));
		}
		
		
		
		Slagalica slagalica = new Slagalica(
				new KonfiguracijaSlagalice(polje)
		);

		Node<KonfiguracijaSlagalice> rješenje =
				SearchUtil.bfsv(slagalica, slagalica, slagalica);

		if(rješenje==null) {
			System.out.println("Nisam uspio pronaći rješenje.");
		} else {
			SlagalicaViewer.display(rješenje);
			System.out.println(
					"Imam rješenje. Broj poteza je: " + rješenje.getCost()
					);
		
			List<KonfiguracijaSlagalice> lista = new ArrayList<>();
			Node<KonfiguracijaSlagalice> trenutni = rješenje;
			
			while(trenutni != null) {
				lista.add(trenutni.getState());
				trenutni = trenutni.getParent();
			}
			
			Collections.reverse(lista);
			
			lista.stream().forEach(k -> {
				System.out.println(k);
				System.out.println();
			});
		}
	}

}
