package searching.algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * This class contains tree search algorithms: 
 * bfs and bfsv.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class SearchUtil {

	/**
	 * Bfs algorithm.
	 * 
	 * @param s0 start state
	 * @param succ successors function
	 * @param goal goal function
	 * @return node
	 */
	public static <S> Node<S> bfs(
			 Supplier<S> s0,
			 Function<S, List<Transition<S>>> succ,
			 Predicate<S> goal){
		List<Node<S>> toSearch = new LinkedList<Node<S>>();
		toSearch.add(new Node<S>(null, s0.get(), 0));
		
		while(!toSearch.isEmpty()) {
			Node<S> ni = toSearch.remove(0);
						
			if(goal.test(ni.getState())) {
				return ni;
			}
			
			List<Transition<S>> successors = succ.apply(ni.getState());
						
			for(Transition<S> successor : successors) {
				Node<S> newNode =new Node<S>(ni, successor.getState(), ni.getCost() + successor.getCost());				
				toSearch.add(newNode);
			}						
		}
		
		return null;
	}
	
	/**
	 * Bfsv algorithm.
	 * 
	 * @param s0 start state
	 * @param succ successors function
	 * @param goal goal function
	 * @return node
	 */
	public static <S> Node<S> bfsv(
			 Supplier<S> s0,
			 Function<S, List<Transition<S>>> succ,
			 Predicate<S> goal){
		List<Node<S>> toSearch = new LinkedList<Node<S>>();
		toSearch.add(new Node<S>(null, s0.get(), 0));
		
		Set<S> visited = new HashSet<S>();
		visited.add(s0.get());
		
		while(!toSearch.isEmpty()) {
			Node<S> ni = toSearch.remove(0);
									
			if(goal.test(ni.getState())) {
				return ni;
			}
			
			List<Transition<S>> successors = succ.apply(ni.getState());
									
			for(Transition<S> successor : successors) {
				if(!visited.contains(successor.getState())) {
					Node<S> newNode =new Node<S>(ni, successor.getState(), ni.getCost() + successor.getCost());				

					toSearch.add(newNode);
					visited.add(successor.getState());
				}
			}				
		}
		
		return null;
	}

	
}
