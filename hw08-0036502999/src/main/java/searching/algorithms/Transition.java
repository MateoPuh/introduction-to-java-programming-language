package searching.algorithms;

/**
 * This class represents transition between states.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 * @param <S> type of transition
 */
public class Transition<S> {
	
	private S state;
	private double cost;
	
	/**
	 * Creates new {@link Transition} with given state and cost.
	 * 
	 * @param state state
	 * @param cost cost
	 */
	public Transition(S state, double cost) {
		super();
		this.state = state;
		this.cost = cost;
	}
	
	/**
	 * Returns state of transition.
	 * 
	 * @return state
	 */
	public S getState() {
		return state;
	}
	
	/**
	 * Returns cost of transition.
	 * 
	 * @return cost
	 */
	public double getCost() {
		return cost;
	}

}
