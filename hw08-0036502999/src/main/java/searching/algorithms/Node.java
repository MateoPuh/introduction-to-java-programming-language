package searching.algorithms;

/**
 * This class represents Node in tree search algorithm.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 * @param <S> type of Node
 */
public class Node<S> {
	
	private Node<S> parent;
	private S state;
	private double cost;
	
	/**
	 * Creates new {@link Node} with given parent node, 
	 * state and cost.
	 * 
	 * @param parent parent node
	 * @param state state
	 * @param cost cost
	 */
	public Node(Node<S> parent, S state, double cost) {
		this.parent = parent;
		this.state = state;
		this.cost = cost;
	}

	/**
	 * Returns parent node.
	 * 
	 * @return parent node
	 */
	public Node<S> getParent() {
		return parent;
	}
	
	/**
	 * Returns state of node.
	 * 
	 * @return state
	 */
	public S getState() {
		return state;
	}
	
	/**
	 * Returns cost of node.
	 * 
	 * @return cost
	 */
	public double getCost() {
		return cost;
	}

}
