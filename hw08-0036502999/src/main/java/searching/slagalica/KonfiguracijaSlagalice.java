package searching.slagalica;

import java.util.Arrays;

/**
 * This class represents state of a {@link Slagalica}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class KonfiguracijaSlagalice {
	
	private int[] polje;

	/**
	 * Creates new {@link KonfiguracijaSlagalice} with given
	 * array.
	 * 
	 * @param polje array 
	 */
	public KonfiguracijaSlagalice(int[] polje) {
		this.polje = polje;
	}

	/**
	 * Returns array that represents state of a {@link Slagalica}.
	 * 
	 * @return array that represents state of {@link Slagalica}
	 */
	public int[] getPolje() {
		return Arrays.copyOf(polje, polje.length);
	}
	
	/**
	 * Returns index of empty space in array.
	 * 
	 * @return index of empty space
	 */
	public int indexOfSpace() {
		for(int i = 0; i < polje.length; i++) {
			if(polje[i] == 0) {
				return i;
			}
		}
		
		return -1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(polje);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof KonfiguracijaSlagalice))
			return false;
		KonfiguracijaSlagalice other = (KonfiguracijaSlagalice) obj;
		return Arrays.equals(polje, other.polje);
	}

	@Override
	public String toString() {		
		String[] poljeCopy = new String[9];
		
		for(int i = 0; i < polje.length; i++) {
			if(polje[i] == 0) {
				poljeCopy[i] = "*";
			} else {
				poljeCopy[i] = String.valueOf(polje[i]);
			}
		}
		
		return poljeCopy[0] + " " + poljeCopy[1] + " " + poljeCopy[2] + "\n" 
				+ poljeCopy[3] + " " + poljeCopy[4] + " " + poljeCopy[5] + "\n" 
				+ poljeCopy[6] + " " + poljeCopy[7] + " " + poljeCopy[8] + "\n";
	}
	
}
