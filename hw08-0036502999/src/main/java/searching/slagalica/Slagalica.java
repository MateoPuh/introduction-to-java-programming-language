package searching.slagalica;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import searching.algorithms.*;

/**
 * This class represents puzzle and structure
 * needed for tree search algorithms.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Slagalica implements Supplier<KonfiguracijaSlagalice>, 
	Function<KonfiguracijaSlagalice,List<Transition<KonfiguracijaSlagalice>>>, 
	Predicate<KonfiguracijaSlagalice> {

	private KonfiguracijaSlagalice startingConfiguration;
	
	/**
	 * Creates new {@link Slagalica} with given 
	 * {@link KonfiguracijaSlagalice}.
	 * 
	 * @param startingConfiguration state of {@link Slagalica}
	 */
	public Slagalica(KonfiguracijaSlagalice startingConfiguration) {
		this.startingConfiguration = startingConfiguration;
	}

	@Override
	public boolean test(KonfiguracijaSlagalice t) {
		int[] correctArray = {1, 2, 3, 4, 5, 6, 7, 8, 0};
		
		return Arrays.equals(t.getPolje(), correctArray);
	}

	@Override
	public List<Transition<KonfiguracijaSlagalice>> apply(KonfiguracijaSlagalice t) {
		List<Transition<KonfiguracijaSlagalice>> configs = new ArrayList<>();
		
		int index = t.indexOfSpace();
		
		// up
		if(index > 2) {
			int[] array = swap(t.getPolje(), index, index - 3);
			configs.add(
					new Transition<KonfiguracijaSlagalice>(
							new KonfiguracijaSlagalice(array), 1)
			);
		}
		
		// down
		if(index < 6) {
			int[] array = swap(t.getPolje(), index, index + 3);
			configs.add(
					new Transition<KonfiguracijaSlagalice>(
							new KonfiguracijaSlagalice(array), 1)
			);
		}
		
		// right
		if(index % 3 != 2) {
			int[] array = swap(t.getPolje(), index, index + 1);
			configs.add(
					new Transition<KonfiguracijaSlagalice>(
							new KonfiguracijaSlagalice(array), 1)
			);
		}
		
		// right
		if(index % 3 != 0) {
			int[] array = swap(t.getPolje(), index, index - 1);
			configs.add(
					new Transition<KonfiguracijaSlagalice>(
							new KonfiguracijaSlagalice(array), 1)
			);
		}
		
		return configs;
	}

	@Override
	public KonfiguracijaSlagalice get() {
		return startingConfiguration;
	}
	
	/**
	 * Switches elements in array.
	 * 
	 * @param array array
	 * @param index1 index of first element
	 * @param index2 index of second element
	 * @return array
	 */
	private static int[] swap(int[] array, int index1, int index2) {
		int temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
		
		return array;
	}

}
