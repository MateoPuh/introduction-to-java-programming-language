package hr.fer.zemris.java.hw06.shell;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import hr.fer.zemris.java.hw06.shell.commands.MassrenameCommand;

/**
 * This class represents Parser which creates {@link NameBuilder}
 * object from expression given in constructor in {@link String} 
 * format.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class NameBuilderParser {

	/** Regex that matches substitution commands. */
	private static final String SUBSTITUTION_REGEX = "\\$\\{[^\\}]*\\}";
	
	/** Character that separates arguments in command. */
	private static final String ARGUMENT_SEPARATOR = ",";
	/** String that indicates beginning of substitution command. */
	private static final String SUBSTITUTION_BEGINNING = "${";
	/** String that indicates end of substitution command. */
	private static final String SUBSTITUTION_END = "}";
	
	/** Character 0 used when padding with 0 is needed. */
	private static final char ZERO_PADDING = '0';
	/** Character ' ' used when padding with 0 is not needed. */
	private static final char BLANK_PADDING = ' ';
	
	private NameBuilder nameBuilder;
	
	/**
	 * This class represents substitution command in {@link MassrenameCommand}.
	 * It is used in 'show' and 'execute' subcommands.
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 *
	 */
	private static class SubstitutionCommand {
		
		/** Number of group that is substituted. */
		private Integer groupNumber;
		/** Substitution extra info. */
		private Integer minimalSpace = null;
		/** Indicates whether to fill remaining space with zeroes. */ 
		private boolean fillZeroes = false;
		
		/**
		 * Creates new {@link SubstitutionCommand} with given group number 
		 * and extra info. Extra info is given in String format from which
		 * information about filling with zeroes and minimal number of spaces 
		 * is taken. 
		 * 
		 * @param groupNumber group number
		 * @param extraInfo extra info
		 * @throws IllegalArgumentException if given group number or minimal
		 * 		space number is negative
		 */
		public SubstitutionCommand(int groupNumber, String extraInfo) {
			if(groupNumber < 0) {
				throw new IllegalArgumentException("groupNumber should ne non-negative value.");
			}
			this.groupNumber = groupNumber;
			
			if(extraInfo == null || extraInfo.trim().isEmpty()) {
				return;
			}
			
			String minSpace = extraInfo;
			if(extraInfo.trim().startsWith("0") && extraInfo.trim().length() >= 2) {
				this.fillZeroes = true;
				minSpace = extraInfo.substring(1, extraInfo.length());
			}
			
			int minSpaceNum = Integer.parseInt(minSpace.trim());
			if(minSpaceNum < 0) {
				throw new IllegalArgumentException("extraInfo should ne non-negative value.");
			}
			
			this.minimalSpace = minSpaceNum;
		}
		
		/**
		 * Creates new {@link SubstitutionCommand} with given group number.
		 * Minimal space number is set to 0.
		 * 
		 * @param groupNumber group number
		 * @throws IllegalArgumentException if given group number is negative
		 */
		public SubstitutionCommand(int groupNumber) {
			this(groupNumber, null);
		}
		
		/**
		 * Returns list of substitution commands from given expression.
		 * 
		 * @param expression expression from which commands will be taken
		 * @return list of substitution commands
		 * @throws NumberFormatException if arguments are not in Integer
		 * 			format
		 */
		public static List<SubstitutionCommand> extractCommands(String expression) {			
			List<String> commands = getExpressionParts(expression, true);
			List<SubstitutionCommand> subtitutionCommands = new ArrayList<>();
			
			for(String command : commands) {
				// extracts arguments from command
				String args = command.replace(SUBSTITUTION_BEGINNING, "").replace(SUBSTITUTION_END, "");
				
				// if command contains ',' then treat it like it has 2 arguments
				if(args.contains(ARGUMENT_SEPARATOR)) {
					int first = Integer.parseInt(args.substring(0, args.indexOf(ARGUMENT_SEPARATOR)));
					
					subtitutionCommands.add(new SubstitutionCommand(
							first, args.substring(args.indexOf(ARGUMENT_SEPARATOR) + 1, args.length()))
					);					
					continue;
				}

				// if there is no ',' treat it as a single argument
				int first = Integer.parseInt(args);
				
				subtitutionCommands.add(new SubstitutionCommand(first));	
			}
			
			return subtitutionCommands;
		}

		/**
		 * Returns number of group.
		 * 
		 * @return number of group
		 */
		public Integer getGroupNumber() {
			return groupNumber;
		}

		/**
		 * Returns minimal space that command takes.
		 * 
		 * @return minimal space that command takes
		 */
		public Integer getMinimalSpace() {
			return minimalSpace;
		}

		/**
		 * Returns whether remaining space is filled with zeroes.
		 * 
		 * @return true if remaining space is filled with zeroes
		 */
		public boolean isFillZeroes() {
			return fillZeroes;
		}
		
	}
	
	/**
	 * Creates new {@link NameBuilderParser} with given expression and parses
	 * a {@link NameBuilder} object.
	 * 
	 * @param expression expression from which {@link NameBuilder} is parsed
	 */
	public NameBuilderParser(String expression) {
		List<SubstitutionCommand> subs = SubstitutionCommand.extractCommands(expression);
		List<String> expressionParts = getExpressionParts(expression, false);
		
		this.nameBuilder = createNameBuilder(expressionParts, subs);
	}
	
	/**
	 * Returns {@link NameBuilder} parsed from given expression.
	 * 
	 * @return name builder
	 */
	public NameBuilder getNameBuilder() {
		return nameBuilder;
	}
	
	/**
	 * Splits String by substitution commands, which begin with "${" and end
	 * with "}". String is split in a way that separates part with no commands
	 * and commands.
	 * 
	 * @param expression expression to split
	 * @return separated expression
	 */
	private static List<String> splitExpressionByCommands(String expression) {
		Pattern substitution = Pattern.compile(SUBSTITUTION_REGEX, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Matcher matcher = substitution.matcher(expression);
		
		List<String> parts = new ArrayList<>();
		
		while (matcher.find()) {
			String command = matcher.group();						
			String left = expression.substring(0, expression.indexOf(SUBSTITUTION_BEGINNING));
			parts.add(left);
			parts.add(command);
			
			expression = expression.substring(expression.indexOf(SUBSTITUTION_END) + 1, expression.length());
		} 
		
		return parts;
	}
	
	/**
	 * If given boolean flag commands is true, it returns commands 
	 * extracted from given expression. If flag commands is false, 
	 * it returns part of given expression that don't contain commands.
	 * 
	 * @param expression expression
	 * @param commands commands are returns if true; otherwise parts
	 * 		without commands are returned
	 * @return parts of expression
	 */
	private static List<String> getExpressionParts(String expression, boolean commands) {
		List<String> expressionParts = splitExpressionByCommands(expression);
		Pattern commandPattern = Pattern.compile(SUBSTITUTION_REGEX, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		
		List<String> parts = expressionParts.stream()
				.filter(p -> {
					Matcher matcher = commandPattern.matcher(p);
					return !matcher.matches() && !commands
							|| matcher.matches() && commands;
				})
				.collect(Collectors.toList());
		
		return parts;
	}
	
	/**
	 * Substitutes substitution commands with actual data.
	 * 
	 * @param expression parts of expression without commands
	 * @param filterResult result of filtering
	 * @param commands substitution commands
	 * @return substituted Expression 
	 * @throws IllegalArgumentException if error occurs
	 */
	private static NameBuilder createNameBuilder(List<String> expression, List<SubstitutionCommand> commands) {
		if((expression.size() != commands.size() + 1)
				&& (expression.size() != commands.size())) {
			throw new IllegalArgumentException("Wrong number of commands");
		}
		
		NameBuilder nameBuilder = NameBuilder.blank();
		
		for(int i = 0; i < commands.size(); i++) {			
			nameBuilder = nameBuilder.then(NameBuilder.text(expression.get(i)));
			
			SubstitutionCommand sub = commands.get(i);
			int groupNum = sub.getGroupNumber();			

			if(sub.getMinimalSpace() != null) {
				nameBuilder = nameBuilder.then(
						NameBuilder.group(groupNum, sub.isFillZeroes() ? ZERO_PADDING : BLANK_PADDING, sub.getMinimalSpace()));
			}
			nameBuilder = nameBuilder.then(NameBuilder.group(groupNum));
		}
		
		if(expression.size() == commands.size() + 1) {
			nameBuilder = nameBuilder.then(NameBuilder.text(expression.get(expression.size() - 1)));
		}
		
		return nameBuilder;
	}
	
}
