package hr.fer.zemris.java.hw06.shell;

import java.util.List;

/**
 * This class represents result of filtering files in directory.
 * It contains name of a file and list of groups.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class FilterResult {
	
	/** Name of a file. */
	private String filename;
	/** List of groups. */
	private List<String> groups;
	
	/**
	 * Creates new {@link FilterResult} with given filename and
	 * list of groups.
	 * 
	 * @param filename name of a file
	 * @param groups list of groups
	 */
	public FilterResult(String filename, List<String> groups) {
		this.filename = filename;
		this.groups = groups;
	}
	
	/**
	 * Returns name of a file.
	 * 
	 * @return filename
	 */
	public String toString() {
		return filename;
	}
	
	/**
	 * Returns number of found groups.
	 * 
	 * @return number of groups
	 */
	public int numberOfGroups() {
		return groups.size();
	}
	
	/**
	 * Returns group with given index.
	 * Index should be between 0 and numberOfGroups().
	 * 
	 * @param index index of group
	 * @return group with given index
	 * @throws IndexOutOfBoundsException if index is out of range
	 */
	public String group(int index) {
		return groups.get(index);
	}

}
