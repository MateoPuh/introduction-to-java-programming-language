package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.FilterResult;
import hr.fer.zemris.java.hw06.shell.NameBuilder;
import hr.fer.zemris.java.hw06.shell.NameBuilderParser;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents massrename command.
 * It is used for mass renaming or replacing files.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class MassrenameCommand extends Command {
	
	/** Minimal number of arguments of massrename command. */
	private static final int MIN_NUMBER_OF_ARGUMENTS = 4;
	/** Maximum number of arguments of massrename command. */
	private static final int MAX_NUMBER_OF_ARGUMENTS = 5;
	
	/** Filter subcommand. */
	private static final String FILTER = "filter";
	/** Groups subcommand. */
	private static final String GROUPS = "groups";
	/** Show subcommand. */
	private static final String SHOW = "show";
	/** Execute subcommand. */
	private static final String EXECUTE = "execute";
	
	/**
	 * Creates new {@link MassrenameCommand} and initializes it's description.
	 */
	public MassrenameCommand() {
		NAME = "massrename";
		
		List<String> description = new ArrayList<>();
		
		description.add("massrename DIR1 DIR2 CMD MASK [rest]");
		description.add("");
		description.add("-DIR1 : source directory");
		description.add("-DIR2 : destination directory");
		description.add("-CMD : subcommand");
		description.add("-MASK : regex");
		description.add("-rest : optinal additional arguments");
		description.add("");
		description.add("Command \'massrename\' takes 4 or 5 argument: DIR1, which represents");
		description.add("directory that contains files which will be used in this command, DIR2,");
		description.add("which represents destination directory in which files will be copied to,");
		description.add("CMD, which represents subcommand which determines in which way files will");
		description.add("be used and MASK which represents regex which determines which files");
		description.add("will be used.");
		description.add("");
		description.add("Subcommand CMD can be filter, which filters files and displays them");
		description.add("on console, groups, which groups files and displays them, show, which");
		description.add("displays files in way that is defined in EXPRESSION part of command,");
		description.add("which will be fifth argument and finally, subcommand execute does");
		description.add("everything like show, but additionally, it copies files to DIR2 in");
		description.add("a way that is defines by EXPRESSION part.");

		MassrenameCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);
		
		// wrong mumber of arguments
		if(argument.length < MIN_NUMBER_OF_ARGUMENTS
				|| argument.length > MAX_NUMBER_OF_ARGUMENTS) {
			env.writeln("Command \'massrename\' takes 4 or 5 arguments.");
			return ShellStatus.CONTINUE;
		}
		
		Path dir1 = CommandUtil.getAbsolute(argument[0], env);
				
		// if given directory does not exist
		if(!Files.exists(dir1)) {
			env.writeln(argument[0] + " does not exist.");
			return ShellStatus.CONTINUE;
		}

		// if given path is not directory
		if(!Files.isDirectory(dir1)) {
			env.writeln(argument[0] + " is a file.");
			return ShellStatus.CONTINUE;
		}

		Path dir2 = CommandUtil.getAbsolute(argument[1], env);

		// if given directory does not exist
		if(!Files.exists(dir2)) {
			env.writeln(argument[1] + " does not exist.");
			return ShellStatus.CONTINUE;
		}

		// if given path is not directory
		if(!Files.isDirectory(dir2)) {
			env.writeln(argument[1] + " is a file.");
			return ShellStatus.CONTINUE;
		}
		
		String subcommand = argument[2];
		String regex = argument[3];
		
		try {
			List<FilterResult> results = filter(dir1, regex);
			
			switch(subcommand) {
			case FILTER:
				if(argument.length != 4) {
					env.writeln("Invalid number of arguments for filter subcommand.");
					return ShellStatus.CONTINUE;
				}
				
				for(FilterResult result : results) {
					env.writeln(result.toString());
				}
				return ShellStatus.CONTINUE;
			case GROUPS:
				if(argument.length != 4) {
					env.writeln("Invalid number of arguments for groups subcommand.");
					return ShellStatus.CONTINUE;
				}
				
				for(FilterResult result : results) {
					env.write(result.toString());
					for(int i = 0; i<result.numberOfGroups(); i++) {
						env.write(" " + i + ": " + result.group(i));
					}
					env.write("\n");
				}
				return ShellStatus.CONTINUE;
			case SHOW:
			case EXECUTE:
				if(argument.length != 5) {
					env.writeln("Invalid number of arguments for show subcommand.");
					return ShellStatus.CONTINUE;
				}
								
				String expression = argument[4];
				
				NameBuilderParser parser = new NameBuilderParser(expression);
				NameBuilder builder = parser.getNameBuilder();
				
				for(FilterResult result : results) {
					StringBuilder sb = new StringBuilder();
					builder.execute(result, sb);
					String novoIme = sb.toString();
					env.writeln(result.toString() + " => " + novoIme);
					if(subcommand.equals(EXECUTE)) {
						Files.move(
								Paths.get(dir1.toString(), result.toString()), 
								Paths.get(dir2.toString(), novoIme), StandardCopyOption.REPLACE_EXISTING);
					}
				}
				return ShellStatus.CONTINUE;
			default:
				env.writeln("Subcommand " + subcommand + " does not exist.");
			}
		} catch(Exception e) {
			env.writeln("Error occured while filtering files: " + e.getLocalizedMessage());
		} 
		
		return ShellStatus.CONTINUE;
	}
	
	/**
	 * Returns all files from given directory that match given pattern
	 * in form of list of {@link FilterResult}.
	 * 
	 * @param dir directory from which files are taken
	 * @param pattern pattern that files must match
	 * @return list of {@link FilterResult} that match given pattern
	 * @throws IOException if error occurs
	 */
	private static List<FilterResult> filter(Path dir, String pattern) throws IOException {
		Pattern regex = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		
		return Files.list(dir)
				.filter(Predicate.not(Files::isDirectory))
				.filter(f -> {
					Matcher matcher = regex.matcher(f.getFileName().toString());
					return matcher.matches();
				})
				.map(f -> {
					Matcher matcher = regex.matcher(f.getFileName().toString());
					List<String> groups = new ArrayList<>();
										
					while(matcher.find()) {
						for(int i = 0; i <= matcher.groupCount(); i++) {
							groups.add(matcher.group(i));
						}
					}
					
					return new FilterResult(f.getFileName().toString(), groups);
				})
				.collect(Collectors.toList());
	}
	
}