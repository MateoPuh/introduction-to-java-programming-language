package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents cat command.
 * It takes one or two arguments: filename and charset.
 * Filename is mandatory and charset is optinal. If
 * charset is not given, default one for user platform 
 * is used. It opens file and writes it to user. 
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class CatCommand extends Command {
	
	/** Minimal number of arguments for cat command. */
	private static final int MIN_NUMBER_OF_ARGUMENTS = 1;
	/** Maximal number of arguments for cat command. */
	private static final int MAX_NUMBER_OF_ARGUMENTS = 2;
	
	/**
	 * Creates new {@link CatCommand} and initializes it's description.
	 */
	public CatCommand() {
		NAME = "cat";
		
		List<String> description = new ArrayList<>();
		
		description.add("cat [file] [charset]");
		description.add("");
		description.add("-file : name of a file");
		description.add("-charset : name charset for intrepreting file");
		description.add("");
		description.add("Command \'cat\' opens file and writes it to console. It uses");
		description.add("given charset for intrepreting. If charset is not provided, ");
		description.add("default platform charset will be used.");

		DESCRIPTION = Collections.unmodifiableList(description);	
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);
		
		// Wrong number of arguments
		if(argument.length < MIN_NUMBER_OF_ARGUMENTS 
				|| argument.length > MAX_NUMBER_OF_ARGUMENTS) {
			System.out.println("Command \'cat\' takes 1 or 2 arguments.");
			return ShellStatus.CONTINUE;
		}
		
		Path path = CommandUtil.getAbsolute(argument[0], env);
		
		// if source file does not exist
		if(!Files.exists(path)) {
			env.writeln(path.toString() + " does not exist.");
			return ShellStatus.CONTINUE;
		}
		
		// Source file is not a file
		if(Files.isDirectory(path)) {
			env.writeln(path.toString() + " is not a file.");
			return ShellStatus.CONTINUE;
		}
		
		try {			 
			Charset charset = argument.length != MAX_NUMBER_OF_ARGUMENTS ?  
							Charset.defaultCharset() : Charset.forName(argument[1]);
			
			try {
				List<String> content = Files.readAllLines(path, charset);
				for(String line : content) {
					env.writeln(line);
				}
			} catch (IOException e) {
				env.writeln("Error occured while reading file " + argument[0]); 
				env.writeln(e.getLocalizedMessage());
			} 
		} catch(Exception e) {
			env.writeln("Error on charset name"); 
			env.writeln(e.getLocalizedMessage());
		}
		
		return ShellStatus.CONTINUE;
	}

}
