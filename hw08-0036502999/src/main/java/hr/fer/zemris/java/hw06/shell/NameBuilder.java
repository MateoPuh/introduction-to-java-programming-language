package hr.fer.zemris.java.hw06.shell;

/**
 * This interface represents objects that generate parts
 * of name by writing parts of name in given {@link StringBuilder}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public interface NameBuilder {

	/**
	 * Writes part of a name to {@link StringBuilder}
	 * using given {@link FilterResult} object.
	 * 
	 * @param result filter result
	 * @param sb string builder
	 */
	void execute(FilterResult result, StringBuilder sb);
	
	/**
	 * Builds binary composite of two {@link NameBuilder} objects.
	 * 
	 * @param other other name builder
	 * @return binary composite
	 */
	default NameBuilder then(NameBuilder other) {
		return (r, sb) -> {
			execute(r, sb);
			other.execute(r, sb);
		}; 
	}

	/**
	 * Creates {@link NameBuilder} that does nothing.
	 * 
	 * @return name builder
	 */
	static NameBuilder blank() {
		return (r, sb) -> {};
	}

	/**
	 * Creates {@link NameBuilder} for writing given
	 * text to {@link StringBuilder}.
	 * 
	 * @param t text to write to {@link StringBuilder}
	 * @return name builder
	 */
	static NameBuilder text(String t) { 
		return (r, sb) -> {
			sb.append(t);
		}; 
	}
	
	/**
	 * Creates {@link NameBuilder} for writing
	 * group from {@link FilterResult} with given
	 * index.
	 * 
	 * @param index index of group
	 * @return name builder
	 */
	static NameBuilder group(int index) { 
		return (r, sb) -> {
			sb.append(r.group(index));
		};
	}
	
	/**
	 * Creates {@link NameBuilder} for writing
	 * group from {@link FilterResult} with given
	 * index, padding and minimal width.
	 * 
	 * @param index index of group
	 * @param padding padding - ' ' or '0'
	 * @param minWidth minimal width
	 * @return name builder
	 */
	static NameBuilder group(int index, char padding, int minWidth) { 
		return (r, sb) -> {
			String group = r.group(index);
			int numOfSpaces = minWidth - group.length();
			
			if(numOfSpaces > 0) {
				sb.append(String.valueOf(padding).repeat(numOfSpaces));
			}
			
			sb.append(group);
		};
	}
}
