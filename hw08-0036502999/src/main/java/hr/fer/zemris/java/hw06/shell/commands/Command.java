package hr.fer.zemris.java.hw06.shell.commands;

import java.util.List;

import hr.fer.zemris.java.hw06.shell.ShellCommand;

public abstract class Command implements ShellCommand {
	
	/** Command name. */
	public static String NAME; 
	/** Description of command. */
	protected static List<String> DESCRIPTION;
	
	@Override
	public String getCommandName() {
		return NAME;
	}

	@Override
	public List<String> getCommandDescription() {
		return Command.DESCRIPTION;
	}

}
