package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents dropd command.
 * It takes no arguments. It removes directory from cdstack.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class DropdCommand extends Command{
	
	/** Key of cdstack in shared data. */
	private static final String CDSTACK_KEY = "cdstack";
	
	/**
	 * Creates new {@link DropdCommand} and initializes it's description.
	 */
	public DropdCommand() {
		NAME = "dropd";
		
		List<String> description = new ArrayList<>();
		
		description.add("dropd");
		description.add("");
		description.add("Command \'dropd\' removes path from top of cdstack.");

		DropdCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments != null && !arguments.trim().isEmpty()) {
			env.writeln("Command \'dropd\' takes no arguments.");
			return ShellStatus.CONTINUE;
		}
		
		Object data = env.getSharedData(CDSTACK_KEY);
		
		if(data == null) {
			env.writeln("\'dropd\' error: empty cdstack.");
			return ShellStatus.CONTINUE;
		}
		
		try {
			@SuppressWarnings("unchecked")
			Stack<Path> cdStack = (Stack<Path>)data;
			Path path = cdStack.pop();
			
			env.writeln(path.toString() + " removed from cdstack");
		} catch(ClassCastException e) {
			env.writeln("Error occured while pushing current working directory to stack.");
			return ShellStatus.CONTINUE;
		}
		
		return ShellStatus.CONTINUE;
	}

}