package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents exit command.
 * It takes no arguments. It closes shell.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ExitCommand extends Command{
	
	/**
	 * Creates new {@link ExitCommand} and initializes it's description.
	 */
	public ExitCommand() {
		NAME = "exit";
		
		List<String> description = new ArrayList<>();
		
		description.add("exit");
		description.add("");
		description.add("Command \'exit\' terminates shell.");

		ExitCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments == null || arguments.trim().isEmpty()) {
			return ShellStatus.TERMINATE;
		}
		
		env.writeln("Command \'exit\' takes no arguments.");
		return ShellStatus.CONTINUE;
	}

}
