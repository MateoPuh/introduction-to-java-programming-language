package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents pwd command.
 * It takes no arguments. It prints current working
 * directory.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class PwdCommand extends Command{
	
	/**
	 * Creates new {@link PwdCommand} and initializes it's description.
	 */
	public PwdCommand() {
		NAME = "pwd";
		
		List<String> description = new ArrayList<>();
		
		description.add("pwd");
		description.add("");
		description.add("Command \'pwd\' prints current directory.");

		PwdCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments != null && !arguments.trim().isEmpty()) {
			env.writeln("Command \'pwd\' takes no arguments.");
			return ShellStatus.CONTINUE;
		}
		
		env.writeln(env.getCurrentDirectory().toString());
		return ShellStatus.CONTINUE;
	}

}