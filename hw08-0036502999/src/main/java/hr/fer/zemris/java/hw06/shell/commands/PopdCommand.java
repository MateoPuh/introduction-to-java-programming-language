package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents popd command.
 * It takes no arguments. It takes directory from cdstack
 * and changes current working directory to it.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class PopdCommand extends Command{
	
	/** Key of cdstack in shared data. */
	private static final String CDSTACK_KEY = "cdstack";
	
	/**
	 * Creates new {@link PopdCommand} and initializes it's description.
	 */
	public PopdCommand() {
		NAME = "popd";
		
		List<String> description = new ArrayList<>();
		
		description.add("popd");
		description.add("");
		description.add("Command \'popd\' sets working directory to the one on top");
		description.add("of cdstack and remove it from cdstack.");

		PopdCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments != null && !arguments.trim().isEmpty()) {
			env.writeln("Command \'popd\' takes no arguments.");
			return ShellStatus.CONTINUE;
		}
		
		Object data = env.getSharedData(CDSTACK_KEY);
		
		if(data == null) {
			env.writeln("\'popd\' error: empty cdstack.");
			return ShellStatus.CONTINUE;
		}
		
		try {
			@SuppressWarnings("unchecked")
			Stack<Path> cdStack = (Stack<Path>)data;
			Path path = cdStack.pop();
			
			// if given directory does not exist
			if(!Files.exists(path)) {
				env.writeln(path.toString() + " does not exist.");
				return ShellStatus.CONTINUE;
			}

			// if given path is not directory
			if(!Files.isDirectory(path)) {
				env.writeln(path.toString() + " is a file.");
				return ShellStatus.CONTINUE;
			}
							
			try {				
				env.setCurrentDirectory(path);
			} catch(IllegalArgumentException e) {
				env.writeln("Error occured while setting directory " + path.toString()
						+ " as new working directory:\n" + e.getLocalizedMessage());
			}
		} catch(ClassCastException e) {
			env.writeln("Error occured while pushing current working directory to stack.");
			return ShellStatus.CONTINUE;
		}
		
		env.writeln(env.getCurrentDirectory().toString() + " removed from cdstack"
				+ " and set as working directory.");
		return ShellStatus.CONTINUE;
	}

}