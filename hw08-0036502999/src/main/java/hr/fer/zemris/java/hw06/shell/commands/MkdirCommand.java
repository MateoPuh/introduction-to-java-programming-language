package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents mkdir command.
 * It takes one argument: name of directory. It creates
 * directory and all nonexistent parent directories.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class MkdirCommand extends Command {

	/** Number of arguments of mkdir command. */
	private static final int NUMBER_OF_ARGUMENTS = 1;
	
	/**
	 * Creates new {@link MkdirCommand} and initializes it's description.
	 */
	public MkdirCommand() {
		NAME = "mkdir";
		
		List<String> description = new ArrayList<>();
		
		description.add("mkdir [directory]");
		description.add("");
		description.add("-directory : name of a directory");
		description.add("");
		description.add("Command \'mkdir\' creates directory and all nonexistent");
		description.add("parent directories.");

		MkdirCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);

		// wrong number of arguments
		if(argument.length != NUMBER_OF_ARGUMENTS) {
			env.writeln("Command \'mkdir\' takes 1 argument.");
			return ShellStatus.CONTINUE;
		}
		
		try {
			Path path = CommandUtil.getAbsolute(argument[0], env);
			// directory already exists
			if(Files.exists(path)) {
				env.writeln(arguments + " already exist.");
				return ShellStatus.CONTINUE;
			}
							
			try {
				Files.createDirectories(path);
				env.writeln("Directory " + argument[0] + " created.");
			} catch (IOException e) {
				env.writeln("Error occured while creating directory " + argument[0]);
				env.writeln(e.getLocalizedMessage());
			}
			
			return ShellStatus.CONTINUE;
		} catch(Exception e) {
			env.writeln("Error occured while creating directory " + argument[0]);
			env.writeln(e.getLocalizedMessage());
			return ShellStatus.CONTINUE;
		}
	}
	
}
