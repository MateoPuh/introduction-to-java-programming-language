package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents pushd command.
 * It takes one argument: path for new working directory,
 * and changes current working directory to it while pushing
 * current working directory on cdstack.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class PushdCommand extends Command {
	
	/** Number of arguments of pushd command. */
	private static final int NUMBER_OF_ARGUMENTS = 1;
	/** Key of cdstack in shared data. */
	private static final String CDSTACK_KEY = "cdstack";
	
	/**
	 * Creates new {@link PushdCommand} and initializes it's description.
	 */
	public PushdCommand() {
		NAME = "pushd";
		
		List<String> description = new ArrayList<>();
		
		description.add("pushd [path]");
		description.add("");
		description.add("-path : path of a directory");
		description.add("");
		description.add("Command \'pushd\' takes one argument: directory path, which");
		description.add("is set as working directory after current working directory");
		description.add("is pushed on cdstack.");

		PushdCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);

		// wrong mumber of arguments
		if(argument.length != NUMBER_OF_ARGUMENTS) {
			env.writeln("Command \'pushd\' takes 1 argument.");
			return ShellStatus.CONTINUE;
		}
		
		Path path = CommandUtil.getAbsolute(argument[0], env);
				
		// if given directory does not exist
		if(!Files.exists(path)) {
			env.writeln(argument[0] + " does not exist.");
			return ShellStatus.CONTINUE;
		}

		// if given path is not directory
		if(!Files.isDirectory(path)) {
			env.writeln(argument[0] + " is a file.");
			return ShellStatus.CONTINUE;
		}
						
		try {
			Path current = env.getCurrentDirectory();
			
			env.setCurrentDirectory(path);

			Object data = env.getSharedData(CDSTACK_KEY);
			
			if(data == null) {
				Stack<Path> cdStack = new Stack<Path>();
				cdStack.push(current);
				
				env.setSharedData(CDSTACK_KEY, cdStack);
			} else {
				try {
					@SuppressWarnings("unchecked")
					Stack<Path> cdStack = (Stack<Path>)data;
					cdStack.push(current);
				} catch(ClassCastException e) {
					env.writeln("Error occured while pushing current working directory to stack.");
					return ShellStatus.CONTINUE;
				}
			}
			
			env.writeln(current.toString() + " pushed to cdstack.");
		} catch(IllegalArgumentException e) {
			env.writeln("Error occured while setting directory " + argument[0]
					+ " as new working directory:\n" + e.getLocalizedMessage());
		}
		
		env.writeln("Working directory: " + env.getCurrentDirectory().toString());
		return ShellStatus.CONTINUE;
	}

}