package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents copy command.
 * It takes two arguments: source file and destination.
 * Both arguments are mandatory. Destination can be
 * file or directory. It copies source file to destination.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class CopyCommand extends Command {

	/** Number of arguments for copy command. */
	private static final int NUMBER_OF_ARGUMENTS = 2;
	
	/**
	 * Creates new {@link CopyCommand} and initializes it's description.
	 */
	public CopyCommand() {
		NAME = "copy";
		
		List<String> description = new ArrayList<>();
		
		description.add("copy [src] [dest]");
		description.add("");
		description.add("-src : name of a source file");
		description.add("-dest : name of a destination file or directory");
		description.add("");
		description.add("Command \'copy\' takes two argument: source file and destination file");
		description.add("or directory. It copies source file to the destination.");

		CopyCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);

		if(argument.length != NUMBER_OF_ARGUMENTS) {
			env.writeln("Command \'copy\' takes 2 argument.");
			return ShellStatus.CONTINUE;
		}
		
		Path src = CommandUtil.getAbsolute(argument[0], env);
		
		// if first argument does not exist
		if(!Files.exists(src)) {
			env.writeln(src.toString() + " does not exist");
			return ShellStatus.CONTINUE;
		}
		
		// if first argument is not a file
		if(Files.isDirectory(src)) {
			env.writeln(argument[0] + " is not a file.");
			return ShellStatus.CONTINUE;
		}
		
		// if arguments are same
		if(argument[0].equals(argument[1])) {
			env.writeln("Source and destination are equal: " + argument[1]);
			return ShellStatus.CONTINUE;
		}
				
		Path dest = CommandUtil.getAbsolute(argument[1], env);

		// if second argument exists
		if(Files.exists(dest)) {
			// if second argument is file then ask user if he wants 
			// to override it
			if(!Files.isDirectory(dest)) {	
				if(!toOverride(dest.toString(), env)) {
					return ShellStatus.CONTINUE;
				}				
			} else {
				// if second argument is directory then determine if 
				// second argument + "/" + first argument exist
				// and ask user if he wants to override it
				Path file = Paths.get(argument[0]);
				dest = dest.resolve(file.getFileName());
				if(Files.exists(dest)) {
					if(!toOverride(dest.toString(), env)) {
						return ShellStatus.CONTINUE;
					}					
				} 				
			}
		} else {
			// if second argument does not exist then it is treated as
			// path to new file
			
			// if the parent directory to new file does not exist
			// then quit
			if(!CommandUtil.parentExists(dest.toString())) {					
				env.writeln("Parent directory of " + dest.toString() + " does not exist.");
				return ShellStatus.CONTINUE;
			}			
		}
				
		copyFile(src.toString(), dest.toString(), env);
		env.writeln(argument[0] + " copied to " 
		+ (!Files.isDirectory(dest) ? "" : "directory ")  + argument[1]);
		return ShellStatus.CONTINUE;
	}
	
	/**
	 * Asks user to override file with given filename.
	 * Returns true if user agreed to overriding and false
	 * otherwise.
	 * 
	 * @param filename name of file
	 * @param env environment of shell
	 * @return true if user agreed; false otherwise
	 */
	private static boolean toOverride(String filename, Environment env) {
		String[] output = new String[] {"File " + filename + " already exists.", 
				"Are you sure you want to override it? (y/n)"}; 
		return getAnswer(output, env);
	}
	
	/**
	 * Gets user's answer which can be yes ('y') or
	 * no ('n').
	 * Returns true if user entered 'y' and false if user entered 'n'.
	 * If user enters something else, he is asked again to enter his 
	 * answer.
	 * 
	 * @param output output written to user
	 * @param env environment of shell
	 * @return true if user entered 'y'; false if user entered 'n'
	 */
	private static boolean getAnswer(String[] output, Environment env) {
		while(true) {
			for(String s : output) {
				System.out.println(s);
			}
			
			String answer = env.readLine();
			
			if(answer.equals("y")) {
				return true;
			}
			
			if(answer.equals("n")) {
				return false;
			}
		}
	}
	
	/**
	 * Copies given source file to the destination path.
	 * 
	 * @param filename source path
	 * @param copyPath destination path
	 * @param env environment of shell
	 * @throws IllegalArgumentException if any of given argument don't exist
	 * 		or if they are not file
	 */
	private static void copyFile(String filename, String copyPath, Environment env) {
		Path input = Paths.get(filename);
		Path output = Paths.get(copyPath);
		
		if(!Files.exists(input) || Files.isDirectory(input)) {
			throw new IllegalArgumentException("Invalid argument: " + filename);
		}
		
		try {
			InputStream is = Files.newInputStream(input);
			OutputStream os = Files.newOutputStream(output);
			
			byte[] buff = new byte[4 * 1024];
			while(true) {
				int r = is.read(buff);
				if(r<1) break;
				
				byte[] outputArray = Arrays.copyOfRange(buff, 0, r);
				
				os.write(outputArray);
			}
		} catch(IOException e) {
			env.writeln("Error occured while copying file " + filename 
					+ " to " + copyPath + ": " + e.getLocalizedMessage());
		}
		
	}

}
