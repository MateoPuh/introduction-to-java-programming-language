package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents help command.
 * It takes zero or one argument. Argument can be 
 * command name. If no arguments are given, list
 * of commands will be displayed. If argument is given,
 * the name and description of given command are displayed.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class HelpCommand extends Command {

	/** Maximal number of arguments. */
	private static final int MAX_NUMBER_OF_ARGUMENTS = 1;
	
	/**
	 * Creates new {@link HelpCommand} and initializes it's description.
	 */
	public HelpCommand() {
		NAME = "help";
		
		List<String> description = new ArrayList<>();
		
		description.add("help [command]");
		description.add("");
		description.add("Command \'help\' displays the information about given command");
		description.add("if command is given as an argument or displays all existing");
		description.add("commands if no argument is given.");

		HelpCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments == null || arguments.trim().isEmpty()) {
			SortedMap<String, ShellCommand> commands = env.commands();
			
			env.writeln("Commands:\n");
			
			for(Map.Entry<String, ShellCommand> command : commands.entrySet()) {
				env.writeln("\t" + command.getKey());
			}
			
			return ShellStatus.CONTINUE; 
		}
		
		String[] argument = CommandUtil.splitArgumentsByBlanks(arguments);
		
		if(argument.length > MAX_NUMBER_OF_ARGUMENTS) {
			env.writeln("Wrong number of arguments for command \'help\'.");
			return ShellStatus.CONTINUE;
		} 
		
		ShellCommand command = env.commands().get(argument[0]);
		env.writeln("");
		env.writeln("NAME\n");
		env.writeln("\t" + command.getCommandName() + "\n");
		env.writeln("DESCRIPTION\n");
		
		for(String line : command.getCommandDescription()) {
			env.writeln("\t" + line);
		}
		
		env.writeln("");
				
		return ShellStatus.CONTINUE;
	}

}
