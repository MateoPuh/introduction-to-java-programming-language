package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents charsets command.
 * It takes no arguments. It lists all charsets avaliable
 * for users platform.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class CharsetsCommand extends Command {
	
	/**
	 * Creates new {@link CharsetsCommand} and initializes it's description.
	 */
	public CharsetsCommand() {
		NAME = "charsets";
		
		List<String> description = new ArrayList<>();
		
		description.add("charsets");
		description.add("");
		description.add("Command \'charsets\' displays list of all supported charsets");
		description.add("for your platform.");

		CharsetsCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments != null && !arguments.trim().isEmpty()) {
			System.out.println("Command \'charsets\' takes no arguments.");
			return ShellStatus.CONTINUE;
		}
		SortedMap<String, Charset> charsets= Charset.availableCharsets();
		
		for(Map.Entry<String, Charset> entry : charsets.entrySet()) {
			env.writeln(entry.getValue().toString());
		}
		
		return ShellStatus.CONTINUE;
	}

}
