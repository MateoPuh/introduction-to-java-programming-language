package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents listd command.
 * It takes no arguments. It prints all directories 
 * from cdstack.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ListdCommand extends Command{
	
	/** Key of cdstack in shared data. */
	private static final String CDSTACK_KEY = "cdstack";
	
	/**
	 * Creates new {@link ListdCommand} and initializes it's description.
	 */
	public ListdCommand() {
		NAME = "listd";
		
		List<String> description = new ArrayList<>();
		
		description.add("listd");
		description.add("");
		description.add("Command \'listd\' displays content of cdstack starting with");
		description.add("the path that is last pushed.");

		ListdCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments != null && !arguments.trim().isEmpty()) {
			env.writeln("Command \'listd\' takes no arguments.");
			return ShellStatus.CONTINUE;
		}
		
		Object data = env.getSharedData(CDSTACK_KEY);
		
		if(data == null) {
			env.writeln("Nema pohranjenih direktorija.");
			return ShellStatus.CONTINUE;
		}
		
		try {
			@SuppressWarnings("unchecked")
			Stack<Path> cdStack = (Stack<Path>)data;
			
			Stack<Path> cdStackCopy = new Stack<Path>(); 
			
			while(!cdStack.isEmpty()) {
				Path path = cdStack.pop();
				env.writeln(path.toString());
				cdStackCopy.push(path);
			}
			
			while(!cdStackCopy.isEmpty()) {
				cdStack.push(cdStackCopy.pop());
			}
			
		} catch(ClassCastException e) {
			env.writeln("Error occured while pushing current working directory to stack.");
			return ShellStatus.CONTINUE;
		}
		
		return ShellStatus.CONTINUE;
	}

}