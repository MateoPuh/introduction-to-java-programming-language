package hr.fer.zemris.java.hw06.shell;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.zemris.java.hw06.shell.commands.CatCommand;
import hr.fer.zemris.java.hw06.shell.commands.CdCommand;
import hr.fer.zemris.java.hw06.shell.commands.CharsetsCommand;
import hr.fer.zemris.java.hw06.shell.commands.CopyCommand;
import hr.fer.zemris.java.hw06.shell.commands.DropdCommand;
import hr.fer.zemris.java.hw06.shell.commands.ExitCommand;
import hr.fer.zemris.java.hw06.shell.commands.HelpCommand;
import hr.fer.zemris.java.hw06.shell.commands.HexdumpCommand;
import hr.fer.zemris.java.hw06.shell.commands.ListdCommand;
import hr.fer.zemris.java.hw06.shell.commands.LsCommand;
import hr.fer.zemris.java.hw06.shell.commands.MassrenameCommand;
import hr.fer.zemris.java.hw06.shell.commands.MkdirCommand;
import hr.fer.zemris.java.hw06.shell.commands.PopdCommand;
import hr.fer.zemris.java.hw06.shell.commands.PushdCommand;
import hr.fer.zemris.java.hw06.shell.commands.PwdCommand;
import hr.fer.zemris.java.hw06.shell.commands.SymbolCommand;
import hr.fer.zemris.java.hw06.shell.commands.TreeCommand;

/**
 * Implements {@link Environment} that uses console
 * for communication between {@link ShellCommand} 
 * and user.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ShellEnvironment implements Environment {

	/** Default prompt symbol. */
	private static final char DEFAULT_PROMPT_SYMBOL = '>';
	/** Default symbol for extending command in multiple lines. */
	private static final char DEFAULT_MORELINES_SYMBOL = '\\';
	/** Default symbol for multiline command. */
	private static final char DEFAULT_MULTILINE_SYMBOL = '|';

	/** Prompt symbol. */
	private char promptSymbol = DEFAULT_PROMPT_SYMBOL;
	/** Morelines symbol. */
	private char morelinesSymbol = DEFAULT_MORELINES_SYMBOL;
	/** Multiline symbol. */
	private char multilineSymbol = DEFAULT_MULTILINE_SYMBOL;

	/** Scanner used for getting user's input. */
	private Scanner sc;
	/** Map of commands. */
	private SortedMap<String, ShellCommand> commands;
	
	/** Current working directory. */
	private Path workingDirectory;
	
	/** Map of shared data. */
	private Map<String, Object> sharedData;
	
	{	
		commands = new TreeMap<>();
		commands.put(new ExitCommand().getCommandName(), new ExitCommand()); 
		commands.put(new LsCommand().getCommandName(), new LsCommand());
		commands.put(new CatCommand().getCommandName(), new CatCommand());
		commands.put(new CharsetsCommand().getCommandName(), new CharsetsCommand());
		commands.put(new CopyCommand().getCommandName() , new CopyCommand());
		commands.put(new HelpCommand().getCommandName(), new HelpCommand());
		commands.put(new HexdumpCommand().getCommandName(), new HexdumpCommand());
		commands.put(new MkdirCommand().getCommandName(), new MkdirCommand());
		commands.put(new SymbolCommand().getCommandName(), new SymbolCommand());
		commands.put(new TreeCommand().getCommandName(), new TreeCommand());
		commands.put(new PwdCommand().getCommandName(), new PwdCommand());
		commands.put(new CdCommand().getCommandName(), new CdCommand());
		commands.put(new PushdCommand().getCommandName(), new PushdCommand());
		commands.put(new PopdCommand().getCommandName(), new PopdCommand());
		commands.put(new ListdCommand().getCommandName(), new ListdCommand());
		commands.put(new DropdCommand().getCommandName(), new DropdCommand());
		commands.put(new MassrenameCommand().getCommandName(), new MassrenameCommand());
		
		workingDirectory = Paths.get(".").toAbsolutePath().normalize();
		
		sharedData = new HashMap<String, Object>();
	}
	
	/**
	 * Creates new {@link ShellEnvironment}.
	 */
	public ShellEnvironment() {
		sc = new Scanner(System.in);
	}
	
	@Override
	public String readLine() throws ShellIOException {
		try {
			System.out.print(promptSymbol + " ");
			String in = sc.nextLine().strip();
			
			while(in.endsWith(String.valueOf(morelinesSymbol))) {
				in = in.substring(0, in.length() - 1);
				in += " ";
				System.out.print(multilineSymbol + " ");
				in += sc.nextLine().strip();
			}
			
			return in;
		} catch(Exception e) {
			throw new ShellIOException(e.getLocalizedMessage());
		}
	}

	@Override
	public void write(String text) throws ShellIOException {
		try {
			System.out.print(text);
		} catch(Exception e) {
			throw new ShellIOException(e.getLocalizedMessage());
		}
	}

	@Override
	public void writeln(String text) throws ShellIOException {
		try {
			System.out.println(text); 
		} catch(Exception e) {
			throw new ShellIOException(e.getLocalizedMessage());
		}
	}

	@Override
	public SortedMap<String, ShellCommand> commands() {
		return Collections.unmodifiableSortedMap(commands);
	}

	@Override
	public Character getMultilineSymbol() {
		return multilineSymbol;
	}

	@Override
	public void setMultilineSymbol(Character symbol) {
		this.multilineSymbol = symbol;
	}

	@Override
	public Character getPromptSymbol() {
		return promptSymbol;
	}

	@Override
	public void setPromptSymbol(Character symbol) {
		this.promptSymbol = symbol;
	}

	@Override
	public Character getMorelinesSymbol() {
		return morelinesSymbol;
	}

	@Override
	public void setMorelinesSymbol(Character symbol) {
		this.morelinesSymbol = symbol;
	}

	@Override
	public Path getCurrentDirectory() {
		return workingDirectory;
	}

	@Override
	public void setCurrentDirectory(Path path) {
		if(!Files.exists(path)) {
			throw new IllegalArgumentException(
					"Path " + path.toString() + " does not exist."
			);	
		}
		if(!Files.isDirectory(path)) {
			throw new IllegalArgumentException(
					"Path " + path.toString() + " is not a directory."
			);	
		}

		workingDirectory = path.toAbsolutePath().normalize();
	}

	@Override
	public Object getSharedData(String key) {
		return sharedData.get(key);
	}

	@Override
	public void setSharedData(String key, Object value) {
		sharedData.put(key, value);
	}

}
