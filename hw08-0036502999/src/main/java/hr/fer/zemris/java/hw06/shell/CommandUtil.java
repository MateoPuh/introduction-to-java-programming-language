package hr.fer.zemris.java.hw06.shell;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This method contains utility methods for {@link ShellCommand}
 * objects, such as splitting string with arguments and checking
 * if file with given name exists, does it parent's directory exist
 * and is given path file or directory.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class CommandUtil {
	
	/**
	 * Splits given String with arguments by blanks.
	 * 
	 * @param arguments string with arguments
	 * @return array of strings with arguments
	 */
	public static String[] splitArgumentsByBlanks(String arguments) {
		return arguments.trim().split("\\s+");
	}
	
	/**
	 * Splits given String with arguments by blanks
	 * or quotation marks.
	 * 
	 * @param arguments string with arguments
	 * @return array of strings with arguments
	 */
	public static String[] splitArgumentsByBlanksAndQuotes(String arguments) {
		List<String> matchList = new ArrayList<String>();
		
		// regex for words that don't start with blanks and quotation marks
		// or words between quotation marks
		Pattern regex = Pattern.compile("[^\\s\"']+|\"(\\\\.|[^\"])*\"");
		Matcher regexMatcher = regex.matcher(arguments.trim());
				
		while (regexMatcher.find()) {
			String s = regexMatcher.group();
			
			if(s.startsWith("\"") && s.endsWith("\"")) {
				s = s.replace("\\\\", "\\").replace("\\\"", "\"");
			    matchList.add(s.substring(1, s.length()-1));
			    continue;
			}
			
		    matchList.add(s);
		} 
		
		return matchList.toArray(new String[] {});
	}
	
	/**
	 * Checks if path with given filename is file.
	 * 
	 * @param filename filename
	 * @return true if path with given filename is file; 
	 * 		false otherwise
	 * @throws NoSuchElementException if path with given 
	 * 		filename doesn't exist
	 */
	public static boolean isFile(String filename) 
			throws NoSuchElementException {
		if(!exists(filename)) {
			throw new NoSuchElementException("File " + filename + " does not exist.");
		}
		
		Path path = Paths.get(filename);
		return !Files.isDirectory(path);
	}
	
	/**
	 * Checks if parent directory of path with given 
	 * filename exists.
	 * 
	 * @param filename filename
	 * @return true if parent directory of path exists;
	 * 		false otherwise
	 * @throws NoSuchElementException if path with given
	 * 		filename doesn't exist
	 */
	public static boolean parentExists(String filename) 
			throws NoSuchElementException{
		Path path = Paths.get(filename);
		Path parent = path.toAbsolutePath().getParent();
		return exists(parent.toAbsolutePath().toString());
	}
	
	/**
	 * Checks if path with given filename exist.
	 * 
	 * @param filename filename
	 * @return true if path with given filename exists;
	 * 		false otherwise
	 */
	public static boolean exists(String filename) {
		Path path = Paths.get(filename);
		return Files.exists(path);
	}
	
	/**
	 * Returns absolute path for given path as a String.
	 * If relative path is given as argument, it returns
	 * absolute path resolved against current working directory.
	 * If absolute path is given, the same will be returned.
	 * 
	 * @param directoryPath path in String format
	 * @param env environment of a shell
	 * @return absolute path
	 */
	public static Path getAbsolute(String directoryPath, Environment env) {
		Path path = Paths.get(directoryPath);
		
		if(!path.isAbsolute()) {
			path = env.getCurrentDirectory().resolve(directoryPath).normalize();
		}
		
		return path;
	}
	
}
