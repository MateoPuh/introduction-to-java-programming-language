package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents cd command.
 * It takes one argument: path for new working directory,
 * and changes current working directory to it.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class CdCommand extends Command {
	
	/** Number of arguments of cd command. */
	private static final int NUMBER_OF_ARGUMENTS = 1;
	
	/**
	 * Creates new {@link CdCommand} and initializes it's description.
	 */
	public CdCommand() {
		NAME = "cd";
		
		List<String> description = new ArrayList<>();
		
		description.add("cd [path]");
		description.add("");
		description.add("-path : path of a directory");
		description.add("");
		description.add("Command \'cd\' takes one argument: directory path, which");
		description.add("is then set as a current working directory.");
		
		CdCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);

		// wrong mumber of arguments
		if(argument.length != NUMBER_OF_ARGUMENTS) {
			env.writeln("Command \'cd\' takes 1 argument.");
			return ShellStatus.CONTINUE;
		}
		
		Path path = CommandUtil.getAbsolute(argument[0], env);
				
		// if given directory does not exist
		if(!Files.exists(path)) {
			env.writeln(argument[0] + " does not exist.");
			return ShellStatus.CONTINUE;
		}

		// if given path is not directory
		if(!Files.isDirectory(path)) {
			env.writeln(argument[0] + " is a file.");
			return ShellStatus.CONTINUE;
		}
						
		try {
			env.setCurrentDirectory(path);
			env.writeln(env.getCurrentDirectory().toString());
		} catch(IllegalArgumentException e) {
			env.writeln("Error occured while setting directory " + argument[0]
					+ " as new working directory:\n" + e.getLocalizedMessage());
		}
		
		return ShellStatus.CONTINUE;
	}

}