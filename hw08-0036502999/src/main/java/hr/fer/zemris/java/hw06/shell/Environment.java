package hr.fer.zemris.java.hw06.shell;

import java.nio.file.Path;
import java.util.SortedMap;

/**
 * Through this interface {@link ShellCommand} communicates
 * with user by reading user's input and and writes response.
 * It contains methods for reading input and writing output,
 * getting and setting symbols and getting unmodifiable map
 * of commands.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 * 
 */
public interface Environment {

	/**
	 * Reads the input of user.This method reads 
	 * line until there are no more MORELINES symbol 
	 * present at the end of line.
	 * 
	 * @return user's input
	 * @throws ShellIOException if error occurs
	 */
	String readLine() throws ShellIOException;
	
	/**
	 * Prints the given text to the console.
	 * 
	 * @param text text to print 
	 * @throws ShellIOException if error occurs
	 */
	void write(String text) throws ShellIOException;
	
	/**
	 * Prints the given test to the console with
	 * symbol "\n" at the end.
	 * 
	 * @param text text to print
	 * @throws ShellIOException if error occurs
	 */
	void writeln(String text) throws ShellIOException;
	
	/**
	 * Returns unmodifiable map of commands.
	 * 
	 * @return unmodifiable map of commands
	 */
	SortedMap<String, ShellCommand> commands();
	
	/**
	 * Returns MULTILINE symbol.
	 * 
	 * @return MULTILINE symbol
	 */
	Character getMultilineSymbol();
	
	/**
	 * Sets MULTILINE symbol.
	 * 
	 * @param symbol symbol to set
	 */
	void setMultilineSymbol(Character symbol);
	
	/**
	 * Returns PROMPT symbol.
	 * 
	 * @return PROMPT symbol
	 */
	Character getPromptSymbol();
	
	/**
	 * Sets PROMPT symbol.
	 * 
	 * @param symbol symbol to set
	 */
	void setPromptSymbol(Character symbol);
	
	/**
	 * Returns MORELINES symbol.
	 * 
	 * @return MORELINES symbol.
	 */
	Character getMorelinesSymbol();
	
	/**
	 * Sets MORELINES symbol.
	 * 
	 * @param symbol symbol to set
	 */
	void setMorelinesSymbol(Character symbol);
	
	/**
	 * Returns current directory in {@link MyShell}.
	 * 
	 * @return current directory
	 */
	Path getCurrentDirectory();
	
	/**
	 * Sets given path as current directory in {@link MyShell}.
	 * 
	 * @param path path to set as new current directory
	 * @throws IllegalArgumentException if given path does not exist
	 */
	void setCurrentDirectory(Path path);

	/**
	 * Returns data with given key stored in shared data.
	 * 
	 * @param key key of data
	 * @return object with given key; null otherwise
	 */
	Object getSharedData(String key);
	
	/**
	 * Stores data in shared data with given key.
	 * 
	 * @param key key of data
	 * @param value value of data
	 */
	void setSharedData(String key, Object value);
	
}
