package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents tree command.
 * It takes one argument: directory name. It prints tree
 * structure of directory and all of it's kids recursively.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class TreeCommand extends Command {
	
	/** Number of arguments of tree command. */
	private static final int NUMBER_OF_ARGUMENTS = 1;
	
	/**
	 * Creates new {@link TreeCommand} and initializes it's description.
	 */
	public TreeCommand() {
		NAME = "tree";
		
		List<String> description = new ArrayList<>();
		
		description.add("tree [directory]");
		description.add("");
		description.add("-directory : name of a directory");
		description.add("");
		description.add("Command \'tree\' takes one argument: directory name, for");
		description.add("which it displays tree.");
		
		TreeCommand.DESCRIPTION = Collections.unmodifiableList(description);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);

		// wrong mumber of arguments
		if(argument.length != NUMBER_OF_ARGUMENTS) {
			env.writeln("Command \'tree\' takes 1 argument.");
			return ShellStatus.CONTINUE;
		}
		
		Path path = CommandUtil.getAbsolute(argument[0], env);
		
		// if given directory does not exist
		if(!Files.exists(path)) {
			env.writeln(argument[0] + " does not exist.");
			return ShellStatus.CONTINUE;
		}
		
		// if given path is not directory
		if(!Files.isDirectory(path)) {
			env.writeln(argument[0] + " is a file.");
			return ShellStatus.CONTINUE;
		}
				
		try {
			Files.walkFileTree(path, new FileVisitor<Path>() {

				private int level = 0;
				
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					System.out.println(" ".repeat(level*2) + dir.getFileName());
					
					if(Files.isDirectory(dir)) {
						level++;
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					System.out.println(" ".repeat(level*2) + file.getFileName());

					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
					return FileVisitResult.TERMINATE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					if(Files.isDirectory(dir)) {
						level--;
					}
					
					return FileVisitResult.CONTINUE;
				}
				
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ShellStatus.CONTINUE;
	}

}
