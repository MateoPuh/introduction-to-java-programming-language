package coloring.algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * This class contains coloring algorithms, such as
 * bfs, dfs and bfsv.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class SubspaceExploreUtil {
	
	/**
	 * Bfs algorithm.
	 * 
	 * @param s0 start state
	 * @param process process function
	 * @param succ successor function
	 * @param acceptable accept function
	 */
	public static <S> void bfs(
			 Supplier<S> s0,
			 Consumer<S> process,
			 Function<S,List<S>> succ,
			 Predicate<S> acceptable
			) {
		List<S> toSearch = new LinkedList<S>();
		toSearch.add(s0.get());		
		
		while(!toSearch.isEmpty()) {
			S si = toSearch.remove(0);
			
			if(!acceptable.test(si)) {
				continue;
			}
			
			process.accept(si);
			toSearch.addAll(succ.apply(si));
		}
	}

	/**
	 * Dfs algorithm.
	 * 
	 * @param s0 start state
	 * @param process process function
	 * @param succ successor function
	 * @param acceptable accept function
	 */
	public static <S> void dfs(
			 Supplier<S> s0,
			 Consumer<S> process,
			 Function<S,List<S>> succ,
			 Predicate<S> acceptable
			) {
		List<S> toSearch = new LinkedList<S>();
		toSearch.add(s0.get());		
		
		while(!toSearch.isEmpty()) {
			S si = toSearch.remove(0);
			
			if(!acceptable.test(si)) {
				continue;
			}
			
			process.accept(si);
			
			toSearch.addAll(0, succ.apply(si));
		}
	}
	
	/**
	 * Bfsv algorithm.
	 * 
	 * @param s0 start state
	 * @param process process function
	 * @param succ successor function
	 * @param acceptable accept function
	 */
	public static <S> void bfsv(
			 Supplier<S> s0,
			 Consumer<S> process,
			 Function<S,List<S>> succ,
			 Predicate<S> acceptable
			) {
		List<S> toSearch = new LinkedList<S>();
		toSearch.add(s0.get());	
		
		Set<S> visited = new HashSet<S>();
		visited.add(s0.get());
		
		while(!toSearch.isEmpty()) {
			S si = toSearch.remove(0);
			
			if(!acceptable.test(si)) {
				continue;
			}
			
			process.accept(si);
			
			List<S> children = succ.apply(si);
			
			List<S> childrenToAdd = new LinkedList<S>();
			
			for(S child : children) {
				if(!visited.contains(child)) {
					childrenToAdd.add(child);
				}
			}
			
			toSearch.addAll(childrenToAdd);
			visited.addAll(childrenToAdd);
		}
	}

}
