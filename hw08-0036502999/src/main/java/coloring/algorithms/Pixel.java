package coloring.algorithms;

import java.util.Objects;

/**
 * This class represents one pixel in picture.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Pixel {
	
	private int x;
	private int y;

	/**
	 * Creates new {@link Pixel} with given x and y
	 * coordinates.
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	public Pixel(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Returns x coordinate of a pixel.
	 * 
	 * @return x coordinate
	 */
	public int getX() {
		return x;
	}

	/**
	 * Returns y coordinate of a pixel.
	 * 
	 * @return y coordinate
	 */
	public int getY() {
		return y;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Pixel))
			return false;
		Pixel other = (Pixel) obj;
		return x == other.x && y == other.y;
	}

	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}

}
