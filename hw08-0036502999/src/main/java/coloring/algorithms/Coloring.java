package coloring.algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import marcupic.opjj.statespace.coloring.Picture;

/**
 * This class represents structure needed for coloring 
 * algorithm implementations. 
 * 
 * @author Mateo Puhalović
 *
 */
public class Coloring implements Supplier<Pixel>, 
	Consumer<Pixel>, Function<Pixel,List<Pixel>>, Predicate<Pixel> {

	/** References pixel. */
	private Pixel reference;
	/** Picture to color. */
	private Picture picture;
	/** Filling color. */
	private int fillColor;
	/** Color of references pixel. */
	private int refColor;

	/**
	 * Creates new {@link Coloring} with given referenced pixel,
	 * picture and filling color.
	 * 
	 * @param reference referenced pixel
	 * @param picture picture
	 * @param fillColor filling color
	 */
	public Coloring(Pixel reference, Picture picture, int fillColor) {
		this.reference = reference;
		this.picture = picture;
		this.fillColor = fillColor;
		this.refColor = picture.getPixelColor(reference.getX(), reference.getY());
	}

	@Override
	public boolean test(Pixel t) {
		return picture.getPixelColor(t.getX(), t.getY()) == refColor;
	}

	@Override
	public List<Pixel> apply(Pixel t) {
		List<Pixel> succ = new ArrayList<Pixel>();
		
		// left
		if(t.getX() > 0) {
			succ.add(new Pixel(t.getX() - 1, t.getY()));
		}
		
		// right
		if(t.getX() < picture.getWidth() - 1) {
			succ.add(new Pixel(t.getX() + 1, t.getY()));
		}
		
		// up
		if(t.getY() > 0) {
			succ.add(new Pixel(t.getX(), t.getY() - 1));
		}
		
		// up
		if(t.getY() < picture.getHeight() - 1) {
			succ.add(new Pixel(t.getX(), t.getY() + 1));
		}
		
		return succ;
	}

	@Override
	public void accept(Pixel t) {
		picture.setPixelColor(t.getX(), t.getY(), fillColor);
	}

	@Override
	public Pixel get() {
		return reference;
	}
	
}
