package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * Singleton that extends {@link AbstractLocalizationProvider} 
 * and internally loads resource bundle. Default language is "en".
 * Method {@link #getString(String)} is used to get translation
 * for given key.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class LocalizationProvider extends AbstractLocalizationProvider {
	
	/** Default language. */
	private static final String DEFAULT_LANGUAGE = "en";
	/** Path to resource bundle. */
	private static final String BUNDLE_PATH = "hr.fer.zemris.java.hw11.jnotepadpp.local.prijevodi";
	
	/** Only instance of this class. */
	private static LocalizationProvider instance = new LocalizationProvider();
	
	/** Current language. */
	private String language;
	/** Resource bundle. */
	private ResourceBundle bundle;
	
	/**
	 * Creates new {@link LocalizationProvider} and initializes its
	 * language to "en".
	 */
	private LocalizationProvider() {
		this.language = DEFAULT_LANGUAGE;
		Locale locale = Locale.forLanguageTag(language);
		this.bundle = ResourceBundle.getBundle(BUNDLE_PATH, locale);
	}
	
	/**
	 * Returns instance of {@link LocalizationProvider}.
	 * 
	 * @return instance of {@link LocalizationProvider}
	 */
	public static LocalizationProvider getInstance() {
		return instance;
	}

	@Override
	public String getCurrentLanguage() {
		return language;
	}
	
	/**
	 * Sets current language to given one.
	 * 
	 * @param language new language
	 * @throws NullPointerException if given language is null
	 */
	public void setLanguage(String language) {
		this.language = Objects.requireNonNull(language);
		Locale locale = Locale.forLanguageTag(language);
		this.bundle = ResourceBundle.getBundle(BUNDLE_PATH, locale);
		fire();
	}

	@Override
	public String getString(String key) {
		return bundle.getString(key);
	}

}
