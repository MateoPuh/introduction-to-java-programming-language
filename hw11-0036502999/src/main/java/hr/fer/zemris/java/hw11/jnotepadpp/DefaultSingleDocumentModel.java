package hr.fer.zemris.java.hw11.jnotepadpp;

import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import hr.fer.zemris.java.hw11.jnotepadpp.documents.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.documents.SingleDocumentModel;

/**
 * Default implementation of {@link SingleDocumentModel}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class DefaultSingleDocumentModel implements SingleDocumentModel {

	/** Path of file. */
	private Path filePath;
	/** Text area of file. */
	private JTextArea textArea;
	/** Modified flag. */
	private boolean modified;
	/** Listeners of this {@link SingleDocumentModel}. */
	private List<SingleDocumentListener> listeners;
	
	/**
	 * Creates new {@link DefaultMultipleDocumentModel} with given path and
	 * content of document.
	 * 
	 * @param filePath path of file
	 * @param text content of a file
	 */
	public DefaultSingleDocumentModel(Path filePath, String text) {
		textArea = new JTextArea(text);
		textArea.getDocument().addDocumentListener(new NotepadPPDocumentListener());
		this.filePath = filePath;
		
		listeners = new CopyOnWriteArrayList<SingleDocumentListener>();
	}
	
	@Override
	public JTextArea getTextComponent() {
		return textArea;
	}

	@Override
	public Path getFilePath() {
		return filePath;
	}

	@Override
	public void setFilePath(Path path) {
		this.filePath = path;	
		notifyListenersFilePath();
	}

	@Override
	public boolean isModified() {
		return modified;
	}

	@Override
	public void setModified(boolean modified) {
		this.modified = modified;
		notifyListenersModification();
	}

	@Override
	public void addSingleDocumentListener(SingleDocumentListener l) {
		Objects.requireNonNull(l);
		
		if(!listeners.contains(l)) {
			listeners.add(l);		
		}
	}

	@Override
	public void removeSingleDocumentListener(SingleDocumentListener l) {
		Objects.requireNonNull(l);
		
		listeners.remove(l);			
	}
	
	/**
	 * Notifies all listeners about change of file path.
	 */
	private void notifyListenersFilePath() {
		for(SingleDocumentListener l : listeners) {
			l.documentFilePathUpdated(this);
		}
	}
	
	/**
	 * Notifies all listeners about modification of document.
	 */
	private void notifyListenersModification() {
		for(SingleDocumentListener l : listeners) {
			l.documentModifyStatusUpdated(this);
		}
	}
	
	/**
	 * {@link DocumentListener} that sets modified flag to true
	 * whenever document is modified.
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 */
	private class NotepadPPDocumentListener implements DocumentListener {

		@Override
		public void insertUpdate(DocumentEvent e) {
			setModified(true);
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			setModified(true);
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			setModified(true);			
		}
		
	}

}
