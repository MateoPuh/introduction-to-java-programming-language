package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.documents.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.documents.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.documents.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

/**
 * Represents Notepad++ application. It provides GUI with functionalities
 * such as opening multiple text documents and creating new ones, saving
 * those documents, sorting lines in ascending and descending order, 
 * toggling selected part of text to upper case, to lower case and inverting
 * case. It offers localization in english, croatian and german language.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class JNotepadPP extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	/** {@link FormLocalizationProvider} for notepad. */
	private FormLocalizationProvider flp;
	
	/** Menu item for "to upper case" action. */
	private JMenuItem toUpper;
	/** Menu item for "to lower case" action. */
	private JMenuItem toLower;
	/** Menu item for "invert case" action. */
	private JMenuItem invert;
	
	/** Menu item for "sort in ascending order" action. */
	private JMenuItem ascendingSort;
	/** Menu item for "sort in descending order" action. */
	private JMenuItem descendingSort;
	
	/** Menu item for "copy" action. */
	private JMenuItem copy;
	/** Menu item for "cut" action. */
	private JMenuItem cut;
	/** Menu item for "paste" action. */
	private JMenuItem paste;
	
	/** Toolbar item for "copy" action. */
	private JButton copyButton;
	/** Toolbar item for "cut" action. */
	private JButton cutButton;
	/** Toolbar item for "paste" action. */
	private JButton pasteButton;
	
	/** Statusbar panel item for "length" status. */
	private JLabel lengthLabel;
	/** Statusbar panel item for "line, column and selection" status. */
	private JLabel lineColSel;
	/** Statusbar panel item for "date" status. */
	private JLabel dateLabel;
	
	/** Copies part of text. */
	private String copiedPart = null;
	/** Indicates whether operation is cut. */
	private boolean isCut = false;
	
	private MultipleDocumentModel multipleDocumentModel;
	
	/**
	 * Creates new {@link JNotepadPP} and initializes its content and GUI.
	 */
	public JNotepadPP() {
		setFlp();
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setTitle("JNotepad++");
		setSize(800, 500);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if(JNotepadPPUtils.canClose(multipleDocumentModel)) {
					JNotepadPPUtils.saveAllModified(multipleDocumentModel);
					dispose();
				}	
			}
		});
		
		initGUI();
		
		setLocationRelativeTo(null);
	}
	
	/**
	 * Returns {@link FormLocalizationProvider} and sets it if it
	 * is null.
	 * 
	 * @return {@link FormLocalizationProvider}
	 */
	private FormLocalizationProvider getFlp() {
		if(flp == null) {
			setFlp();
		}
		
		return flp;
	}
	
	/**
	 * Sets {@link FormLocalizationProvider} to initial value.
	 */
	private void setFlp() {
		flp = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);
	}
	
	/**
	 * Initializes GUI.
	 */
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		
		multipleDocumentModel = new DefaultMultipleDocumentModel();
		cp.add((DefaultMultipleDocumentModel)multipleDocumentModel, BorderLayout.CENTER);

		multipleDocumentModel.addMultipleDocumentListener(new MultipleDocumentListener() {
			
			@Override
			public void documentRemoved(SingleDocumentModel model) {
			}
			
			@Override
			public void documentAdded(SingleDocumentModel model) {
				model.getTextComponent().addCaretListener(new CaretListener() {
					
					@Override
					public void caretUpdate(CaretEvent e) {	
						if(!model.equals(multipleDocumentModel.getCurrentDocument())) {
							return;
						}
						
						setLnColSel(model);
						setLength(model);
					}
				});					
			}
			
			@Override
			public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
				setLnColSel(currentModel);
				setLength(currentModel);
			}
			
		});
		
		multipleDocumentModel.addMultipleDocumentListener(new MultipleDocumentListener() {
			
			@Override
			public void documentRemoved(SingleDocumentModel model) {				
			}
			
			@Override
			public void documentAdded(SingleDocumentModel model) {
				model.getTextComponent().addCaretListener(new CaretListener() {
					
					@Override
					public void caretUpdate(CaretEvent e) {
						if(!model.equals(multipleDocumentModel.getCurrentDocument())) {
							return;
						}
						
						setCopyCutPaste(model);	
						setChangeCase(model);
						setSort(model);
					}
				});				
			}
			
			@Override
			public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
				setTitle(JNotepadPPUtils.getFilename(currentModel) + " - JNotepad++");	
				setCopyCutPaste(currentModel);	
				setChangeCase(currentModel);
				setSort(currentModel);
			}
		});
	
		configureActions();
		createMenus();
		createToolbar();
		createStatusBar();
	}

	/**
	 * Sets sort menu items enabled or disabled, depending on whether 
	 * text is selected.
	 * 
	 * @param model document
	 */
	private void setSort(SingleDocumentModel model) {		
		if(JNotepadPPUtils.findSel(model) > 0) {
			ascendingSort.setEnabled(true);
			descendingSort.setEnabled(true);
		} else {
			ascendingSort.setEnabled(false);
			descendingSort.setEnabled(false);;
		};	
	}
	
	/**
	 * Sets to upper case, to lower case and invert case menu items enabled 
	 * or disabled, depending on whether text is selected.
	 * 
	 * @param model document
	 */
	private void setChangeCase(SingleDocumentModel model) {		
		if(JNotepadPPUtils.findSel(model) > 0) {
			toUpper.setEnabled(true);
			toLower.setEnabled(true);
			invert.setEnabled(true);
		} else {
			toUpper.setEnabled(false);
			toLower.setEnabled(false);
			invert.setEnabled(false);
		};	
	}
	
	/**
	 * Sets copy, cut and paste button enabled or disabled, depending
	 * on whether text is selected.
	 * 
	 * @param model document
	 */
	private void setCopyCutPaste(SingleDocumentModel model) {		
		if(JNotepadPPUtils.findSel(model) > 0) {
			copy.setEnabled(true);
			cut.setEnabled(true);
			
			copyButton.setEnabled(true);
			cutButton.setEnabled(true);
		} else {
			copy.setEnabled(false);
			cut.setEnabled(false);
			
			copyButton.setEnabled(false);
			cutButton.setEnabled(false);
		};	
	}
	
	/**
	 * Sets length in status bar.
	 * 
	 * @param model document
	 */
	private void setLength(SingleDocumentModel model) {
		lengthLabel.setText("length: " + model.getTextComponent().getDocument().getLength());
	}
	
	/**
	 * Sets parameters in status bar.
	 * 
	 * @param model document
	 */
	private void setLnColSel(SingleDocumentModel model) {
		int ln = JNotepadPPUtils.findLn(model);
		int col = JNotepadPPUtils.findCol(model);
		int sel = JNotepadPPUtils.findSel(model);
		
		lineColSel.setText("Ln: " + ln + "  Col: " + col + "  Sel: " + sel);
	}
	
	/**
	 * Configures all notepad actions.
	 */
	private void configureActions() {
		configureAction(openNewDocument, KeyStroke.getKeyStroke("control N"), KeyEvent.VK_N, "Create new file");
		configureAction(openDocument, KeyStroke.getKeyStroke("control O"), KeyEvent.VK_O, "Open file from disk");
		configureAction(saveDocument, KeyStroke.getKeyStroke("control S"), KeyEvent.VK_S, "Save file to disk");
		configureAction(saveAsDocument, KeyStroke.getKeyStroke("F12"), KeyEvent.VK_A, "Save file to disk as...");
		configureAction(exitApplication, KeyStroke.getKeyStroke("alt F4"), KeyEvent.VK_E, "Terimnates application");
		configureAction(closeDocument, KeyStroke.getKeyStroke("control W"), KeyEvent.VK_L, "Closes current document");
		configureAction(copySelectedPart, KeyStroke.getKeyStroke("control C"), KeyEvent.VK_C, "Copy selected part of text");	
		configureAction(cutSelectedPart, KeyStroke.getKeyStroke("control X"), KeyEvent.VK_U, "Cut selected part of text");
		configureAction(pasteCopiedPart, KeyStroke.getKeyStroke("control V"), KeyEvent.VK_P, "Paste copied part of text");
		configureAction(statisticalInfo, KeyStroke.getKeyStroke("control I"), KeyEvent.VK_I, "Statistical info");
		configureAction(toLowerCase, KeyStroke.getKeyStroke("control L"), KeyEvent.VK_L, "Converts every upper case into lower case");
		configureAction(toUpperCase, KeyStroke.getKeyStroke("control U"), KeyEvent.VK_U, "Converts every lower case into upper case");
		configureAction(invertCase, KeyStroke.getKeyStroke("control G"), KeyEvent.VK_I, "Toggle every case");
		configureAction(sortAscending, KeyStroke.getKeyStroke("control E"), KeyEvent.VK_A, "Sorts selected lines");
		configureAction(sortDescending, KeyStroke.getKeyStroke("control D"), KeyEvent.VK_D, "Sorts selected lines");
		configureAction(unique, KeyStroke.getKeyStroke("control U"), KeyEvent.VK_U, "Removes duplicate lines");
	}
	
	/**
	 * Configures given action.
	 * 
	 * @param action action
	 * @param keyStroke key stroke
	 * @param keyEvent key event
	 * @param description description
	 */
	private void configureAction(Action action, KeyStroke keyStroke, int keyEvent, String description) {
		action.putValue(Action.ACCELERATOR_KEY, keyStroke);
		action.putValue(Action.MNEMONIC_KEY, keyEvent);
		action.putValue(Action.SHORT_DESCRIPTION, description);
	}

	/**
	 * Creates File, Edit, Tools and Languages menus.
	 */
	private void createMenus() {
		JMenuBar mb = new JMenuBar();
		
		JMenu file = new JMenu(new LocalizableAction("file", getFlp()));
		mb.add(file);
		file.add(new JMenuItem(openNewDocument));
		file.add(new JMenuItem(openDocument));
		file.add(new JMenuItem(saveDocument));
		file.add(new JMenuItem(saveAsDocument));
		file.addSeparator();
		file.add(new JMenuItem(closeDocument));
		file.add(new JMenuItem(exitApplication));

		copy = new JMenuItem(copySelectedPart);
		copy.setEnabled(false);
		cut = new JMenuItem(cutSelectedPart);
		cut.setEnabled(false);
		paste = new JMenuItem(pasteCopiedPart);
		paste.setEnabled(false);
		
		JMenu edit = new JMenu(new LocalizableAction("edit", getFlp()));
		mb.add(edit);
		edit.add(copy);
		edit.add(cut);
		edit.add(paste);
		edit.addSeparator();
		edit.add(new JMenuItem(statisticalInfo));
		
		toUpper = new JMenuItem(toUpperCase);
		toUpper.setEnabled(false);
		toLower = new JMenuItem(toLowerCase);
		toLower.setEnabled(false);
		invert = new JMenuItem(invertCase);
		invert.setEnabled(false);
		
		JMenu changeCase = new JMenu(new LocalizableAction("changeCase", getFlp()));
		changeCase.add(toUpper);
		changeCase.add(toLower);
		changeCase.add(invert);
		
		ascendingSort = new JMenuItem(sortAscending);
		ascendingSort.setEnabled(false);
		descendingSort = new JMenuItem(sortDescending);
		descendingSort.setEnabled(false);
		
		JMenu sort = new JMenu(new LocalizableAction("sort", getFlp()));
		sort.add(ascendingSort);
		sort.add(descendingSort);
		
		JMenu tools = new JMenu(new LocalizableAction("tools", getFlp()));
		mb.add(tools);
		tools.add(changeCase);
		tools.add(sort);
		tools.add(new JMenuItem(unique));
		
		JMenu languages = new JMenu(new LocalizableAction("languages", getFlp()));
		mb.add(languages);
		
		languages.add(new JMenuItem(new LocalizableAction("english", getFlp()) {
			private static final long serialVersionUID = 1L;
			
			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLanguage("en");				
			}
		}));
		languages.add(new JMenuItem(new LocalizableAction("croatian", getFlp()) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLanguage("hr");				
			}
		}));
		languages.add(new JMenuItem(new LocalizableAction("german", getFlp()) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLanguage("de");				
			}
		}));

		setJMenuBar(mb);
	}

	/**
	 * Creates floatable toolbar with options from File and Edit menues.
	 */
	private void createToolbar() {
		JToolBar tb = new JToolBar();
		tb.setFloatable(true);
		
		tb.add(new JButton(openNewDocument));
		tb.add(new JButton(openDocument));
		tb.add(new JButton(saveDocument));
		tb.add(new JButton(saveAsDocument));
		tb.addSeparator();
		tb.add(new JButton(closeDocument));
		tb.add(new JButton(exitApplication));
		tb.addSeparator();
		
		copyButton = new JButton(copySelectedPart);
		cutButton = new JButton(cutSelectedPart);
		pasteButton = new JButton(pasteCopiedPart);
		copyButton.setEnabled(false);
		cutButton.setEnabled(false);
		pasteButton.setEnabled(false);
		
		tb.add(copyButton);		
		tb.add(cutButton);
		tb.add(pasteButton);
		tb.addSeparator();
		tb.add(new JButton(statisticalInfo));
		
		getContentPane().add(tb, BorderLayout.PAGE_START);
	}
	
	/**
	 * Creates status bar.
	 */
	private void createStatusBar() {
		JPanel statusBar = new JPanel(new GridLayout(1, 3));
	
		lengthLabel = new JLabel();
		lengthLabel.setBorder(new LineBorder(Color.BLACK));
		lineColSel = new JLabel();
		lineColSel.setBorder(new LineBorder(Color.BLACK));
		dateLabel = new JLabel();
		dateLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		dateLabel.setBorder(new LineBorder(Color.BLACK));
		JNotepadPPUtils.startClock(dateLabel);
		
		statusBar.add(lengthLabel);
		statusBar.add(lineColSel);
		statusBar.add(dateLabel);
		
		getContentPane().add(statusBar, BorderLayout.PAGE_END);
	}
	
	/** Action that creates new document. */
	private final Action openNewDocument = new LocalizableAction("new", getFlp()) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			multipleDocumentModel.createNewDocument();			
		}
	
	}; 
	
	/** Action that loads document. */
	private final Action openDocument = new LocalizableAction("open", getFlp()) {
	
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {			
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle("Open file");
			if(jfc.showOpenDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
				return;
			}
			
			Path path = jfc.getSelectedFile().toPath();
			
			if(!Files.isReadable(path)) {
				JOptionPane.showMessageDialog(
					JNotepadPP.this, 
					"File is not readable.",
					"Error", 
					JOptionPane.ERROR_MESSAGE
				);
				return;
			}
			
			try {
				multipleDocumentModel.loadDocument(path);
			} catch(IllegalArgumentException e2) {
				JOptionPane.showMessageDialog(
						JNotepadPP.this, 
						e2.getLocalizedMessage(),
						"Error", 
						JOptionPane.ERROR_MESSAGE
					);
					return;
			}
		}
	};
	
	/** Save as... action */
	private final Action saveAsDocument = new LocalizableAction("saveAs", getFlp()) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			SingleDocumentModel model = multipleDocumentModel.getCurrentDocument();
			
			JNotepadPPUtils.saveDocumentAs(model, multipleDocumentModel);
		}
		
	};
	
	/** Action that saves document. */
	private final Action saveDocument = new LocalizableAction("save", getFlp()) {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			SingleDocumentModel model = multipleDocumentModel.getCurrentDocument();
			
			JNotepadPPUtils.saveDocument(model, multipleDocumentModel);
		}
	};

	/** Action that pastes copied part. */
	private final Action pasteCopiedPart = new LocalizableAction("paste", getFlp()) {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Document doc = multipleDocumentModel.getCurrentDocument().getTextComponent().getDocument();
			Caret caret = multipleDocumentModel.getCurrentDocument().getTextComponent().getCaret();
			
			try {
				doc.insertString(caret.getDot(), copiedPart, null);
				if(isCut) {
					copiedPart = null;
					paste.setEnabled(false);
					pasteButton.setEnabled(false);
				}
			} catch (BadLocationException ignorable) {
			}
		}
	};
	
	/** Action that cuts selected part. */
	private final Action cutSelectedPart = new LocalizableAction("cut", getFlp()) {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Document doc = multipleDocumentModel.getCurrentDocument().getTextComponent().getDocument();
			
			try {
				copiedPart = doc.getText(
						JNotepadPPUtils.findCaretStart(multipleDocumentModel.getCurrentDocument()), 
						JNotepadPPUtils.findSel(multipleDocumentModel.getCurrentDocument()));
				
				doc.remove(JNotepadPPUtils.findCaretStart(multipleDocumentModel.getCurrentDocument()), 
						JNotepadPPUtils.findSel(multipleDocumentModel.getCurrentDocument()));
				isCut = true;
				paste.setEnabled(true);
				pasteButton.setEnabled(true);
			} catch (BadLocationException ignorable) {
			}
		}
	};
	
	/** Action that copies selected part. */
	private final Action copySelectedPart = new LocalizableAction("copy", getFlp()) {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Document doc = multipleDocumentModel.getCurrentDocument().getTextComponent().getDocument();
			Caret caret = multipleDocumentModel.getCurrentDocument().getTextComponent().getCaret();
			
			try {
				copiedPart = doc.getText(Math.min(caret.getDot(), caret.getMark()), Math.abs(caret.getDot() - caret.getMark()));
				isCut = false;
				paste.setEnabled(true);
				pasteButton.setEnabled(true);
			} catch (BadLocationException ignorable) {
			}
		}
	};
	
	/** Action that converts all selected characters to upper case. */
	private final Action toUpperCase = new LocalizableAction("toUpperCase", getFlp()) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			SingleDocumentModel model = multipleDocumentModel.getCurrentDocument();
			
			String text = JNotepadPPUtils.getSelectedText(model);
			JNotepadPPUtils.replaceSelected(model, JNotepadPPUtils.toUpperCase(text));
		}
		
	};
	
	/** Action that converts all selected characters to lower case. */
	private final Action toLowerCase = new LocalizableAction("toLowerCase", getFlp()) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			SingleDocumentModel model = multipleDocumentModel.getCurrentDocument();
			
			String text = JNotepadPPUtils.getSelectedText(model);
			JNotepadPPUtils.replaceSelected(model, JNotepadPPUtils.toLowerCase(text));
		}
		
	};
	
	/** Action that inverts case of all selected characters in document. */
	private final Action invertCase = new LocalizableAction("invertCase", getFlp()) {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			SingleDocumentModel model = multipleDocumentModel.getCurrentDocument();
			
			String text = JNotepadPPUtils.getSelectedText(model);
			JNotepadPPUtils.replaceSelected(model, JNotepadPPUtils.toggleCase(text));
		}
		
	};

	/** Action that sorts selected lines in ascending order */
	private final Action sortAscending = new LocalizableAction("ascending", getFlp()) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			SingleDocumentModel model = multipleDocumentModel.getCurrentDocument();
			
			List<String> lines = JNotepadPPUtils.getSelectedLines(model);
			JNotepadPPUtils.replaceSelectedLines(model, JNotepadPPUtils.sortAscending(lines));
		}
		
	};
	
	/** Action that sorts selected lines in descending order */
	private final Action sortDescending = new LocalizableAction("descending", getFlp()) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			SingleDocumentModel model = multipleDocumentModel.getCurrentDocument();
			
			List<String> lines = JNotepadPPUtils.getSelectedLines(model);
			JNotepadPPUtils.replaceSelectedLines(model, JNotepadPPUtils.sortDescending(lines));
		}
		
	};
	
	/** Action that removes duplicate lines from document. */
	private final Action unique = new LocalizableAction("unique", getFlp()) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JNotepadPPUtils.unique(multipleDocumentModel.getCurrentDocument());
		}
		
	};

	/** Action that closes application. */
	private final Action exitApplication = new LocalizableAction("exit", getFlp()) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
		
	};
	
	/** Action used for closing document. */
	private final Action closeDocument = new LocalizableAction("close", getFlp()) {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			SingleDocumentModel model = multipleDocumentModel.getCurrentDocument();
			
			if(model == null) {
				return;
			}
			
			if(model.isModified()) {
				JNotepadPPUtils.saveDocumentWithAsking(model, multipleDocumentModel);
			} 
			
			multipleDocumentModel.closeDocument(model);
		}
	};
	
	/** Action used for displaying statistical info. */
	private final Action statisticalInfo = new LocalizableAction("info", getFlp()) {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {			
			int numOfChars = JNotepadPPUtils.getNumOfChars(multipleDocumentModel.getCurrentDocument());			
			int charsWithoutBlanks = JNotepadPPUtils.getNumOfCharsWithoutBlanks(
					multipleDocumentModel.getCurrentDocument());
			int numOfLines = JNotepadPPUtils.getNumOfLines(multipleDocumentModel.getCurrentDocument());
			
			JOptionPane.showMessageDialog(
					JNotepadPP.this, 
					"Your document has " + numOfChars + " characters, "
					+ charsWithoutBlanks + " non-blank characters and " + numOfLines + " lines",
					"Statistical information", 
					JOptionPane.INFORMATION_MESSAGE
			);
		}
	};
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new JNotepadPP().setVisible(true);
		});
	}

}
