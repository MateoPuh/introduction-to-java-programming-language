package hr.fer.zemris.java.hw11.jnotepadpp;

import java.util.List;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Locale;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.Element;

import hr.fer.zemris.java.hw11.jnotepadpp.documents.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.documents.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

/**
 * Class that contains static utility methods for {@link JNotepadPP}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class JNotepadPPUtils {
	
	/** Format of date. */
	private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
	
	/**
	 * Returns number of characters in given {@link SingleDocumentModel}.
	 * 
	 * @param model document
	 * @return number of character in given document
	 */
	public static int getNumOfChars(SingleDocumentModel model) {
		Document doc = model.getTextComponent().getDocument();
		return doc.getLength();
	}
	
	/**
	 * Returns number of characters in given {@link SingleDocumentModel},
	 * not counting blanks.
	 * 
	 * @param model document
	 * @return number of character in given document, not counting blanks
	 */
	public static int getNumOfCharsWithoutBlanks(SingleDocumentModel model) {
		Document doc = model.getTextComponent().getDocument();
		
		String text = null;
		try {
			text = doc.getText(0, doc.getLength());
		} catch (BadLocationException ignorable) {
		}

		return text.replaceAll("\\s+", "").length();
	}
	
	/**
	 * Returns number of lines in given {@link SingleDocumentModel}.
	 * 
	 * @param model document
	 * @return number of lines in given document
	 */
	public static int getNumOfLines(SingleDocumentModel model) {
		Document doc = model.getTextComponent().getDocument();
		
		String text = null;
		try {
			text = doc.getText(0, doc.getLength());
		} catch (BadLocationException ignorable) {
		}
		
		return text.length() - text.replace("\n", "").length() + 1;
	}
	
	/**
	 * Finds index of line (starting from 1) of current position
	 * of a caret.
	 * 
	 * @param model document
	 * @return index of a line
	 */
	public static int findLn(SingleDocumentModel model) {
		Document doc = model.getTextComponent().getDocument();		
		int pos = findCaretStart(model);		
		Element root = doc.getDefaultRootElement();

		return root.getElementIndex(pos) + 1;
	}
	
	/**
	 * Finds index of column (starting from 1) of current position
	 * of a caret.
	 * 
	 * @param model document
	 * @return index of a column
	 */
	public static int findCol(SingleDocumentModel model) {
		Document doc = model.getTextComponent().getDocument();		
		int pos = findCaretStart(model);		
		Element root = doc.getDefaultRootElement();
		int row = root.getElementIndex(pos); 
		return pos - root.getElement(row).getStartOffset() + 1;
	}
	
	/**
	 * Returns size of selection.
	 * 
	 * @param model document
	 * @return size of selection
	 */
	public static int findSel(SingleDocumentModel model) {
		Caret caret = model.getTextComponent().getCaret();

		return Math.abs(caret.getDot() - caret.getMark());	
	}
	
	/**
	 * Finds starting position of a caret.
	 * 
	 * @param model document
	 * @return starting position of a caret
	 */
	public static int findCaretStart(SingleDocumentModel model) {
		Caret caret = model.getTextComponent().getCaret();
		
		return Math.min(caret.getDot(), caret.getMark());
	}
	
	/**
	 * Returns selected text.
	 * 
	 * @param model document
	 * @return selected text
	 */
	public static String getSelectedText(SingleDocumentModel model) {
		Document doc = model.getTextComponent().getDocument();
		
		String text = null;
		try {
			text = doc.getText(findCaretStart(model), findSel(model));
		} catch(BadLocationException ignorable) {
		}
		
		return text;
	}
	
	/**
	 * Finds beginning of a current line.
	 * 
	 * @param model document
	 * @param index index of current position
	 * @return index of a line beginning
	 */
	public static int findBeginningOfLine(SingleDocumentModel model, int index) {
		Document doc = model.getTextComponent().getDocument();
		int pos = findCaretStart(model);
		Element root = doc.getDefaultRootElement();
		int row = root.getElementIndex(pos);
				
		return root.getElement(row).getStartOffset();
	}
	
	/**
	 * Returns index of an end of a line.
	 * 
	 * @param model document
	 * @param index xurrent position
	 * @return index of an end of a line
	 */
	public static int findEndOfLine(SingleDocumentModel model, int index) {
		Document doc = model.getTextComponent().getDocument();
		int pos = findCaretStart(model) + findSel(model);
		Element root = doc.getDefaultRootElement();
		int row = root.getElementIndex(pos);
				
		return root.getElement(row).getEndOffset() - 1;
	}
	
	/**
	 * Returns selected lines.
	 * 
	 * @param model document
	 * @return selected lines
	 */
	public static List<String> getSelectedLines(SingleDocumentModel model) {
		Document doc = model.getTextComponent().getDocument();
		
		int start = findBeginningOfLine(model, findCaretStart(model));
		int length = findEndOfLine(model, start + findSel(model)) - start;
		
		String text = null;
		
		try {
			text = doc.getText(start, length);
		} catch(BadLocationException ignorable) {
		}
				
		return List.of(text.split("\n"));
	}
	
	/**
	 * Sorts given lines in ascending order.
	 * 
	 * @param lines lines
	 * @return sorted lines
	 */
	public static List<String> sortAscending(List<String> lines) {
		Locale hrLocale = new Locale(LocalizationProvider.getInstance().getCurrentLanguage());
		Collator collator = Collator.getInstance(hrLocale);
		
		List<String> linesCopy = new ArrayList<String>(lines);
		Collections.sort(linesCopy, collator);
		
		return linesCopy;
	}
	
	/**
	 * Sorts given lines in descending order.
	 * 
	 * @param lines lines
	 * @return sorted lines
	 */
	public static List<String> sortDescending(List<String> lines) {
		List<String> linesCopy = new ArrayList<String>(lines);
		Collections.reverse(linesCopy);
		return linesCopy;
	}
	
	/**
	 * Inserts given text to a given document.
	 * 
	 * @param model document
	 * @param text text
	 */
	public static void insertInDocument(SingleDocumentModel model, String text) {
		Document doc = model.getTextComponent().getDocument();
		
		try {
			doc.insertString(findCaretStart(model), text, null);
		} catch(BadLocationException ignorable) {
		}
	}
	
	/**
	 * Inserts given lines to the document.
	 * 
	 * @param model document
	 * @param lines lines
	 */
	public static void insertLinesInDocument(SingleDocumentModel model, List<String> lines) {
		Document doc = model.getTextComponent().getDocument();
		
		int start = findBeginningOfLine(model, findCaretStart(model));
				
		try {
			doc.insertString(start, String.join("\n", lines), null);
		} catch(BadLocationException ignorable) {
		}
	}
	
	/**
	 * Removes selected text from document.
	 * 
	 * @param model document
	 */
	public static void removeSelected(SingleDocumentModel model) {
		Document doc = model.getTextComponent().getDocument();
		
		try {
			doc.remove(findCaretStart(model), findSel(model));
		} catch(BadLocationException ignorable) {
		}
	}
	
	/**
	 * Removes selected lines from document.
	 * 
	 * @param model document
	 */
	public static void removeSelectedLines(SingleDocumentModel model) {
		Document doc = model.getTextComponent().getDocument();
		
		int start = findBeginningOfLine(model, findCaretStart(model));
		int length = findEndOfLine(model, start + findSel(model)) - start;
		
		try {
			doc.remove(start, length);
		} catch(BadLocationException ignorable) {
		}
	}
	
	/**
	 * Replaces selected text with given one.
	 * 
	 * @param model document
	 * @param text text
	 */
	public static void replaceSelected(SingleDocumentModel model, String text) {
		JNotepadPPUtils.removeSelected(model);
		JNotepadPPUtils.insertInDocument(model, text);
	}
	
	/**
	 * Replaces selected liens with given ones.
	 * 
	 * @param model document
	 * @param lines lines
	 */
	public static void replaceSelectedLines(SingleDocumentModel model, List<String> lines) {
		JNotepadPPUtils.removeSelectedLines(model);
		JNotepadPPUtils.insertLinesInDocument(model, lines);
	}
	
	/**
	 * Converts all text to upper case.
	 * 
	 * @param text text to convert 
	 * @return converted text
	 */
	public static String toUpperCase(String text) {
		char[] chars = text.toCharArray();
		for(int i = 0; i < chars.length; i++) {
			char c = chars[i];
			if(Character.isLowerCase(c)) {
				chars[i] = Character.toUpperCase(c);
			}
		}
		return new String(chars);
	}
	
	/**
	 * Converts all text to lower case.
	 * 
	 * @param text text to convert
	 * @return converted text
	 */
	public static String toLowerCase(String text) {
		char[] chars = text.toCharArray();
		for(int i = 0; i < chars.length; i++) {
			char c = chars[i];
			if(Character.isUpperCase(c)) {
				chars[i] = Character.toLowerCase(c);
			}
		}
		return new String(chars);
	}
	
	/**
	 * Inverts upper cases to lower cases and vice-versa.
	 * 
	 * @param text text to invert
	 * @return inverted text
	 */
	public static String toggleCase(String text) {
		char[] chars = text.toCharArray();
		for(int i = 0; i < chars.length; i++) {
			char c = chars[i];
			if(Character.isUpperCase(c)) {
				chars[i] = Character.toLowerCase(c);
			} else if(Character.isLowerCase(c)) {
				chars[i] = Character.toUpperCase(c);
			}
		}
		return new String(chars);
	}
	
	
	/**
	 * Returns name of document or "(unnamed)" if it doesn't have name.
	 * 
	 * @param model document
	 * @return name of the document
	 */
	public static String getFilename(SingleDocumentModel model) {		
		return model.getFilePath() == null ? DefaultMultipleDocumentModel.UNNAMED 
				: model.getFilePath().getFileName().toString();
	}
	
	/**
	 * Returns index of first modified document that it finds.
	 * 
	 * @param multipleDocumentModel multipleDocumentModel
	 * @return index of modified docuent
	 */
	public static int getIndexOfModified(MultipleDocumentModel multipleDocumentModel) {
		for(int i = 0; i < multipleDocumentModel.getNumberOfDocuments(); i++) {
			SingleDocumentModel model = multipleDocumentModel.getDocument(i);
			if(model.isModified()) {
				   return i;
			}
		}
		
		return -1;
	}
	
	/**
	 * Removes all duplicate lines in text.
	 * 
	 * @param model document
	 */
	public static void unique(SingleDocumentModel model) {
		Document doc = model.getTextComponent().getDocument();
		
		List<String> lines = null;
		
		try {
			lines = List.of(doc.getText(0, doc.getLength()).split("\n"));
			LinkedHashSet<String> uniqueLines = new LinkedHashSet<String>(lines);
			
			doc.remove(0, doc.getLength());
			doc.insertString(0, String.join("\n", new ArrayList<String>(uniqueLines)), null);

		} catch(BadLocationException ignorable) {
		}
		
	}

	/**
	 * Asks user if he wants to save document before closing it.
	 * 
	 * @param model document
	 * @param multipleDocumentModel multipleDocumentModel
	 */
	public static void saveDocumentWithAsking(SingleDocumentModel model, MultipleDocumentModel multipleDocumentModel) {
		String filename = JNotepadPPUtils.getFilename(model);
		
		int reply = JOptionPane.showConfirmDialog(null, 
				"Do you want to save changes in " + filename + "?", 
				"Save progress?",  JOptionPane.YES_NO_CANCEL_OPTION);
		if(reply == JOptionPane.YES_OPTION)
		{
		   saveDocument(model, multipleDocumentModel);
		   JOptionPane.showMessageDialog(
					null, 
					"Your document has been saved",
					"Success", 
					JOptionPane.INFORMATION_MESSAGE
			);
		   return;
		} else if(reply == JOptionPane.CANCEL_OPTION) {
			return;
		} 
	}
	
	/**
	 * Saves document. 
	 * 
	 * @param model document
	 * @param multipleDocumentModel multipleDocumentModel
	 */
	public static void saveDocument(SingleDocumentModel model, MultipleDocumentModel multipleDocumentModel) {
		if(model.getFilePath() == null) {
			saveDocumentAs(model, multipleDocumentModel);
			return;
		}
		
		multipleDocumentModel.saveDocument(model, null);
	}
	
	/**
	 * Asks user how he wants to save document before saving it.
	 * 
	 * @param model document 
	 * @param multipleDocumentModel multipleDocumentModel
	 */
	public static void saveDocumentAs(SingleDocumentModel model, MultipleDocumentModel multipleDocumentModel) {		
		JFileChooser jfc = new JFileChooser();
		jfc.setDialogTitle("Save file");
		if(jfc.showSaveDialog(null) != JFileChooser.APPROVE_OPTION) {
			JOptionPane.showMessageDialog(
					null, 
					"Saving was aborted.",
					"Information", 
					JOptionPane.INFORMATION_MESSAGE
				);
			return;
		}
		
		Path savePath = jfc.getSelectedFile().toPath();
		
		if(Files.exists(savePath)) {
			int reply = JOptionPane.showConfirmDialog(null, 
					"File " + savePath.toString() + " already exists. Are you sure you want to overwrite it?",
					"Overwrite?",  JOptionPane.YES_NO_OPTION);
			if(reply == JOptionPane.NO_OPTION) {
				return;
			}
		}
		multipleDocumentModel.saveDocument(model, savePath);
		JOptionPane.showMessageDialog(
				null, 
				"Your document has been saved",
				"Success", 
				JOptionPane.INFORMATION_MESSAGE
		);
	}
	
	/**
	 * Asks user for every unsaved document if he wants to save it.
	 * 
	 * @param multipleDocumentModel multipleDocumentModel
	 */
	public static void saveAllModified(MultipleDocumentModel multipleDocumentModel) {
		for(int i = 0; i < multipleDocumentModel.getNumberOfDocuments(); i++) {
			SingleDocumentModel model = multipleDocumentModel.getDocument(i);
			if(model.isModified()) {
				   JNotepadPPUtils.saveDocumentWithAsking(model, multipleDocumentModel);
			}
		}
	}
	
	/**
	 * Returns true if {@link JNotepadPP} can close.
	 * 
	 * @param multipleDocumentModel multipleDocumentModel
	 * @return true if notepad can be closed; false otherwise
	 */
	public static boolean canClose(MultipleDocumentModel multipleDocumentModel) {
		int index = JNotepadPPUtils.getIndexOfModified(multipleDocumentModel);
		
		if(index == -1) {
			return true;
		}
				
		int reply = JOptionPane.showConfirmDialog(null, 
				"All unsaved progress will be lose. Are you sure you want to exit?", 
				"Exit?",  JOptionPane.OK_CANCEL_OPTION);
		
		return reply != JOptionPane.CANCEL_OPTION;		
	}
	
	/**
	 * Sets texts of an label to current time every second.
	 * 
	 * @param label label 
	 */
	public static void startClock(JLabel label) {			
		Thread t = new Thread(()->{
			while(true) {
				try {
					Thread.sleep(500);
				} catch(Exception ex) {}
				SwingUtilities.invokeLater(()->{
					label.setText(FORMATTER.format(new Date()).toString());
				});
			}
		});
		t.setDaemon(true);
		t.start();
	}
	
}
