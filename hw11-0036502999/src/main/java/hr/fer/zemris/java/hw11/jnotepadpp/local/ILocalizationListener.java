package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * Listener for {@link ILocalizationProvider} class.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public interface ILocalizationListener {

	/**
	 * This method is called when localization changes in 
	 * {@link ILocalizationProvider} class.
	 */
	void localizationChanged();

}
