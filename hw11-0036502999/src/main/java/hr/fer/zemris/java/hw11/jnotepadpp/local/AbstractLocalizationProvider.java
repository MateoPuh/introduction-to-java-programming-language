package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * {@link ILocalizationProvider} with implemented methods
 * {@link #addLocalizationListener(ILocalizationListener)} and 
 * {@link #removeLocalizationListener(ILocalizationListener)}
 * and with a method {@link #fire()} which notifies all listeners
 * about change in localization.
 *  
 * @author Mateo Puhalović
 * @version 1.0
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {
	
	/** Collection of listeners. */
	private List<ILocalizationListener> listeners;
	
	/**
	 * Initializes collection of listeners.
	 */
	public AbstractLocalizationProvider() {
		listeners = new CopyOnWriteArrayList<ILocalizationListener>();
	}
	
	@Override
	public void addLocalizationListener(ILocalizationListener listener) {
		listeners.add(listener);
	}
	
	@Override
	public void removeLocalizationListener(ILocalizationListener listener) {
		listeners.remove(listener);
	}
	
	/**
	 * Notifies all listeners about change in localization.
	 */
	public void fire() {
		for(ILocalizationListener listener : listeners) {
			listener.localizationChanged();
		}
	}

}
