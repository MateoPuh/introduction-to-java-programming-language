package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * This class provides localization. Object of classes that implement
 * this interface are able to give translations for a given key. For
 * that, method {@link #getString(String)} is used. It also represents
 * Subject for {@link ILocalizationListener} objects.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public interface ILocalizationProvider {
	
	/**
	 * Adds {@link ILocalizationListener} to internal collection of listeners.
	 * 
	 * @param listener listener
	 */
	void addLocalizationListener(ILocalizationListener listener);
	
	/**
	 * Removes {@link ILocalizationListener} from internal collection of 
	 * listeners.
	 * 
	 * @param listener listener
	 */
	void removeLocalizationListener(ILocalizationListener listener);

	/**
	 * Returns current language.
	 * 
	 * @return current language
	 */
	String getCurrentLanguage();
	
	/**
	 * Returns translation for a given key.
	 * 
	 * @param key key of translation
	 * @return translation
	 */
	String getString(String key);

}
