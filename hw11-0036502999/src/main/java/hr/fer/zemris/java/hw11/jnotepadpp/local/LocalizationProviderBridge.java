package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * Decorator for {@link ILocalizationProvider}. Offers methods {@link #connect()} 
 * and {@link #disconnect()} which are used to manage connection status. When user
 * calls connect, {@link ILocalizationListener} is registered to {@link ILocalizationProvider}
 * which this class decorates. Method disconnect removes de-registeres that listener.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider {

	/** Provider which is decorated by this class. */
	private ILocalizationProvider provider;
	/** Listener that is added to provider with connect method. */
	private ILocalizationListener listener;
	/** Connection status. */
	private boolean connected = false;
	
	/**
	 * Creates new {@link LocalizationProviderBridge} with given
	 * {@link ILocalizationProvider}.
	 * 
	 * @param provider provider
	 */
	public LocalizationProviderBridge(ILocalizationProvider provider) {
		this.provider = provider;
	}
	
	/**
	 * Connects listener to provider.
	 */
	public void connect() {
		if(connected) {
			return;
		}
		connected = true;
		
		if(listener == null) {
			listener = new ILocalizationListener() {
				@Override
				public void localizationChanged() {
					fire();
				}
			};
		}
		
		provider.addLocalizationListener(listener);
	}
	
	/**
	 * Disconnects listener from provider.
	 */
	public void disconnect() {
		if(!connected) {
			return;
		}
		connected = false;
		
		provider.removeLocalizationListener(listener);
	}
	
	@Override
	public String getString(String key) {
		return provider.getString(key);
	}

	@Override
	public String getCurrentLanguage() {
		return provider.getCurrentLanguage();
	}

}
