package hr.fer.zemris.java.hw11.jnotepadpp;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import hr.fer.zemris.java.hw11.jnotepadpp.documents.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.documents.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.documents.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.documents.SingleDocumentModel;

/**
 * Default implementation of {@link MultipleDocumentModel} and {@link JTabbedPane}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel {

	private static final long serialVersionUID = 1L;

	/** Name for files that are not names. */
	public static final String UNNAMED = "(unnamed)";
	/** Path for green diskette icon. */
	private static final String SAVED_ICON_PATH = "icons/green_diskette.png";
	/** Path for red diskette icon. */
	private static final String UNSAVED_ICON_PATH = "icons/red_diskette.png";
	/** Size of an icon. */
	private static final int ICON_SIZE = 20;

	/** Icon of saved file. */
	private ImageIcon saved;
	/** Icon of unsaved file. */
	private ImageIcon unsaved;
	/** Collection of documents. */
	private List<SingleDocumentModel> documents;
	/** Current document. */
	private SingleDocumentModel currentDocument;
	/** Listeners of this {@link MultipleDocumentModel}. */
	private List<MultipleDocumentListener> listeners;
	/** Number of modifications. */
	private long modificationCount;
	
	/**
	 * Creates new {@link DefaultMultipleDocumentModel} and initializes its content.
	 */
	public DefaultMultipleDocumentModel() {
		try {
			saved = getIcon(SAVED_ICON_PATH);
			unsaved = getIcon(UNSAVED_ICON_PATH);
		} catch (IOException e) {
			System.out.println(e);
			System.exit(1);
		}
		
		// listeners of changing tabs - changes current document
		addChangeListener(new ChangeListener() {
	        public void stateChanged(ChangeEvent e) {
	        	if(getSelectedIndex() != -1) {
	        		SingleDocumentModel previous = currentDocument;
	        		currentDocument = documents.get(getSelectedIndex());
	        		notifyListenersCurrentChanged(previous);
	        	}        	
	        }
	    });
		
		listeners = new CopyOnWriteArrayList<MultipleDocumentListener>();
		
		addMultipleDocumentListener(new MultipleDocumentListener() {
			
			@Override
			public void documentRemoved(SingleDocumentModel model) {
				remove(documents.indexOf(model));
			}
			
			@Override
			public void documentAdded(SingleDocumentModel model) {
				addDocumentTab(model, model.getFilePath() == null ? UNNAMED : model.getFilePath().toString());		
			}
			
			@Override
			public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
				setSelectedIndex(documents.indexOf(currentModel));				
			}
		});
		
		documents = new ArrayList<SingleDocumentModel>();
		modificationCount = 0;
	}
	
	/**
	 * Loads icon from resources.
	 * 
	 * @param resource path to resource
	 * @return icon
	 * @throws IOException if error occurs
	 */
	private ImageIcon getIcon(String resource) throws IOException {
		InputStream is = this.getClass().getResourceAsStream(resource);
		if(is == null) {
			throw new IOException("Cannot load resource: " + resource);
		}
		
		byte[] bytes = is.readAllBytes();
		is.close();
		
		ImageIcon icon = new ImageIcon(bytes);
		
		return new ImageIcon(icon.getImage().getScaledInstance(
				ICON_SIZE, ICON_SIZE, java.awt.Image.SCALE_SMOOTH));
	}
	
	/**
	 * {@link Iterator} for {@link MultipleDocumentModel} class.
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 *
	 */
	private class MultipleDocumentIterator implements Iterator<SingleDocumentModel> {

		int currentIndex;
		private long savedModificationCount;
		
		/**
		 * Creates new {@link MultipleDocumentIterator}.
		 */
		public MultipleDocumentIterator() {
			currentIndex = 0;
			savedModificationCount = modificationCount;
		}
		
		@Override
		public boolean hasNext() {
			return currentIndex < getNumberOfDocuments();
		}

		@Override
		public SingleDocumentModel next() {
			if (savedModificationCount != modificationCount) {
				throw new ConcurrentModificationException("Content of collection changed while "
						+ "using this ElementsGetter!");
			}
			
			if(hasNext()) {
				return documents.get(currentIndex++);
			} else {
				throw new NoSuchElementException("There is no next element!");
			}	
		}
		
	}
	
	@Override
	public Iterator<SingleDocumentModel> iterator() {
		return new MultipleDocumentIterator();
	}

	@Override
	public SingleDocumentModel createNewDocument() {
		SingleDocumentModel model = new DefaultSingleDocumentModel(null, "");
		documents.add(model);
						
		notifyListenersAdded(model);
		
		SingleDocumentModel previous = currentDocument;
		currentDocument = model;
		
		notifyListenersCurrentChanged(previous);
		
		modificationCount++;
		return model;
	}

	@Override
	public SingleDocumentModel getCurrentDocument() {
		return currentDocument;
	}

	@Override
	public SingleDocumentModel loadDocument(Path path) {
		if(!Files.isReadable(Objects.requireNonNull(path))) {
			throw new IllegalArgumentException("Document with path: " + path.toString() + " is not readable.");
		}
		
		if(path != null) {
			for(SingleDocumentModel document : documents) {
				if(document.getFilePath() != null && document.getFilePath().equals(path)) {
					throw new IllegalArgumentException("File with path: " + path + " already open.");
				}
			}
		}
		
		String text = null;
		try {
			text = Files.readString(path);
		} catch (IOException e) {
			throw new IllegalArgumentException("Error occures while reading file.");
		}
		
		SingleDocumentModel model = new DefaultSingleDocumentModel(path, text);
		documents.add(model);
		
		notifyListenersAdded(model);
		
		SingleDocumentModel previous = currentDocument;
		currentDocument = model;
		
		notifyListenersCurrentChanged(previous);
		
		modificationCount++;
		return model;
	}
	
	/**
	 * Returns true if file with given path is already open.
	 * 
	 * @param model document
	 * @param newPath path
	 * @return true if already open; false otherwise
	 */
	private boolean checkIfAlreadyOpen(SingleDocumentModel model, Path newPath) {
		for(SingleDocumentModel document : documents) {
			if(document.equals(model)) {
				continue;
			}
			if(document.getFilePath() != null && document.getFilePath().equals(newPath)) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public void saveDocument(SingleDocumentModel model, Path newPath) {
		if(newPath != null) {
			if(checkIfAlreadyOpen(model, newPath)) {
				throw new IllegalArgumentException(
						"File with path: " + newPath + " already open.");
			}
		}
		
		try {
			Path path = newPath;
			if(newPath == null) {
				path = model.getFilePath();
			}
			if(path == null) {
				throw new IllegalArgumentException("No path is specified for saving.");
			}
			
			Files.write(path, model.getTextComponent().getText().getBytes(Charset.forName("UTF-8")));
			model.setModified(false);
			model.setFilePath(path);
		} catch (IOException e) {
			throw new IllegalArgumentException("Error occured while saving file.");
		}	
	}

	@Override
	public void closeDocument(SingleDocumentModel model) {
		notifyListenersRemoved(model);
		documents.remove(model);
		
		if(!documents.contains(currentDocument) && documents.size() > 0) {
			changeCurrent(documents.get(0));
		}
		
		modificationCount++;
	}

	/**
	 * Changes current document and notifies all listeners.
	 * 
	 * @param model new document
	 */
	private void changeCurrent(SingleDocumentModel model) {
		SingleDocumentModel previous = currentDocument;
		currentDocument = model;
		
		notifyListenersCurrentChanged(previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @throws NullPointerException if given listener is null
	 */
	@Override
	public void addMultipleDocumentListener(MultipleDocumentListener l) {
		Objects.requireNonNull(l);
		if(listeners.contains(l)){
			return;
		}
		
		listeners.add(l);		
	}

	@Override
	public void removeMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.remove(l);		
	}

	@Override
	public int getNumberOfDocuments() {
		return documents.size();
	}

	/**
	 * {@inheritDoc}
	 * @throws IndexOutOfBoundsException if given index is out of bounds
	 */
	@Override
	public SingleDocumentModel getDocument(int index) {
		return documents.get(index);
	}

	/**
	 * Adds tab for document. If document is created it will be named 
	 * "(unnamed)", otherwise it will be named as it's filename. Tooltip
	 * will be set to full path or to "(unnamed)" if path is null.
	 * 
	 * @param model document
	 * @param path path of document
	 */
	private void addDocumentTab(SingleDocumentModel model, String path) {
		addTab(path.equals(UNNAMED) ? UNNAMED : Paths.get(path).getFileName().toString(),
				saved, new JScrollPane(model.getTextComponent()), path);
		
		model.addSingleDocumentListener(new SingleDocumentListener() {
			
			@Override
			public void documentModifyStatusUpdated(SingleDocumentModel model) {
				setIconAt(documents.indexOf(model), model.isModified() ? unsaved : saved);
			}
			
			@Override
			public void documentFilePathUpdated(SingleDocumentModel model) {
				setTitleAt(documents.indexOf(model), model.getFilePath().getFileName().toString());
				setToolTipTextAt(documents.indexOf(model), model.getFilePath().toString());
			}
		});
	}
	
	/**
	 * Notifies all listeners that document has been removed. It
	 * should be called before document is removed from internal
	 * collection of documents.
	 * 
	 * @param model removed documents
	 */
	private void notifyListenersRemoved(SingleDocumentModel model) {
		for(MultipleDocumentListener l : listeners) {
			l.documentRemoved(model);
		}
	}
	
	/**
	 * Notifies all listeners that document has been added to internal
	 * collection of documents.
	 * 
	 * @param model added document
	 */
	private void notifyListenersAdded(SingleDocumentModel model) {
		for(MultipleDocumentListener l : listeners) {
			l.documentAdded(model);
		}
	}
	
	/**
	 * Notifies all listeners that current document has been
	 * changed.
	 * 
	 * @param model previous document
	 */
	private void notifyListenersCurrentChanged(SingleDocumentModel model) {
		for(MultipleDocumentListener l : listeners) {
			l.currentDocumentChanged(model, currentDocument);;
		}
	}
}
