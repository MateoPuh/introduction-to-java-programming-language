package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

/**
 * {@link LocalizationProviderBridge} for {@link JFrame}. It registeres
 * {@link WindowListener} to given JFrame in a way that it calls {@link #connect()}
 * method when frame is opened and {@link #disconnect()} method when frame 
 * is closed.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class FormLocalizationProvider extends LocalizationProviderBridge {

	/**
	 * Creates new {@link FormLocalizationProvider} with given {@link ILocalizationProvider}
	 * and {@link JFrame}.
	 * 
	 * @param provider provider
	 * @param frame frame
	 */
	public FormLocalizationProvider(ILocalizationProvider provider, JFrame frame) {
		super(provider);
		
		frame.addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				super.windowOpened(e);
				connect();
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				super.windowClosed(e);
				disconnect();
			}
			
		});
	}

}
