package hr.fer.zemris.java.hw11.jnotepadpp.documents;

import java.nio.file.Path;

import javax.swing.JComponent;
import javax.swing.JTextArea;

/**
 * Represents a model of single document. Contains information about
 * file path from which document was loaded, document modification 
 * status and reference to the javax.swing {@link JComponent} which
 * is used for editing. Each document has it's own editor component.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public interface SingleDocumentModel {
	
	/**
	 * Returns {@link JTextArea} with text from document set as it's 
	 * text content.
	 * 
	 * @return text area with text set from document
	 */
	JTextArea getTextComponent();
	
	/**
	 * Returns file path of this document.
	 * 
	 * @return document's file path
	 */
	Path getFilePath();
	
	/**
	 * Sets the file path for a document.
	 * 
	 * @param path new file path for a document
	 * @throws NullPointerException if given path is null
	 */
	void setFilePath(Path path);
	
	/**
	 * Returns true if document is modified.
	 * 
	 * @return true if document is modified; false otherwise
	 */
	boolean isModified();
	
	/**
	 * Sets document's modified flag to given value.
	 * 
	 * @param modified value to set to modified flag
	 */
	void setModified(boolean modified);
	
	/**
	 * Adds {@link SingleDocumentListener} to internal collection
	 * of listeners.
	 * 
	 * @param l listener
	 * @throws NullPointerException if given listener is null
	 */
	void addSingleDocumentListener(SingleDocumentListener l);
	
	/**
	 * Removes given {@link SingleDocumentListener} from internal collection 
	 * of listeners.
	 * 
	 * @param l listener
	 * @throws NullPointerException if given listener is null
	 */
	void removeSingleDocumentListener(SingleDocumentListener l);

}
