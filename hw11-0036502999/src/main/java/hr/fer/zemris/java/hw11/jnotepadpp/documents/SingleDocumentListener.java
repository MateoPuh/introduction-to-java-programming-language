package hr.fer.zemris.java.hw11.jnotepadpp.documents;

/**
 * Listener used in {@link SingleDocumentModel}. It contains methods
 * which are called when document modifies it's status and file path.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public interface SingleDocumentListener {
	
	/**
	 * This method is called whenever {@link SingleDocumentModel} modifies
	 * it's status.
	 * 
	 * @param model subject that is observed by this listener
	 */
	void documentModifyStatusUpdated(SingleDocumentModel model); 
	
	/**
	 * This method is called whenever {@link SingleDocumentModel} modifies
	 * it's file path.
	 * 
	 * @param model subject that is observed by this listener
	 */
	void documentFilePathUpdated(SingleDocumentModel model);

}
