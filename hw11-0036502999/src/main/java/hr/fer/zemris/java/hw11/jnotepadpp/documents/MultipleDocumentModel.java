package hr.fer.zemris.java.hw11.jnotepadpp.documents;

import java.nio.file.Path;

/**
 * Represents a model capable of holding zero, one or
 * more documents and having a concept of current document.
 * Current document is shown to the user and user works
 * on it.
 * 
 * <p>Documents which {@link MultipleDocumentModel} holds
 * are represented by {@link SingleDocumentModel}.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public interface MultipleDocumentModel extends Iterable<SingleDocumentModel> {
	
	/**
	 * Creates and returns new {@link SingleDocumentModel}.
	 * 
	 * @return created document
	 */
	SingleDocumentModel createNewDocument();
	
	/**
	 * Returns current document, i.e. the document that is shown 
	 * to the user and on which user is currently working.
	 * 
	 * @return current document
	 */
	SingleDocumentModel getCurrentDocument();
	
	/**
	 * Loads the document with given path.
	 * 
	 * @param path file path of document
	 * @return document that was loaded
	 */
	SingleDocumentModel loadDocument(Path path);
	
	/**
	 * Saves given document to the given file path. If given path is null,
	 * then original document's path is used.
	 * 
	 * @param model model of a document
	 * @param newPath new file path of a document
	 */
	void saveDocument(SingleDocumentModel model, Path newPath);
	
	/**
	 * Closes the given document.
	 * 
	 * @param model model of a document
	 */
	void closeDocument(SingleDocumentModel model);
	
	/**
	 * Adds {@link MultipleDocumentListener} to internal collection
	 * of listeners.
	 * 
	 * @param l listener
	 */
	void addMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Removes given {@link MultipleDocumentListener} from internal
	 * collection of listeners.
	 * 
	 * @param l listener
	 */
	void removeMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Returns the number of documents.
	 * 
	 * @return number of documents
	 */
	int getNumberOfDocuments();
	  
	/**
	 * Returns document with given index.
	 * 
	 * @param index index of document
	 * @return document with given index
	 */
	SingleDocumentModel getDocument(int index);

}
