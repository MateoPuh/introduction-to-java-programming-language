package hr.fer.zemris.java.hw11.jnotepadpp.documents;

/**
 * Listener used in {@link MultipleDocumentModel}. It contains methods
 * which are called when current document is changed, new document is
 * added and when document is removed.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public interface MultipleDocumentListener {
	
	/**
	 * This method is called whenever current document is changed in
	 * {@link MultipleDocumentModel}. One of arguments can be null, but
	 * not both
	 * 
	 * @param previousModel previous document
	 * @param currentModel current document
	 * @throws NullPointerException if both previousModel and currentModel
	 * 		are null
	 */
	void currentDocumentChanged(SingleDocumentModel previousModel, 
			SingleDocumentModel currentModel);
	
	/**
	 * This method is called whenever new document is added to
	 * {@link MultipleDocumentModel}.
	 * 
	 * @param model added document
	 */
	void documentAdded(SingleDocumentModel model);
	
	/**
	 * This method is called whenever document is removed from
	 * {@link MultipleDocumentModel}.
	 * 
	 * @param model removed document
	 */
	void documentRemoved(SingleDocumentModel model);

}
