package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

/**
 * {@link AbstractAction} that contains key for translation of 
 * component's name and translates name whenever localization
 * changes.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class LocalizableAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates new {@link LocalizableAction} with given key for translation
	 * of a name and {@link ILocalizationProvider}.
	 * 
	 * @param key key
	 * @param lp provider
	 */
	public LocalizableAction(String key, ILocalizationProvider lp) {
		putValue(NAME, lp.getString(key));
		lp.addLocalizationListener(() -> {
			putValue(NAME, lp.getString(key));
		});
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {		
	}

}
