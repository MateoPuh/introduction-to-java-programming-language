package hr.fer.zemris.java.hw17.jvdraw.JVDraw.color;

import java.awt.Color;

/**
 * Listener that listens change if color.
 * 
 * @author Mateo Puhalović
 *
 */
public interface ColorChangeListener {
	
	/**
	 * Method called when color changes.
	 * 
	 * @param source source of color
	 * @param oldColor old color
	 * @param newColor new color
	 */
	public void newColorSelected(IColorProvider source, Color oldColor, Color newColor);

}
