package hr.fer.zemris.java.hw17.jvdraw.JVDraw;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Supplier;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.color.JColorArea;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing.DrawingModelImpl;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing.DrawingObjectListModel;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing.JDrawingCanvas;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.tools.CircleTool;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.tools.FilledCircleTool;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.tools.LineTool;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.tools.Tool;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors.GeometricalObjectSaver;

/**
 * Main class for running Application.
 * 
 * @author Mateo Puhalović
 *
 */
public class JVDraw extends JFrame {

	private static final long serialVersionUID = 1L;
	private static final Color DEFAULT_FOREGROUND_COLOR = Color.BLACK;
	private static final Color DEFAULT_BACKGROUND_COLOR = Color.WHITE;

	private IColorProvider fgColorProvider;
    private IColorProvider bgColorProvider;
	
	private DrawingModel drawingModel;
	private DrawingObjectListModel listModel;
	private JList<GeometricalObject> list;
	private Tool currentTool;
	private Supplier<Tool> toolSupplier;
    private JDrawingCanvas canvas;
	    
    private Path currentFile;
    
    /**
     * Creates new {@link JVDraw}.
     */
	public JVDraw() {
		setTitle("JVDraw");
		setSize(800, 600);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(null);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exit();
			}
		});
		
		initGUI();
	}
	
	/**
	 * Initializes GUI.
	 */
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
				
		toolSupplier = new Supplier<Tool>() {
			@Override
			public Tool get() {
				return currentTool;
			}
		};
		
		drawingModel = new DrawingModelImpl();
		canvas = new JDrawingCanvas(toolSupplier, drawingModel);
		
		canvas.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if(currentTool != null) {
					currentTool.mouseReleased(e);				
				}				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				if(currentTool != null) {
					currentTool.mousePressed(e);				
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if(currentTool != null) {
					currentTool.mouseClicked(e);				
				}
			}
		});
		
		canvas.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {	
				if(currentTool != null) {
					currentTool.mouseMoved(e);
				}
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				if(currentTool != null) {
					currentTool.mouseDragged(e);				
				}
			}
		});
		
		cp.add(canvas, BorderLayout.CENTER);
		
		listModel = new DrawingObjectListModel(drawingModel);	
		list = new JList<GeometricalObject>(listModel);
		list.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() != 2) {
                    return;
                }

                GeometricalObject obj = list.getSelectedValue();
                if(obj == null) {
                	return;
                }
                
                GeometricalObjectEditor editor = obj.createGeometricalObjectEditor();
                
                while(true) {
                	if (JOptionPane.showConfirmDialog(JVDraw.this, editor, "Edit", JOptionPane.OK_CANCEL_OPTION)
                            == JOptionPane.OK_OPTION) {
                		try {
                            editor.checkEditing();
                		} catch (Exception ex) {
                			JOptionPane.showMessageDialog(
                        			JVDraw.this,
                					ex.getMessage(),
                					"Invalid parameters!",
                					JOptionPane.ERROR_MESSAGE
                				);
                			continue;
						}
                		
                        try {
                            editor.acceptEditing();
                            canvas.repaint();
                        } catch (Exception ex) {
                        	JOptionPane.showMessageDialog(
                        			JVDraw.this,
                					ex.getMessage(),
                					"Error occured while editing!",
                					JOptionPane.ERROR_MESSAGE
                				);
                        }
                        break;
                    } else {
                    	break;
                    }
                }
			}
			
		});
		
		list.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				GeometricalObject obj = list.getSelectedValue();
                if(obj == null) {
                	return;
                }
				
				if(e.getKeyChar() == KeyEvent.VK_DELETE) {
					drawingModel.remove(obj);
				}
				
				if(e.getKeyChar() == KeyEvent.VK_PLUS) {
					drawingModel.changeOrder(obj, 1);
				}
				
				if(e.getKeyChar() == KeyEvent.VK_MINUS) {
					drawingModel.changeOrder(obj, -1);
				}
			}
		});
		
		cp.add(new JScrollPane(list), BorderLayout.LINE_END);
		
		configureActions();
		createMenus();
		createToolbarAndStatusBar();
	}

	/**
	 * Creates toolbar and statusbar and initializes them.
	 */
	private void createToolbarAndStatusBar() {
		JToolBar tb = new JToolBar();
		tb.setLayout(new FlowLayout(FlowLayout.LEFT));
		tb.setFloatable(true);
		
		fgColorProvider = new JColorArea(DEFAULT_FOREGROUND_COLOR);
		tb.add((JColorArea)fgColorProvider);
		bgColorProvider = new JColorArea(DEFAULT_BACKGROUND_COLOR);
		tb.add((JColorArea)bgColorProvider);
		
		ButtonGroup buttonGroup = new ButtonGroup();

		LineTool lineTool = new LineTool(fgColorProvider, drawingModel);
		JToggleButton line = new JToggleButton("Line");
		line.addActionListener(e -> {
			currentTool = lineTool;
		});
		buttonGroup.add(line);
		tb.add(line);
		
		CircleTool circleTool = new CircleTool(fgColorProvider, drawingModel);
		JToggleButton circle = new JToggleButton("Circle");
		circle.addActionListener(e -> {
			currentTool = circleTool;
		});
		buttonGroup.add(circle);
		tb.add(circle);
		
		FilledCircleTool filledCircleTool = new FilledCircleTool(fgColorProvider, bgColorProvider, drawingModel);
		JToggleButton filledCircle = new JToggleButton("Filled Circle");
		filledCircle.addActionListener(e -> {
			currentTool = filledCircleTool;
		});
		buttonGroup.add(filledCircle);
		tb.add(filledCircle);
		
		getContentPane().add(tb, BorderLayout.PAGE_START);
		
		StatusBarLabel statusBar = new StatusBarLabel(fgColorProvider, bgColorProvider);
		getContentPane().add(statusBar, BorderLayout.PAGE_END);	
	}

	/**
	 * Creates and initializes menu.
	 */
	private void createMenus() {
		JMenuBar mb = new JMenuBar();
		
		JMenu file = new JMenu("File");
		mb.add(file);
		file.add(new JMenuItem(openDocument));
		file.add(new JMenuItem(saveDocument));
		file.add(new JMenuItem(saveAsDocument));
		file.addSeparator();
		file.add(new JMenuItem(export));
		file.addSeparator();
		file.add(new JMenuItem(exitApplication));

		setJMenuBar(mb);
	}

	/**
	 * Configures given action.
	 * 
	 * @param action action
	 * @param keyStroke key stroke
	 * @param keyEvent key event
	 * @param description description
	 */
	private void configureAction(Action action, KeyStroke keyStroke, int keyEvent, String description) {
		action.putValue(Action.ACCELERATOR_KEY, keyStroke);
		action.putValue(Action.MNEMONIC_KEY, keyEvent);
		action.putValue(Action.SHORT_DESCRIPTION, description);
	}
	
	/**
	 * Configures actions for menu.
	 */
	private void configureActions() {
		configureAction(openDocument, KeyStroke.getKeyStroke("control O"), KeyEvent.VK_O, "Open file from disk");
		configureAction(saveDocument, KeyStroke.getKeyStroke("control S"), KeyEvent.VK_S, "Save file to disk");
		configureAction(saveAsDocument, KeyStroke.getKeyStroke("F12"), KeyEvent.VK_A, "Save file to disk as...");
		configureAction(export, KeyStroke.getKeyStroke("control E"), KeyEvent.VK_E, "Export image");
		configureAction(exitApplication, KeyStroke.getKeyStroke("alt F4"), KeyEvent.VK_E, "Terimnates application");
	}
	
	private void exit() {
		int reply = JOptionPane.showConfirmDialog(null, 
				"Are you sure you want to quit?\nAll unsaved changes will be lost.", 
				"Exit?",  JOptionPane.OK_CANCEL_OPTION);
		if(reply == JOptionPane.OK_OPTION)
		{
			dispose();
		} 	
	}
	
	/** Action that loads document. */
	private final Action openDocument = new AbstractAction("open") {
	
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {			
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle("Open file");
			if(jfc.showOpenDialog(JVDraw.this) != JFileChooser.APPROVE_OPTION) {
				return;
			}
			
			Path path = jfc.getSelectedFile().toPath();
			
			if(!Files.isReadable(path)) {
				JOptionPane.showMessageDialog(
						JVDraw.this, 
					"File is not readable.",
					"Error", 
					JOptionPane.ERROR_MESSAGE
				);
				return;
			}
			
			try {
				DrawingModel m = Utils.loadFile(path);
				
				currentFile = path;
				
				drawingModel.clear();
				for(int i = 0; i < m.getSize(); i++) {
					drawingModel.add(m.getObject(i));
				}
			} catch(Exception e2) {
				JOptionPane.showMessageDialog(
						JVDraw.this, 
						e2.getLocalizedMessage(),
						"Error", 
						JOptionPane.ERROR_MESSAGE
					);
					return;
			}
		}
	};
	
	/** Save as... action */
	private final Action saveAsDocument = new AbstractAction("saveAs") {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			String file = createFile();
			currentFile = Utils.saveFileAs(file);				
		}
		
	};
	
	/** Save action */
	private final Action saveDocument = new AbstractAction("save") {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			String file = createFile();
			
			if(currentFile == null) {
				currentFile = Utils.saveFileAs(file);				
			} else {
				Utils.saveFile(currentFile, file);
			}
		}
		
	};
	
	/** Exit action */
	private final Action exitApplication = new AbstractAction("exit") {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			exit();		
		}
		
	};
	
	/** Export action */
	private final Action export = new AbstractAction("export") {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Utils.exportFile(drawingModel);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
		}
		
	};
	
	/**
	 * Creates file to save.
	 * 
	 * @return file in string format
	 */
	private String createFile() {
		GeometricalObjectSaver saver = new GeometricalObjectSaver();

		for(int i = 0; i < drawingModel.getSize(); i++) {
			GeometricalObject o = drawingModel.getObject(i);
			o.accept(saver);
		}
		
		return saver.getFile();
	}
	
	/**
	 * Main method.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new JVDraw().setVisible(true);;
		});
	}
	
}
