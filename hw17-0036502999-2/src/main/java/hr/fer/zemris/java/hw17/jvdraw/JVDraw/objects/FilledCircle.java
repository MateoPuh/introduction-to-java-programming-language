package hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects;

import java.awt.Color;
import java.awt.Point;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.editors.FilledCircleEditor;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors.GeometricalObjectVisitor;

/**
 * {@link GeometricalObject} of filled circle.
 * 
 * @author Mateo Puhalović
 *
 */
public class FilledCircle extends GeometricalObject {

	private Point center;
	private double radius;
	private Color edgeColor;
	private Color fillColor;
	
	/**
	 * Creates new {@link FilledCircle}.
	 * 
	 * @param center
	 * @param radius
	 * @param edgeColor
	 * @param fillColor
	 */
	public FilledCircle(Point center, double radius, Color edgeColor, Color fillColor) {
		this.center = center;
		this.radius = radius;
		this.edgeColor = edgeColor;
		this.fillColor = fillColor;
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new FilledCircleEditor(this);
	}

	/**
	 * Returns center point.
	 * 
	 * @return center point.
	 */
	public Point getCenter() {
		return center;
	}

	/**
	 * Returns radius.
	 * 
	 * @return radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * Returns color of the edge.
	 * 
	 * @return edge color
	 */
	public Color getEdgeColor() {
		return edgeColor;
	}

	/**
	 * Returns fill color.
	 * 
	 * @return fill color
	 */
	public Color getFillColor() {
		return fillColor;
	}
	
	/**
	 * Sets center point.
	 * 
	 * @param center center point
	 */
	public void setCenter(Point center) {
		this.center = center;
	}

	/**
	 * Sets radius.
	 * 
	 * @param radius radius
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/**
	 * Returns color of the edge.
	 * 
	 * @param edgeColor edge color
	 */
	public void setEdgeColor(Color edgeColor) {
		this.edgeColor = edgeColor;
	}

	/**
	 * Returns fill color.
	 * 
	 * @param fillColor fill color
	 */
	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}

	@Override
	public String toString() {
		return "Filled circle (" + center.x + "," + center.y  
				+ "), " + (int)radius + ", #" + 
				Integer.toHexString(fillColor.getRGB()).substring(2);
	}
	
}
