package hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing;

import javax.swing.AbstractListModel;
import javax.swing.ListModel;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.GeometricalObject;

/**
 * {@link ListModel} that lists {@link GeometricalObject}s.
 * 
 * @author Mateo Puhalović
 *
 */
public class DrawingObjectListModel extends AbstractListModel<GeometricalObject> implements DrawingModelListener {

	private static final long serialVersionUID = 1L;
	private DrawingModel model;
	
	/**
	 * Creates new {@link DrawingObjectListModel}.
	 * 
	 * @param model {@link DrawingModel}
	 */
	public DrawingObjectListModel(DrawingModel model) {
		this.model = model;
		model.addDrawingModelListener(this);
	}

	@Override
	public int getSize() {
		return model.getSize();
	}

	@Override
	public GeometricalObject getElementAt(int index) {
		return model.getObject(index);
	}

	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		super.fireIntervalAdded(source, index0, index1);		
	}

	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		super.fireIntervalRemoved(source, index0, index1);		
	}

	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		super.fireContentsChanged(source, index0, index1);		
	}

}
