package hr.fer.zemris.java.hw17.jvdraw.JVDraw;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing.DrawingModelImpl;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Circle;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Line;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors.GeometricalObjectBBCalculator;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors.GeometricalObjectPainter;

/**
 * Class that contains useful methods.
 * 
 * @author Mateo Puhalović
 *
 */
public class Utils {
	
	/**
	 * Extension for JVDraw files.
	 */
	private static final String EXTENSION = "jvd";
	/**
	 * Allowed extensions for export.
	 */
	private static final String[] ALLOWED_EXTENSIONS = {"png", "jpg", "gif"};
	
	/**
	 * Asks user for path and saves the file.
	 * 
	 * @param file file to save
	 * @return path of saved file
	 */
	public static Path saveFileAs(String file) {
		JFileChooser jfc = new JFileChooser();
		jfc.setDialogTitle("Save file");
		if(jfc.showSaveDialog(null) != JFileChooser.APPROVE_OPTION) {
			JOptionPane.showMessageDialog(
					null, 
					"Saving was aborted.",
					"Information", 
					JOptionPane.INFORMATION_MESSAGE
				);
			throw new RuntimeException();
		}
		
		Path savePath = jfc.getSelectedFile().toPath();
		
		if(Files.exists(savePath)) {
			int reply = JOptionPane.showConfirmDialog(null, 
					"File " + savePath.toString() + " already exists. Are you sure you want to overwrite it?",
					"Overwrite?",  JOptionPane.YES_NO_OPTION);
			if(reply == JOptionPane.NO_OPTION) {
				throw new RuntimeException();
			}
		}
		saveFile(savePath, file);
		JOptionPane.showMessageDialog(
				null, 
				"Your document has been saved",
				"Success", 
				JOptionPane.INFORMATION_MESSAGE
		);
		
		return savePath;
	}
	
	/**
	 * Saves file.
	 * 
	 * @param path path to save file
	 * @param file file content
	 */
	public static void saveFile(Path path, String file) {
		try {
			if(path == null) {
				throw new IllegalArgumentException("No path is specified for saving.");
			}
			
			Path withExtesion = path;
			
			if(!path.getFileName().toString().endsWith(EXTENSION)) {
				withExtesion = path.resolveSibling(path.getFileName() + "." + EXTENSION);
			}
			
			
			Files.write(withExtesion, file.getBytes(Charset.forName("UTF-8")));
		} catch (IOException e) {
			throw new RuntimeException("Error occured while saving file.");
		}
	}
	
	/**
	 * Loads file and returns DrawingModel with parsed objects.
	 * 
	 * @param path path of file
	 * @return drawing model with parsed objects
	 * @throws IOException if error occurs
	 */
	public static DrawingModel loadFile(Path path) throws IOException {
		String extension = "";

		int i = path.getFileName().toString().lastIndexOf('.');
		if (i > 0) {
		    extension = path.getFileName().toString().substring(i+1);
		}		
		
		if(!extension.equals(EXTENSION)) {
			throw new RuntimeException("Cannot open file: Invalid extension");
		}
		
		List<String> lines = Files.readAllLines(path);
		DrawingModel drawingModel = new DrawingModelImpl();
		
		for(String line : lines) {
			if(line.startsWith("LINE")) {
				drawingModel.add(parseLine(line));
			} else if(line.startsWith("CIRCLE")) {
				drawingModel.add(parseCircle(line));
			} else if(line.startsWith("FCIRCLE")) {
				drawingModel.add(parseFilledCircle(line));
			} else {
				throw new RuntimeException();
			}
		}
		
		return drawingModel;		
	}
	
	/**
	 * Parses {@link Line} from line of text.
	 * 
	 * @param line line from file
	 * @return Line
	 */
	public static Line parseLine(String line) {
		if(!line.startsWith("LINE")) {
			throw new RuntimeException("Cannot parse line");
		}
		
		line = line.substring("LINE ".length());
		
		String[] parts = line.split(" ");
		
		Point start = new Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
		Point end = new Point(Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
		Color color = new Color(Integer.parseInt(parts[4]), Integer.parseInt(parts[5]), Integer.parseInt(parts[6]));
		
		return new Line(start, end, color);
	}
	
	/**
	 * Parses {@link Circle} from line of text.
	 * 
	 * @param line line from file
	 * @return Circle
	 */
	public static Circle parseCircle(String line) {
		if(!line.startsWith("CIRCLE")) {
			throw new RuntimeException("Cannot parse circle");
		}
		
		line = line.substring("CIRCLE ".length());
		
		String[] parts = line.split(" ");
		
		Point center = new Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
		int radius = Integer.parseInt(parts[2]);
		Color color = new Color(Integer.parseInt(parts[3]), Integer.parseInt(parts[4]), Integer.parseInt(parts[5]));
		
		return new Circle(center, radius, color);
	}
	
	/**
	 * Parses {@link FilledCircle} from line of text.
	 * 
	 * @param line line from file
	 * @return {@link FilledCircle}
	 */
	public static FilledCircle parseFilledCircle(String line) {
		if(!line.startsWith("FCIRCLE")) {
			throw new RuntimeException("Cannot parse filled circle");
		}
		
		line = line.substring("FCIRCLE ".length());
		
		String[] parts = line.split(" ");
		
		Point center = new Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
		int radius = Integer.parseInt(parts[2]);
		Color color = new Color(Integer.parseInt(parts[3]), Integer.parseInt(parts[4]), Integer.parseInt(parts[5]));
		Color fillColor = new Color(Integer.parseInt(parts[6]), Integer.parseInt(parts[7]), Integer.parseInt(parts[8]));

		return new FilledCircle(center, radius, color, fillColor);
	}

	/**
	 * Exports file as image.
	 * 
	 * @param drawingModel model with objects
	 * @throws IOException if error occurs
	 */
	public static void exportFile(DrawingModel drawingModel) throws IOException {
		JFileChooser jfc = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Image files", "jpg", "png", "gif");
        jfc.setFileFilter(filter);
		jfc.setDialogTitle("Export image");
		if(jfc.showSaveDialog(null) != JFileChooser.APPROVE_OPTION) {
			JOptionPane.showMessageDialog(
				null, 
				"Export was aborted.",
				"Information", 
				JOptionPane.INFORMATION_MESSAGE
			);
			throw new RuntimeException();
		}
		
		Path savePath = jfc.getSelectedFile().toPath();
		
		GeometricalObjectBBCalculator bbcalc = new GeometricalObjectBBCalculator();
		for(int i = 0; i < drawingModel.getSize(); i++) {
			drawingModel.getObject(i).accept(bbcalc);
		}
		Rectangle box = bbcalc.getBoundingBox();
		BufferedImage image = new BufferedImage(
				box.width, box.height, BufferedImage.TYPE_3BYTE_BGR
		);
		Graphics2D g = image.createGraphics();
		
		
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, box.width, box.height);
		
		g.translate(-box.x, -box.y);
		
		GeometricalObjectPainter painter = new GeometricalObjectPainter(g);
        for (int i = 0; i < drawingModel.getSize(); i++) {
            drawingModel.getObject(i).accept(painter);
        }
        g.dispose();
		
		if(Files.exists(savePath)) {
			int reply = JOptionPane.showConfirmDialog(null, 
					"File " + savePath.toString() + " already exists. Are you sure you want to overwrite it?",
					"Overwrite?",  JOptionPane.YES_NO_OPTION);
			if(reply == JOptionPane.NO_OPTION) {
				throw new RuntimeException();
			}
		}
		
		String extension = "";

		int i = savePath.getFileName().toString().lastIndexOf('.');
		if (i > 0) {
		    extension = savePath.getFileName().toString().substring(i+1);
		}
		
		boolean valid = false;
		for(String ext : ALLOWED_EXTENSIONS) {
			if(ext.equals(extension)) {
				valid = true;
			}
		}
		
		if(!valid) {
			JOptionPane.showMessageDialog(
				null, 
				"Invalid extension",
				"Error", 
				JOptionPane.INFORMATION_MESSAGE
			);
			
			exportFile(drawingModel);
			return;
		}
		
		ImageIO.write(image, extension, savePath.toFile());
		
		JOptionPane.showMessageDialog(
				null, 
				"Your document has been saved",
				"Success", 
				JOptionPane.INFORMATION_MESSAGE
		);		
	}
	
	
}
