package hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Circle;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Line;

/**
 * {@link GeometricalObjectVisitor} used for painting {@link GeometricalObject}s
 * with given {@link Graphics2D}.
 * 
 * @author Mateo Puhalović
 *
 */
public class GeometricalObjectPainter implements GeometricalObjectVisitor {

	private Graphics2D graphics2d;
	
	/**
	 * Creates new {@link GeometricalObjectPainter}.
	 * 
	 * @param graphics2d graphics
	 */
	public GeometricalObjectPainter(Graphics2D graphics2d) {
		this.graphics2d = graphics2d;
	}

	@Override
	public void visit(Line line) {
		graphics2d.setColor(line.getColor());
		graphics2d.drawLine(
				(int)line.getStart().getX(),
				(int)line.getStart().getY(),
				(int)line.getEnd().getX(),
				(int)line.getEnd().getY()
                );		
	}

	@Override
	public void visit(Circle circle) {
		graphics2d.setColor(circle.getEdgeColor());
		Shape circleShape = new Ellipse2D.Double(
				(int)(circle.getCenter().getX() - circle.getRadius()),
				(int)(circle.getCenter().getY() - circle.getRadius()),
				2 * circle.getRadius(), 
				2 * circle.getRadius());
		graphics2d.draw(circleShape);
	}

	@Override
	public void visit(FilledCircle circle) {
		Shape circleShape = new Ellipse2D.Double(
				(int)(circle.getCenter().getX() - circle.getRadius()),
				(int)(circle.getCenter().getY() - circle.getRadius()),
				2 * circle.getRadius(), 
				2 * circle.getRadius());
		
		graphics2d.setColor(circle.getFillColor());
		graphics2d.fill(circleShape);
		
		graphics2d.setColor(circle.getEdgeColor());
		graphics2d.draw(circleShape);	
	}

}
