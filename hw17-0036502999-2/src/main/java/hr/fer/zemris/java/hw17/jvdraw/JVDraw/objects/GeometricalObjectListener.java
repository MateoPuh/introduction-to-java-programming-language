package hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects;

/**
 * Listeners for {@link GeometricalObject}.
 * 
 * @author Mateo Puhalović
 *
 */
public interface GeometricalObjectListener {
	
	/**
	 * Method that is called when object changes.
	 * 
	 * @param o object
	 */
	public void geometricalObjectChanged(GeometricalObject o);

}
