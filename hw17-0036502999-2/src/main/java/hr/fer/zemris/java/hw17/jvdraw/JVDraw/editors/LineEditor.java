package hr.fer.zemris.java.hw17.jvdraw.JVDraw.editors;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;

import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Line;

/**
 * {@link GeometricalObjectEditor} for {@link Line}.
 * 
 * @author Mateo Puhalović
 *
 */
public class LineEditor extends GeometricalObjectEditor {

	private static final long serialVersionUID = 1L;

	private Line line;
	
	private JTextField startX;
	private JTextField startY;
	private JTextField endX;
	private JTextField endY;
	private JColorChooser color;
	
	private Line newLine;
	
	/**
	 * Creates new {@link LineEditor}.
	 * 
	 * @param line line
	 */
	public LineEditor(Line line) {
		this.line = line;
		initGUI();
	}
	
	/**
	 * Initializes values on GUI.
	 */
	private void initGUI() {
		initValues();
		
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
        
        constraints.gridx = 0;
        constraints.gridy = 0;     
        add(new JLabel("Starting point:"), constraints);
        
        constraints.gridx = 1;
        add(startX, constraints);
        constraints.gridx = 2;
        add(startY, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 2;     
        add(new JLabel("Ending point:"), constraints);
        
        constraints.gridx = 1;
        add(endX, constraints);
        constraints.gridx = 2;
        add(endY, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 3;     
        add(new JLabel("Color:"), constraints);
        
        constraints.gridx = 1;
        add(color, constraints);
	}

	/**
	 * Initializes values on GUI
	 */
	private void initValues() {
		startX = new JTextField(String.valueOf((int)line.getStart().getX()));
		startY = new JTextField(String.valueOf((int)line.getStart().getY()));
		endX = new JTextField(String.valueOf((int)line.getEnd().getX()));
		endY = new JTextField(String.valueOf((int)line.getEnd().getY()));
		color = new JColorChooser(line.getColor());
	}

	@Override
	public void checkEditing() {
		newLine = new Line(
			new Point(
					Integer.parseInt(startX.getText()),
					Integer.parseInt(startY.getText())),
			new Point(
					Integer.parseInt(endX.getText()),
					Integer.parseInt(endY.getText())),
			color.getColor()
		);
	}

	@Override
	public void acceptEditing() {
		line.setStart(newLine.getStart());	
		line.setEnd(newLine.getEnd());		
		line.setColor(newLine.getColor());	
	}

}
