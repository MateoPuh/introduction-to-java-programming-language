package hr.fer.zemris.java.hw17.jvdraw.JVDraw.tools;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Line;

/**
 * {@link Tool} for creating {@link Line}.
 * 
 * @author Mateo Puhalović
 *
 */
public class LineTool implements Tool {

	private IColorProvider colorProvider;
    private DrawingModel drawingModel;
    private boolean startingPoint = true;
    
	private Point start;
	private Point end;
	
	private Line newLine;
	
	/**
	 * Creates new {@link LineTool}.
	 * 
	 * @param colorProvider color
	 * @param drawingModel model
	 */
	public LineTool(IColorProvider colorProvider, DrawingModel drawingModel) {
		this.colorProvider = colorProvider;
		this.drawingModel = drawingModel;
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(startingPoint) {
			start = new Point(e.getX(), e.getY());
			startingPoint = false;
		} else {
			drawingModel.remove(newLine);

			end = new Point(e.getX(), e.getY());
			drawingModel.add(new Line(start, end, colorProvider.getCurrentColor()));
			startingPoint = true;
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		drawingModel.remove(newLine);
		if(!startingPoint) {
			end = new Point(e.getX(), e.getY());
			newLine = new Line(start, end, colorProvider.getCurrentColor());
			drawingModel.add(newLine);
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void paint(Graphics2D g2d) {	
	}

}
