package hr.fer.zemris.java.hw17.jvdraw.JVDraw.editors;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;

import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Circle;

/**
 * {@link GeometricalObjectEditor} for {@link Circle}.
 * 
 * @author Mateo Puhalović
 *
 */
public class CircleEditor extends GeometricalObjectEditor {

	private static final long serialVersionUID = 1L;

	private Circle circle;
	
	private JTextField centerX;
	private JTextField centerY;
	private JTextField radius;
	private JColorChooser color;
	
	private Circle newCircle;
	
	/**
	 * Creates new {@link CircleEditor}.
	 * 
	 * @param circle circle to edit
	 */
	public CircleEditor(Circle circle) {
		this.circle = circle;
		initGUI();
	}
	
	/**
	 * Initializes GUI for circle editor.
	 */
	private void initGUI() {
		initValues();
		
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
        
        constraints.gridx = 0;
        constraints.gridy = 0;     
        add(new JLabel("Center point:"), constraints);
        
        constraints.gridx = 1;
        add(centerX, constraints);
        constraints.gridx = 2;
        add(centerY, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 2;     
        add(new JLabel("Radius:"), constraints);
        
        constraints.gridx = 1;
        add(radius, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 3;     
        add(new JLabel("Color:"), constraints);
        
        constraints.gridx = 1;
        add(color, constraints);
	}

	/**
	 * Initializes values on GUI
	 */
	private void initValues() {
		centerX = new JTextField(String.valueOf((int)circle.getCenter().getX()));
		centerY = new JTextField(String.valueOf((int)circle.getCenter().getY()));
		radius = new JTextField(String.valueOf((int)circle.getRadius()));
		color = new JColorChooser(circle.getEdgeColor());
	}

	@Override
	public void checkEditing() {
		newCircle = new Circle(
				new Point(
						Integer.parseInt(centerX.getText()),
						Integer.parseInt(centerY.getText())),
				Integer.parseInt(radius.getText()),
				color.getColor());
	}

	@Override
	public void acceptEditing() {
		circle.setCenter(newCircle.getCenter());
		circle.setRadius(newCircle.getRadius());
		circle.setEdgeColor(newCircle.getEdgeColor());	
	}

}
