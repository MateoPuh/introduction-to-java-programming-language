package hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing;

/**
 * Listener that listens {@link DrawingModel}.
 * 
 * @author Mateo Puhalović
 *
 */
public interface DrawingModelListener {

	/**
	 * Method that is called when object is added.
	 * 
	 * @param source model
	 * @param index0 index of object
	 * @param index1 index of object
	 */
	public void objectsAdded(DrawingModel source, int index0, int index1);
	
	/**
	 * Method that is called when object is removed.
	 * 
	 * @param source model
	 * @param index0 index of object
	 * @param index1 index of object
	 */
	public void objectsRemoved(DrawingModel source, int index0, int index1);
	
	/**
	 * Method that is called when object is changed in model.
	 * 
	 * @param source model
	 * @param index0 old index of object
	 * @param index1 new index of object
	 */
	public void objectsChanged(DrawingModel source, int index0, int index1);

}
