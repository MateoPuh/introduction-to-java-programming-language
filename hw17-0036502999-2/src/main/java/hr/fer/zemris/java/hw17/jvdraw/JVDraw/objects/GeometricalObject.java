package hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors.GeometricalObjectVisitor;

/**
 * Geometrical Object.
 * 
 * @author Mateo Puhalović
 *
 */
public abstract class GeometricalObject {
	
	private List<GeometricalObjectListener> listeners = new ArrayList<GeometricalObjectListener>();
	
	/**
	 * Method for accepting visitor.
	 * 
	 * @param v visitor
	 */
	public abstract void accept(GeometricalObjectVisitor v);
	
	/**
	 * Creates editor for object.
	 * 
	 * @return editor
	 */
	public abstract GeometricalObjectEditor createGeometricalObjectEditor();
	
	/**
	 * Adds {@link GeometricalObjectListener}.
	 * 
	 * @param l listener
	 */
	public void addGeometricalObjectListener(GeometricalObjectListener l) {
		listeners.add(Objects.requireNonNull(l));
	}
	
	/**
	 * Removes {@link GeometricalObjectListener}
	 * 
	 * @param l listener
	 */
	public void removeGeometricalObjectListener(GeometricalObjectListener l) {
		listeners.remove(l);
	}

}
