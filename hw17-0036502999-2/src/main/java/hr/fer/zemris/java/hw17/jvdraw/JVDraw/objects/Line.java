package hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects;

import java.awt.Color;
import java.awt.Point;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.editors.LineEditor;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors.GeometricalObjectVisitor;

/**
 * {@link GeometricalObject} of line.
 * 
 * @author Mateo Puhalović
 *
 */
public class Line extends GeometricalObject {

	private Point start;
	private Point end;
	private Color color;
	
	/**
	 * Creates new {@link Line}.
	 * 
	 * @param start starting point 
	 * @param end ending point
	 * @param color color
	 */
	public Line(Point start, Point end, Color color) {
		this.start = start;
		this.end = end;
		this.color = color;
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new LineEditor(this);
	}

	/**
	 * Returns starting point.
	 * 
	 * @return starting point
	 */
	public Point getStart() {
		return start;
	}

	/**
	 * Returns ending point.
	 * 
	 * @return ending point
	 */
	public Point getEnd() {
		return end;
	}

	/**
	 * Returns color.
	 * 
	 * @return color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Sets starting point.
	 * 
	 * @param start starting point
	 */
	public void setStart(Point start) {
		this.start = start;
	}

	/**
	 * Sets ending point.
	 * 
	 * @param end ending point
	 */
	public void setEnd(Point end) {
		this.end = end;
	}

	/**
	 * Sets color.
	 * 
	 * @param color color
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	@Override
	public String toString() {
		return "Line (" + start.x + "," + start.y  
				+ ")-(" + end.x + "," + end.y +")";
	}
	
}
