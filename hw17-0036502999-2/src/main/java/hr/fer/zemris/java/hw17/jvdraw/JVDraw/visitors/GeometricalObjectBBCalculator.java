package hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors;

import java.awt.Rectangle;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Circle;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Line;

/**
 * {@link GeometricalObjectVisitor} that calculates Bounding box.
 * 
 * @author Mateo Puhalović
 *
 */
public class GeometricalObjectBBCalculator implements GeometricalObjectVisitor {

	private Integer minX;
	private Integer minY;
	private Integer maxX;
	private Integer maxY;
	
	@Override
	public void visit(Line line) {
		int minX = (int)line.getStart().getX();
		int minY = (int)line.getStart().getY();
		int maxX = (int)line.getStart().getX();
		int maxY = (int)line.getStart().getY();
		
		if(line.getEnd().getX() < line.getStart().getX()) {
			minX = (int)line.getEnd().getX();
			minY = (int)line.getEnd().getY();
		} else {
			maxX = (int)line.getEnd().getX();
			maxY = (int)line.getEnd().getY();
		}
		
		updateValues(minX, minY, maxX, maxY);
	}

	@Override
	public void visit(Circle circle) {
		updateValues((int)(circle.getCenter().getX() - circle.getRadius()), 
				(int)(circle.getCenter().getY() - circle.getRadius()), 
				(int)(circle.getCenter().getX() + circle.getRadius()),
				(int)(circle.getCenter().getY() + circle.getRadius()));		
	}

	@Override
	public void visit(FilledCircle circle) {
		updateValues((int)(circle.getCenter().getX() - circle.getRadius()), 
				(int)(circle.getCenter().getY() - circle.getRadius()), 
				(int)(circle.getCenter().getX() + circle.getRadius()),
				(int)(circle.getCenter().getY() + circle.getRadius()));			
	}
	
	/**
	 * Updates values of bounding box.
	 * 
	 * @param newMinX new minX
	 * @param newMinY new minY
	 * @param newMaxX new maxX
	 * @param newMaxY new maxY
	 */
	private void updateValues(int newMinX, int newMinY, int newMaxX, int newMaxY) {
		if(minX == null) {
			minX = newMinX;
		}
		if(minY == null) {
			minY = newMinY;
		}
		if(maxX == null) {
			maxX = newMaxX;
		}
		if(maxY == null) {
			maxY = newMaxY;
		}
		
		minX = newMinX < minX ? newMinX : minX;
		minY = newMinY < minY ? newMinY : minY;
		maxX = newMaxX > maxX ? newMaxX : maxX;
		maxY = newMaxY > maxY ? newMaxY : maxY;
	}

	/**
	 * Returns bounding box.
	 * 
	 * @return bounding box
	 */
	public Rectangle getBoundingBox() {
		return new Rectangle(minX, minY, maxX - minX, maxY - minY);
	}

}
