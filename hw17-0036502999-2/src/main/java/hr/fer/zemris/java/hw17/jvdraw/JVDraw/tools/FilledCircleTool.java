package hr.fer.zemris.java.hw17.jvdraw.JVDraw.tools;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.FilledCircle;

/**
 * {@link Tool} for creating {@link FilledCircle}.
 * 
 * @author Mateo Puhalović
 *
 */
public class FilledCircleTool implements Tool {
	
	private IColorProvider edgeColorProvider;
	private IColorProvider fillColorProvider;
    private DrawingModel drawingModel;
    private boolean startingPoint = true;
    
	private Point center;
	
	private FilledCircle newCircle;
	
	/**
	 * Creates new {@link FilledCircleTool}.
	 * 
	 * @param edgeColorProvider edge color
	 * @param fillColorProvider fill color
	 * @param drawingModel model
	 */
	public FilledCircleTool(IColorProvider edgeColorProvider, IColorProvider fillColorProvider,
			DrawingModel drawingModel) {
		this.edgeColorProvider = edgeColorProvider;
		this.fillColorProvider = fillColorProvider;
		this.drawingModel = drawingModel;
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(startingPoint) {
			center = new Point(e.getX(), e.getY());
			startingPoint = false;
		} else {
			drawingModel.remove(newCircle);

			Point point = new Point(e.getX(), e.getY());
			double radius = point.distance(center);
					
			drawingModel.add(new FilledCircle(center, radius, edgeColorProvider.getCurrentColor(), fillColorProvider.getCurrentColor()));
			startingPoint = true;
		}		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		drawingModel.remove(newCircle);
		if(!startingPoint) {
			Point point = new Point(e.getX(), e.getY());
			double radius = point.distance(center);
			newCircle = new FilledCircle(center, radius, edgeColorProvider.getCurrentColor(), fillColorProvider.getCurrentColor());
			drawingModel.add(newCircle);
		}		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void paint(Graphics2D g2d) {
	}

}
