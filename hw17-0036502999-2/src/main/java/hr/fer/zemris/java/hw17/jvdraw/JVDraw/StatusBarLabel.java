package hr.fer.zemris.java.hw17.jvdraw.JVDraw;

import java.awt.Color;
import java.awt.HeadlessException;

import javax.swing.JLabel;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.color.ColorChangeListener;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.color.IColorProvider;

/**
 * Label for status bar.
 * 
 * @author Mateo Puhalović
 *
 */
public class StatusBarLabel extends JLabel implements ColorChangeListener {

	private static final long serialVersionUID = 1L;

	private IColorProvider fgColorProvider;
	private IColorProvider bgColorProvider;

	/**
	 * Creates new {@link StatusBarLabel}.
	 * 
	 * @param fgColorProvider foreground color
	 * @param bgColorProvider background color
	 * @throws HeadlessException if error occurs
	 */
	public StatusBarLabel(IColorProvider fgColorProvider, IColorProvider bgColorProvider) throws HeadlessException {
		this.fgColorProvider = fgColorProvider;
		this.bgColorProvider = bgColorProvider;

		fgColorProvider.addColorChangeListener(this);
		bgColorProvider.addColorChangeListener(this);
		
		update();
	}

	@Override
	public void newColorSelected(IColorProvider source, Color oldColor, Color newColor) {
		update();
	}

	/**
	 * Updates values in status bar.
	 */
	private void update() {
		Color fgColor = fgColorProvider.getCurrentColor();
		Color bgColor = bgColorProvider.getCurrentColor();

		setText("Foreground color: (" + fgColor.getRed() + ", " + fgColor.getGreen() + ", " + fgColor.getBlue()
				+ "), background color: (" + bgColor.getRed() + ", " + bgColor.getGreen() + ", " + bgColor.getBlue()
				+ ").");
	}

}
