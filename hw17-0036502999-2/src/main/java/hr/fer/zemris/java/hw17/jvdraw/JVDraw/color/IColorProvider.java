package hr.fer.zemris.java.hw17.jvdraw.JVDraw.color;

import java.awt.Color;

/**
 * Provides color.
 * 
 * @author Mateo Puhalović
 *
 */
public interface IColorProvider {

	/** 
	 * Returns current color.
	 * 
	 * @return current color
	 */
	public Color getCurrentColor();
	
	/**
	 * Adds listener to this color provider.
	 * 
	 * @param l listener
	 */
	public void addColorChangeListener(ColorChangeListener l);
	
	/**
	 * Removes listener from this color provider.
	 * 
	 * @param l listener
	 */
	public void removeColorChangeListener(ColorChangeListener l);

}

