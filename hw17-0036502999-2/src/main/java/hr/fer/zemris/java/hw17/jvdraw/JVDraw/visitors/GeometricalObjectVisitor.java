package hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Circle;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Line;

/**
 * Visitor for {@link GeometricalObject}.
 * 
 * @author Mateo Puhalović
 *
 */
public interface GeometricalObjectVisitor {
	
	/**
	 * Method called when {@link Line} is visited.
	 * 
	 * @param line line
	 */
	public abstract void visit(Line line);
	 
	/**
	 * Method called when {@link Circle} is visited.
	 * 
	 * @param circle circle
	 */
	public abstract void visit(Circle circle);
	
	/**
	 * Method called when {@link FilledCircle} is visited.
	 * 
	 * @param filledCircle filled circle
	 */
	public abstract void visit(FilledCircle filledCircle);
}
