package hr.fer.zemris.java.hw17.jvdraw.JVDraw.tools;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Circle;

/**
 * {@link Tool} for creating {@link Circle}.
 * 
 * @author Mateo Puhalović
 *
 */
public class CircleTool implements Tool {

	private IColorProvider colorProvider;
    private DrawingModel drawingModel;
    private boolean startingPoint = true;
    
	private Point center;
	
	private Circle newCircle;
	
	/**
	 * Creates new {@link CircleTool}.
	 * 
	 * @param colorProvider color
	 * @param drawingModel drawing model
	 */
	public CircleTool(IColorProvider colorProvider, DrawingModel drawingModel) {
		super();
		this.colorProvider = colorProvider;
		this.drawingModel = drawingModel;
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(startingPoint) {
			center = new Point(e.getX(), e.getY());
			startingPoint = false;
		} else {
			drawingModel.remove(newCircle);

			Point point = new Point(e.getX(), e.getY());
			double radius = point.distance(center);
					
			drawingModel.add(new Circle(center, radius, colorProvider.getCurrentColor()));
			startingPoint = true;
		}		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		drawingModel.remove(newCircle);
		if(!startingPoint) {
			Point point = new Point(e.getX(), e.getY());
			double radius = point.distance(center);
			newCircle = new Circle(center, radius, colorProvider.getCurrentColor());
			drawingModel.add(newCircle);
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void paint(Graphics2D g2d) {
	}

}
