package hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.GeometricalObject;

/**
 * Implementation of {@link DrawingModel}.
 * 
 * @author Mateo Puhalović
 *
 */
public class DrawingModelImpl implements DrawingModel {

	private List<GeometricalObject> objects = new ArrayList<GeometricalObject>();
	private List<DrawingModelListener> listeners = new ArrayList<DrawingModelListener>();
	private boolean modified = false;
	
	@Override
	public int getSize() {
		return objects.size();
	}

	@Override
	public GeometricalObject getObject(int index) {
		if(index >= getSize()) {
			return null;
		}
		
		return objects.get(index);
	}

	@Override
	public void add(GeometricalObject object) {
		objects.add(object);
		object.addGeometricalObjectListener(this);
		
		for(DrawingModelListener l : listeners) {
			l.objectsAdded(this, objects.size() - 1, objects.size() - 1);
		}
		modified = true;
	}

	@Override
	public void remove(GeometricalObject object) {
		int index = objects.indexOf(object);
		if(index == -1) {
			return;
		}
		
		objects.remove(object);
		object.removeGeometricalObjectListener(this);
		
		for(DrawingModelListener l : listeners) {
			l.objectsRemoved(this, index, index);
		}
		modified = true;
	}

	@Override
	public void changeOrder(GeometricalObject object, int offset) {
		int pos = objects.indexOf(object);
		int newPos = pos + offset;
		
		if(pos == -1 || newPos < 0 || newPos >= getSize()) {
			return;
		}
		
		objects.remove(object);
		objects.add(newPos, object);
		
		for(DrawingModelListener l : listeners) {
			l.objectsChanged(this, pos, newPos);
		}
		modified = true;
	}

	@Override
	public int indexOf(GeometricalObject object) {
		return objects.indexOf(object);
	}

	@Override
	public void clear() {
		for(int i = objects.size() - 1; i >= 0; i--) {
			remove(objects.remove(i));
		}
		modified = true;
	}

	@Override
	public void clearModifiedFlag() {
		modified = false;
	}

	@Override
	public boolean isModified() {
		return modified;
	}

	@Override
	public void addDrawingModelListener(DrawingModelListener l) {
		listeners.add(l);
	}

	@Override
	public void removeDrawingModelListener(DrawingModelListener l) {
		listeners.remove(l);
	}

	@Override
	public void geometricalObjectChanged(GeometricalObject o) {
		int index = objects.indexOf(o);

		for(DrawingModelListener l : listeners) {
			l.objectsChanged(this, index, index);
		}
	}

}
