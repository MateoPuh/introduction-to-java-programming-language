package hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Circle;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.Line;

/**
 * {@link GeometricalObjectVisitor} used for savinf objects.
 * 
 * @author Mateo Puhalović
 *
 */
public class GeometricalObjectSaver implements GeometricalObjectVisitor {

	private String file;
		
	/**
	 * Creates new {@link GeometricalObjectSaver}.
	 */
	public GeometricalObjectSaver() {
		this.file = "";
	}

	@Override
	public void visit(Line line) {
		file += "LINE " + line.getStart().x + " " + line.getStart().y + " "
				+ line.getEnd().x + " " + line.getEnd().y + " "
				+ line.getColor().getRed() + " " + line.getColor().getGreen()
				+ " " + line.getColor().getBlue() + "\n";
	}

	@Override
	public void visit(Circle circle) {
		file += "CIRCLE " + circle.getCenter().x + " " + circle.getCenter().y + " "
				+ (int)circle.getRadius() + " "
				+ circle.getEdgeColor().getRed() + " " + circle.getEdgeColor().getGreen()
				+ " " + circle.getEdgeColor().getBlue() + "\n";
	}

	@Override
	public void visit(FilledCircle circle) {
		file += "FCIRCLE " + circle.getCenter().x + " " + circle.getCenter().y + " "
				+ (int)circle.getRadius() + " "
				+ circle.getEdgeColor().getRed() + " " + circle.getEdgeColor().getGreen()
				+ " " + circle.getEdgeColor().getBlue() + " "
				+ circle.getFillColor().getRed() + " " + circle.getFillColor().getGreen()
				+ " " + circle.getFillColor().getBlue() + "\n";
	}
	
	/**
	 * Returns file for saving.
	 * 
	 * @return file.
	 */
	public String getFile() {
		return file;
	}

}
