package hr.fer.zemris.java.hw17.jvdraw.JVDraw.color;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JColorChooser;
import javax.swing.JComponent;

/**
 * Color area as {@link JComponent}.
 * 
 * @author Mateo Puhalović
 *
 */
public class JColorArea extends JComponent implements IColorProvider {

	private static final long serialVersionUID = 1L;
	
	private Color selectedColor;
	private List<ColorChangeListener> observers = new ArrayList<ColorChangeListener>();	
	
	/**
	 * Creates new {@link JColorArea}.
	 * 
	 * @param selectedColor color
	 */
	public JColorArea(Color selectedColor) {
		this.selectedColor = selectedColor;
		setBackground(selectedColor);
		setOpaque(true);
		setPreferredSize(new Dimension(15, 15));
		
		addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				Color background = JColorChooser.showDialog(null, "Change Button Background",
			            selectedColor);
		        if (background != null) {
		        	setColor(background);
		        }
			}			
		});		
		
		repaint();
	}
	
	/**
	 * Sets color to {@link JColorArea}.
	 * 
	 * @param newColor new color
	 */
	public void setColor(Color newColor) {
		Color oldColor = selectedColor;
		this.selectedColor = newColor;
		setBackground(selectedColor);
		repaint();
		
		for (ColorChangeListener observer : observers) {
			observer.newColorSelected(this, oldColor, selectedColor);
		}	
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(15, 15);
	}
	
	@Override
	public Color getCurrentColor() {
		return selectedColor;
	}

	@Override
	public void addColorChangeListener(ColorChangeListener l) {
		observers.add(l);		
	}

	@Override
	public void removeColorChangeListener(ColorChangeListener l) {
		observers.remove(l);		
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		g.setColor(selectedColor);
		g.fillRect(0, 0, getWidth(), getHeight());
	}

}