package hr.fer.zemris.java.hw17.jvdraw.JVDraw.tools;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.GeometricalObject;

/**
 * Creates {@link GeometricalObject}.
 * 
 * @author Mateo Puhalović
 *
 */
public interface Tool {

	/**
	 * Method called when mouse is pressed.
	 * 
	 * @param e event
	 */
	public void mousePressed(MouseEvent e);
	
	/**
	 * Method called when mouse is released.
	 * 
	 * @param e event
	 */
	public void mouseReleased(MouseEvent e);
	
	/**
	 * Method called when mouse is clicked.
	 * 
	 * @param e event
	 */
	public void mouseClicked(MouseEvent e);
	
	/**
	 * Method called when mouse is moved.
	 * 
	 * @param e event
	 */
	public void mouseMoved(MouseEvent e);
	
	/**
	 * Method called when mouse is dragged.
	 * 
	 * @param e event
	 */
	public void mouseDragged(MouseEvent e);
	
	/**
	 * Paints tool.
	 * 
	 * @param g2d graphics
	 */
	public void paint(Graphics2D g2d);
	 
}
