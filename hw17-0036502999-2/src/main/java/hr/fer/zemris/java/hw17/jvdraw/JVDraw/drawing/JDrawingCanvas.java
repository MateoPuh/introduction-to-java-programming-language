package hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.function.Supplier;

import javax.swing.JComponent;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.tools.Tool;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors.GeometricalObjectPainter;

/**
 * Canvas on which {@link GeometricalObject}s are drawn.
 * 
 * @author Mateo Puhalović
 *
 */
public class JDrawingCanvas extends JComponent implements DrawingModelListener {

	private static final long serialVersionUID = 1L;
	private DrawingModel model;
	private Supplier<Tool> supplier;
	
	/**
	 * Creates new {@link JDrawingCanvas}.
	 * 
	 * @param supplier supplier of tool
	 * @param model drawing model
	 */
	public JDrawingCanvas(Supplier<Tool> supplier, DrawingModel model) {
		this.supplier = supplier;
		this.model = model;
		
		model.addDrawingModelListener(this);
	}

	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		repaint();
	}

	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		repaint();
	}

	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		repaint();
	}
	
	@Override
    protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(Color.WHITE);
	    g2d.fillRect(0, 0, getWidth(), getHeight());
		
        GeometricalObjectPainter painter = new GeometricalObjectPainter(g2d);
        for (int i = 0; i < model.getSize(); i++) {
            model.getObject(i).accept(painter);
        }
        	    
	    if(supplier.get() != null) {
	    	supplier.get().paint(g2d);
	    }
	}

}
