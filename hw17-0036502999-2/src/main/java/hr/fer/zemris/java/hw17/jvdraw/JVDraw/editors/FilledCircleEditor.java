package hr.fer.zemris.java.hw17.jvdraw.JVDraw.editors;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;

import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.FilledCircle;

/**
 * {@link GeometricalObjectEditor} for {@link FilledCircle}.
 * 
 * @author Mateo Puhalović
 *
 */
public class FilledCircleEditor extends GeometricalObjectEditor {

	private static final long serialVersionUID = 1L;

	private FilledCircle circle;
	
	private JTextField centerX;
	private JTextField centerY;
	private JTextField radius;
	private JColorChooser color;
	private JColorChooser fillColor;
	
	private FilledCircle newCircle;
	
	/**
	 * Creates new {@link FilledCircleEditor}.
	 * 
	 * @param circle filled circle
	 */
	public FilledCircleEditor(FilledCircle circle) {
		this.circle = circle;
		initGUI();
	}

	/**
	 * Initializes GUI for filled circle editor.
	 */
	private void initGUI() {
		initValues();
		
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
        
        constraints.gridx = 0;
        constraints.gridy = 0;     
        add(new JLabel("Center point:"), constraints);
        
        constraints.gridx = 1;
        add(centerX, constraints);
        constraints.gridx = 2;
        add(centerY, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 2;     
        add(new JLabel("Radius:"), constraints);
        
        constraints.gridx = 1;
        add(radius, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 3;     
        add(new JLabel("Edge color:"), constraints);
        
        constraints.gridx = 1;
        add(color, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 4; 
        add(new JLabel("Fill color:"), constraints);
        
        constraints.gridx = 1;
        add(fillColor, constraints);
	}

	/**
	 * Initializes values on GUI
	 */
	private void initValues() {
		centerX = new JTextField(String.valueOf((int)circle.getCenter().getX()));
		centerY = new JTextField(String.valueOf((int)circle.getCenter().getY()));
		radius = new JTextField(String.valueOf((int)circle.getRadius()));
		color = new JColorChooser(circle.getEdgeColor());
		fillColor = new JColorChooser(circle.getFillColor());
	}

	@Override
	public void checkEditing() {
		newCircle = new FilledCircle(
				new Point(
						Integer.parseInt(centerX.getText()),
						Integer.parseInt(centerY.getText())),
				Integer.parseInt(radius.getText()),
				color.getColor(),
				fillColor.getColor());
	}

	@Override
	public void acceptEditing() {
		circle.setCenter(newCircle.getCenter());
		circle.setRadius(newCircle.getRadius());
		circle.setEdgeColor(newCircle.getEdgeColor());	
		circle.setFillColor(newCircle.getFillColor());	
	}

}
