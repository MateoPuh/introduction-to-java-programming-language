package hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects;

import java.awt.Color;
import java.awt.Point;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.editors.CircleEditor;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.visitors.GeometricalObjectVisitor;

/**
 * {@link GeometricalObject} of circle.
 * 
 * @author Mateo Puhalović
 *
 */
public class Circle extends GeometricalObject {

	private Point center;
	private double radius;
	private Color edgeColor;
	
	/**
	 * Creates new {@link Circle}.
	 * 
	 * @param center center point
	 * @param radius radius
	 * @param edgeColor color of edge
	 */
	public Circle(Point center, double radius, Color edgeColor) {
		super();
		this.center = center;
		this.radius = radius;
		this.edgeColor = edgeColor;
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new CircleEditor(this);
	}

	/**
	 * Returns center point.
	 * 
	 * @return center
	 */
	public Point getCenter() {
		return center;
	}

	/**
	 * Returns radius.
	 * 
	 * @return radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * Returns edge color.
	 * 
	 * @return edge color
	 */
	public Color getEdgeColor() {
		return edgeColor;
	}

	/**
	 * Sets center point.
	 * 
	 * @param center center point
	 */
	public void setCenter(Point center) {
		this.center = center;
	}

	/**
	 * Sets radius.
	 * 
	 * @param radius radius
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/**
	 * Sets edge color.
	 * 
	 * @param edgeColor edge color
	 */
	public void setEdgeColor(Color edgeColor) {
		this.edgeColor = edgeColor;
	}
	
	@Override
	public String toString() {
		return "Circle (" + center.x + "," + center.y  
				+ "), " + (int)radius;
	}
	
}
