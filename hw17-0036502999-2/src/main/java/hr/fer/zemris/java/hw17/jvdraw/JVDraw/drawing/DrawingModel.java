package hr.fer.zemris.java.hw17.jvdraw.JVDraw.drawing;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.JVDraw.objects.GeometricalObjectListener;

/**
 * Model for drawing that contains object in scene.
 * 
 * @author Mateo Puhalović
 *
 */
public interface DrawingModel extends GeometricalObjectListener {
	 
	/**
	 * Returns size of scene.
	 * 
	 * @return number of objects
	 */
	public int getSize();
	
	/**
	 * Returns object on given index.
	 * 
	 * @param index index
	 * @return object on index
	 */
	public GeometricalObject getObject(int index);
	
	/**
	 * Adds given object.
	 * 
	 * @param object object
	 */
	public void add(GeometricalObject object);
	
	/**
	 * Removes given object.
	 * 
	 * @param object object
	 */
	public void remove(GeometricalObject object);
	
	/**
	 * Changes order of objects.
	 * 
	 * @param object object
	 * @param offset offset
	 */
	public void changeOrder(GeometricalObject object, int offset);
	
	/**
	 * Returns index of object.
	 * 
	 * @param object object
	 * @return index of object
	 */
	public int indexOf(GeometricalObject object);
	
	/**
	 * Clears the model.
	 */
	public void clear();
	
	/**
	 * Clears modified flag.
	 */
	public void clearModifiedFlag();
	
	/**
	 * Returns true if modified.
	 * 
	 * @return true if modified
	 */
	public boolean isModified();
	
	/**
	 * Adds drawing listener to model.
	 * 
	 * @param l listener
	 */
	public void addDrawingModelListener(DrawingModelListener l);
	
	/**
	 * Removes given listener from model.
	 * 
	 * @param l listener
	 */
	public void removeDrawingModelListener(DrawingModelListener l);

}
