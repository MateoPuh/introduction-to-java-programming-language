package hr.fer.zemris.java.hw17.trazilica;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class that contains useful methods.
 * 
 * @author Mateo Puhalović
 *
 */
public class SearchUtils {
	
	/**
	 * Returns unique words from document.
	 * 
	 * @param lines list of lines
	 * @param stopWords set of stop words
	 * @return unique words
	 */
	public static List<String> getUniqueWords(List<String> lines, Set<String> stopWords) {
		List<String> words = getWords(lines, stopWords);
		
		List<String> unique = new ArrayList<String>();
		for(String w : words) {
			if(!unique.contains(w)) {
				unique.add(w);
			}
		}
		
		return unique;
	}
	
	/**
	 * Returns unique words from document.
	 * 
	 * @param lines list of lines
	 * @return unique words
	 */
	public static List<String> getUniqueWords(List<String> lines) {
		return getUniqueWords(lines, new HashSet<String>());
	}
	
	/**
	 * Returns words from document.
	 * 
	 * @param lines list of lines
	 * @return list of words
	 */
	public static List<String> getWords(List<String> lines) {
		return getWords(lines, new HashSet<String>());
	}
	
	/**
	 * Returns words from document.
	 * 
	 * @param lines list of lines
	 * @param stopWords set of stop words
	 * @return list of words
	 */
	public static List<String> getWords(List<String> lines, Set<String> stopWords) {
		List<String> words = new ArrayList<String>();
		
		for(String line : lines) {
			addWords(line, words, stopWords);
		}
		
		return words;
	}
	
	/**
	 * Adds words from one line to list of words.
	 * 
	 * @param line line with words
	 * @param words list of words
	 * @param stopWords set of stop words
	 */
	public static void addWords(String line, List<String> words, Set<String> stopWords){
		line = line.trim();
		
		boolean word = false;
		Character c = null;
		int lastIndex = -1;
		
		for(int i = 0; i < line.trim().length(); i++) {
			c = line.charAt(i);
			if(!Character.isAlphabetic(c)) {
				if(word) {
					if(lastIndex < i - 1) {
						String w = line.substring(lastIndex + 1, i);
						if(!stopWords.contains(w)) {
							words.add(w);
						}
					}
					word = false;
					lastIndex = i;
				}
			}
			
			if(!word) {
				word = true;
			}
		}
		
		if(lastIndex < line.length()) {
			String w = line.substring(lastIndex + 1, line.length());
			if(!stopWords.contains(w)) {
				words.add(w);
			}
		}
	}

}
