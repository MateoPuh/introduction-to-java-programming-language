package hr.fer.zemris.java.hw17.trazilica;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * Represents console for interaction with user.
 * 
 * @author Mateo Puhalović
 *
 */
public class Console {
	
	/**
	 * Main method.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.err.println("Wrong number of arguments.\nExpected: 1"
					+ "\nGot: " + args.length);
			System.exit(1);
		}
		
		try(Scanner sc = new Scanner(System.in);){
			Environment env = new Environment(args[0]);
	        System.out.println("Veličina riječnika je " + env.getVocabularySize() + " riječi.");
			
			List<Document> res=null;		
	        			
			while(true) {
				System.out.print("Enter command>> ");
				String l = sc.nextLine().trim();
				
				if(l.equals("exit")) {
					break;
				}
				
				String[] parts = l.split("\\s+");
				String command = parts[0];
				
				switch(command) {
				case "query":
					String words = l.substring(l.indexOf(' '));

			        System.out.println("Query is: " + env.getQuery(words.split("\\s+")));
					res = env.getResults(words.split("\\s+"));
					printResults(res);

					break;
				case "type":
					if(res==null) {
						System.out.println("Command results can only be run after query command.");
						break;
					}
					
					int docNum = Integer.parseInt(l.substring(l.indexOf(' ')).trim());
					
					Document d = res.get(docNum);
					
					for(String s : Files.readAllLines(Paths.get(d.getFilepath()))) {
						System.out.println(s);
					}
					
					break;
				case "results":
					if(res==null) {
						System.out.println("Command results can only be run after query command.");
						break;
					}
					printResults(res);
					
					break;
				default:
					System.out.println("Unknown command: " + command + ".");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Prints given results.
	 * 
	 * @param res list of results
	 */
	private static void printResults(List<Document> res) {
		for(int i = 0; i < res.size(); i++) {
			Document d = res.get(i);
			
		    System.out.printf("[%2d] (%.4f) %s\r\n", i, d.getSimilarity(), res.get(i).getFilepath());
		}
	}

}
