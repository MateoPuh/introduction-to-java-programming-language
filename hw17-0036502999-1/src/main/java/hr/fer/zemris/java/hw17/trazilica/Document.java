package hr.fer.zemris.java.hw17.trazilica;

/**
 * Represents document with path and vector.
 * 
 * @author Mateo Puhalović
 *
 */
public class Document {
	
	private String filepath;
	private TFIDFVector vector;
	private double similarity;
	
	/**
	 * Creates new {@link Document}
	 * 
	 * @param filepath path
	 * @param vector vector
	 */
	public Document(String filepath, TFIDFVector vector) {
		this.filepath = filepath;
		this.vector = vector;
	}

	/**
	 * Returns filepath.
	 * 
	 * @return filepath
	 */
	public String getFilepath() {
		return filepath;
	}
	
	/**
	 * Returns vector.
	 * 
	 * @return vector
	 */
	public TFIDFVector getVector() {
		return vector;
	}

	/**
	 * Returns similarity.
	 * 
	 * @return similarity
	 */
	public double getSimilarity() {
		return similarity;
	}

	/**
	 * Sets similarity
	 * 
	 * @param similarity similarity
	 */
	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}

}
