package hr.fer.zemris.java.hw17.trazilica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents TDF-IF vector.
 *  
 * @author Mateo Puhalović
 *
 */
public class TFIDFVector {
	
	private List<Double> values = new ArrayList<Double>();
	
	/**
	 * Calculates values of this vector.
	 * 
	 * @param document list of lines
	 * @param vocabulary vocabulary
	 * @param IDFVector vector with calculated IDF values
	 */
	public void calculateValues(List<String> document, List<String> vocabulary, TFIDFVector IDFVector) {
		List<String> words = SearchUtils.getWords(document);
		
		Map<String, Integer> wordCount = new HashMap<String, Integer>();
		for(String w : words) {
			if(!wordCount.containsKey(w.toLowerCase())) {
				wordCount.put(w.toLowerCase(), 1);
			} else {
				wordCount.put(w, wordCount.get(w.toLowerCase()) + 1);
			}
		}
		
		values = new ArrayList<Double>();
		
		for(int i = 0; i < vocabulary.size(); i++) {
			String w = vocabulary.get(i);
			
			if(wordCount.get(w.toLowerCase()) == null) {
				values.add(0.0);
				continue;
			}
			
			values.add(wordCount.get(w.toLowerCase()) * IDFVector.valueAt(i));
		}
	}
	
	/**
	 * Calculates IDF values for this vector.
	 * 
	 * @param vocabulary vocabulary
	 * @param wordDocumentCount map of words and number of documents they appeared in
	 * @param documentNum number of documents
	 */
	public void calculateIDFValues(List<String> vocabulary, Map<String, Integer> wordDocumentCount, int documentNum) {
		for(String word : vocabulary) {
			values.add(Math.log(documentNum / (double)wordDocumentCount.get(word)));
		}
	}
	
	/**
	 * Returns value at given index.
	 * 
	 * @param index index of object
	 * @return value
	 */
	public double valueAt(int index) {
		return values.get(index);
	}
	
	/**
	 * Returns norm of a vector.
	 * 
	 * @return norm
	 */
	public double norm() {
		double norm = 0;
		
		for(Double d : values) {
			norm += d * d;
		}
		
		return Math.sqrt(norm);
	}
	
	/**
	 * Returns scalar product of this vector and given one.
	 * 
	 * @param vector other vector
	 * @return scalar product
	 */
	public double multiply(TFIDFVector vector) {
		double res = 0;
		
		for(int i = 0; i < values.size(); i++) {
			res += values.get(i) * vector.valueAt(i);
		}

		return res;
	}
	
	/**
	 * Returns similarity between this vector and given one.
	 * 
	 * @param vector other vector
	 * @return similarity
	 */
	public double getSimilarity(TFIDFVector vector) {
		return this.multiply(vector) / (norm() * vector.norm());
	}
	
}
