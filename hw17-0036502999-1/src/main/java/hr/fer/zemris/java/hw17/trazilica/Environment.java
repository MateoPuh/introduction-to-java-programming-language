package hr.fer.zemris.java.hw17.trazilica;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Represents environment of a console.
 * 
 * @author Mateo Puhalović
 *
 */
public class Environment {
	
	private static final String STOP_WORDS_PATH = "stoprijeci.txt";
	
	private String filepath;
	private List<String> vocabulary = new ArrayList<String>();
	private Set<String> stopWords = new HashSet<String>();
	private TFIDFVector IDFVector;
	private List<Document> documents = new ArrayList<Document>();
	
	/**
	 * Creates new {@link Environment}.
	 * 
	 * @param filepath path with documents
	 * @throws URISyntaxException if error occurs
	 * @throws IOException if error occurs
	 */
	public Environment(String filepath) throws URISyntaxException, IOException {
		this.filepath = filepath;
		initStopWords();
		initVocabularyAndIDF();
		initDocuments();
	}
	
	/**
	 * Initializes vector for each document.
	 * 
	 * @throws IOException if error occurs
	 */
	private void initDocuments() throws IOException {
		Files.list(Paths.get(filepath)).forEach(path -> {
			TFIDFVector v = new TFIDFVector();
			try {
				v.calculateValues(Files.readAllLines(path), vocabulary, IDFVector);
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			documents.add(new Document(path.toString(), v));
		});

	}

	/**
	 * Initializes Set of stop words.
	 * 
	 * @throws URISyntaxException if error occurs
	 * @throws IOException if error occurs
	 */
	private void initStopWords() throws URISyntaxException, IOException {
		Path stopWordsPath = Paths.get(
				Thread.currentThread()
				.getContextClassLoader()
				.getResource(STOP_WORDS_PATH)
				.toURI());
		
		for(String stopWord : Files.readAllLines(stopWordsPath)) {
			stopWords.add(stopWord.trim().toLowerCase());
		}
	}
	
	/**
	 * Initializes vocabulary and IDF vector.
	 * 
	 * @throws IOException if error occurs
	 */
	private void initVocabularyAndIDF() throws IOException {
		Map<String, Integer> wordDocumentCount = new HashMap<String, Integer>();
		
		Files.list(Paths.get(filepath)).forEach(path -> {
            try {
            	List<String> words = SearchUtils.getUniqueWords(Files.readAllLines(path), stopWords);
    			for(String w : words) {
    				if(!vocabulary.contains(w.toLowerCase()) && !stopWords.contains(w.toLowerCase())) {
    					vocabulary.add(w.toLowerCase());
    				}
    				
    				if(!wordDocumentCount.containsKey(w.toLowerCase())) {
    					wordDocumentCount.put(w.toLowerCase(), 1);
    				} else {
    					wordDocumentCount.put(w.toLowerCase(), wordDocumentCount.get(w.toLowerCase()) + 1);
    				}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
        });
		
		initIDF(wordDocumentCount);
	}
	
	/**
	 * Initializes IDF vector.
	 * 
	 * @param wordDocumentCount map of words and number of documents they appear in
	 */
	private void initIDF(Map<String, Integer> wordDocumentCount) {
		IDFVector = new TFIDFVector();
		IDFVector.calculateIDFValues(vocabulary, wordDocumentCount, new File(filepath).list().length);
	}
	
	/**
	 * Returns maximum 10 best result with highest similarities.
	 * 
	 * @param words given words
	 * @return list of results
	 */
	public List<Document> getResults(String... words) {
		List<String> document = new ArrayList<String>();
		for(String w : words) {
			if(vocabulary.contains(w.trim())) {
				document.add(w.trim().toLowerCase());
			} 
		}
		
		TFIDFVector v = new TFIDFVector();
		v.calculateValues(document, vocabulary, IDFVector);
				
		List<Document> docs = new ArrayList<Document>();
		
		for(Document d : documents) {
			d.setSimilarity(v.getSimilarity(d.getVector()));
			if(d.getSimilarity() > 0) {
				docs.add(d);
			}
		}
		
		docs.sort((d1, d2) -> (Double.compare(d2.getSimilarity(), d1.getSimilarity())));
		
		List<Document> res = new ArrayList<Document>();
		
		for(int i = 0; i < 10; i++) {
			if(i >= docs.size()) {
				break;
			}
			
			res.add(docs.get(i));
		}
		
		return res;
	}
	
	/**
	 * Returns query from given words.
	 * 
	 * @param words given words
	 * @return query
	 */
	public Set<String> getQuery(String... words) {
		Set<String> query = new LinkedHashSet<String>();
		
		for(String w : words) {
			if(vocabulary.contains(w.trim().toLowerCase())) {
				query.add(w);
			}
		}
		
		return query;
	}
	
	/**
	 * Returns size of vocabulary.
	 * 
	 * @return size of vocabulary
	 */
	public int getVocabularySize() {
		return vocabulary.size();
	}

}
