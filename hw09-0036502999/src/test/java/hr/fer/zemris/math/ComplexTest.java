package hr.fer.zemris.math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ComplexTest {
	
	@Test
	public void moduleTest() {
		Complex c = new Complex(1, 0);
		
		assertEquals(1, c.module());
		
		Complex c2 = new Complex(1, 1);
		
		assertEquals(Math.sqrt(2), c2.module(), 1E-6);
		
		Complex c3 = new Complex(-1, 1);
		
		assertEquals(Math.sqrt(2), c3.module(), 1E-6);
	}
	
	@Test
	public void multiplyTest() {
		Complex c1 = new Complex(3, 2);		
		Complex c2 = new Complex(1, 4);
		
		assertTrue(c1.multiply(c2).equals(new Complex(-5, 14)));
		
		Complex c3 = new Complex(3, 2);		
		Complex c4 = new Complex(1, 7);
		
		assertTrue(c3.multiply(c4).equals(new Complex(-11, 23)));
	}
	
	@Test
	public void divideTest() {
		Complex c1 = new Complex(3, 2);		
		Complex c2 = new Complex(4, -3);
				
		assertTrue(c1.divide(c2).equals(new Complex(6.0/25, 17.0/25)));
		
		Complex c3 = new Complex(4, 5);		
		Complex c4 = new Complex(2, 6);
		
		assertTrue(c3.divide(c4).equals(new Complex(19.0/20, -7.0/20)));
	}
	
	public void powerTest() {
		Complex c1 = new Complex(1, 1);		
		
		assertTrue(c1.power(8).equals(new Complex(16, 0)));
	}

}
