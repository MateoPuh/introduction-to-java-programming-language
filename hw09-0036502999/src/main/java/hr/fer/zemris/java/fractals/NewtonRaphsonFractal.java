package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * {@link IFractalProducer} that produces Newton Raphson fractal.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class NewtonRaphsonFractal implements IFractalProducer {

	/** Number of iterations. */
	private static final int ITERATION_NUMBER = 4096;
	/** Default root treshold. */
	private static final double ROOT_TRESHOLD = 0.0002;
	/** Default convergence treshold. */
	private static final double CONVERGENCE_TRESHOLD = 0.0001;
	
	private ComplexRootedPolynomial polynomial;
	private ExecutorService pool;
	private ComplexPolynomial derived;
	
	/**
	 * Represents job of calculating {@link NewtonRaphsonFractal}.
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 */
	private class NewtonRaphsonJob implements Callable<Void> {
		
		double reMin;
		double reMax;
		double imMin;
		double imMax;
		int width;
		int height;
		int yMin;
		int yMax;
		short[] data;
		
		/**
		 * Creates new {@link NewtonRaphsonJob} with given minimal real number,
		 * maximal real number, minimal imaginary number, maximal imaginary number,
		 * width, height, minimal y, maximal y and data array.
		 * 
		 * @param reMin minimal real number
		 * @param reMax maximal real number
		 * @param imMin minimal imaginary number
		 * @param imMax maximal imaginary number
		 * @param width width
		 * @param height height
		 * @param yMin minimal y
		 * @param yMax maximal y
		 * @param data data array
		 */
		public NewtonRaphsonJob(double reMin, double reMax, double imMin, double imMax, 
				int width, int height, int yMin, int yMax, short[] data) {
			super();
			this.reMin = reMin;
			this.reMax = reMax;
			this.imMin = imMin;
			this.imMax = imMax;
			this.width = width;
			this.height = height;
			this.yMin = yMin;
			this.yMax = yMax;
			this.data = data;
		}
	
		@Override
		public Void call() throws Exception {
			for(int y = yMin; y <= yMax; y++) {
				for(int x = 0; x < width; x++) {
					Complex c = mapToComplexPlain(x, y);
					Complex zn = c;
					int iter = 0;
					derived = polynomial.toComplexPolynom().derive();
					
					while(iter < ITERATION_NUMBER) {
						Complex numerator = polynomial.apply(zn);
						Complex denominator = derived.apply(zn);
						Complex znold = zn;
						try {
							Complex fraction = numerator.divide(denominator);
							zn = zn.sub(fraction);
							double module = znold.sub(zn).module();
														
							if(module < CONVERGENCE_TRESHOLD) {
								break;
							}
						} catch(ArithmeticException e) {
							break;
						}
						
						iter++;
					}
					int index = polynomial.indexOfClosestRootFor(zn, ROOT_TRESHOLD);
														
					data[y*width + x] = (short)(index != -1 ? index + 1 : index + 2);
				}
			}
			
			return null;
		}
		
		/**
		 * Maps coordinates to complex plain.
		 * 
		 * @param x x coordinate
		 * @param y y coordinate
		 * @return complex number
		 */
		private Complex mapToComplexPlain(int x, int y) {
			double re = ((double) x / width) * (reMax - reMin) + reMin;
			double im = ((double) (height - 1 - y) / height) * (imMax - imMin) + imMin;
			return new Complex(re, im);
		}
		
	}
	
	/**
	 * Creates new {@link NewtonRaphsonFractal} with given polynomial.
	 * 
	 * @param polynomial polynomial
	 */
	public NewtonRaphsonFractal(ComplexRootedPolynomial polynomial) {
		pool = Executors.newFixedThreadPool(
				Runtime.getRuntime().availableProcessors(), 
				new DaemonicThreadFactory()
		);	
		this.polynomial = polynomial;
	}
		
	@Override
	public void produce(double reMin, double reMax, double imMin, double imMax,
			int width, int height, long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {		
		int numOfJobs = 8 * Runtime.getRuntime().availableProcessors();
		short[] data = new short[width * height];
		int jobsInGroup = height / numOfJobs;
				
		List<Future<Void>> results = new ArrayList<>();
		
		for(int i = 0; i < numOfJobs; i++) {
			int yMin = i * jobsInGroup;
			int yMax = (i==numOfJobs-1) ? height - 1 : (i+1) * jobsInGroup - 1;
						
			NewtonRaphsonJob job = 
					new NewtonRaphsonJob(reMin, reMax, imMin, imMax, width, height, yMin, yMax, data);
			results.add(pool.submit(job));
		}
		
		for(Future<Void> job : results) {
			try {
				job.get();
			} catch (InterruptedException | ExecutionException e) {
			}
		}
				
		observer.acceptResult(data, 
				(short)(polynomial.toComplexPolynom().order()+1), requestNo);
	}

}
