package hr.fer.zemris.java.raytracer;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.IRayTracerAnimator;
import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * Represents ray casting program which creates scene
 * and spheres in it and performs ray casting with multiple
 * threads.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class RayCasterParallel2 {

	/** Number of rows. */
	private static final int NUM_OF_ROWS = 16;
	
	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(), getIRayTracerAnimator(), 30, 30);

	}
	
	private static IRayTracerAnimator getIRayTracerAnimator() {
		return new IRayTracerAnimator() {
			long time;
		
			@Override
			public void update(long deltaTime) {
				time += deltaTime;
			}
	
			@Override
			public Point3D getViewUp() { // fixed in time
				return new Point3D(0,0,10);
			}
	
			@Override
			public Point3D getView() { // fixed in time
				return new Point3D(-2,0,-0.5);
			}

			@Override
			public long getTargetTimeFrameDuration() {
				return 150; // redraw scene each 150 milliseconds
			}
		
			@Override
			public Point3D getEye() { // changes in time
				double t = (double)time / 10000 * 2 * Math.PI;
				double t2 = (double)time / 5000 * 2 * Math.PI;
				double x = 50*Math.cos(t);
				double y = 50*Math.sin(t);
				double z = 30*Math.sin(t2);
				return new Point3D(x,y,z);
			}
		};
	}
	
	/**
	 * Creates {@link IRayTracerProducer} which perform ray casting with multiple threads.
	 * 
	 * @return {@link IRayTracerProducer}
	 */
	private static IRayTracerProducer getIRayTracerProducer() {
		return new IRayTracerProducer() {
			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical, int width,
					int height, long requestNo, IRayTracerResultObserver observer, AtomicBoolean cancel) {
				System.out.println("Započinjem izračune...");
				short[] red = new short[width*height];
				short[] green = new short[width*height];
				short[] blue = new short[width*height];

				Point3D zAxis = view.sub(eye).normalize();
				Point3D yAxis = viewUp.sub(zAxis.scalarMultiply(zAxis.scalarProduct(viewUp))).normalize();
				Point3D xAxis = zAxis.vectorProduct(yAxis).normalize();
				
				Point3D screenCorner = view.sub(xAxis.scalarMultiply(horizontal / 2)).add(yAxis.scalarMultiply(vertical / 2));

				Scene scene = RayTracerViewer.createPredefinedScene();

				ForkJoinPool pool = new ForkJoinPool();
				pool.invoke(new RayCastingJob(eye, xAxis, yAxis, screenCorner, red, green, blue, scene, 0, height, width, height,
						vertical, horizontal));
				pool.shutdown();

				observer.acceptResult(red, green, blue, requestNo);
			}

		};		
	}

	/**
	 * Jobs that one thread is doing in ForkJoinPool in {@link RayCasterParallel2}.
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 *
	 */
	private static class RayCastingJob extends RecursiveAction {
				
		private static final long serialVersionUID = 1L;
		private Point3D eye;
		private Point3D xAxis;
		private Point3D yAxis;
		private Point3D screenCorner;
		private short[] red;
		private short[] green;
		private short[] blue;
		private Scene scene;
		private int yMin;
		private int yMax;
		private int width;
		private int height;
		private double vertical;
		private double horizontal;

		/**
		 * Creates new {@link RayCastingJob} with given eye, xAxis, yAxis, screenCorner,
		 * red, green, blue, scene, yMin, yMax, width, height, vertical and horizontal.
		 * 
		 * @param eye eye point
		 * @param xAxis x axis
		 * @param yAxis y axis
		 * @param screenCorner screen corner point
		 * @param red red array
		 * @param green green array
		 * @param blue blue array
		 * @param scene scene
		 * @param yMin y minimum
		 * @param yMax y maximal
		 * @param width width
		 * @param height height
		 * @param vertical vertical
		 * @param horizontal horizontal
		 */
		public RayCastingJob(Point3D eye, Point3D xAxis, Point3D yAxis, Point3D screenCorner, short[] red, short[] green,
				short[] blue, Scene scene, int yMin, int yMax, int width, int height, double vertical,
				double horizontal) {
			this.eye = eye;
			this.xAxis = xAxis;
			this.yAxis = yAxis;
			this.screenCorner = screenCorner;
			this.red = red;
			this.green = green;
			this.blue = blue;
			this.scene = scene;
			this.yMin = yMin;
			this.yMax = yMax;
			this.width = width;
			this.height = height;
			this.vertical = vertical;
			this.horizontal = horizontal;
		}

		@Override
		protected void compute() {
			if (height / (yMax - yMin + 1) >= NUM_OF_ROWS) {
				short[] rgb = new short[3];
				int offset = yMin * width;
				for (int y = yMin; y < yMax; y++) {
					Point3D corner = yAxis.scalarMultiply(-((double) y / (height - 1)) * vertical)
							.add(screenCorner);

					for (int x = 0; x < width; x++) {
						Point3D screenPoint = corner
								.add(xAxis.scalarMultiply(((double) x / (width - 1)) * horizontal));
						Ray ray = Ray.fromPoints(eye, screenPoint);

						tracer(scene, ray, rgb);

						red[offset] = rgb[0] > 255 ? 255 : rgb[0];
						green[offset] = rgb[1] > 255 ? 255 : rgb[1];
						blue[offset] = rgb[2] > 255 ? 255 : rgb[2];
						offset++;
					}
				}
				return;
			}
			invokeAll(
					new RayCastingJob(eye, xAxis, yAxis, screenCorner, red, green, blue, scene, yMin,
							yMin + (yMax - yMin) / 2 + 1, width, height, vertical, horizontal),
					new RayCastingJob(eye, xAxis, yAxis, screenCorner, red, green, blue, scene,
							yMin + (yMax - yMin) / 2 + 1, yMax, width, height, vertical, horizontal));
		}
		
	}
	
	/**
	 * Performs ray tracing for one pixel.
	 * 
	 * @param scene scene
	 * @param ray ray
	 * @param rgb rgb array
	 */
	protected static void tracer(Scene scene, Ray ray, short[] rgb) {
		rgb[0] = 0;
		rgb[1] = 0;
		rgb[2] = 0;
		
		RayIntersection closest = findClosestIntersection(scene, ray);
		if(closest==null) {
		return;
		}
		
		determineColorFor(closest, scene, ray, rgb);
	}
	
	/**
	 * Determines the color for scene with given intersection, ray and rgb array.
	 * 
	 * @param closest intersection
	 * @param scene scene
	 * @param ray ray
	 * @param rgb rgb array
	 */
	private static void determineColorFor(RayIntersection closest, Scene scene, Ray ray, short[] rgb) {
		rgb[0] = 15;
		rgb[1] = 15;
		rgb[2] = 15;

		for (LightSource lightSource : scene.getLights()) {
			Ray lightSourceRay = Ray.fromPoints(lightSource.getPoint(), closest.getPoint());
			RayIntersection closestToLightSource = findClosestIntersection(scene, lightSourceRay);

			if(closestToLightSource == null) {
				continue;
			}
			
			if (Math.abs(closestToLightSource.getDistance() 
					- closest.getPoint().sub(lightSource.getPoint()).norm()) > 1E-6) {
				continue;
			}

			Point3D normal = closest.getNormal();
			Point3D ls = lightSourceRay.direction;
			Point3D reflectedVector = normal.scalarMultiply(normal.scalarProduct(ls) * 2).modifySub(ls);
			Point3D vectorEye = ray.direction;
			if (Math.abs(closest.getPoint().sub(lightSource.getPoint()).norm()) < 1E-6) {
				return;
			}

			double diffuse = Math.abs(ls.scalarProduct(normal));
			double reflective = Math.abs(Math.pow(reflectedVector.scalarProduct(vectorEye), closest.getKrn()));

			rgb[0] += lightSource.getR() * (closest.getKdr() * diffuse + closest.getKrr() * reflective);
			rgb[1] += lightSource.getG() * (closest.getKdg() * diffuse + closest.getKrg() * reflective);
			rgb[2] += lightSource.getB() * (closest.getKdb() * diffuse + closest.getKrb() * reflective);
		}
	}

	/**
	 * Finds closest intersection of all objects in scene with given ray.
	 * 
	 * @param scene scene
	 * @param ray ray 
	 * @return intersection
	 */
	private static RayIntersection findClosestIntersection(Scene scene, Ray ray) {
		List<GraphicalObject> objects = scene.getObjects();
		RayIntersection rayIntersection = null;
		
		for(GraphicalObject obj : objects) {
			RayIntersection intersection = obj.findClosestRayIntersection(ray);
			if(intersection == null) {
				continue;
			}
			
			if(rayIntersection == null) {
				rayIntersection = intersection;
				continue;
			}
			
			if(intersection.getDistance() < rayIntersection.getDistance()) {
				rayIntersection = intersection;
			}
		}
		
		return rayIntersection;		
	}
	
}
