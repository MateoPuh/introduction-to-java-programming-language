package hr.fer.zemris.java.raytracer.model;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;

/**
 * {@link GraphicalObject} of sphere. It is consisted of 
 * center point and radius. It contains method {@link #findClosestRayIntersection(Ray)}
 * that finds intersection with given {@link Ray}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class Sphere  extends GraphicalObject {
	
	private Point3D center;
	private double radius;
	private double kdr;
	private double kdg;
	private double kdb;
	private double krr;
	private double krg;
	private double krb;
	private double krn;
	
	/**
	 * Creates new {@link Sphere} with given center, radius, kdr, kdg, 
	 * kdb, krr, krg, krb, krn.
	 * 
	 * @param center center point
	 * @param radius radius of a sphere
	 * @param kdr kdr
	 * @param kdg kdg
	 * @param kdb kdb
	 * @param krr krr
	 * @param krg krg
	 * @param krb krb
	 * @param krn krn
	 */
	public Sphere(Point3D center, double radius, double kdr, double kdg, 
			double kdb, double krr, double krg, double krb, double krn) {
		this.center = center;
		this.radius = radius;
		this.kdr = kdr;
		this.kdg = kdg;
		this.kdb = kdb;
		this.krr = krr;
		this.krg = krg;
		this.krb = krb;
		this.krn = krn;
	}
	
	/**
	 * Finds closes intersections of this sphere with given {@link Ray}.
	 * If there are no intersections, null will be returned.
	 * 
	 * @return {@link RayIntersection} object
	 */
	public RayIntersection findClosestRayIntersection(Ray ray) {
		double a = ray.direction.norm();
		double b = 2 * ray.direction.scalarProduct(ray.start.sub(center));
		double c = ray.start.sub(center).scalarProduct(ray.start.sub(center)) - radius*radius; 		
		double determinant = b * b - 4 * a * c;
		
		if(determinant < 0) {
			return null;
		}
		
		double determinantRoot = Math.sqrt(determinant);
		
		double t1 = -b + determinantRoot;
		t1 /= 2;
		
		double t2 = -b - determinantRoot;
		t2 /= 2;
				
		double result;
		boolean outer;
		
		if (t1 > 0 && t2 > 0) {
			result = Math.min(t1, t2);
			outer = true;
		} else if (t1 > 0 || t2 > 0) {
			result = Math.max(t1, t2);
			outer = false;
		} else {
			return null;
		}

		Point3D directionVector = ray.direction.scalarMultiply(result);
		return new RayIntersection(ray.start.add(directionVector), directionVector.norm(), outer) {
			
			@Override
			public Point3D getNormal() {
				return getPoint().sub(center).normalize();
			}
			
			@Override
			public double getKrr() {
				return krr;
			}
			
			@Override
			public double getKrn() {
				return krn;
			}
			
			@Override
			public double getKrg() {
				return krg;
			}
			
			@Override
			public double getKrb() {
				return krb;
			}
			
			@Override
			public double getKdr() {
				return kdr;
			}
			
			@Override
			public double getKdg() {
				return kdg;
			}
			
			@Override
			public double getKdb() {
				return kdb;
			}
		};
	}

}
