package hr.fer.zemris.java.fractals;

import java.util.concurrent.ThreadFactory;

/**
 * {@link ThreadFactory} that produces threads which have daemon flag
 * set to true.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class DaemonicThreadFactory implements ThreadFactory {

	/**
	 * Creates thread with daemon flag set to true.
	 * 
	 * @return thread with daemon flag set to true
	 */
	@Override
	public Thread newThread(Runnable r) {
		Thread thread = new Thread(r);
		thread.setDaemon(true);
		return thread;
	}

}
