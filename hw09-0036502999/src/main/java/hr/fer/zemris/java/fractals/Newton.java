package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Program that takes polynomial roots as user's input and 
 * draws Newton Raphson fractal.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class Newton {
	
	/** Word that indicates end of input. */
	private static final String QUIT_WORD = "done";
	/** Plus character. */
	private static final char PLUS = '+';
	/** Minus character. */
	private static final char MINUS = '-';
	/** Character that indicates imaginary number. */
	private static final String IMAGINARY_CHAR = "i";
	
	/**
	 * This method is first run after program is started.
	 * It takes roots as users input and draws Newton Raphson fractal.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.\r\n" + 
				"Please enter at least two roots, one root per line. Enter 'done' when done.");
		
		List<Complex> roots = new ArrayList<Complex>();
		
		Scanner sc = new Scanner(System.in);
		int i = 1;
		while(true) {
			System.out.print("Root " + i + "> ");
			String line = sc.nextLine().strip(); 
						
			if(line.equals(QUIT_WORD)) {
				if(i >= 3) {
					System.out.println("Image of fractal will appear shortly. Thank you.\r\n");
					break;
				} else {
					System.out.println("Please enter at least two roots.");
					continue;
				}
			}
			
			try {
				Complex root = parseComplex(line);
				roots.add(root);
				i++;
			} catch (Exception e) {
				System.out.println(e.getLocalizedMessage());
				System.out.println("Please try again.");
			}
		}
		sc.close();

		ComplexRootedPolynomial polynomial = 
				new ComplexRootedPolynomial(Complex.ONE, roots.toArray(new Complex[roots.size()]));
		
		NewtonRaphsonFractal fractal = new NewtonRaphsonFractal(polynomial);
		FractalViewer.show(fractal);
	}
	
	/**
	 * Parses complex number from given string.
	 * 
	 * @param line string which is parsed
	 * @return complex number
	 * @throws IllegalArgumentException if error occurs
	 */
	private static Complex parseComplex(String line) {
		String root = line.strip();
		boolean negativeIm = false;
		int separatorIndex = findSeparatorIndex(root);
		
		if(separatorIndex != -1) {
			if(root.charAt(separatorIndex) == MINUS) {
				negativeIm = true;
			}
		}
				
		// if there is no + or - then treat it as one argument
		if(separatorIndex == -1 || separatorIndex == 0) {
			boolean imaginary = false;
			if(separatorIndex == -1 && root.startsWith(IMAGINARY_CHAR)) {
				imaginary = true;
				root = root.substring(1).strip();
				if(root.length() == 0) {
					return Complex.IM;
				}
			}
			if(separatorIndex == 0) {
				if(root.startsWith(MINUS + IMAGINARY_CHAR)) {
					imaginary = true;
					negativeIm = true;
				} else if (root.startsWith(PLUS + IMAGINARY_CHAR)){
					imaginary = true;
					negativeIm = false;
				}
				
				if(imaginary) {
					root = root.substring(2).strip();
					if(root.length() == 0) {
						if(negativeIm) {
							return Complex.IM_NEG;
						} else {
							return Complex.IM;
						}
					}
				} else {
					root = root.substring(1).strip();
				}
			}
						
			try {
				int arg = Integer.parseInt(root);
				
				if(imaginary) {
					return new Complex(0, negativeIm? -arg : arg);
				} else {
					return new Complex(arg, 0);
				}
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Invalid root format!");
			}
		}
		
		// if there is + or - treat it as two arguments
		try {
			int re = Integer.parseInt(root.substring(0, separatorIndex).strip());
						
			String second = root.substring(separatorIndex + 1, root.length()).strip();
			if(!second.startsWith(IMAGINARY_CHAR)) {
				throw new IllegalArgumentException("invalid root format: " + second 
						+ " should start with 'i'");
			}
			
			second = second.substring(1).strip();
			
			int im = second.length() != 0 ? Integer.parseInt(second) : 1;
			
			return new Complex(re, negativeIm? -im : im);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid root format!");
		}
	}
	
	/**
	 * Returns index of characters '+' or '-' in a given string. 
	 * If there are no such characters, -1 is returned.
	 * 
	 * @param line string which is parsed
	 * @return index of separator character
	 */
	private static int findSeparatorIndex(String line) {
		int separatorIndex = line.indexOf(PLUS);
		
		if(separatorIndex == 0) {
			return findSeparatorIndex(line.substring(1)) + 1;
		}
		
		if(separatorIndex == -1) {
			separatorIndex = line.indexOf(MINUS);
			if(separatorIndex == 0) {
				return findSeparatorIndex(line.substring(1)) + 1;
			}
		}
		
		return separatorIndex;
	}

}
