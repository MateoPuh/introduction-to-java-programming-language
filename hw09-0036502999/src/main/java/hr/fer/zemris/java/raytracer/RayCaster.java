package hr.fer.zemris.java.raytracer;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * Represents ray casting program which creates scene
 * and spheres in it and performs ray casting.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class RayCaster {

	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(),
				new Point3D(10,0,0),
				new Point3D(0,0,0),
				new Point3D(0,0,10),
				20, 20);
	}
	
	/**
	 * Creates {@link IRayTracerProducer} which perform ray casting.
	 * 
	 * @return {@link IRayTracerProducer}
	 */
	private static IRayTracerProducer getIRayTracerProducer() {
		return new IRayTracerProducer() {
			
			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp,
					double horizontal, double vertical, int width, int height,
					long requestNo, IRayTracerResultObserver observer, AtomicBoolean cancels) {
				System.out.println("Započinjem izračune...");
				short[] red = new short[width*height];
				short[] green = new short[width*height];
				short[] blue = new short[width*height];
				
				Point3D og = view.sub(eye).normalize();
				Point3D vuv = viewUp.normalize();
				
				//Point3D zAxis = og;
				Point3D yAxis = vuv.sub(og.scalarMultiply(og.scalarProduct(vuv))).normalize();
				Point3D xAxis = og.vectorProduct(vuv).normalize();
				
				Point3D screenCorner = view.sub(xAxis.scalarMultiply(horizontal/2))
						.add(yAxis.scalarMultiply(vertical/2));
				
				Scene scene = RayTracerViewer.createPredefinedScene();
		
				short[] rgb = new short[3];
				int offset = 0;
				for(int y = 0; y < height; y++) {
					for(int x = 0; x < width; x++) {
						Point3D screenPoint = screenCorner
								.add(xAxis.scalarMultiply(x * horizontal/(width-1)))
								.sub(yAxis.scalarMultiply(y * vertical/(height-1)));
						Ray ray = Ray.fromPoints(eye, screenPoint);
						
						tracer(scene, ray, rgb);
						
						red[offset] = rgb[0] > 255 ? 255 : rgb[0];
						green[offset] = rgb[1] > 255 ? 255 : rgb[1];
						blue[offset] = rgb[2] > 255 ? 255 : rgb[2];
		
						offset++;
					}
				}
				
				System.out.println("Izračuni gotovi...");
				
				observer.acceptResult(red, green, blue, requestNo);
				System.out.println("Dojava gotova...");
			}

		};		
	}

	/**
	 * Performs ray tracing for one pixel.
	 * 
	 * @param scene scene
	 * @param ray ray
	 * @param rgb rgb array
	 */
	protected static void tracer(Scene scene, Ray ray, short[] rgb) {
		rgb[0] = 0;
		rgb[1] = 0;
		rgb[2] = 0;
		
		RayIntersection closest = findClosestIntersection(scene, ray);
		if(closest==null) {
		return;
		}
		
		determineColorFor(closest, scene, ray, rgb);
	}
	
	/**
	 * Determines the color for scene with given intersection, ray and rgb array.
	 * 
	 * @param closest intersection
	 * @param scene scene
	 * @param ray ray
	 * @param rgb rgb array
	 */
	private static void determineColorFor(RayIntersection closest, Scene scene, Ray ray, short[] rgb) {
		rgb[0] = 15;
		rgb[1] = 15;
		rgb[2] = 15;

		for (LightSource lightSource : scene.getLights()) {
			Ray lightSourceRay = Ray.fromPoints(lightSource.getPoint(), closest.getPoint());
			RayIntersection closestToLightSource = findClosestIntersection(scene, lightSourceRay);

			if(closestToLightSource == null) {
				continue;
			}
			
			if (Math.abs(closestToLightSource.getDistance() 
					- closest.getPoint().sub(lightSource.getPoint()).norm()) > 1E-6) {
				continue;
			}

			Point3D normal = closest.getNormal();
			Point3D ls = lightSourceRay.direction;
			Point3D reflectedVector = normal.scalarMultiply(normal.scalarProduct(ls) * 2).modifySub(ls);
			Point3D vectorEye = ray.direction;
			if (Math.abs(closest.getPoint().sub(lightSource.getPoint()).norm()) < 1E-6) {
				return;
			}

			double diffuse = Math.abs(ls.scalarProduct(normal));
			double reflective = Math.abs(Math.pow(reflectedVector.scalarProduct(vectorEye), closest.getKrn()));

			rgb[0] += lightSource.getR() * (closest.getKdr() * diffuse + closest.getKrr() * reflective);
			rgb[1] += lightSource.getG() * (closest.getKdg() * diffuse + closest.getKrg() * reflective);
			rgb[2] += lightSource.getB() * (closest.getKdb() * diffuse + closest.getKrb() * reflective);
		}
	}

	/**
	 * Finds closest intersection of all objects in scene with given ray.
	 * 
	 * @param scene scene
	 * @param ray ray 
	 * @return intersection
	 */
	private static RayIntersection findClosestIntersection(Scene scene, Ray ray) {
		List<GraphicalObject> objects = scene.getObjects();
		RayIntersection rayIntersection = null;
		
		for(GraphicalObject obj : objects) {
			RayIntersection intersection = obj.findClosestRayIntersection(ray);
			if(intersection == null) {
				continue;
			}
			
			if(rayIntersection == null) {
				rayIntersection = intersection;
				continue;
			}
			
			if(intersection.getDistance() < rayIntersection.getDistance()) {
				rayIntersection = intersection;
			}
		}
		
		return rayIntersection;		
	}
	
}
