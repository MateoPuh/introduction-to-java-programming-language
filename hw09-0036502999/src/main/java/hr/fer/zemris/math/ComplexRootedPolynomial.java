package hr.fer.zemris.math;

/**
 * Represents polynomial with complex numbers. It represents
 * polynomial of n-th order f(zn) = z0*(z-z1)*...*(z-zn), where 
 * z1,...,zn represent it's roots and z0 constant.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ComplexRootedPolynomial {
	
	/** Constant of polynomial. */
	private Complex constant;
	/** Roots of polynomial. */
	private Complex[] roots;
	
	/**
	 * Creates new {@link ComplexRootedPolynomial} with given constant and roots.
	 * 
	 * @param constant constant
	 * @param roots roots
	 */
	public ComplexRootedPolynomial(Complex constant, Complex ... roots) {
		this.constant = constant;
		this.roots = roots.clone();
	}
	
	/**
	 * Computes polynomial value at given point z.
	 * 
	 * @param z point for which polynomial value will be calculated
	 * @return polynomial value
	 */
	public Complex apply(Complex z) {
		Complex result = new Complex();
		result = result.add(constant);
		
		for(Complex root : roots) {
			result = result.multiply(z.sub(root));
		}
		
		return result;
	}
	
	/**
	 * Converts this representation to {@link ComplexPolynomial} type.
	 * 
	 * @return this polynomial in {@link ComplexPolynomial} type
	 */
	public ComplexPolynomial toComplexPolynom() {
		ComplexPolynomial complexPolynomial = new ComplexPolynomial(constant);
		
		for(Complex root : roots) {
			complexPolynomial = complexPolynomial.multiply(new ComplexPolynomial(root.negate(), Complex.ONE));
		}
		
		return complexPolynomial;
	}
	
	/**
	 * Finds index of closest root for given complex number z that is within
	 * treshold; if there is no such root, returns -1. First root has index 0, 
	 * second index 1, etc.
	 * 
	 * @param z complex number
	 * @param treshold treshold 
	 * @return index of closest root for complex number
	 */
	public int indexOfClosestRootFor(Complex z, double treshold) {
		int index = -1;
		Double distance = null;
		
		for(int i = 1; i < roots.length; i++) {
			Complex root = z.sub(roots[i]);
			
			if(root.module() < treshold 
					&& (distance == null 
					||treshold - root.module() < distance)) {
				
				index = i;
				distance = treshold - root.module();
			}
		}
		
		return index;
	}
	
	/**
	 * Returns String representation of {@link ComplexRootedPolynomial} in format
	 * "z0*(z-z1)*...*(z-zn)"
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(constant.toString());
		
		for(Complex root : roots) {
			sb.append("*(z-" + root.toString() + ")");
		}
		
		return sb.toString();
	}

}
