package hr.fer.zemris.math.demo;

import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Represents usage of {@link ComplexPolynomial} and 
 * {@link ComplexRootedPolynomial} objects.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class PolynomialDemo {
	
	/**
	 * Represents usage of {@link ComplexPolynomial} and 
	 * {@link ComplexRootedPolynomial} objects.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		ComplexPolynomial p = new ComplexPolynomial(Complex.ZERO, Complex.ONE, Complex.ZERO);
		System.out.println(p.order());
		
		ComplexRootedPolynomial crp = new ComplexRootedPolynomial(
				new Complex(2,0), Complex.ONE, Complex.ONE_NEG, Complex.IM, Complex.IM_NEG
				);
				ComplexPolynomial cp = crp.toComplexPolynom();
				System.out.println(crp);
				System.out.println(cp);
				System.out.println(cp.derive());

	}

}
