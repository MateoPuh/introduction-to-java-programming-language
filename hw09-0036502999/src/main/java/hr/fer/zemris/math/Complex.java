package hr.fer.zemris.math;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents complex number with real and imaginary part which 
 * are in double format.
 * 
 * <p>Contains methods for adding {@link #add(Complex)}, subtracting
 * {@link #sub(Complex)}, multiplying {@link #multiply(Complex)}, dividing
 * {@link #divide(Complex)}, module {@link #module()}, negation {@link #negate()},
 * exponentiation {@link #power(int)} and root {@link #root(int)}.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Complex {
	
	/** Real part of complex number. */
	private double re;
	/** Imaginary part of complex number. */
	private double im;
	
	/** Minimal difference that indicates that two doubles are equal. */
	private static final double EPSILON = 1E-6;
	
	/** Complex number with values (re, im) = (0, 0). */
	public static final Complex ZERO = new Complex(0,0);
	/** Complex number with values (re, im) = (1, 0). */
	public static final Complex ONE = new Complex(1,0);
	/** Complex number with values (re, im) = (-1, 0). */
	public static final Complex ONE_NEG = new Complex(-1,0);
	/** Complex number with values (re, im) = (0, 1). */
	public static final Complex IM = new Complex(0,1);
	/** Complex number with values (re, im) = (0, -1). */
	public static final Complex IM_NEG = new Complex(0,-1);
	
	/**
	 * Creates new {@link Complex} with values (re, im) = (0, 0).
	 */
	public Complex() {
		this(0, 0);
	}
	
	/**
	 * Creates new {@link Complex} with given real and imaginary parts
	 * of complex number.
	 * 
	 * @param re real part
	 * @param im imaginary part
	 */
	public Complex(double re, double im) {
		this.re = re;
		this.im = im;
	}
		
	/**
	 * Returns module of complex number.
	 * 
	 * @return module of complex number
	 */
	public double module() {
		return Math.sqrt(re * re + im * im);
	}
	
	/**
	 * Returns new {@link Complex} which is result of multiplying
	 * this complex number with the given one.
	 * 
	 * @param c other complex number
	 * @return result of multiplication
	 * @throws NullPointerException if given complex number is null
	 */
	public Complex multiply(Complex c) {
		Objects.requireNonNull(c);
		return new Complex(re * c.re - im * c.im, re * c.im + im * c.re);
	}
	
	/**
	 * Returns new {@link Complex} which is result of dividing
	 * this complex number with the given one.
	 * 
	 * @param c other complex number
	 * @return result of division
	 * @throws NullPointerException if given complex number is null
	 * @throws ArithmeticException if division with 0 occurs
	 */
	public Complex divide(Complex c) throws ArithmeticException {
		Objects.requireNonNull(c);
		
		if (c.equals(ZERO)) {
			throw new ArithmeticException("Division by zero (0 + i0)!");
		}

		return new Complex(
				(re * c.re + im * c.im)/(c.re * c.re + c.im * c.im), 
				(im * c.re - re * c.im)/(c.re * c.re + c.im * c.im));
	}
	
	/**
	 * Returns new {@link Complex} which is result of adding
	 * given complex number to this one.
	 * 
	 * @param c other complex number
	 * @return result of adding
	 * @throws NullPointerException if given complex number is null
	 */
	public Complex add(Complex c) {
		Objects.requireNonNull(c);
		return new Complex(re + c.re, im + c.im);
	}

	/**
	 * Returns new {@link Complex} which is result of subtracting
	 * given complex number from this one.
	 * 
	 * @param c other complex number
	 * @return result of subtraction
	 * @throws NullPointerException if given complex number is null
	 */
	public Complex sub(Complex c) {
		Objects.requireNonNull(c);
		return new Complex(re - c.re, im - c.im);
	}
	
	/**
	 * Returns new {@link Complex} which is result of negating
	 * this complex number.
	 * 
	 * @return result of negation
	 */
	public Complex negate() {
		return new Complex(-re, -im);
	}
	
	/**
	 * Returns new {@link Complex} which is result of exponentiation
	 * of this complex number to the power of given n. So, the result
	 * is this^n.
	 * 
	 * @param n power of exponentiation
	 * @return result of exponentiation
	 * @throws IllegalArgumentException if given n is negative
	 */
	public Complex power(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("Power of exponent should be >= 0!");
		}
		
		return fromMagnitudeAndAngle(Math.pow(module(),  n), n * getAngle());
	}
	
	/**
	 * Returns list of {@link Complex} objects which are n-th root of
	 * this complex number. 
	 * 
	 * @param n degree of root
	 * @return list of n-th roots
	 * @throws IllegalArgumentException if given n is negative or 0
	 */
	public List<Complex> root(int n) {
		if (n <= 0) {
			throw new IllegalArgumentException("Degree of root should be > 0!");
		}
		
		List<Complex> roots = new ArrayList<Complex>(n);
				
		for (int i = 0; i < n; i++) {
			roots.add(fromMagnitudeAndAngle(Math.pow(module(),  1.0 / n), (getAngle() + (double)i * 2.0 * Math.PI) / n));
		}
		
		return roots;
	}
	
	/**
	 * Creates new {@link Complex} from given module and angle between real
	 * and imaginary part of the number.
	 * 
	 * @param module module of complex number
	 * @param angle angle between real and imaginary part
	 * @return complex number
	 */
	private static Complex fromMagnitudeAndAngle(double module, double angle) {
		return new Complex(module * Math.cos(angle), module * Math.sin(angle));
	}
	
	/**
	 * Returns angle, in radians, between real and imaginary part of this 
	 * complex number.
	 * 
	 * @return angle
	 */
	private double getAngle() {
		double angle = Math.atan(im / re);

		if (re < 0) {
			angle += Math.PI;
		}
		
		return angle < 0 ? 2 * Math.PI + angle : angle;
	}

	/**
	 * Returns String representation of complex number in format "({re}+i{im})".
	 */
	@Override
	public String toString() {
		return "(" + String.valueOf(re) + (im < 0 ? "-" : "+") + "i" + String.valueOf(Math.abs(im)) + ")";
	}

	@Override
	public int hashCode() {
		return Objects.hash(im, re);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Complex))
			return false;
		Complex other = (Complex) obj;
		return Math.abs(im - other.im) < EPSILON
				&& Math.abs(re - other.re) < EPSILON;
	}

}
