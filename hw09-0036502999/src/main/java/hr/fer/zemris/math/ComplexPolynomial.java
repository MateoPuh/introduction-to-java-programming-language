package hr.fer.zemris.math;

import java.util.Arrays;

/**
 * Represents polynomial with complex numbers. It represents
 * polynomial of n-th order f(zn) = zn*z^n + zn-1*z^n-1 + ... + z1*z^1 + z0,
 * where z0,...,zn represent coefficient of polynomial.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ComplexPolynomial {
	
	/** Factory of polynomial. */
	private Complex[] factors;
	
	/**
	 * Creates new {@link ComplexPolynomial} with given factors.
	 * 
	 * @param factors factors of polynomial
	 */
	public ComplexPolynomial(Complex ...factors) {
		this.factors = factors.clone();
	}
	
	/**
	 * returns order of this polynomial; eg. For (7+2i)z^3+2z^2+5z+1 returns 3.
	 * 
	 * @return order of this polynomial.
	 */
	public short order() {
		short order = (short)(factors.length - 1);
		
		for(int i = factors.length - 1; i > 0; i--) {
			if(!factors[i].equals(Complex.ZERO)) {
				break;
			}
			
			order--;
		}
		
		return order;
	}
	
	/**
	 * Computes a new polynomial that is equal to this polynomial
	 * multiplyed with given complex number p.
	 * 
	 * @param p complex number
	 * @return this * p
	 */
	public ComplexPolynomial multiply(ComplexPolynomial p) {
		Complex[] newFactors = new Complex[this.factors.length + p.factors.length - 1];

		for(int i = 0; i < newFactors.length; i++) {
			newFactors[i] = new Complex();
		}
		
		for(int i = 0; i < this.factors.length; i++) {
			for(int j = 0; j < p.factors.length; j++) {
				newFactors[i + j] = newFactors[i + j]
						.add(this.factors[i].multiply(p.factors[j]));
			}
		}
		
		return new ComplexPolynomial(newFactors);
	}
	
	/**
	 * computes first derivative of this polynomial; for example, for
	 * (7+2i)z^3+2z^2+5z+1 returns (21+6i)z^2+4z+5.
	 * 
	 * @return derivative of this polynomial
	 */
	public ComplexPolynomial derive() {
		Complex[] newFactors = Arrays.copyOfRange(factors, 1, factors.length);
		
		for(int i = 0; i < newFactors.length; i++) {
			newFactors[i] = newFactors[i].multiply(new Complex(i + 1, 0));
		}
		
		return new ComplexPolynomial(newFactors);
	}
	
	/**
	 * Computes polynomial value at given point z;
	 * 
	 * @param z point for which polynomial is calculated
	 * @return polynomial value at given point z
	 */
	public Complex apply(Complex z) {
		Complex result = factors[0];
		
		for(int i = 1; i < factors.length; i++) {
			result = result.add(factors[i].multiply(z.power(i)));
		}
		
		return result;
	}
	
	/**
	 * Returns String representation of {@link ComplexPolynomial} in 
	 * format "zn*z^n + ... + z1*z + z0".
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
				
		for(int i = 0; i < factors.length; i++) {
			sb.insert(0, (i != factors.length - 1 ? "+" : "") + factors[i].toString() + (i != 0 ? "*z^" + i : ""));
		}
		
		return sb.toString();
	}

}
