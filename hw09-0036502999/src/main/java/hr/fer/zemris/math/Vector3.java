package hr.fer.zemris.math;

import java.util.Objects;
import java.util.Vector;

/**
 * This class represents vector in 3D space. It is
 * represented by three coordinates: x, y and z, which 
 * are real numbers.
 * 
 * <p>It contains methods for translating, rotating 
 * and scaling 3D vectors.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Vector3 {

	/** Maximal difference between two doubles to consider them equal */
	private static final double EPSILON = 1e-6;
	
	private double x;
	private double y;
	private double z;
	
	/**
	 * Creates new {@link Vector3} with given x and y
	 * coordinates.
	 * 
	 * @param x x coordinate of 2D vector
	 * @param y y coordinate of 2D vector
	 */
	public Vector3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Returns norm of {@link Vector3} (it's length, or
	 * absolute value).
	 * 
	 * @return norm of {@link Vector3}
	 */
	public double norm() {
		return Math.sqrt(x * x + y * y + z * z); 
	} 

	/**
	 * Returns normalized {@link Vector3}. Normalized vector
	 * is vector whose norm is equal to 1.
	 * 
	 * @return normalized {@link Vector3}
	 */
	public Vector3 normalized() {
        return scale(1 / norm());
	} 
	
	/**
	 * Adds the given {@link Vector3} to this {@link Vector3}
	 * and returns new vector which represents their sum.
	 * 
	 * @param other other {@link Vector}
	 * @return sum of vectors
	 * @throws NullPointerException if given vector is null
	 */
	public Vector3 add(Vector3 other) {
		Objects.requireNonNull(other);
		return new Vector3(x + other.x, y + other.y, z + other.z);
	} 
	
	/**
	 * Subtracts the given {@link Vector3} from this {@link Vector3}
	 * and returns new vector which represents their difference.
	 * 
	 * @param other other {@link Vector}
	 * @return difference of vectors
	 * @throws NullPointerException if given vector is null
	 */
	public Vector3 sub(Vector3 other) {
		Objects.requireNonNull(other);
		return new Vector3(x - other.x, y - other.y, z - other.z);
	} 
	
	/**
	 * Returns dot product of this {@link Vector3} and the 
	 * given {@link Vector3}.
	 * 
	 * @param other other vector
	 * @return dot product
	 * @throws NullPointerException if given vector is null
	 */
	public double dot(Vector3 other) {
		Objects.requireNonNull(other);
		return x * other.x + y * other.y + z * other.z;
	} 

	/**
	 * Returns new {@link Vector3} which represents cross product of this 
	 * {@link Vector3} and the given {@link Vector3}.
	 * 
	 * @param other other {@link Vector}
	 * @return cross product of vectors
	 * @throws NullPointerException if given vector is null
	 */
	public Vector3 cross(Vector3 other) {
		Objects.requireNonNull(other);
		return new Vector3(
				y * other.z - z * other.y, 
				-x * other.z + z * other.x, 
				x * other.y - y * other.x
		);
	} 
	
	/**
	 * Returns new {@link Vector3} which is result of 
	 * scaling this {@link Vector3} by given scaler.
	 * 
	 * @param scaler scaler used for scaling
	 * @return new vector which is scaled
	 */
	public Vector3 scale(double s) {
		return new Vector3(x * s, y * s, z * s);
	}
	
	/**
	 * Returns cosine of angle between this {@link Vector3} and 
	 * the given one.
	 * 
	 * @param other other vector
	 * @return cosine of angle 
	 * @throws NullPointerException if given vector is null
	 */
	public double cosAngle(Vector3 other) {
		Objects.requireNonNull(other);
		return dot(other) / (norm() * other.norm());
	}
	
	/**
	 * Returns x coordinate of {@link Vector3}.
	 * 
	 * @return x coordinate
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * Returns y coordinate of {@link Vector3}.
	 * 
	 * @return y coordinate
	 */
	public double getY() {
		return y;
	}

	/**
	 * Returns z coordinate of {@link Vector3}.
	 * 
	 * @return z coordinate
	 */
	public double getZ() {
		return z;
	}

	/**
	 * Returns {@link Vector3} as array with 3 elements (x, y and z
	 * coordinate).
	 * 
	 * @return {@link Vector3} as array
	 */
	public double[] toArray() {
		return new double[] {x, y, z};
	} 
	
	/**
	 * Returns String representation of 3d vector in format
	 * "({x},{y},{z})".
	 */
	@Override
	public String toString() {
		return String.format("(%.6f, %.6f, %.6f)", x, y, z);
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y, z);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vector3))
			return false;
		Vector3 other = (Vector3) obj;
		return Math.abs(x - other.x) < EPSILON 
				&& Math.abs(y - other.y) < EPSILON
				&& Math.abs(z - other.z) < EPSILON;
	}
	
}
