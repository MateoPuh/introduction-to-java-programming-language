<%@page import="java.util.Random"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
final String VALUES = "0123456789ABCDEF";
final Random RANDOM = new Random();

String color = (String)session.getAttribute("bgColor");
if(color == null){
	color = "FFFFFF";
}
%>

<%!
public String getRandomColor(String values, Random random) {
	StringBuilder sb = new StringBuilder();
	for(int i = 0; i < 6; i++) {
		int j = random.nextInt(values.length());
		sb.append(values.charAt(j));
	}
	
	return sb.toString();
}
%>

<html>
	<body bgcolor="#<%=color%>">
		<p><font color="#<%out.print(getRandomColor(VALUES, RANDOM));%>">Today at the bank, an old lady asked me to help check her balance. 
		So I pushed her over.</font></p>
	</body>
</html>