<%@page import="java.net.http.HttpResponse"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
String color = (String)session.getAttribute("bgColor");
if(color == null){
	color = "FFFFFF";
}
%>

<%!
public String getSin(int x) {
	return String.valueOf(Math.sin(Math.toRadians(x)));
}
public String getCos(int x) {
	return String.valueOf(Math.cos(Math.toRadians(x)));
}
%>

<html>
	<head>
		<style>
			TABLE {border: 1px solid black;}
			TD {border: 1px solid black;}
		</style>
	</head>
	<body bgcolor="#<%=color%>">
		<table>
			<tr><td>x</td><td>sin(x)</td><td>cos(x)</td></tr>
			<c:forEach var="angle" begin="${a}" end="${b}">
				<tr><td>${angle}</td>
				<td><% int x = (Integer)pageContext.getAttribute("angle"); out.print(getSin(x)); %></td>
				<td><% out.print(getCos(x)); %></td></tr>
			</c:forEach>
		</table>
	</body>
</html>