<%@page import="java.net.http.HttpResponse"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
String color = (String)session.getAttribute("bgColor");
if(color == null){
	color = "FFFFFF";
}
%>

<html>
	<body  bgcolor="#<%=color%>">
		<a href="<%=response.encodeRedirectUrl(request.getContextPath() + "/setcolor?pickedBgCol=FFFFFF")%>">WHITE</a>
		<a href="<%=response.encodeRedirectUrl(request.getContextPath() + "/setcolor?pickedBgCol=FF0000")%>">RED</a>
		<a href="<%=response.encodeRedirectUrl(request.getContextPath() + "/setcolor?pickedBgCol=00FF00")%>">GREEN</a>
		<a href="<%=response.encodeRedirectUrl(request.getContextPath() + "/setcolor?pickedBgCol=00FFFF")%>">CYAN</a>
	</body>
</html>