<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
String color = (String)session.getAttribute("bgColor");
if(color == null){
	color = "FFFFFF";
}
%>

<html>
	<head>
		<style type="text/css">
			table.rez td {text-align: center;}
		</style>
	</head>
	<body  bgcolor="#<%=color%>">

		<h1>Rezultati glasanja</h1>
		<p>Ovo su rezultati glasanja.</p>
		<table border="1" cellspacing="0" class="rez">
		<thead><tr><th>Bend</th><th>Broj glasova</th></tr></thead>
		<tbody>
 			
 			<c:forEach var="e" items="${results}">
				<tr><td>${e.band.name}</td><td>${e.votes}</td></tr>
			</c:forEach>

		</tbody>
		</table>
		
		<h2>Grafički prikaz rezultata</h2>
		<img alt="Pie-chart" src="<%=request.getContextPath()%>/glasanje-grafika" />

		<h2>Rezultati u XLS formatu</h2>
 		<p>Rezultati u XLS formatu dostupni su <a href="<%=request.getContextPath()%>/glasanje-xls">ovdje</a></p>
	
		<h2>Razno</h2>
		<p>Primjeri pjesama pobjedničkih bendova:</p>
	 	<p><% if(((List)request.getAttribute("winners")).isEmpty()) {
	 			out.print("Pobjednik nije izabran");
	 	}
	 	%></p>
	 	
	 	<ul>
	 		<c:forEach var="w" items="${winners}">
	 			<li><a href="${w.band.link}" target="_blank">${w.band.name}</a></li>
			</c:forEach>
		</ul>
	</body>
</html>
