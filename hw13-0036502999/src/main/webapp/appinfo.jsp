<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
String color = (String)session.getAttribute("bgColor");
if(color == null){
	color = "FFFFFF";
}
%>

<%!
public long getDays() {
	Long time = System.currentTimeMillis() - (Long)getServletContext().getAttribute("startTime");
	return time / (24 * 60 * 60 * 1000);
}

public long getHours() {
	Long time = System.currentTimeMillis() - (Long)getServletContext().getAttribute("startTime");
	return time % (24 * 60 * 60 * 1000) / (60 * 60 * 1000);
}

public long getMins() {
	Long time = System.currentTimeMillis() - (Long)getServletContext().getAttribute("startTime");
	return time % (60 * 60 * 1000) / (60 * 1000);
}

public long getSecs() {
	Long time = System.currentTimeMillis() - (Long)getServletContext().getAttribute("startTime");
	return time % (60 * 1000) / 1000;
}

public long getMillis() {
	Long time = System.currentTimeMillis() - (Long)getServletContext().getAttribute("startTime");
	return time % 1000;
}

public String getFormattedTime() {
	return String.format("%d days %d hours %d minutes %d seconds and %d milliseconds", 
			getDays(), getHours(), getMins(), getSecs(), getMillis());
}
%>

<html>
	<body bgcolor="#<%=color%>">
		<p>Elapsed time since web application started : <%=getFormattedTime()%></p>
	</body>
</html>