<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
String color = (String)session.getAttribute("bgColor");
if(color == null){
	color = "FFFFFF";
}
%>

<html>
	<body bgcolor="#<%=color%>">
		<ul>
			<li><a href="<%=request.getContextPath()%>/colors.jsp">Background color chooser</a></li>
			<li><a href="<%=request.getContextPath()%>/report.jsp">Report</a></li>
			<li><a href="<%=request.getContextPath()%>/glasanje">Vote</a></li>
			<li><a href="<%=request.getContextPath()%>/powers?a=1&b=100&n=3">Powers</a></li>
			<li><a href="<%=request.getContextPath()%>/appinfo.jsp">Elapsed time</a></li>
			<li><a href="<%=request.getContextPath()%>/stories/funny.jsp">Funny story</a></li>
			<li><a href="<%=request.getContextPath()%>/trigonometric?a=0&b=90">Trigonometric</a></li>
		</ul>
		<form action="trigonometric" method="GET">
 			Početni kut:<br><input type="number" name="a" min="0" max="360" step="1" value="0"><br>
 			Završni kut:<br><input type="number" name="b" min="0" max="360" step="1" value="360"><br>
 			<input type="submit" value="Tabeliraj"><input type="reset" value="Reset">
		</form>
	</body>
</html>