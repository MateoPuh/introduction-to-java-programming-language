package hr.fer.zemris.java.hw13.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * {@link ServletContextListener} that remembers time when web 
 * application was started, which is then used in "/appinfo.jsp".
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class AppInfoContextListener implements ServletContextListener {

	
	@Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
		servletContextEvent.getServletContext().setAttribute("startTime", System.currentTimeMillis());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
	
	

}
