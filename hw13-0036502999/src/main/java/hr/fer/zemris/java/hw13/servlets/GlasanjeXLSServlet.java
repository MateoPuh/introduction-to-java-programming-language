package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import hr.fer.zemris.java.hw13.voting.VotingResult;
import hr.fer.zemris.java.hw13.voting.VotingUtils;

/**
 * {@link HttpServlet} that is used for generating excel document with results 
 * of voting.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="glasanje-xls", urlPatterns={"/glasanje-xls"})
public class GlasanjeXLSServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final String FILENAME = "rezultati.xls";
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<VotingResult> results = VotingUtils.getResults(req);

		Collections.sort(results, new Comparator<VotingResult>() {
			 public int compare(VotingResult b1, VotingResult b2) {
				 return b2.getVotes() - b1.getVotes(); 
			 }
		});

		HSSFWorkbook hwb=new HSSFWorkbook();
		
		HSSFSheet sheet =  hwb.createSheet("Rezultati");

		for(int i = 0; i < results.size(); i++) {
			VotingResult r = results.get(i);
			HSSFRow rowhead= sheet.createRow(i + 1);
			rowhead.createCell(0).setCellValue(r.getBand().getName());
			rowhead.createCell(1).setCellValue( r.getVotes());
		}
		
		resp.setHeader("Content-Disposition", "attachment; filename=\"" 
				+ FILENAME + "\"");
		resp.setContentType("application/vnd.ms-exce");
		
		try {
			hwb.write(resp.getOutputStream());
			resp.getOutputStream().flush();
		} catch ( Exception ex ) {
			ex.printStackTrace();
			ServletUtils.sendError(req, resp, "Could not generate ddocument. Please try again later!");
		} finally {
			hwb.close();
		}
	}

}