package hr.fer.zemris.java.hw13.voting;

/**
 * This class represents result of voting. It contains information
 * about band's number of votes and band info ({@link Band} class).
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class VotingResult {
	
	private Band band;
	private int votes;
	
	/**
	 * Creates new {@link VotingResult} with given band and number of votes
	 * they received.
	 * 
	 * @param band band 
	 * @param votes number of votes
	 */
	public VotingResult(Band band, int votes) {
		this.band = band;
		this.votes = votes;
	}
	
	/**
	 * Returns band information.
	 * 
	 * @return band
	 */
	public Band getBand() {
		return band;
	}
	
	/**
	 * Returns number of votes band received.
	 * 
	 * @return number of votes
	 */
	public int getVotes() {
		return votes;
	}

}
