package hr.fer.zemris.java.hw13.voting;

/**
 * This class represents info of a band. It contains information about
 * band's name, id and link to one of their song.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class Band {
	
	private int id;
	private String name;
	private String link;
	
	/**
	 * Creates new {@link Band} with given id, name and link.
	 * 
	 * @param id id
	 * @param name name
	 * @param link link
	 */
	public Band(int id, String name, String link) {
		this.id = id;
		this.name = name;
		this.link = link;
	}

	/**
	 * Returns band's id.
	 * 
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Returns band's name.
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns link to one of band's song.
	 * 
	 * @return link to a song
	 */
	public String getLink() {
		return link;
	}

}
