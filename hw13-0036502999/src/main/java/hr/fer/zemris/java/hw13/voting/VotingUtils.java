package hr.fer.zemris.java.hw13.voting;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * This class contains static method used in voting web page. It contains
 * methods for reading from glasanje-definicija.txt and glasanje-rezultati.txt.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class VotingUtils {
	
	/**
	 * Returns list of {@link Band} object from glasanje-definicija.txt file.
	 * 
	 * @param req request
	 * @return list of bands
	 * @throws IOException if error occurs
	 */
	public static List<Band> getBands(HttpServletRequest req) throws IOException {
		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
		
		List<String> lines = Files.readAllLines(Paths.get(fileName), Charset.defaultCharset());
		List<Band> bands = new ArrayList<Band>();
		for(String line : lines) {
			String[] parts = line.split("\t");
			if(parts.length != 3) {
				continue;
			}
			try {
				Band band = new Band(Integer.parseInt(parts[0].trim()), parts[1].trim(), parts[2].trim());
				bands.add(band);
			} catch(NumberFormatException e) {
				e.printStackTrace();
			}
		}
		
		return bands;
	}
	
	/**
	 * Returns list of {@link VotingResult} objects from glasanje-definicija.txt
	 * and glasanje-rezultati.txt files.
	 * 
	 * @param req request
	 * @return list of reslts
	 * @throws IOException if error occurs
	 */
	public static List<VotingResult> getResults(HttpServletRequest req) throws IOException {				
		List<Band> bands = VotingUtils.getBands(req);
		Map<Integer, Integer> votes = getVotes(req);

		List<VotingResult> results = new ArrayList<VotingResult>();

		for(Band b : bands) {
			Integer vote = votes.get(b.getId());
			
			if(vote == null) {
				vote = 0;
			}
			
			results.add(new VotingResult(b, vote));
		}
		
		return results;
	}
	
	/**
	 * Returns map of bands and votes they received from glasanje-rezultati.txt file.
	 * 
	 * @param req request
	 * @return map of voting results
	 * @throws IOException if error occurs
	 */
	public static Map<Integer, Integer> getVotes(HttpServletRequest req) throws IOException {
		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");

		List<String> lines = Files.readAllLines(Paths.get(fileName), Charset.defaultCharset());

		HashMap<Integer, Integer> votes = new HashMap<Integer, Integer>();
		
		for(String line : lines) {
			String[] parts = line.split("\t");
			if(parts.length != 2) {
				continue;
			}
			
			votes.put(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
		}
		
		return votes;
	}

}
