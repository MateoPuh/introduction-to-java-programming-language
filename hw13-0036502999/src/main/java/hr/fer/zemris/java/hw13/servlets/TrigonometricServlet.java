package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * {@link HttpServlet} that is used for displaying table of
 * sine and cosine values for every integer between a and b
 * which are given as parameters. It is mapped to "/trigonometric".
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="trigonometric", urlPatterns={"/trigonometric"})
public class TrigonometricServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	/** Default value for parameter a. */
	private static final int DEFAULT_A = 0;
	/** Default value for parameter b. */
	private static final int DEFAULT_B = 360;
	/** Maximal difference between a and b. */
	private static final int MAX_DIFFERENCE = 720;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int a = DEFAULT_A;
		int b = DEFAULT_B;
		try {
			a = Integer.parseInt(req.getParameter("a"));		
		}catch(NullPointerException | NumberFormatException ignorable) {
		}
		
		try {
			b = Integer.parseInt(req.getParameter("b"));
		}catch(NullPointerException | NumberFormatException ignorable) {
		}
		
		if(a < 0) {
			a = DEFAULT_A;
		}
		if(b < 0) {
			b = DEFAULT_B;
		}
		
		if(a > b) {
			int t = a;
			a = b;
			b = t;
		}
		
		if(b > a + MAX_DIFFERENCE) {
			b = a + MAX_DIFFERENCE;
		}
		
		req.setAttribute("a", a);
		req.setAttribute("b", b);
		req.getRequestDispatcher("/WEB-INF/pages/trigonometric.jsp").forward(req, resp);
	}

}