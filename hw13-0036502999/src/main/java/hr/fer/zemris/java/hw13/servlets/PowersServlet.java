package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * {@link HttpServlet} that is used for generating Excel (.xls) document
 * with <code>n</code> pages. On page <code>i</code> (1 <= i <= 5) there are
 * 2 columns: in first column there are integers from a to b and on second
 * column there are number from first column on i-th power. It is mapped on
 * "/powers" url pattern.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="powers", urlPatterns={"/powers"})
public class PowersServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/** Name of a file that is generated. */
	private static final String FILENAME = "tablica.xls";
	
	/** Minimal value of integer (a and b). */
	private static final int MIN_INT = -100;
	/** Maximal value of integer (a and b). */
	private static final int MAX_INT = 100;
	/** Minimal value of number of sheets (n). */
	private static final int MIN_N = 1;	
	/** Maximal value of number of sheets (n). */
	private static final int MAX_N = 5;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Integer a = null;
		Integer b = null;
		Integer n = null;

		String errorMessage = "Arguments should be in range:\na: [-100, 100]\nb: [-100, 100]\nn: [1, 5].";
		
		try {
			a = Integer.parseInt(req.getParameter("a"));
			b = Integer.parseInt(req.getParameter("b"));
			n = Integer.parseInt(req.getParameter("n"));
		}catch(NullPointerException | NumberFormatException ignorable) {
			ServletUtils.sendError(req, resp, errorMessage);
			return;
		}
		
		if(a < MIN_INT || a > MAX_INT 
				|| b < MIN_INT || b > MAX_INT
				|| n < MIN_N || n > MAX_N) {
			ServletUtils.sendError(req, resp, errorMessage);
			return;
		}
		
		if(a > b) {
			int t = a;
			a = b;
			b = t;
		}

		HSSFWorkbook hwb=new HSSFWorkbook();
		for(int i = 1; i <= n; i++) {
			HSSFSheet sheet =  hwb.createSheet("Page " + i);

			for(int j = a; j < b; j++) {
				HSSFRow rowhead= sheet.createRow(j - a);
				rowhead.createCell(0).setCellValue(j);
				rowhead.createCell(1).setCellValue(Math.pow(j, i));
			}
		}
		
		resp.setHeader("Content-Disposition", "attachment; filename=\"" 
				+ FILENAME + "\"");
		resp.setContentType("application/vnd.ms-exce");
		
		try {
			hwb.write(resp.getOutputStream());
			resp.getOutputStream().flush();
		} catch ( Exception ex ) {
			ex.printStackTrace();
			ServletUtils.sendError(req, resp, "Could not generate ddocument. Please try again later!");
		} finally {
			hwb.close();
		}
	}

}