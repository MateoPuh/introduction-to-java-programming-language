package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * {@link HttpServlet} that is used for changing background color
 * for all pages. It is mapped on "/setcolor" url pattern.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="bgColor", urlPatterns={"/setcolor"})
public class BgColorServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String bgColor = req.getParameter("pickedBgCol");
		boolean isHex = bgColor == null ? false : bgColor.matches("^[0-9a-fA-F]+$");
						
		// Set color if valid color is given
		if(!(bgColor == null || bgColor.length() != 6 || !isHex)) {
			req.getSession().setAttribute("bgColor", bgColor);
		} 
		
		resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath()+"/index.jsp"));
	}

}