package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;

import hr.fer.zemris.java.hw13.voting.VotingResult;
import hr.fer.zemris.java.hw13.voting.VotingUtils;

/**
 * {@link HttpServlet} that is used for generating pie chart for voting results.
 * It is mapped to "/glasanje-grafika".
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="glasanje-grafika", urlPatterns={"/glasanje-grafika"})
public class GlasanjeGrafikaServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		List<VotingResult> results = VotingUtils.getResults(req);

		Collections.sort(results, new Comparator<VotingResult>() {
			 public int compare(VotingResult b1, VotingResult b2) {
				 return b2.getVotes() - b1.getVotes(); 
			 }
		});
		
		resp.setContentType("image/png");
		OutputStream os = resp.getOutputStream();
		
		DefaultPieDataset result = new DefaultPieDataset();
		
		for(VotingResult r : results) {
			result.setValue(r.getBand().getName(), r.getVotes());
		}

		JFreeChart chart = ChartFactory.createPieChart3D(
				 "Rezultati glasanja", 
				 result, 
				 true, 
				 false, 
				 false
				 );
		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.9f);
		chart.setBorderVisible(true); 
	       
		int width = 500;
	    int height = 400;
	       	     
	    ChartFactory.createPieChart("Voting results", result);
	    ChartUtils.writeChartAsPNG(os, chart, width, height);	
	 }

}