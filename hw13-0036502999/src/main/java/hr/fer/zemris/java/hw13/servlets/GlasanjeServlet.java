package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw13.voting.Band;
import hr.fer.zemris.java.hw13.voting.VotingUtils;

/**
 * {@link HttpServlet} that is used for displaying list of band to used
 * and allowing them to vote for favorite band. It is mapped to "/glasanje".
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="glasanje", urlPatterns={"/glasanje"})
public class GlasanjeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Band> bands = VotingUtils.getBands(req);
		
		Collections.sort(bands, new Comparator<Band>() {
			 public int compare(Band b1, Band b2) {
				 return b2.getId() - b1.getId(); 
			 }
		});
		
		req.setAttribute("bandList", bands);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
	}

}
