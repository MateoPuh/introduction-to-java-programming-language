package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class that contains static methods used in servlets.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class ServletUtils {
	
	/**
	 * Method used for displaying given error message to user.
	 * 
	 * @param req request
	 * @param resp response
	 * @param message error message
	 * @throws ServletException if error occurs
	 * @throws IOException if error occurs
	 */
	public static void sendError(HttpServletRequest req, HttpServletResponse resp, String message) throws ServletException, IOException {
		req.setAttribute("message", message);
		req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
	}

}
