package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw13.voting.VotingResult;
import hr.fer.zemris.java.hw13.voting.VotingUtils;

/**
 * {@link HttpServlet} that is used for displaying result of voting to user.
 * It is mapped to "/glasanje-rezultati".
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="glasanje-rezultati", urlPatterns={"/glasanje-rezultati"})
public class GlasanjeRezultatiServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<VotingResult> results = VotingUtils.getResults(req);

		Collections.sort(results, new Comparator<VotingResult>() {
			 public int compare(VotingResult b1, VotingResult b2) {
				 return b2.getVotes() - b1.getVotes(); 
			 }
		});
		
		req.setAttribute("results", results);
			
		List<VotingResult> winners = new ArrayList<VotingResult>();

		if(!results.isEmpty()) {
			int winnerVotes = results.get(0).getVotes();
			
			if(winnerVotes != 0) {
				for(VotingResult result : results) {
					if(result.getVotes() != winnerVotes) {
						break;
					}
					
					winners.add(result);
				}
			}
		}
		
		req.setAttribute("winners", winners);		
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
	}

}
