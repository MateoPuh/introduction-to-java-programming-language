package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * {@link HttpServlet} that is used for voting for favorite band.
 * It is mapped to "/glasanje-glasaj".
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="glasanje-glasaj", urlPatterns={"/glasanje-glasaj"})
public class GlasanjeGlasajServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			int id = Integer.parseInt(req.getParameter("id"));
			voteFor(req, id);
		} catch(NumberFormatException e) {
			e.printStackTrace();
			ServletUtils.sendError(req, resp, "Id of a band that you sent in not valid!");
			return;
		} catch(IOException e) {
			e.printStackTrace();
			ServletUtils.sendError(req, resp, "Error occured on server. Please try voting again later!");
			return;
		}
		
		resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
	}
	
	/**
	 * Thread-safe method for incrementing number of votes in glasanje-rezultati.txt file.
	 * 
	 * @param req request
	 * @param id id of a band
	 * @throws IOException if error occurs
	 */
	private synchronized void voteFor(HttpServletRequest req, int id) throws IOException {
		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		
		Path path = Paths.get(fileName);
		
		// create file if it does not exist
		if(!Files.exists(path)) {
			Files.createFile(path);
		}
		
		List<String> lines = Files.readAllLines(path, Charset.defaultCharset());
		
		boolean found = false;
		
		// find record of votes for band with given id and increment number of votes
		for(int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			String[] parts = line.split("\t");
			
			if(parts.length != 2) {
				continue;
			}
			
			if(Integer.parseInt(parts[0].trim()) == id) {
				int votes = Integer.parseInt(parts[1].trim());
				votes++;
				lines.remove(i);
				lines.add(i, id + "\t" + votes);
				found = true;
				break;
			}
		}
		
		// if no record of votes for band is found, add record to the end of file
		if(!found) {
			lines.add(id + "\t" + 1);
		}
		
		Files.write(path, lines);
	}

}
