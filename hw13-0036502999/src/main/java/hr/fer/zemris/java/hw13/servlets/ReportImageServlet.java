package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;

/**
 * {@link HttpServlet} that is used for generating PNG image 
 * with pie chart that indicates OS usage. It is mapped on "/reportImage".
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="reportImage", urlPatterns={"/reportImage"})
public class ReportImageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		 resp.setContentType("image/png");
		 
		 DefaultPieDataset result = new DefaultPieDataset();
		 result.setValue("Linux", 29);
		 result.setValue("Mac", 20);
		 result.setValue("Windows", 51);

		 JFreeChart chart = ChartFactory.createPieChart3D(
				 "OS usage", 
				 result, 
				 true, 
				 true, 
				 false);
		 
		 PiePlot3D plot = (PiePlot3D) chart.getPlot();
		 plot.setStartAngle(290);
		 plot.setDirection(Rotation.CLOCKWISE);
		 plot.setForegroundAlpha(0.5f);
		 chart.setBorderVisible(false);		 
	       
		 int width = 500;
	     int height = 400;
	       	     
	     ChartFactory.createPieChart("OS usage", result);
	     ChartUtils.writeChartAsPNG(resp.getOutputStream(), chart, width, height);	
	 }

}