package hr.fer.zemris.java.custom.scripting.parser;

/**
 * This Exception is used in {@link SmartScriptParser} class
 * whenever there is error.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class SmartScriptParserException extends RuntimeException {

private static final long serialVersionUID = 1L; 
	
	/**
	 * Creates new SmartScriptParserException.
	 */
	public SmartScriptParserException() {
		
	}
	
	/**
	 * Creates new SmartScriptParserException with a given message.
	 * 
	 * @param message exception message
	 */
	public SmartScriptParserException(String message) {
		super(message);
	}
	
	/**
	 * Creates new SmartScriptParserException with a given cause.
	 * 
	 * @param cause exception cause
	 */
	public SmartScriptParserException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates new SmartScriptParserException with a given message and cause.
	 * 
	 * @param message exception message
	 * @param cause exception cause
	 */
	public SmartScriptParserException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
