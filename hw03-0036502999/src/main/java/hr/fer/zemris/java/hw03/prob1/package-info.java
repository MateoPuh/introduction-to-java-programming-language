﻿/**
 * This package provides classes {@link Lexer} and
 * {@link Token} used for performing lexical analysis.
 * It also provides two enums: {@link LexerState}, 
 * which represents state in which Lexer can be in and
 * {@link TokenType} which represent type of token that
 * is analyzed.
 * 
 * <p>It performs lexical analysis on document which consists of
 * tags words, numbers and symbols.</p>
 * 
 * <p>If any error happens while analyzing given document, 
 * {@link LexerException} is thrown.</p>	
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
package hr.fer.zemris.java.hw03.prob1;