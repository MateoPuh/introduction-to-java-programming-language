package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * This class represents {@link Node} used for representing 
 * an entire structured document.
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.nodes
 */
public class DocumentNode extends Node {

}
