package hr.fer.zemris.java.hw03.prob1;

import java.util.Arrays;
import java.util.Objects;

import hr.fer.zemris.java.custom.collections.Tester;

/**
 * This class represents lexical analyzer which breaks
 * texts into series of tokens (see {@link Token}). It 
 * uses two methods: nextToken() and getToken().
 * 
 * <p>Method nextToken() generates and returns next token. If there
 * is an error in generating token, {@link LexerException} is thrown.
 * Method getToken() returns last generated token.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Lexer {
	
	/** Backslash character */
	private static final char BACKSLASH = '\\';
	
	/** Special character that represents beginning and end of extended lexer state */
	private static final char SPECIAL_SYMBOL = '#';
	
	/** A string that contains all blank characters */
	private static final String BLANK_CHARACTERS = " \n\r\t";
	
	/** Input data, split into chracters */
	private char[] data; 
	
	/** Last generated token */
	private Token token; 
	
	/** Index of currently analyzed character */
	private int currentIndex; 

	/** Current state of lexer */
	private LexerState state;
	
	/**
	 * Creates new Lexer with given text which it uses to
	 * generate tokens.
	 * 
	 * @param text text from which tokens will be generated
	 * @throws NullPointerException if given text is null
	 */
	public Lexer(String text) throws NullPointerException{ 
		Objects.requireNonNull(text);
		this.data = text.toCharArray();
		this.state = LexerState.BASIC;
	}

	/**
	 * Generates and returns next token. 
	 * 
	 * @return next token
	 * @throws LexerException if error occurs while generating token
	 */
	public Token nextToken() throws LexerException{
		// when everything is read and nextToken is called, throw lexer exception
		if(currentIndex > data.length) {
			throw new LexerException("Error occured while reading next token after EOF was read!");
		}
		
		
		// if end of string was reached, return new token with type of EOF and value null
		if(currentIndex == data.length) {
			token = new Token(TokenType.EOF, null);
			currentIndex ++;

			return token;
		}
		
		// if current character is blank character, read next character
		if(BLANK_CHARACTERS.contains(Character.toString((data[currentIndex ])))) {
			currentIndex ++;

			return nextToken();
		}
		
		if(this.state.equals(LexerState.EXTENDED)) {
			if(Character.valueOf(data[currentIndex]).equals(SPECIAL_SYMBOL)) {
				token = new Token(TokenType.SYMBOL, Character.valueOf(data[currentIndex]));
				currentIndex++;
				return token;
			}
			
			generateWordExtended();
			return token;
		}
		
		if(Character.isLetter(data[currentIndex]) || Character.valueOf(data[currentIndex]).equals(BACKSLASH)) {
			generateWord();
			return token;
		} else if(Character.isDigit(data[currentIndex])) {
			generateNumber();
			return token;
		} else {
			token = new Token(TokenType.SYMBOL, Character.valueOf(data[currentIndex]));
			currentIndex++;
			return token;
		}
	}

	/**
	 * Generates token of WORD type in extended lexer's state. 
	 * Word is everything between two blank characters or special symbol.
	 */
	private void generateWordExtended() {
		String s = readInputData((ch) -> {
			return !BLANK_CHARACTERS.contains(String.valueOf((char)ch)) 
					&& !Character.valueOf((char)ch).equals(SPECIAL_SYMBOL);
		});
		
		token = new Token(TokenType.WORD, s);
	}
	
	/**
	 * Generates token of WORD type. It only uses
	 * letter to generate token.
	 * 
	 * @throws LexerException if there is error
	 */
	private void generateWord() throws LexerException{
		String value = "";
		
		while(true) {
			if (currentIndex >= data.length) {
				break;
			}
			
			if(Character.valueOf(data[currentIndex]).equals(BACKSLASH)) {
				currentIndex++;
				
				if(currentIndex < data.length 
						&& (Character.valueOf(data[currentIndex]).equals(BACKSLASH)
						|| Character.isDigit(data[currentIndex]))) {
					value += data[currentIndex];
					currentIndex++;
					continue;
				} else {
					throw new LexerException("Invalid escape sequence!");
				}
			}
			
			String s = readInputData((ch) -> {
				return Character.isLetter((char)ch);
			});
			
			if (s.length() == 0) break;
			
			value += s;
		}
		
		token = new Token(TokenType.WORD, value);
	}
	
	/**
	 * Generates token of NUMBER type in long format.
	 * 
	 * @throws LexerException if there is error
	 */
	private void generateNumber() throws LexerException {
		String s = readInputData((ch) -> {
			return Character.isDigit((char)ch);
		});
		
		try {
			long value = Long.valueOf(s);
			token = new Token(TokenType.NUMBER, value);
		} catch(NumberFormatException ex) {
			throw new LexerException("Number should be in long format!");
		}
	}
	
	/**
	 * Reads input data while tester is accepting it and
	 * returns string that it read.
	 * 
	 * @param tester tester used for accepting input data
	 */
	private String readInputData(Tester tester) {
		int indexBeginning = currentIndex;
		
		while (currentIndex < data.length) {
			
			if (tester.test(data[currentIndex])) {
				currentIndex++;
			}
			else {
				break;
			}
		}
		
		char[] value = Arrays.copyOfRange(data, indexBeginning, currentIndex);
		return String.valueOf(value);
	}
	
	/**
	 * Returns last generated token. Calling this method
	 * will not result in generating new token.
	 * 
	 * @return last generated token
	 */
	public Token getToken() {
		return token;
	}
	
	/**
	 * Sets the state of lexer to a given state.
	 * 
	 * @param state new state of lexer
	 * @throws NullPointerException if given state is null
	 */
	public void setState(LexerState state) throws NullPointerException{
		this.state = Objects.requireNonNull(state);
	}

	
}