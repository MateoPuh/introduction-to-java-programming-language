package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enumeration of state that {@link SmartScriptLexer} can be in. 
 * The states are TEXT, which represents lexer's behavior when analyzing
 * a piece of textual data outside of tags, and TAG, which represents 
 * analyzing content inside of tags.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public enum SmartScriptLexerState {
	
	/** Analyzing text outside of tags */
	TEXT,
	/** Analyzing content inside of tags */
	TAG

}
