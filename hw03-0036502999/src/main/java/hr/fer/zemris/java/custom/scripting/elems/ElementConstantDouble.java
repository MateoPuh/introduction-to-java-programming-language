package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents {@link Element} which represents number
 * which type is Double in an expression. 
 * 
 * <p>It provides method for getting its value.</p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.elems
 */
public class ElementConstantDouble extends Element {

	/** Value of Double number */
	private double value;
	
	/**
	 * Creates new {@link ElementConstantDouble} with given
	 * value of a Double number.
	 * 
	 * @param value value of a Double number
	 */
	public ElementConstantDouble(double value) {
		this.value = value;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText() {
		return String.valueOf(value);
	}
	
	/**
	 * Returns value of a Double number.
	 * 
	 * @return value of a Double number
	 */
	public double getValue() {
		return value;
	}

}
