package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Exception used in {@link SmartScriptLexer} class. 
 * It is generally used for all error in SmartScriptLexer 
 * class.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class SmartScriptLexerException extends RuntimeException {
	
	private static final long serialVersionUID = 1L; 
	
	/**
	 * Creates new SmartScriptLexerException.
	 */
	public SmartScriptLexerException() {
		
	}
	
	/**
	 * Creates new SmartScriptLexerException with a given message.
	 * 
	 * @param message exception message
	 */
	public SmartScriptLexerException(String message) {
		super(message);
	}
	
	/**
	 * Creates new SmartScriptLexerException with a given cause.
	 * 
	 * @param cause exception cause
	 */
	public SmartScriptLexerException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates new SmartScriptLexerException with a given message and cause.
	 * 
	 * @param message exception message
	 * @param cause exception cause
	 */
	public SmartScriptLexerException(String message, Throwable cause) {
		super(message, cause);
	}
	
}