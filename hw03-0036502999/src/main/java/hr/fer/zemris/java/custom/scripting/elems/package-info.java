﻿/**
 * This package provides classes which represent different
 * elements. Elements are used for the representation of 
 * expressions. Class {@link Element} is base class for 
 * all elements. There are also {@link ElementVariable}, 
 * {@link ElementConstantInteger}, {@link ElementConstantDouble}, 
 * {@link ElementString}, {@link ElementFunction} and 
 * {@link ElementOperator}.
 *  
 * @author Mateo Puhalović
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.elems;