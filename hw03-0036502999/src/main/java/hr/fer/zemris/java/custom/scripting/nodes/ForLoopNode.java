package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.Objects;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;

/**
 * This class represents {@link Node} used for representing a 
 * single for-loop construct in a structured document.
 * 
 * <p>It provides methods for getting its variable, its starting
 * expression, ending expression and increment. Increment can be null.
 * Variable is represented with {@link ElementVariable} class, while
 * other properties are represented with {@link Element} class.</p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.nodes
 */
public class ForLoopNode extends Node {

	/** Left bound of for node */
	private static final String LEFT_BOUND = "{$";

	/** Right bound of for node */
	private static final String RIGHT_BOUND = "$}";
	
	/** For-loop tag name */
	private static final String FOR = "FOR";
	
	/** Variable used in for-loop */
	private ElementVariable variable;
	
	/** Variable starting state */
	private Element startExpression;
	
	/** Variable ending state */
	private Element endExpression;
	
	/** Variable increment, can be null */
	private Element stepExpression;
	
	/**
	 * Creates new {@link ForLoopNode} with given variable,
	 * starting expression, ending expression and increment.
	 * Increment (stepExpression) can be null.
	 * 
	 * @param variable for-loop variable
	 * @param startExpression starting expression
	 * @param endExpression ending expression
	 * @param stepExpression increment
	 * @throws NullPointerException if given variable, starting 
	 * 			expression or ending expression are null.
	 */
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression,
			Element stepExpression) throws NullPointerException {
		this.variable = Objects.requireNonNull(variable);
		this.startExpression = Objects.requireNonNull(startExpression);
		this.endExpression = Objects.requireNonNull(endExpression);
		this.stepExpression = stepExpression;
	}
	
	/**
	 * Returns for-loop variable.
	 * 
	 * @return for-loop variable
	 */
	public ElementVariable getVariable() {
		return variable;
	}
	
	/**
	 * Returns starting expression of for-loop.
	 * 
	 * @return starting expression
	 */
	public Element getStartExpression() {
		return startExpression;
	}
	
	/**
	 * Returns ending expression of for-loop.
	 * 
	 * @return ending expression
	 */
	public Element getEndExpression() {
		return endExpression;
	}
	
	/**
	 * Returns increment of for-loop
	 * 
	 * @return for-loop increment
	 */
	public Element getStepExpression() {
		return stepExpression;
	}
	
	/**
	 * Returns string in format: 
	 * "{$ FOR variable startExpression endExpression stepExpression $}".
	 * 
	 * @return formatted string
	 */
	@Override
	public String toString() {
		return LEFT_BOUND + " " + FOR  
				+ " " + variable.asText() 
				+ " " + startExpression.asText() 
				+ " " + endExpression.asText() 
				+ (stepExpression != null ? " " + stepExpression.asText() : "")
				+ " " + RIGHT_BOUND;
	}
	
}
