package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enumeration of types of tokens used in {@link SmartScriptLexer} class.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public enum SmartScriptTokenType {
	
	 /** End of file, last token in lexical analysis **/
	 EOF,
	 /** String of characters outside of tags **/
	 TEXT,
	 /** Variable used inside of tags */
	 VARIABLE,
	 /** Function used inside of tags */
	 FUNCTION,
	 /** String of characters used inside of tags */
	 STRING,
	 /** Operator used inside of tags */
	 OPERATOR,
	 /** Number in Integer format */
	 DOUBLE_NUMBER, 
	 /** Number in Double format */
	 INTEGER_NUMBER,
	 /** For-loop tag name */
	 TAG_NAME_FOR,
	 /** Echo tag name */
	 TAG_NAME_ECHO,
	 /** End tag name*/
	 TAG_NAME_END,
	 /** Tag name which is not for, echo nor end*/
	 TAG_NAME,
	 /** Left bound of tag */
	 TAG_LEFT_BOUND,
	 /** Right bound of tag */
	 TAG_RIGHT_BOUND

}
