package hr.fer.zemris.java.hw03.prob1;

/**
 * Enumeration of state that {@link Lexer} can be in. The 
 * states are BASIC, which represents basic Lexer behaviour, 
 * and EXTENDED, which represents extended Lexer behaviour.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public enum LexerState {
	 /** Basic behaviour of Lexer **/
	 BASIC,
	 /** Extended behaviour of Lexer **/
	 EXTENDED
}
