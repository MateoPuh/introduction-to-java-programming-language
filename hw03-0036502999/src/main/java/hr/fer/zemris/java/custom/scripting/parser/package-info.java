﻿/**
 * This package provides classes {@link SmartScriptParser} used
 * for parsing given document. 
 * 
 * <p>It uses {@link SmartScriptLexer}
 * for generating token and then {@link Node} and {@link Element}
 * classes for creating tree construction.</p>
 * 
 * <p>If any error happens while parsing given document, 
 * {@link SmartScriptParserException} is thrown.</p>	
 *  
 * @author Mateo Puhalović
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.parser;