package hr.fer.zemris.java.hw03.prob1;

/**
 * This class represents token used in lexical analysis
 * in {@link Lexer} class.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Token {
	
	private TokenType type;
	private Object value;
	
	/**
	 * Creates new Token with given type and value.
	 * 
	 * @param type type of token
	 * @param value value of token
	 */
	public Token(TokenType type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	/**
	 * Returns value of token.
	 * 
	 * @return value of token
	 */
	public Object getValue() {
		return value;
	}
	
	/**
	 * Returns type of token.
	 * 
	 * @return type of token
	 */
	public TokenType getType() {
		return type;
	}
	
}
