package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.Collection;
import hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection;

/**
 * This class demonstrates usage of addAllSatisfying(Collection, Tester)
 * method.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class AddAllSatisfyingDemo {
	
	/**
	 * This method is first run after program is started.
	 * It demonstrates usage of addAllSatisfying(Collection, Tester)
	 * method from {@link Collection} interface.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		Collection col1 = new LinkedListIndexedCollection();
		Collection col2 = new ArrayIndexedCollection();
		col1.add(2);
		col1.add(3);
		col1.add(4);
		col1.add(5);
		col1.add(6);
		col2.add(12);
		col2.addAllSatisfying(col1, new EvenIntegerTester());
		col2.forEach(System.out::println);
	}

}
