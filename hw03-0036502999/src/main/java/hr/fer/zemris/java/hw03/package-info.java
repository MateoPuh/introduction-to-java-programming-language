﻿/**
 * This package provides classes which can be used
 * for manipulating collections and two collection
 * implementations: ArrayIndexedCollection which 
 * implements resizable array-backed collection of objects
 * and LinkedListIndexedCollection which implements 
 * linked list-backed collection of objects.
 * 
 * <p> It also includes ObjectStack and EmptyStackException
 * classes. ObjectStack class is used as stack and acts as 
 * an interface between user and Collection, providing usual
 * methods used in stack. EmptyStackException is thrown if
 * user tries to pop object when stack is empty.</p>
 * 
 * <p> Processor class is also provided in this package. It 
 * represents model of an object capable of performing some 
 * operation on the passed object. </p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 */
package hr.fer.zemris.java.hw03;