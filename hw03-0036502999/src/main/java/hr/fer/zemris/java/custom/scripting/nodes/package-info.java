﻿/**
 * This package provides classes which represent different
 * nodes. Nodes are used for representation of structured 
 * document. Class {@link Node} is base class for all nodes.
 * There are also {@link TextNode}, {@link DocumentNode}, 
 * {@link ForLoopNode} and {@link EchoNode}.
 *  
 * @author Mateo Puhalović
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.nodes;