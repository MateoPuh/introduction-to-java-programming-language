package hr.fer.zemris.java.hw03;

import hr.fer.zemris.java.custom.scripting.nodes.*;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * This class demonstrates usage of {@link SmartScriptParser} class.
 * It reads file which contains structured document which consists 
 * of tags(bounded by {$ and $}) and rest of the text.  
 * 
 * <p>It uses one command line argument, which is path to the 
 * file with document to parse.</p>
 * 
 * <p>It demonstrates output after parsing and output after parsing
 * what is already parsed. Those two should be the same. Output that is
 * result of parsing the file is not required to be exactly the same
 * as content of the file, but it should gave same result if parsed again.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class SmartScriptTester {

	/**
	 * This method is first run after program is started.
	 * It demonstrates usage of {@link SmartScriptParser}
	 * in parsing document and then parsing the result of
	 * first parsing to prove that they it gives same result.
	 * 
	 * <p>It uses one command line argument, which is path to the 
	 * file with document to parse.</p>
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("There should be only 1 command line argument!");
			System.exit(1);
		}
	 	
		String docBody = null;
		
		try {
			docBody = new String(
					 Files.readAllBytes(Paths.get(args[0])),
					 StandardCharsets.UTF_8
					);
		} catch (IOException ex) {
			System.out.println("Cannot read file: " + ex.getMessage());
			System.exit(1);
		} catch (Exception e1) {
			System.out.println(e1.getLocalizedMessage());
			System.exit(1);
		}
		
		System.out.println("original>>");
		System.out.println(docBody);
		SmartScriptParser parser = null;
		try {
		 parser = new SmartScriptParser(docBody);
		} catch(SmartScriptParserException e) {
			System.out.println(e.getMessage());
			System.out.println("Unable to parse document!");
			System.exit(-1);
		} catch(Exception e) {
			System.out.println(e.getMessage());
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}
		
		DocumentNode document = parser.getDocumentNode();
		String originalDocumentBody = createOriginalDocumentBody(document);
		System.out.println("\ngenerirano iz originala>>");
		System.out.println(originalDocumentBody);
		System.out.println("\ngenerirano iz parsiranog teksta>>");
		
		SmartScriptParser parser2 = null;
		try {
			parser2 = new SmartScriptParser(originalDocumentBody);
		} catch(SmartScriptParserException e) {
			System.out.println(e.getMessage());
			System.out.println("Unable to parse document!");
			System.exit(-1);
		} catch(Exception e) {
			System.out.println(e.getMessage());
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}
		
		DocumentNode document2 = parser2.getDocumentNode();
		String copiedDocumentBody =  createOriginalDocumentBody(document2);
		System.out.println(copiedDocumentBody);
		
		System.out.println("\noriginalDocumentBody.equals(copiedDocumentBody) = " 
				+ originalDocumentBody.equals(copiedDocumentBody));
	}
	
	/**
	 * Recreates document from given document node.
	 * It searches tree structure using nodes children and
	 * constructs document. That document is not necessary
	 * same as original document, but parsing this document
	 * will result in same tree structure as tree structure
	 * which is result from parsing original. 
	 * 
	 * @param document document node
	 * @return recreated document
	 */
	public static String createOriginalDocumentBody(Node document) {
		String result = new String();

		if(document instanceof TextNode) {
			result += ((TextNode) document).getText();
		} else if(document instanceof ForLoopNode) {
			result += ((ForLoopNode) document).toString();
		} else if (document instanceof EchoNode) {
			result += ((EchoNode) document).toString();
		} else {
			result += "";
		}
		
		for (int i = 0; i < document.numberOfChildren(); i++) {
			result += createOriginalDocumentBody(document.getChild(i));
		}
		
		return result;
	}
	
}
