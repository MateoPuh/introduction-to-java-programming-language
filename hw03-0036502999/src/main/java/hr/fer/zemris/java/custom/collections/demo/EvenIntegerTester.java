package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.Tester;

/**
 * This class represents {@link Tester} class 
 * which checks whether given object is even Integer.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
class EvenIntegerTester implements Tester {
	 
	/**
	 * Checks whether given object is even Integer.
	 * 
	 * @return true if object is even Integer; false otherwise
	 */
	@Override
	public boolean test(Object obj) {
		if(!(obj instanceof Integer)) return false;
		Integer i = (Integer)obj;
		return i % 2 == 0;
	} 
	 
	/**
	 * This method is first run after program is started.
	 * It demonstrates usage of EvenIntegerTester class.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		Tester t = new EvenIntegerTester();
		System.out.println(t.test("Ivo"));
		System.out.println(t.test(22));
		System.out.println(t.test(3));
	}
	
}
