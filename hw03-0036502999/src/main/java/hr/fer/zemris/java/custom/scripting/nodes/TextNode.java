package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * This class represents {@link Node} used for representing 
 * a piece of textual data in a structured document.
 * 
 * <p>It provides methods for getting its text, along with other
 * Node methods.</p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.nodes
 */
public class TextNode extends Node{

	/** Text which this node represents */
	private String text;
	
	/** Quotation mark character */
	private static final char LEFT_CURLY_BRACKET = '{';
	
	/** Backslash used for escaping */
	private static final String STRING_BACKSLASH = "\\";
	
	/** Backslash character */
	private static final char BACKSLASH = '\\';
	
	/**
	 * Creates new {@link TextNode} with given
	 * text.
	 * 
	 * @param text text which this node represents
	 */
	public TextNode(String text) {
		this.text = text;
	}
	
	/**
	 * Returns text which this node represents.
	 * 
	 * @return text which this node represents
	 */
	public String getText() {
		char[] data = text.toCharArray();
		
		String result = new String();
		
		for(int i = 0; i < data.length; i++) {
			if(Character.valueOf(data[i]).equals(LEFT_CURLY_BRACKET)
					|| Character.valueOf(data[i]).equals(BACKSLASH)) {
				result += STRING_BACKSLASH;
			}
			result += data[i];
		}
		
		return result;
	}
	
}
