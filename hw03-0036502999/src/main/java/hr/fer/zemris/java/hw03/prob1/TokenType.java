package hr.fer.zemris.java.hw03.prob1;

/**
 * Enumeration of types of tokens used in {@link Lexer} class.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public enum TokenType {
	 /** End of file, last token in lexical analysis **/
	 EOF,
	 /** String of characters **/
	 WORD, 
	 /** Number in long format */
	 NUMBER, 
	 /** Every character that is left after removing numbers and words */
	 SYMBOL
}
