package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents {@link Element} which represents variable
 * in an expression. 
 * 
 * <p>It provides method for getting its name.</p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.elems
 */
public class ElementVariable extends Element {

	/**	Name of variable */
	private String name;
	
	/**
	 * Creates new {@link ElementVariable} with given name.
	 * 
	 * @param name name of variable
	 */
	public ElementVariable(String name) {
		this.name = name;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText() {
		return name;
	}

	/**
	 * Returns name of variable.
	 * 
	 * @return name of variable
	 */
	public String getName() {
		return name;
	}
	
}
