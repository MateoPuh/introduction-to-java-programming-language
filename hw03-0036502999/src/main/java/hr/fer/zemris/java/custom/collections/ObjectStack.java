package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * This class represents interface between user and Collection 
 * and acts as a stack, providing usual methods used in stack.
 * This class provides user the methods which are natural for a 
 * stack and hide everything else.
 * 
 * <p> Methods push, pop and peek have complexity O(n) in worst case
 * (if ArrayIndexedCollection is used) and average complexity Θ(1). </p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class ObjectStack {
	
	private ArrayIndexedCollection collection;
	
	/**
	 * Creates empty stack
	 */
	public ObjectStack() {
		collection = new ArrayIndexedCollection();
	}
	
	/**
	 * Returns true if stack is empty.
	 * 
	 * @return true if stack is empty; false otherwise
	 * @see ArrayIndexedCollection#isEmpty()
	 */
	public boolean isEmpty() {
		return collection.isEmpty();
	}
	
	/**
	 * Returns number of elements on stack.
	 * 
	 * @return number of elements on stack
	 * @see ArrayIndexedCollection#size()
	 */
	public int size() {
		return collection.size();
	}

	/**
	 * Insert given value on stack. Average complexity 
	 * of this method is Θ(1). 
	 * 
	 * @param value value to push on stack
	 * @throws NullPointerException if given value is null
	 * @see ArrayIndexedCollection#add(Object)
	 */
	public void push(Object value) throws NullPointerException {
		collection.add(Objects.requireNonNull(value));
	}
	
	/**
	 * Removes and returns last object from stack. Average 
	 * complexity of this method is Θ(1). 
	 * 
	 * @return object poped from stack
	 * @throws EmptyStackException if stack is empty
	 * @see ArrayIndexedCollection#remove(Object)
	 * @see ArrayIndexedCollection#get(Object)
	 */
	public Object pop() throws EmptyStackException {
		try {
			Object obj = collection.get(collection.size() - 1);
			collection.remove(collection.size() - 1);
			return obj;
		} catch(IndexOutOfBoundsException ex) {
			throw new EmptyStackException();
		}
	}
	
	/**
	 * Returns last object from stack without removing it. 
	 * Average complexity of this method is Θ(1). 
	 * 
	 * @return last object on stack
	 * @throws EmptyStackException if stack is empty
	 * @see ArrayIndexedCollection#get(Object)
	 */
	public Object peek() throws EmptyStackException {
		try {
			return collection.get(collection.size() - 1);
		} catch(IndexOutOfBoundsException ex) {
			throw new EmptyStackException();
		}
	}
	
	/**
	 * Removes all objects from stack.
	 * 
	 * @see ArrayIndexedCollection#clear()
	 */
	public void clear() {
		collection.clear();
	}
	
	
}
