package hr.fer.zemris.java.hw03.prob1;

/**
 * Exception that is thrown when error occurs while generating
 * next token in {@link Lexer} class.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class LexerException extends RuntimeException {
	
	private static final long serialVersionUID = 1L; 
	
	/**
	 * Creates new LexerException.
	 */
	public LexerException() {
		
	}
	
	/**
	 * Creates new LexerException with a given message.
	 * 
	 * @param message exception message
	 */
	public LexerException(String message) {
		super(message);
	}
	
	/**
	 * Creates new LexerException with a given cause.
	 * 
	 * @param cause exception cause
	 */
	public LexerException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates new LexerException with a given message and cause.
	 * 
	 * @param message exception message
	 * @param cause exception cause
	 */
	public LexerException(String message, Throwable cause) {
		super(message, cause);
	}

}