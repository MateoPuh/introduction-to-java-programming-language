package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.Objects;

/**
 * This class represents token used in lexical analysis
 * in {@link SmartScriptLexer} class and in parsing in
 * {@link SmartScriptParser} class.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class SmartScriptToken {

	/** Type of token */
	private SmartScriptTokenType type;
	
	/** Value of a token */
	private Object value;
	
	/**
	 * Creates new Token with given type and value.
	 * 
	 * @param type type of token
	 * @param value value of token
	 */
	public SmartScriptToken(SmartScriptTokenType type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	/**
	 * Returns value of token.
	 * 
	 * @return value of token
	 */
	public Object getValue() {
		return value;
	}
	
	/**
	 * Returns type of token.
	 * 
	 * @return type of token
	 */
	public SmartScriptTokenType getType() {
		return type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SmartScriptToken))
			return false;
		SmartScriptToken other = (SmartScriptToken) obj;
		return type == other.type && Objects.equals(value, other.value);
	}	
	
}
