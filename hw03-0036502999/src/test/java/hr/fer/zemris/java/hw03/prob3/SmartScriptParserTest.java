package hr.fer.zemris.java.hw03.prob3;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;
import hr.fer.zemris.java.hw03.SmartScriptTester;


public class SmartScriptParserTest {

	@Test
	public void testParserNull() {
		String docBody = null;		
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	@Test
	public void testParserEmpty() {
		String docBody = "";
		
		SmartScriptParser parser = new SmartScriptParser(docBody);
		
		DocumentNode document = parser.getDocumentNode();
		String originalDocumentBody = SmartScriptTester.createOriginalDocumentBody(document);
		
		assertEquals(docBody, originalDocumentBody);
	}
	
	@Test
	public void testParserTextOnly() {
		String docBody = "tekst, tekst \\\\ \\{$ for $}";
		
		SmartScriptParser parser = new SmartScriptParser(docBody);
		
		DocumentNode document = parser.getDocumentNode();
		String originalDocumentBody = SmartScriptTester.createOriginalDocumentBody(document);
		
		assertEquals(docBody, originalDocumentBody);
	}
	
	@Test
	public void testParserInvalidEscaping() {
		String docBody = "tekst, tekst \\1";
		
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	@Test
	public void testParserTagNotClosed() {
		String docBody = "{$ for i 1 2 3 $}";
		
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	@Test
	public void testParserTagValidEscaping() {
		String docBody = "{$ = \" \\\" \\\\ \" $}";
		
		SmartScriptParser parser = new SmartScriptParser(docBody);
		
		DocumentNode document = parser.getDocumentNode();
		String originalDocumentBody = SmartScriptTester.createOriginalDocumentBody(document);
		
		assertEquals(docBody, originalDocumentBody);
	}
	
	@Test
	public void testParserTagInvalidEscaping1() {
		String docBody = "{$ = \\\\ $}";
	
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	@Test
	public void testParserTagInvalidEscaping2() {
		String docBody = "{$ = \\\" $}";
	
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	@Test
	public void testParserTagInvalidEscaping3() {
		String docBody = "{$ = \" \\1 \" $}";
	
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	@Test
	public void testParserForLoopTagInvalidParams1() {
		String docBody = "{$ for 1 1 2 3 $} {$ end $}";
	
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	@Test
	public void testParserForLoopTagInvalidParams2() {
		String docBody = "{$ for i $} {$ end $}";
	
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	@Test
	public void testParserForLoopTagInvalidParams3() {
		String docBody = "{$ for * 1 2 3 $} {$ end $}";
	
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	@Test
	public void testParserForLoopTagInvalidParams4() {
		String docBody = "{$ for i 1 2 3 4 5 6 $} {$ end $}";
	
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	@Test
	public void testParserForLoopTagInvalidParams5() {
		String docBody = "{$ for i 1 @sin 3 $} {$ end $}";
	
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	@Test
	public void testParserTags() {
		String docBody = "{$ for i 1 2 3 $} {$ eNd $}";
		SmartScriptParser parser = new SmartScriptParser(docBody);
		
		DocumentNode document = parser.getDocumentNode();
		String originalDocumentBody = SmartScriptTester.createOriginalDocumentBody(document);
		
		SmartScriptParser parser2 = new SmartScriptParser(originalDocumentBody);
		
		DocumentNode document2 = parser2.getDocumentNode();
		String copiedDocumentBody = SmartScriptTester.createOriginalDocumentBody(document2);
		
		assertEquals(copiedDocumentBody, originalDocumentBody);	
	}
	
	@Test
	public void testParserTestDoc1() {
		String docBody = loader("testDoc1.txt");
		
		SmartScriptParser parser = new SmartScriptParser(docBody);
		
		DocumentNode document = parser.getDocumentNode();
		String originalDocumentBody = SmartScriptTester.createOriginalDocumentBody(document);
		
		SmartScriptParser parser2 = new SmartScriptParser(originalDocumentBody);
		
		DocumentNode document2 = parser2.getDocumentNode();
		String copiedDocumentBody = SmartScriptTester.createOriginalDocumentBody(document2);
		
		assertEquals(copiedDocumentBody, originalDocumentBody);	
	}
	
	/* in testDoc2.txt one end tag is missing */
	@Test
	public void testParserTestDoc2() {
		String docBody = loader("testDoc2.txt");
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	/* in testDoc3.txt there is one extra end tag */
	@Test
	public void testParserTestDoc3() {
		String docBody = loader("testDoc3.txt");
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	/* in testDoc4.txt right bound of one tag is missing */
	@Test
	public void testParserTestDoc4() {
		String docBody = loader("testDoc4.txt");
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
	}
	
	/**
	 * Loads document with given filename and returns it
	 * as a String.
	 * 
	 * @param filename name of a file
	 * @return document in String format
	 */
	private String loader(String filename) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		try(InputStream is =
			this.getClass().getClassLoader().getResourceAsStream(filename)) {
			byte[] buffer = new byte[1024];
			while(true) {
				int read = is.read(buffer);
				if(read<1) break;
				bos.write(buffer, 0, read);
			}
			return new String(bos.toByteArray(), StandardCharsets.UTF_8);
		} catch(IOException ex) {
			return null;
		}
	}
	
}
