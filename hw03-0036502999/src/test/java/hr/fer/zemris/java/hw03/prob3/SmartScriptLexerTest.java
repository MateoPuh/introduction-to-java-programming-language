package hr.fer.zemris.java.hw03.prob3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexerException;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptToken;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptTokenType;


public class SmartScriptLexerTest {

	//@Disabled
	@Test
	public void testNotNull() {
		SmartScriptLexer lexer = new SmartScriptLexer("");
		
		assertNotNull(lexer.nextToken(), "Token was expected but null was returned.");
	}
	
	//@Disabled
	@Test
	public void testNullInput() {
		// must throw!
		assertThrows(NullPointerException.class, () -> new SmartScriptLexer(null));
	}
	
	//@Disabled
	@Test
	public void testEmpty() {
		SmartScriptLexer lexer = new SmartScriptLexer("");
		
		assertEquals(SmartScriptTokenType.EOF, lexer.nextToken().getType(), "Empty input must generate only EOF token.");
	}
	
	//@Disabled
	@Test
	public void testGetReturnsLastNext() {
		// Calling getToken once or several times after calling nextToken must return each time what nextToken returned...
		SmartScriptLexer lexer = new SmartScriptLexer("");
		
		SmartScriptToken token = lexer.nextToken();
		assertEquals(token, lexer.getToken(), "getToken returned different token than nextToken.");
		assertEquals(token, lexer.getToken(), "getToken returned different token than nextToken.");
	}
	
	//@Disabled
	@Test
	public void testRadAfterEOF() {
		SmartScriptLexer lexer = new SmartScriptLexer("");
	
		// will obtain EOF
		lexer.nextToken();
		// will throw!
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());
	}
	
	//@Disabled
	@Test
	public void testBlankContent() {
		// When input is only of spaces, tabs, newlines, etc...
		SmartScriptLexer lexer = new SmartScriptLexer("   \r\n\t    ");
		
		// We expect the following stream of tokens
		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TEXT, "   \r\n\t    "),
			//new SmartScriptToken(SmartScriptTokenType.TEXT, "Automobil"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null)
		};
	
		checkTokenStream(lexer, correctData);
	}
	
	//@Disabled
	@Test
	public void testTwoWords() {
		// Lets check for several words...
		SmartScriptLexer lexer = new SmartScriptLexer("  Štefanija\r\n\t Automobil   ");
	
		// We expect the following stream of tokens
		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TEXT, "  Štefanija\r\n\t Automobil   "),
			//new SmartScriptToken(SmartScriptTokenType.TEXT, "Automobil"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null)
		};
	
		checkTokenStream(lexer, correctData);
	}
	
	//@Disabled
	@Test
	public void testValidEscaping() {
		// Lets check for several words...
		SmartScriptLexer lexer = new SmartScriptLexer("  Štefanija\r\n\t Automobil  \\{");
	
		// We expect the following stream of tokens
		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TEXT, "  Štefanija\r\n\t Automobil  {"),
			//new SmartScriptToken(SmartScriptTokenType.TEXT, "Automobil"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null)
		};
	
		checkTokenStream(lexer, correctData);
	}
		
	//@Disabled
	@Test
	public void testValidEscaping2() {
		// Lets check for several words...
		SmartScriptLexer lexer = new SmartScriptLexer("  Štefanija\r\n\t Automobil  \\\\ ");
	
		// We expect the following stream of tokens
		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TEXT, "  Štefanija\r\n\t Automobil  \\ "),
			//new SmartScriptToken(SmartScriptTokenType.TEXT, "Automobil"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null)
		};
	
		checkTokenStream(lexer, correctData);
	}
	
	//@Disabled
	@Test
	public void testInvalidEscaping() {
		// Lets check for several words...
		SmartScriptLexer lexer = new SmartScriptLexer("  Štefanija\r\n\t Automobil  \\");
		
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());
	}
	
	//@Disabled
	@Test
	public void testInvalidEscaping2() {
		// Lets check for several words...
		SmartScriptLexer lexer = new SmartScriptLexer("\\  Štefanija\r\n\t Automobil  \\");
		
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());
	}
	
	//@Disabled
	@Test
	public void testLeftBound() {
		// Lets check for several words...
		SmartScriptLexer lexer = new SmartScriptLexer(" \\{ \\{$ Štefanija\r\n\t Automobil  \\\\ {$");
	
		SmartScriptToken correctData[] = {
				new SmartScriptToken(SmartScriptTokenType.TEXT, " { {$ Štefanija\r\n\t Automobil  \\ "),	
				new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
				new SmartScriptToken(SmartScriptTokenType.EOF, null),	
			};	
			
		checkTokenStream(lexer, correctData);
	}
	
	//@Disabled
	@Test
	public void testInvalidEscape() {
		SmartScriptLexer lexer = new SmartScriptLexer("   \\a    ");
	
		// will throw!
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());
	}
	
	//@Disabled
	@Test
	public void testOnlyLeftBound() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$");

		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),	
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}

	@Test
	public void testLeftBoundAndFor() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ For");

		
		// will throw!
		// We expect the following stream of tokens
		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_FOR, "For"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testLeftBoundAndEquals() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ =");

		
		// will throw!
		// We expect the following stream of tokens
		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_ECHO, "="),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testLeftBoundAndOtherName() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ marko");

		
		// will throw!
		// We expect the following stream of tokens
		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME, "marko"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testLeftBoundAndEnd() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ eNd");

		
		// will throw!
		// We expect the following stream of tokens
		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_END, "eNd"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testTagNoArgs() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ For $}");

		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_FOR, "For"),
			new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testTagNumberArgs() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ For 1 4.56 $}");

		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_FOR, "For"),
			new SmartScriptToken(SmartScriptTokenType.INTEGER_NUMBER, 1),
			new SmartScriptToken(SmartScriptTokenType.DOUBLE_NUMBER, 4.56),
			new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testTagNegativeNumberArgs() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ For -4.56 $}");

		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_FOR, "For"),
			new SmartScriptToken(SmartScriptTokenType.DOUBLE_NUMBER, -4.56),
			new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testTagStringArgs() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ For \"-4.56\" $}");

		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_FOR, "For"),
			new SmartScriptToken(SmartScriptTokenType.STRING, "-4.56"),
			new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testTagFunctionArgs() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ For @sin $}");

		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_FOR, "For"),
			new SmartScriptToken(SmartScriptTokenType.FUNCTION, "@sin"),
			new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testTagOperatorArgs() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ For + $}");

		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_FOR, "For"),
			new SmartScriptToken(SmartScriptTokenType.OPERATOR, "+"),
			new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testMinus() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ For - -1 4-5.8 $}");

		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_FOR, "For"),
			new SmartScriptToken(SmartScriptTokenType.OPERATOR, "-"),
			new SmartScriptToken(SmartScriptTokenType.INTEGER_NUMBER, -1),
			new SmartScriptToken(SmartScriptTokenType.INTEGER_NUMBER, 4),
			new SmartScriptToken(SmartScriptTokenType.DOUBLE_NUMBER, -5.8),
			new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}

	@Test
	public void testEcho() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ = 1 1.3 -4 \"tttt\" $}");

		
		// will throw!
		// We expect the following stream of tokens
		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_ECHO, "="),
			new SmartScriptToken(SmartScriptTokenType.INTEGER_NUMBER, 1),
			new SmartScriptToken(SmartScriptTokenType.DOUBLE_NUMBER, 1.3),
			new SmartScriptToken(SmartScriptTokenType.INTEGER_NUMBER, -4),
			new SmartScriptToken(SmartScriptTokenType.STRING, "tttt"),
			new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testEcho2() {
		SmartScriptLexer lexer = new SmartScriptLexer("	ovo je tekst\r\n	{$ =  i 4 5 $}");

		
		// will throw!
		// We expect the following stream of tokens
		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TEXT, "	ovo je tekst\r\n	"),
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_ECHO, "="),
			new SmartScriptToken(SmartScriptTokenType.VARIABLE, "i"),
			new SmartScriptToken(SmartScriptTokenType.INTEGER_NUMBER, 4),
			new SmartScriptToken(SmartScriptTokenType.INTEGER_NUMBER, 5),
			new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testTagStringArgsEscape() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ For \"testiranje \\\"escapeanja\\\"\" $}");

		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_FOR, "For"),
			new SmartScriptToken(SmartScriptTokenType.STRING, "testiranje \"escapeanja\""),
			new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testTagStringArgsEscape2() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ For \"testiranje \\\\ escapeanja\" $}");

		SmartScriptToken correctData[] = {
			new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
			new SmartScriptToken(SmartScriptTokenType.TAG_NAME_FOR, "For"),
			new SmartScriptToken(SmartScriptTokenType.STRING, "testiranje \\ escapeanja"),
			new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
			new SmartScriptToken(SmartScriptTokenType.EOF, null),	
		};	
		
		checkTokenStream(lexer, correctData);
	}
	

	@Test
	public void testTagStringArgsInvalidEscape() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$ For \"escapeanje \\1 fail\" $}");

		lexer.nextToken();
		lexer.nextToken();
		
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());
	}
	
	@Test
	public void testTextAndTags() {
		SmartScriptLexer lexer = 
				new SmartScriptLexer("Ovo je tekst \\{$ \n {$=   \"ovo je string unutar taga\" $} opet tekst");

		SmartScriptToken correctData[] = {
				new SmartScriptToken(SmartScriptTokenType.TEXT, "Ovo je tekst {$ \n "),
				new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, "{$"),
				new SmartScriptToken(SmartScriptTokenType.TAG_NAME_ECHO, "="),
				new SmartScriptToken(SmartScriptTokenType.STRING, "ovo je string unutar taga"),
				new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, "$}"),
				new SmartScriptToken(SmartScriptTokenType.TEXT, " opet tekst"),
				new SmartScriptToken(SmartScriptTokenType.EOF, null),	
			};	
			
			checkTokenStream(lexer, correctData);
	}
	
	// Helper method for checking if lexer generates the same stream of tokens
	// as the given stream.
	private void checkTokenStream(SmartScriptLexer lexer, SmartScriptToken[] correctData) {
		int counter = 0;
		for(SmartScriptToken expected : correctData) {
			SmartScriptToken actual = lexer.nextToken();
			String msg = "Checking token "+counter + ":";
			assertEquals(expected.getType(), actual.getType(), msg);
			assertEquals(expected.getValue(), actual.getValue(), msg);
			counter++;
		}
	}
	
	@Test
	public void testNullState() {
		assertThrows(NullPointerException.class, () -> new SmartScriptLexer("").setState(null));
	}
	
	@Test
	public void curlyBracketsTest() {
		SmartScriptLexer lexer = new SmartScriptLexer("{");

		assertEquals(new SmartScriptToken(SmartScriptTokenType.TEXT, "{").getValue(), 
				(String)lexer.nextToken().getValue());
		
	}
	
	
}