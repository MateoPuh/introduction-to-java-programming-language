package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;

public class ElementsGetterTest {

	@Test
	public void elementsGetterTestArray() {
		Collection col = new ArrayIndexedCollection();
		col.add("Ivo");
		col.add("Ana");
		col.add("Jasna");
		ElementsGetter getter = col.createElementsGetter();
		assertTrue(getter.hasNextElement());
		assertEquals("Ivo", (String)getter.getNextElement());
		assertTrue(getter.hasNextElement());
		assertEquals("Ana", (String)getter.getNextElement());
		assertTrue(getter.hasNextElement());
		assertEquals("Jasna", (String)getter.getNextElement());
		assertFalse(getter.hasNextElement());
		assertThrows(NoSuchElementException.class, () -> getter.getNextElement());
	}
	
	@Test
	public void elementsGetterTestList() {
		Collection col = new LinkedListIndexedCollection();
		col.add("Ivo");
		col.add("Ana");
		col.add("Jasna");
		ElementsGetter getter = col.createElementsGetter();
		assertTrue(getter.hasNextElement());
		assertEquals("Ivo", (String)getter.getNextElement());
		assertTrue(getter.hasNextElement());
		assertEquals("Ana", (String)getter.getNextElement());
		assertTrue(getter.hasNextElement());
		assertEquals("Jasna", (String)getter.getNextElement());
		assertFalse(getter.hasNextElement());
		assertThrows(NoSuchElementException.class, () -> getter.getNextElement());
	}
	
	@Test
	public void elementsGetterTest2Array() {
		Collection col = new ArrayIndexedCollection();
		col.add("Ivo");
		col.add("Ana");
		col.add("Jasna");
		ElementsGetter getter = col.createElementsGetter();
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertEquals("Ivo", (String)getter.getNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertEquals("Ana", (String)getter.getNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertEquals("Jasna", (String)getter.getNextElement());
		assertFalse(getter.hasNextElement());
		assertFalse(getter.hasNextElement());
	}
	
	@Test
	public void elementsGetterTest2List() {
		Collection col = new LinkedListIndexedCollection();
		col.add("Ivo");
		col.add("Ana");
		col.add("Jasna");
		ElementsGetter getter = col.createElementsGetter();
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertEquals("Ivo", (String)getter.getNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertEquals("Ana", (String)getter.getNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertTrue(getter.hasNextElement());
		assertEquals("Jasna", (String)getter.getNextElement());
		assertFalse(getter.hasNextElement());
		assertFalse(getter.hasNextElement());
	}
	
	@Test
	public void elementsGetterTest3Array() {
		Collection col = new ArrayIndexedCollection();
		col.add("Ivo");
		col.add("Ana");
		col.add("Jasna");
		ElementsGetter getter1 = col.createElementsGetter();
		ElementsGetter getter2 = col.createElementsGetter();
		assertEquals("Ivo", (String)getter1.getNextElement());
		assertEquals("Ana", (String)getter1.getNextElement());
		assertEquals("Ivo", (String)getter2.getNextElement());
		assertEquals("Jasna", (String)getter1.getNextElement());
		assertEquals("Ana", (String)getter2.getNextElement());
	}
	
	@Test
	public void elementsGetterTest3List() {
		Collection col = new LinkedListIndexedCollection();
		col.add("Ivo");
		col.add("Ana");
		col.add("Jasna");
		ElementsGetter getter1 = col.createElementsGetter();
		ElementsGetter getter2 = col.createElementsGetter();
		assertEquals("Ivo", (String)getter1.getNextElement());
		assertEquals("Ana", (String)getter1.getNextElement());
		assertEquals("Ivo", (String)getter2.getNextElement());
		assertEquals("Jasna", (String)getter1.getNextElement());
		assertEquals("Ana", (String)getter2.getNextElement());
	}
	
	@Test
	public void elementsGetterTest4Array() {
		Collection col1 = new ArrayIndexedCollection();
		Collection col2 = new ArrayIndexedCollection();
		col1.add("Ivo");
		col1.add("Ana");
		col1.add("Jasna");
		col2.add("Jasmina");
		col2.add("Štefanija");
		col2.add("Karmela");
		ElementsGetter getter1 = col1.createElementsGetter();
		ElementsGetter getter2 = col1.createElementsGetter();
		ElementsGetter getter3 = col2.createElementsGetter();
		assertEquals("Ivo", (String)getter1.getNextElement());
		assertEquals("Ana", (String)getter1.getNextElement());
		assertEquals("Ivo", (String)getter2.getNextElement());
		assertEquals("Jasmina", (String)getter3.getNextElement());
		assertEquals("Štefanija", (String)getter3.getNextElement());
	}
	
	@Test
	public void elementsGetterTest4AList() {
		Collection col1 = new LinkedListIndexedCollection();
		Collection col2 = new LinkedListIndexedCollection();
		col1.add("Ivo");
		col1.add("Ana");
		col1.add("Jasna");
		col2.add("Jasmina");
		col2.add("Štefanija");
		col2.add("Karmela");
		ElementsGetter getter1 = col1.createElementsGetter();
		ElementsGetter getter2 = col1.createElementsGetter();
		ElementsGetter getter3 = col2.createElementsGetter();
		assertEquals("Ivo", (String)getter1.getNextElement());
		assertEquals("Ana", (String)getter1.getNextElement());
		assertEquals("Ivo", (String)getter2.getNextElement());
		assertEquals("Jasmina", (String)getter3.getNextElement());
		assertEquals("Štefanija", (String)getter3.getNextElement());
	}
	
	@Test
	public void elementsGetterTest5Array() {
		Collection col = new ArrayIndexedCollection();
		col.add("Ivo");
		col.add("Ana");
		col.add("Jasna");
		ElementsGetter getter = col.createElementsGetter();
		assertEquals("Ivo", (String)getter.getNextElement());
		assertEquals("Ana", (String)getter.getNextElement());
		col.clear();
		assertThrows(ConcurrentModificationException.class, () -> getter.getNextElement());
	}
	
	@Test
	public void elementsGetterTest5List() {
		Collection col = new LinkedListIndexedCollection();
		col.add("Ivo");
		col.add("Ana");
		col.add("Jasna");
		ElementsGetter getter = col.createElementsGetter();
		assertEquals("Ivo", (String)getter.getNextElement());
		assertEquals("Ana", (String)getter.getNextElement());
		col.clear();
		assertThrows(ConcurrentModificationException.class, () -> getter.getNextElement());
	}
	
	@Test
	public void elementsGetterTest6Array() {
		Collection col = new ArrayIndexedCollection();
		col.add("Ivo");
		col.add("Ana");
		col.add("Jasna");
		ElementsGetter getter = col.createElementsGetter();
		getter.getNextElement();
		
		Collection col2 = new ArrayIndexedCollection();
		
		getter.processRemaining((value) ->  {
			col2.add(value);
		});
		
		assertTrue(col2.contains("Ana"));
		assertTrue(col2.contains("Jasna"));
	}
	
	@Test
	public void elementsGetterTest6List() {
		Collection col = new LinkedListIndexedCollection();
		col.add("Ivo");
		col.add("Ana");
		col.add("Jasna");
		ElementsGetter getter = col.createElementsGetter();
		getter.getNextElement();
				
		Collection col2 = new LinkedListIndexedCollection();
		
		getter.processRemaining((value) ->  {
			col2.add(value);
		});
		
		assertTrue(col2.contains("Ana"));
		assertTrue(col2.contains("Jasna"));
	}
	
}
