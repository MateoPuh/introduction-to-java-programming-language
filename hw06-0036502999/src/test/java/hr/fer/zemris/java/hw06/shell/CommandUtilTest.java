package hr.fer.zemris.java.hw06.shell;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class CommandUtilTest {
	
	@Test
	public void testSplitArgumentsByBlanks() {
		String arguments = "  pom.xml src/main/resources \t 5";
		String[] correct = new String[] {"pom.xml", "src/main/resources", "5"};
		
		String[] actual = CommandUtil.splitArgumentsByBlanks(arguments);		
		for(int i = 0; i < 3; i++) {
			assertEquals(correct[i], actual[i]);
		}
	}
	
	@Test
	public void testSplitArgumentsByBlanksAndQuotes() {
		String arguments = "  pom.xml \"src/main/resources\" \t 5";
		String[] correct = new String[] {"pom.xml", "\"src/main/resources\"", "5"};
		
		String[] actual = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);		
		for(int i = 0; i < 3; i++) {
			assertEquals(correct[i], actual[i]);
		}
	}

}
