package hr.fer.zemris.java.hw06.crypto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class UtilTest {
	
	@Test
	public void hextobyteTest() {
		byte[] correct = new byte[] {1, -82, 34};
		
		byte[] actual = Util.hextobyte("01aE22");
		
		for(int i = 0; i < correct.length; i++) {
			assertEquals(correct[i], actual[i]);
		}
	}
	
	@Test
	public void bytetohexTest() {
		String correct = "01ae22";
		String actual = Util.bytetohex(new byte[] {1, -82, 34});
		
		assertEquals(correct, actual);
	}

}
