package hr.fer.zemris.java.hw06.shell;

/**
 * Command-line shell program that reads user's input
 * ad outputs response to given commands.
 * 
 * <p>Commands that exists in {@link MyShell} are
 * ls, charsets, cat, tree, copy, mkdir, hexdump,
 * exit and symbol.</p>  
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class MyShell {

	/**
	 * This method is first run after program is started.
	 * It reads user's input until user enters exit command.
	 * 
	 * @param args command-line arguments
	 */
	public static void main(String[] args) {
		Environment env = new ShellEnvironment();
		
		env.writeln("Welcome to MyShell v 1.0");
		
		while(true) {
			try {
				String l = env.readLine().trim();
				int index = l.indexOf(' ');
				String commandName = l.substring(0, index != -1 ? index : l.length());
				String arguments = index != -1 ?
						l.substring(index, l.length()).trim() : "";
				ShellCommand command = env.commands().get(commandName);
				if(command == null) {
					env.writeln("Invalid command: " + commandName);
					continue;
				}
				
				ShellStatus s = command.executeCommand(env, arguments);
				if(s.equals(ShellStatus.TERMINATE)) {
					break;
				}
			} catch(ShellIOException e) {
				System.out.println(e.getLocalizedMessage());
				break;
			}
		}
	}
	
}
