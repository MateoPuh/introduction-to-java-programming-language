package hr.fer.zemris.java.hw06.shell;

import java.util.Collections;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.zemris.java.hw06.shell.commands.CatCommand;
import hr.fer.zemris.java.hw06.shell.commands.CharsetsCommand;
import hr.fer.zemris.java.hw06.shell.commands.CopyCommand;
import hr.fer.zemris.java.hw06.shell.commands.ExitCommand;
import hr.fer.zemris.java.hw06.shell.commands.HelpCommand;
import hr.fer.zemris.java.hw06.shell.commands.HexdumpCommand;
import hr.fer.zemris.java.hw06.shell.commands.LsCommand;
import hr.fer.zemris.java.hw06.shell.commands.MkdirCommand;
import hr.fer.zemris.java.hw06.shell.commands.SymbolCommand;
import hr.fer.zemris.java.hw06.shell.commands.TreeCommand;

/**
 * Implements {@link Environment} that uses console
 * for communication between {@link ShellCommand} 
 * and user.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ShellEnvironment implements Environment {

	/** Default prompt symbol. */
	private static final char DEFAULT_PROMPT_SYMBOL = '>';
	/** Default symbol for extending command in multiple lines. */
	private static final char DEFAULT_MORELINES_SYMBOL = '\\';
	/** Default symbol for multiline command. */
	private static final char DEFAULT_MULTILINE_SYMBOL = '|';

	/** Prompt symbol. */
	private char promptSymbol = DEFAULT_PROMPT_SYMBOL;
	/** Morelines symbol. */
	private char morelinesSymbol = DEFAULT_MORELINES_SYMBOL;
	/** Multiline symbol. */
	private char multilineSymbol = DEFAULT_MULTILINE_SYMBOL;

	/** Scanner used for getting user's input. */
	private Scanner sc;
	/** Map of commands. */
	private SortedMap<String, ShellCommand> commands;
	
	{
		commands = new TreeMap<>();
		commands.put("exit", new ExitCommand()); 
		commands.put("ls", new LsCommand());
		commands.put("cat", new CatCommand());
		commands.put("charsets", new CharsetsCommand());
		commands.put("copy", new CopyCommand());
		commands.put("help", new HelpCommand());
		commands.put("hexdump", new HexdumpCommand());
		commands.put("mkdir", new MkdirCommand());
		commands.put("symbol", new SymbolCommand());
		commands.put("tree", new TreeCommand());
	}
	
	/**
	 * Creates new {@link ShellEnvironment}.
	 */
	public ShellEnvironment() {
		sc = new Scanner(System.in);
	}
	
	@Override
	public String readLine() throws ShellIOException {
		try {
			System.out.print(promptSymbol + " ");
			String in = sc.nextLine().strip();
			
			while(in.endsWith(String.valueOf(morelinesSymbol))) {
				in = in.substring(0, in.length() - 1);
				in += " ";
				System.out.print(multilineSymbol + " ");
				in += sc.nextLine().strip();
			}
			
			return in;
		} catch(Exception e) {
			throw new ShellIOException(e.getLocalizedMessage());
		}
	}

	@Override
	public void write(String text) throws ShellIOException {
		try {
			System.out.print(text);
		} catch(Exception e) {
			throw new ShellIOException(e.getLocalizedMessage());
		}
	}

	@Override
	public void writeln(String text) throws ShellIOException {
		try {
			System.out.println(text); 
		} catch(Exception e) {
			throw new ShellIOException(e.getLocalizedMessage());
		}
	}

	@Override
	public SortedMap<String, ShellCommand> commands() {
		return Collections.unmodifiableSortedMap(commands);
	}

	@Override
	public Character getMultilineSymbol() {
		return multilineSymbol;
	}

	@Override
	public void setMultilineSymbol(Character symbol) {
		this.multilineSymbol = symbol;
	}

	@Override
	public Character getPromptSymbol() {
		return promptSymbol;
	}

	@Override
	public void setPromptSymbol(Character symbol) {
		this.promptSymbol = symbol;
	}

	@Override
	public Character getMorelinesSymbol() {
		return morelinesSymbol;
	}

	@Override
	public void setMorelinesSymbol(Character symbol) {
		this.morelinesSymbol = symbol;
	}

}
