package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents exit command.
 * It takes no arguments. It closes shell.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ExitCommand implements ShellCommand{

	/** Exit command name. */
	private static final String NAME = "exit"; 
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments == null || arguments.trim().isEmpty()) {
			return ShellStatus.TERMINATE;
		}
		
		env.writeln("Command \'exit\' takes no arguments.");
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return NAME;
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new ArrayList<>();
		
		description.add("exit");
		description.add("");
		description.add("Command \'exit\' terminates shell.");

		return Collections.unmodifiableList(description);
	}

}
