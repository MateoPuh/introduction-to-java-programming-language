package hr.fer.zemris.java.hw06.shell;

/**
 * Enumeration of status of {@link MyShell}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public enum ShellStatus {
	
	/** Status that keeps shell running. */
	CONTINUE,
	
	/** Status that closes shell. */
	TERMINATE
	
}
