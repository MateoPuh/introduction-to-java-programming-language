package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents symbol command.
 * It takes one or two arguments. First argument is
 * symbol name and second is new value for symbol with
 * given symbol name. If one argument is given, then 
 * symbol with symbol name is displayed. If two arguments 
 * are given, then symbol with symbol name is replaces
 * with new value. 
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class SymbolCommand implements ShellCommand{

	/** Symbol command name. */
	private static final String NAME = "symbol"; 
	/** Minimal number of arguments for symbol command. */
	private static final int MIN_NUMBER_OF_ARGUMENTS = 1;
	/** Maximal number of arguments for symbol command. */
	private static final int MAX_NUMBER_OF_ARGUMENTS = 2;
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanks(arguments);
		
		// wrong number of arguments
		if(argument.length < MIN_NUMBER_OF_ARGUMENTS 
				|| argument.length > MAX_NUMBER_OF_ARGUMENTS) {
			env.writeln("Command \'symbol\' takes 1 or 2 arguments.");
			return ShellStatus.CONTINUE;
		}
		
		boolean switchSymbols = false;
		Character newSymbol = null;
		
		// get new value of symbol if it is given
		if(argument.length == MAX_NUMBER_OF_ARGUMENTS) {
			if(argument[1].length() == 1) {
				newSymbol = argument[1].charAt(0);
			} else {
				env.writeln(argument[1] + " is not a valid symbol.");
				return ShellStatus.CONTINUE;
			}
			switchSymbols = true;
		}
		
		Character symbol = null;
		switch(argument[0]) {
		case "PROMPT":
			symbol = env.getPromptSymbol();
			if(switchSymbols) {
				env.setPromptSymbol(newSymbol);
			}
			break;
		case "MORELINES":
			symbol = env.getMorelinesSymbol();
			if(switchSymbols) {
				env.setMorelinesSymbol(newSymbol);
			}
			break;
		case "MULTILINE":
			symbol = env.getMultilineSymbol();
			if(switchSymbols) {
				env.setMultilineSymbol(newSymbol);
			}
			break;
		default:
			env.writeln(argument[0] + " is not a valid symbol name.");
			return ShellStatus.CONTINUE;
		}
		
		if(switchSymbols) {
			env.writeln("Symbol for " + argument[0] + " changed from \'" 
					+ symbol + "\' to \'" + argument[1] + "\'");
		} else {
			env.writeln("Symbol for " + argument[0] + " is \'" + symbol + "\'");
		}
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return NAME;
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new ArrayList<>();
		
		description.add("symbol [SYMBOL_NAME] [value]");
		description.add("");
		description.add("-SYMBOL_NAME : name of a symbol");
		description.add("-value : new value of a symbol");
		description.add("");
		description.add("Command \'symbol\' displays the symbol for a given symbol name,");
		description.add("which can be PROMPTSYMBOL, MORELINESSYMBOL or MULTILINESYMBOL,");
		description.add("if single argument is given.");
		description.add("If two arguments are given, this command changes the current");
		description.add("symbol to a given one.");

		return Collections.unmodifiableList(description);
	}

}
