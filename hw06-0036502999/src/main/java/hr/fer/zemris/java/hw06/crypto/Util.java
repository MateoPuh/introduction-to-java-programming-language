package hr.fer.zemris.java.hw06.crypto;

import java.util.Objects;

/**
 * This class contains method used for converting
 * hex-encoded text to byte array and vice-versa.
 * 
 * <p>The conversion is done in a way that every byte is 
 * converted to two hex numbers since each hex number is
 * 4 bits long.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Util {

	/**
	 * Method used for converting hex-encoded text to byte array.
	 * Two hex numbers are converted to one byte. If length of a
	 * text is zero, then byte array with length 0 is returned.
	 * 
	 * @param keyText hex-encoded text
	 * @return byte array
	 * @throws NullPointerException if given keyText is null
	 * @throws IllegalArgumentException if given keyText is not
	 * 		in valid hex format or if it does not contain even 
	 * 		number of hex numbers
	 */
	public static byte[] hextobyte(String keyText) throws IllegalArgumentException{
		Objects.requireNonNull(keyText);
		if(!keyText.matches("-?[0-9a-fA-F]+") || (keyText.length() % 2 != 0)) {
			throw new IllegalArgumentException("Invalid HEX format.");
		}
		
		if(keyText.length() == 0) {
			return new byte[0];
		}
		
		int arrayLength = keyText.length() / 2;
		byte[] result = new byte[arrayLength];
		
		for(int i = 0; i < arrayLength; i++) {
			int b = Integer.parseInt(keyText.substring(2*i, 2*i+2), 16);
			result[i] = (byte)b;
		}
		
		return result;
	}
	
	/**
	 * Method used for converting byte array to hex-encoded text.
	 * Each byte is converted to two hex numbers. If length of a
	 * byte array is zero, then empty string is returned.
	 * 
	 * @param bytearray byte array
	 * @return hex-encoded text
	 * @throws NullPointerException if given bytearray is null
	 */
	public static String bytetohex(byte[] bytearray) {
		Objects.requireNonNull(bytearray);
		
		StringBuilder sb = new StringBuilder();
		for(byte b : bytearray) {
            sb.append(String.format("%02x", b));
		}
		
		return sb.toString();
	}
	
}
