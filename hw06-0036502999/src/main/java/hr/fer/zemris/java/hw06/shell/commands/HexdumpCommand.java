package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents hexdump command.
 * It takes one command: filename. It produces hex-output
 * for file with given filename.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class HexdumpCommand implements ShellCommand {

	/** Hexdump command name. */
	private static final String NAME = "hexdump"; 
	/** Number of arguments for hexdump command. */
	private static final int NUMBER_OF_ARGUMENTS = 1;
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);

		if(arguments == null 
				|| arguments.trim().isEmpty()
				|| argument.length != NUMBER_OF_ARGUMENTS) {
			env.writeln("Command \'hexdump\' expects 1 argument.");
			return ShellStatus.CONTINUE;
		}

		if(!CommandUtil.exists(argument[0])) {
			env.writeln("File " + arguments + " does not exist.");
			return ShellStatus.CONTINUE;
		}
		
		if(!CommandUtil.isFile(argument[0])) {
			env.writeln("Command \'hexdump\' expects file name as argument.");
			return ShellStatus.CONTINUE;
		}
		
		Path file = Paths.get(argument[0]);
		
		try {
			InputStream is = Files.newInputStream(file);
			
			byte[] buff = new byte[4 * 1024];
			byte[] printBytes = new byte[16];
			String hexRow = "";
			int counter = 0;

			while(true) {
				int r = is.read(buff);
				if(r<1) break;
				long numOfRow = 0;
				for(int i = 0; i < r; i++) {
					byte b = buff[i];
					if(counter == 0) {
						env.write(String.format("%08x: ", numOfRow * 16));
					}
					hexRow += String.format("%02x ", b );
					//env.writef("%02x ", b );//((b > 32 && b < 127) ? b : ".");
					printBytes[counter] = (byte) (b > 32 && b < 127 ? b : '.');
					if(counter == 7) {
						env.write(String.format("%19s", hexRow));
						env.write("| ");
						hexRow = "";
					}
					
					counter++;
					if(counter >= 16) {
						env.write(String.format("%19s", hexRow));
						env.write("| ");
						hexRow = "";
						counter = 0;
						numOfRow++;
						env.write(new String(printBytes));
						printBytes = new byte[16];
						env.writeln("");
					}
				}
			}
			
			if(!hexRow.isEmpty()) {
				env.write(String.format("%-24s", hexRow));
				env.write("| ");
				if(counter < 7) {
					env.write(String.format("%26s", "| "));
				}
				env.writeln(new String(printBytes));
			}
			
			env.writeln("");			
		} catch (IOException e) {
			env.writeln("Error occured while executing \'hexdump\':");
			env.writeln(e.getLocalizedMessage());
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return NAME;
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new ArrayList<>();
		
		description.add("hexdump [file]");
		description.add("");
		description.add("-file : name of a file");
		description.add("");
		description.add("Command \'hexdump\' expects single argument: file name, for");
		description.add("which it produces hex-output.");
		description.add("");
		description.add("In each line, it displays ordinal number in hex-format,");
		description.add("hex-output for 16 characters and those 16 characters.");

		return Collections.unmodifiableList(description);
	}

}
