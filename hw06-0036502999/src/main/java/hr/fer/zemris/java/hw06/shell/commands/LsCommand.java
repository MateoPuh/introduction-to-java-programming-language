package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents ls command.
 * It takes one argument: filename. It displays directory 
 * listing for file with given filename.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class LsCommand implements ShellCommand{

	/** Ls command name. */
	private static final String NAME = "ls"; 
	/** Number of arguments of ls command. */
	private static final int NUMBER_OF_ARGUMENTS = 1;
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);

		// wrong number of arguments
		if(argument.length != NUMBER_OF_ARGUMENTS) {
			env.writeln("Command \'ls\' takes 1 argument.");
			return ShellStatus.CONTINUE;
		}
		
		// directory does not exist
		if(!CommandUtil.exists(argument[0])) {
			env.writeln(argument[0] + " does not exist.");
			return ShellStatus.CONTINUE;
		}
		
		// given path is file
		if(CommandUtil.isFile(argument[0])) {
			env.writeln(argument[0] + " is a file.");
			return ShellStatus.CONTINUE;
		}
		
		Path path = Paths.get(argument[0]);
		
		try {
			List<Path> content = Files.list(path).collect(Collectors.toList());
			
			for(Path p : content) {
				env.write(Files.isDirectory(p) ? "d" : "-");
				env.write(Files.isReadable(p) ? "r" : "-");
				env.write(Files.isWritable(p) ? "w" : "-");
				env.write(Files.isExecutable(p) ? "x" : "-");
				
				long size = Files.size(p);
				env.write(String.format("%10d", size));
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				BasicFileAttributeView faView = Files.getFileAttributeView(
						p, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS
				);
				BasicFileAttributes attributes = faView.readAttributes();
				FileTime fileTime = attributes.creationTime();
				String formattedDateTime = sdf.format(new Date(fileTime.toMillis()));
				env.write(" " + formattedDateTime);
				
				env.write(" " + p.getFileName());
				env.write("\n");
			}
		} catch(IOException e) {
			env.writeln("Error while accessing directory " + path);
			env.writeln(e.getLocalizedMessage());
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return NAME;
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new ArrayList<>();
		
		description.add("ls [directory]");
		description.add("");
		description.add("-directory : name of a directory");
		description.add("");
		description.add("Command \'tree\' takes one argument: directory name, for");
		description.add("which it displays directory listing.");

		return Collections.unmodifiableList(description);
	}

}
