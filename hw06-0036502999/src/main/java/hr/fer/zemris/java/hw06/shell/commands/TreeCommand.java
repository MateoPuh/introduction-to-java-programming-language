package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents tree command.
 * It takes one argument: directory name. It prints tree
 * structure of directory and all of it's kids recursively.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class TreeCommand implements ShellCommand {

	/** Tree command name. */
	private static final String NAME = "tree"; 
	/** Number of arguments of tree command. */
	private static final int NUMBER_OF_ARGUMENTS = 1;
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);

		// wrong mumber of arguments
		if(argument.length != NUMBER_OF_ARGUMENTS) {
			env.writeln("Command \'tree\' takes 1 argument.");
			return ShellStatus.CONTINUE;
		}
		
		// if given directory does not exist
		if(!CommandUtil.exists(argument[0])) {
			env.writeln(argument[0] + " does not exist.");
			return ShellStatus.CONTINUE;
		}
		
		// if given path is not directory
		if(CommandUtil.isFile(argument[0])) {
			env.writeln(argument[0] + " is a file.");
			return ShellStatus.CONTINUE;
		}
		
		Path path = Paths.get(argument[0]);
		printTreeRecursively(path, 0, env);
		
		return ShellStatus.CONTINUE;
	}
	
	/**
	 * Prints tree recursively.
	 * 
	 * @param dir current directory
	 * @param level depth
	 * @param env environment for writing tree
	 */
	private static void printTreeRecursively(Path dir, int level, Environment env) {
		try {
			List<Path> content = Files.list(dir).collect(Collectors.toList());
			
			for(Path p : content) {
				env.writeln(" ".repeat(level * 2) + p.getFileName());
				
				if(Files.isDirectory(p)) {
					printTreeRecursively(p, level + 1, env);
				}
			}
		} catch (IOException e) {
			env.writeln("Error occured while printing directory tree: " + e.getLocalizedMessage());
		}
	}

	@Override
	public String getCommandName() {
		return NAME;
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new ArrayList<>();
		
		description.add("tree [directory]");
		description.add("");
		description.add("-directory : name of a directory");
		description.add("");
		description.add("Command \'tree\' takes one argument: directory name, for");
		description.add("which it displays tree.");

		return Collections.unmodifiableList(description);
	}

}
