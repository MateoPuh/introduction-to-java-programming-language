package hr.fer.zemris.java.hw06.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class represents program used for encrypting and decrypting
 * file using the AES crypto-algorithm and the 128-bit encryption key 
 * or calculating and checking the SHA-256 file digest.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Crypto {
	
	/**
	 * This method is first run when program is started.
	 * 
	 * @param args command-line arguments
	 */
	public static void main(String[] args) {
		if(args.length < 2 || args.length > 3) {
			System.out.println("Wrong number of arguments");
			System.exit(1);
		}
				
		if(args.length == 2 && args[0].equals("checksha")) {	
			System.out.println("Please provide expected sha-256 digest for " + args[1] + ":");
	    	Scanner input = new Scanner(System.in);
	    	
	    	String userSha = input.nextLine();
			
	    	input.close();
		    	try {
		    	String actualSha = calculateSha(args[1]);
				if(userSha.equals(actualSha)) {
					System.out.println(
							"Digesting completed. Digest of " + args[1] + " matches expected digest."
					);
				} else {
					System.out.println(
							"Digesting completed. Digest of " + args[1] + " does not match the expected digest. "
								+ "\nDigest was: " + actualSha
					);
				}
				
				System.exit(0);

			} catch(Exception e) {
				System.out.println("Error occured: " + e.getMessage());
				System.exit(1);
			}
		}
		
		if(args.length == 3 && args[0].equals("encrypt")) {
	    	Scanner input = new Scanner(System.in);
	    	System.out.println(
	    			"Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits):\r\n"  
	    	);
	    	String password = input.nextLine();
	    	System.out.println(
	    			"Please provide initialization vector as hex-encoded text (32 hex-digits):\r\n"
	    	);
	    	String initVector = input.nextLine();
		    try {
				crypt(args[1], args[2], password, initVector, true);
				System.out.println(
						"Encryption completed. Generated file " + args[2] + " based on file " + args[1] + ".\r\n"  
				);
				System.exit(0);
			} catch(Exception e) {
				System.out.println("Error occured: " + e.getMessage());
			}
			input.close();

		}
		
		if(args.length == 3 && args[0].equals("decrypt")) {
			Scanner input = new Scanner(System.in);
	    	System.out.println(
	    			"Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits):\r\n"  
	    	);
	    	String password = input.nextLine();
	    	System.out.println(
	    			"Please provide initialization vector as hex-encoded text (32 hex-digits):\r\n"
	    	);
	    	String initVector = input.nextLine();
			try {
				crypt(args[1], args[2], password, initVector, false);
				System.out.println(
						"Decryption completed. Generated file " + args[2] + " based on file " + args[1] + ".\r\n"  
				);
				System.exit(0);
			} catch(Exception e) {
				System.out.println("Error occured: " + e.getMessage());
			}
			input.close();
		} 
	
		System.out.println("Invalid command.");
		System.exit(1);
	}
	
	/**
	 * Calculates the SHA-256 file digest.
	 * 
	 * @param filename file whose SHA-256 file digest 
	 * 		will be calculated
	 * @return the SHA-256 file digest
	 * @throws NoSuchAlgorithmException if error occurs
	 * @throws IOException if error occurs
	 */
	public static String calculateSha(String filename) throws  IOException, NoSuchAlgorithmException {
		Path p = Paths.get(filename);

		MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(Files.readAllBytes(p));
        byte[] digested = messageDigest.digest();
        
		return  Util.bytetohex(digested);
	}
	
	/**
	 * Encrypts or decrypts file with filename to file with cryptedFilename.
	 * 
	 * @param filename file that will be encrypted or decrypting
	 * @param cryptedFilename encrypted or decrypted file
	 * @param password password for encrypting and decrypting
	 * @param initVector initialization vector
	 * @param encrypt true if encryption; false if decryption
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static void crypt(String filename, String cryptedFilename, String password, String initVector, boolean encrypt) 
			throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, 
			InvalidKeyException, InvalidAlgorithmParameterException, 
			IllegalBlockSizeException, BadPaddingException {
		Path inputPath = Paths.get(filename);
		Path outputPath = Paths.get(cryptedFilename);

		SecretKeySpec keySpec = new SecretKeySpec(Util.hextobyte(password), "AES");
		AlgorithmParameterSpec paramSpec = new IvParameterSpec(Util.hextobyte(initVector));
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);
		
		InputStream is = Files.newInputStream(inputPath);
		OutputStream os = Files.newOutputStream(outputPath);

		byte[] buff = new byte[4 * 1024];
		while(true) {
			int r = is.read(buff);
			if(r<1) break;
			
			byte[] byteArray = Arrays.copyOfRange(buff, 0, r);
			
			byte[] outputArray = cipher.update(byteArray);
			os.write(outputArray);
		}

		cipher.doFinal();
	}

}
