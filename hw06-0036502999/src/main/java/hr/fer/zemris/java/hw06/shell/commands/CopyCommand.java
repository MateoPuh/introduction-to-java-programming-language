package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import hr.fer.zemris.java.hw06.shell.CommandUtil;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * {@link ShellCommand} that represents copy command.
 * It takes two arguments: source file and destination.
 * Both arguments are mandatory. Destination can be
 * file or directory. It copies source file to destination.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class CopyCommand implements ShellCommand {

	/** Copy command name. */
	private static final String NAME = "copy"; 
	/** Number of arguments for copy command. */
	private static final int NUMBER_OF_ARGUMENTS = 2;
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argument = CommandUtil.splitArgumentsByBlanksAndQuotes(arguments);

		if(argument.length != NUMBER_OF_ARGUMENTS) {
			env.writeln("Command \'copy\' takes 2 argument.");
			return ShellStatus.CONTINUE;
		}
		
		try {
			if(!CommandUtil.isFile(argument[0])) {
				env.writeln(argument[0] + " is not a file.");
				return ShellStatus.CONTINUE;
			}
		} catch(NoSuchElementException e) {
			env.writeln(e.getLocalizedMessage());
			return ShellStatus.CONTINUE;
		}
		
		if(argument[0].equals(argument[1])) {
			env.writeln("Source and destination are equal: " + argument[1]);
			return ShellStatus.CONTINUE;
		}
		
		if(CommandUtil.isFile(argument[1])) {	
			if(CommandUtil.exists(argument[1])) {
				if(!toOverride(argument[1], env)) {
					return ShellStatus.CONTINUE;
				}
			}
		} else {
			Path file = Paths.get(argument[0]);
			if(CommandUtil.exists(argument[1] + "/" + file.getFileName())) {
				if(!toOverride(argument[1], argument[0], env)) {
					return ShellStatus.CONTINUE;
				}
			}			
		}
		
		if(!CommandUtil.parentExists(argument[1])) {					
			env.writeln("Parent directory of " + argument[1] + " does not exist.");
			return ShellStatus.CONTINUE;
		}
		
		copyFile(argument[0], argument[1], env);
		env.writeln(argument[0] + " copied to " 
		+ (CommandUtil.isFile(argument[0]) ? "" : "directory ")  + argument[1]);
		return ShellStatus.CONTINUE;

	}
	
	private static boolean toOverride(String dir, String filename, Environment env) {
		Path file = Paths.get(filename);
		
		return toOverride(dir + "/" + file.getFileName(), env);
	}
	
	private static boolean toOverride(String filename, Environment env) {
		String[] output = new String[] {"File " + filename + " already exists.", 
				"Are you sure you want to override it? (y/n)"}; 
		return getAnswer(output, env);
	}
	
	private static boolean getAnswer(String[] output, Environment env) {
		while(true) {
			for(String s : output) {
				System.out.println(s);
			}
			
			String answer = env.readLine();
			
			if(answer.equals("y")) {
				return true;
			}
			
			if(answer.equals("n")) {
				return false;
			}
		}
	}
	
	private static void copyFile(String filename, String copyPath, Environment env) {
		Path input = Paths.get(filename);
		
		String fullCopyPath = copyPath;
		if(CommandUtil.exists(copyPath) && !CommandUtil.isFile(copyPath)) {
			fullCopyPath += "/" + input.getFileName();
			env.writeln(fullCopyPath);
		}
		
		Path output = Paths.get(fullCopyPath);
		
		try {
			InputStream is = Files.newInputStream(input);
			OutputStream os = Files.newOutputStream(output);
			
			byte[] buff = new byte[4 * 1024];
			while(true) {
				int r = is.read(buff);
				if(r<1) break;
				
				byte[] outputArray = Arrays.copyOfRange(buff, 0, r);
				
				os.write(outputArray);
			}
		} catch(IOException e) {
			env.writeln("Error occured while copying file " + filename 
					+ " to " + fullCopyPath + ": " + e.getLocalizedMessage());
		}
		
	}

	@Override
	public String getCommandName() {
		return NAME;
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new ArrayList<>();
		
		description.add("copy [src] [dest]");
		description.add("");
		description.add("-src : name of a source file");
		description.add("-dest : name of a destination file or directory");
		description.add("");
		description.add("Command \'copy\' takes two argument: source file and destination file");
		description.add("or directory. It copies source file to the destination.");

		return Collections.unmodifiableList(description);
	}

}
