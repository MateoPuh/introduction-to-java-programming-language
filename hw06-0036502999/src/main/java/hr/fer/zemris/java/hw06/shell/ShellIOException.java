package hr.fer.zemris.java.hw06.shell;

/**
 * Exception that is thrown whenever error happens in 
 * {@link Environment#readLine()}, {@link Environment#write(String)},
 * {@link Environment#writeln(String)}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ShellIOException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates new {@link ShellIOException}.
	 */
	public ShellIOException() {
		
	}
	
	/**
	 * Creates new {@link ShellIOException} with
	 * given message.
	 * 
	 * @param message exception message
	 */
	public ShellIOException(String message) {
		super(message);
	}
	
	/**
	 * Creates new {@link ShellIOException} with
	 * given cause.
	 * 
	 * @param cause exception cause
	 */
	public ShellIOException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates new {@link ShellIOException} with
	 * given message and cause.
	 * 
	 * @param message exception message
	 * @param cause exception cause
	 */
	public ShellIOException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
