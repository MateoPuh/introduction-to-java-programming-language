package hr.fer.zemris.java.hw06.shell;

import java.util.List;

/**
 * Represents command for {@link MyShell}. Contains
 * methods that every command should have.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public interface ShellCommand {

	/**
	 * Executes command and returns {@link ShellStatus} that
	 * command generates.
	 * 
	 * @param env environment for communicating with user
	 * @param arguments string that contains everything user 
	 * 		entered after command
	 * @return {@link ShellStatus} that command generated
	 */
	ShellStatus executeCommand(Environment env, String arguments);
	
	/**
	 * Returns name of command.
	 * 
	 * @return name of command
	 */
	String getCommandName();
	
	/**
	 * Gets description of command.
	 * 
	 * @return description of command
	 */
	List<String> getCommandDescription();
	
}
