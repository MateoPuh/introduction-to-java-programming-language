package hr.fer.zemris.java.hw16.pictures;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

/**
 * Represents manager for {@link PictureDescriptor} objects.
 * It reads data from provided filepath and returns information
 * about pictures.
 * 
 * @author Mateo Puhalović
 *
 */
public class PictureDescriptorManager {
	
	private List<PictureDescriptor> descriptors;
	private Path filepath;
	
	/**
	 * Creates new {@link PictureDescriptorManager} for given filepath.
	 * 
	 * @param filepath filepath
	 */
	public PictureDescriptorManager(Path filepath) {
		this.filepath = filepath;	
	}
	
	/**
	 * Reads data from given filepath and saves it to descriptor list.
	 * 
	 * @throws IOException
	 */
	public void read() throws IOException {
		if(!Files.exists(filepath) || Files.isDirectory(filepath)) {
			throw new IllegalArgumentException();
		}
		
		List<String> lines = Files.readAllLines(filepath, Charset.defaultCharset());
		lines.removeIf(line -> line.isEmpty());
				
		if(lines.size() % 3 != 0) {
			throw new IllegalArgumentException();
		}
		
		int numOfPics = lines.size() / 3;
		
		descriptors = new ArrayList<PictureDescriptor>();
		
		for(int i = 0; i < numOfPics; i++) {
			String name = lines.get(i * 3);
			String description = lines.get(i * 3 + 1);
			String[] tagStrings = lines.get(i * 3 + 2).split(",");
			
			List<String> tags = new ArrayList<String>();
			
			for(String t : tagStrings) {
				if(!t.equals(t.toLowerCase())) {
					System.out.println("Tag " + t + " is not valid. (Contains uppercase)");
					continue;
				}
				tags.add(t.trim());
			}
			
			PictureDescriptor pd = new PictureDescriptor(name, description, tags);
			descriptors.add(pd);
		}
	}
	
	/**
	 * Returns descriptor list.
	 * 
	 * @return descriptor list
	 * @throws IOException if error occurs
	 */
	public List<PictureDescriptor> getDescriptors() throws IOException {
		read();
		return descriptors;
	}
	
	/**
	 * Returns set of tags.
	 * 
	 * @return set of tags
	 * @throws IOException if error occurs
	 */
	public Set<String> getTags() throws IOException {	
		read();
		Set<String> set = new HashSet<String>();
		
		for(PictureDescriptor pd : descriptors) {
			set.addAll(pd.getTags());
		}
		
		return set;
	}

	/**
	 * Returns list of picture names for a given tag.
	 * 
	 * @param tag tag
	 * @return list of picture names
	 * @throws IOException if error occurs
	 */
	public List<String> getNames(String tag) throws IOException {	
		read();
		List<String> names = new ArrayList<String>();
		
		for(PictureDescriptor pd : descriptors) {
			if(pd.getTags().contains(tag)) {
				names.add(pd.getName());
			}
		}
		
		return names;
	}
	
	/**
	 * Returns descriptor for a given picture name.
	 * 
	 * @param name name of picture
	 * @return descriptor
	 * @throws IOException if error occurs
	 * @throws NoSuchElementException if descriptor doesn't exist
	 */
	public PictureDescriptor getDescriptor(String name) throws IOException {
		read();
		
		Optional<PictureDescriptor> pd = descriptors.stream()
				.filter(d -> d.getName().equals(name))
				.findFirst();
		return pd.get();
	}
	
}
