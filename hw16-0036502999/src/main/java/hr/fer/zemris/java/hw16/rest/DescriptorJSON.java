package hr.fer.zemris.java.hw16.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;

import hr.fer.zemris.java.hw16.pictures.PictureDescriptor;
import hr.fer.zemris.java.hw16.pictures.PDMProvider;

/**
 * REST servlet that is used for getting descriptor for picture
 * with given name.
 * 
 * @author Mateo Puhalović
 *
 */
@Path("/descriptor")
public class DescriptorJSON {
	
	/**
	 * Returns descriptor for picture with given name.
	 * 
	 * @param name name of picture
	 * @return descriptor
	 */
	@Path("{name}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDescriptor(@PathParam("name") String name) {		
		try {
			PictureDescriptor pd = PDMProvider.getManager().getDescriptor(name);
			
			if(pd == null) {
				return Response.status(Status.NOT_FOUND).build();
			}
			
			JSONObject result = new JSONObject();
			result.put("description", pd.getDescription());
			
			JSONArray array = new JSONArray();
			for(String t : pd.getTags()) {
				array.put(t);
			}
			
			result.put("tags", array);

			return Response.status(Status.OK).entity(result.toString()).build();
		} catch(Exception e) {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

}
