package hr.fer.zemris.java.hw16.rest;

import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;

import hr.fer.zemris.java.hw16.pictures.PDMProvider;

/**
 * REST servlet used for getting list of tags.
 * 
 * @author Mateo Puhalović
 *
 */
@Path("/tags")
public class TagsJSON {
	
	/**
	 * Returns list of tags.
	 * 
	 * @return list of tags
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTags() {
		try {
			Set<String> tags = PDMProvider.getManager().getTags();
			
			if(tags == null || tags.size() == 0) {
				return Response.status(Status.NOT_FOUND).build();
			}
			
			JSONArray result = new JSONArray();
			
			for(String tag : tags) {
				result.put(tag);
			}

			return Response.status(Status.OK).entity(result.toString()).build();
		} catch(Exception e) {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

}
