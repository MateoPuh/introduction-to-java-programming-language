package hr.fer.zemris.java.hw16.pictures;

import java.util.List;

/**
 * Represents entry for one picture in descriptor list.
 * 
 * @author Mateo Puhalović
 *
 */
public class PictureDescriptor {
	
	private String name;
	private String description;
	private List<String> tags;
	
	/**
	 * Creates new {@link PictureDescriptor} with given name, description
	 * and tags.
	 * 
	 * @param name name
	 * @param description description
	 * @param tags list of tags
	 */
	public PictureDescriptor(String name, String description, List<String> tags) {
		this.name = name;
		this.description = description;
		this.tags = tags;
	}

	/**
	 * Returns name of picture.
	 * 
	 * @return picture name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns picture description.
	 * 
	 * @return picture description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns list of tags for a picture.
	 * 
	 * @return list of tags
	 */
	public List<String> getTags() {
		return tags;
	}

}
