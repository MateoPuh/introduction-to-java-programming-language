package hr.fer.zemris.java.hw16.servlets;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet used for getting picture with given name.
 * 
 * @author Mateo Puhalović
 *
 */
@WebServlet("/servlets/picture")
public class PictureServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String INPUT_PATH = "/WEB-INF/slike";
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = req.getParameter("name");
		
		if(name == null) {
			return;
		}
		
		Path input = Paths.get(req.getServletContext()
				.getRealPath(INPUT_PATH)).resolve(name);
        ImageInputStream iis = ImageIO.createImageInputStream(input.toFile());
        ImageReader reader = ImageIO.getImageReaders(iis).next();
        iis.close();
  
        resp.setContentType("image/" + reader.getFormatName()); 
        ImageIO.write(ImageIO.read(input.toFile()), reader.getFormatName(), resp.getOutputStream());
	}
	
}