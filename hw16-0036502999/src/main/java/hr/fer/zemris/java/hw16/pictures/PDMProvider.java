package hr.fer.zemris.java.hw16.pictures;

import java.nio.file.Path;

/**
 * Provides with {@link PictureDescriptorManager}.
 * 
 * @author Mateo Puhalović
 *
 */
public class PDMProvider {
	
	private static PictureDescriptorManager manager;
	
	public static void setManager(Path path) {
		 manager = new PictureDescriptorManager(path);
	}
	
	/**
	 * Returns manager.
	 * 
	 * @return manager
	 */
	public static PictureDescriptorManager getManager() {
		return manager;
	}

}
