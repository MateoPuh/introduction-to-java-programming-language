package hr.fer.zemris.java.hw16.init;

import java.nio.file.Paths;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import hr.fer.zemris.java.hw16.pictures.PDMProvider;

@WebListener
public class Initialize implements ServletContextListener {
	
	/** Path for descriptor file. */
	private static final String DESCRIPTOR_PATH = "/WEB-INF/opisnik.txt";

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		PDMProvider.setManager(
				Paths.get(sce.getServletContext().getRealPath(DESCRIPTOR_PATH)));
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}
	
}
