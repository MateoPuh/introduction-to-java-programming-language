package hr.fer.zemris.java.hw16.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;

import hr.fer.zemris.java.hw16.pictures.PDMProvider;

/**
 * REST servlet used for getting list of thumbnails for given tag.
 * 
 * @author Mateo Puhalović
 *
 */
@Path("/thumbnails")
public class ThumbnailList {
	
	/**
	 * Returns list of thumbnails for given tag.
	 * 
	 * @param tag tag
	 * @return list of thumbnails
	 */
	@Path("{tag}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getThumbnails(@PathParam("tag") String tag) {		
		try {
			List<String> names = PDMProvider.getManager().getNames(tag);
			
			if(names == null || names.size() == 0) {
				return Response.status(Status.NOT_FOUND).build();
			}
			
			JSONArray result = new JSONArray();
			
			for(String name : names) {
				result.put(name);
			}

			return Response.status(Status.OK).entity(result.toString()).build();
		} catch(Exception e) {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

}
