package hr.fer.zemris.java.hw16.servlets;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet used for getting thumbnail for given picture name.
 * 
 * @author Mateo Puhalović
 *
 */
@WebServlet("/servlets/thumbnail")
public class ThumbnailServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String OUTPUT_PATH = "/WEB-INF/thumbnails";
	private static final String INPUT_PATH = "/WEB-INF/slike";
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = req.getParameter("name");
		
		if(name == null) {
			return;
		}
		
		Path input = Paths.get(req.getServletContext()
				.getRealPath(INPUT_PATH)).resolve(name);
		Path output = Paths.get(req.getServletContext()
				.getRealPath(OUTPUT_PATH)).resolve(name);
		
		ImageInputStream iis = ImageIO.createImageInputStream(input.toFile());
        ImageReader reader = ImageIO.getImageReaders(iis).next();
        iis.close();
  
        resp.setContentType("image/" + reader.getFormatName()); 
	
		if(!Files.exists(output)) {
			resize(input, output, 150, 150);
		}
		
        ImageIO.write(ImageIO.read(output.toFile()), reader.getFormatName(), resp.getOutputStream());
	}

	/**
	 * Method used for resizing image to given size.
	 * 
	 * @param inputImagePath input path
	 * @param outputImagePath output path
	 * @param scaledWidth width
	 * @param scaledHeight height
	 * @throws IOException if error occurs
	 */
	private static void resize(Path inputImagePath,
			Path outputImagePath, int scaledWidth, int scaledHeight)
			throws IOException {
		
		BufferedImage inputImage = ImageIO.read(inputImagePath.toFile());
        BufferedImage outputImage = new BufferedImage(scaledWidth,
        		scaledHeight, inputImage.getType());
 
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();
  
        ImageInputStream iis = ImageIO.createImageInputStream(inputImagePath.toFile());
        ImageReader reader = ImageIO.getImageReaders(iis).next();
        iis.close();
		
        ImageIO.write(outputImage, reader.getFormatName(), outputImagePath.toFile());
	}
	
}