function findTags() {
	$.ajax(
	 	{
			url: "rest/tags",
		 	data: {},
		  	dataType: "json",
		  	success: function(data) {
				var tags = data; 
				var html = "";
				if(tags.length==0) {
					html = "No results..."
				} else {
					for(var i=0; i<tags.length; i++) {
						html += "<button class = \"tagButton\"onclick=\"findNames(\'" 
						html += tags[i] + "\')\">" +  tags[i] + "</button>"
					}
				}
				$("#buttons").html(html);
		  	}
	  	}
	);
}

findTags();
  
function findImage(name) {
	var html = "<hr><div class=\"picture-container\"><img class=\"center\"" 
	html += "src=\"servlets/picture?name=" + name + "\"/>";
	$.ajax(
		{
			url: "rest/descriptor/" + name,
		  	data: {},
		  	dataType: "json",
		  	success: function(data) {
				html += "<h2>" + data.description + "</h2>"
				var tags = data.tags
				
				html += "<p>Tags: "						
				for(var i=0; i<tags.length; i++) {
					if(i > 0) {
						html += ", "
					}
					html += tags[i]
				}
				html += "</p></div>"
				$("#picture").html(html);
		  }
	  }
	);
	$("#picture").html(html);
}
  
function findNames(tag) {
	$.ajax(
		{
			url: "rest/thumbnails/" + tag,
			data: {},
		  	dataType: "json",
		  	success: function(data) {
				$("#picture").html("");
				var names = data; 
				var html="<p>Showing results for: <b>" + tag + "</b> </p><hr>"
				if(names.length==0) {
					html = "No results..."
				} else {
					for(var i=0; i<names.length; i++) {
						html += "<img class=\"thumbnail\" onclick=\"findImage(\'" 
						html += names[i] + "\')\" src=\"servlets/thumbnail?name=" + names[i] + "\"/>"
					}
				}
				$("#thumbnails").html(html);
		  	}
		}
	);
}