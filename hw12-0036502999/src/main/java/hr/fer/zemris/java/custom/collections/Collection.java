package hr.fer.zemris.java.custom.collections;

/**
 * This interface represents general collection of objects which
 * are called elements. It contains various methods for manipulating 
 * elements in collection which all collections should have, such 
 * as methods for adding and removing elements.
 * 
 * <p>Classes that extend Collection class should implement methods that
 * they want to use, expect for methods isEmpty() and addAll(Collection).</p>
 * 
 * <p>Static method checkBounds(int, int, int) can be used for checking
 * if index is inside allowed range.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.1
 *
 */
public interface Collection {
	
	/**
	 * Returns true if there are no elements in collection.
	 * 
	 * @return true is collection is empty; false otherwise
	 */
	default boolean isEmpty() {		
		return size() == 0;  
	}

	/**
	 * Returns number of elements in collection.
	 * 
	 * @return size of collection
	 */
	int size();
	
	/**
	 * Insert the element with given value to the end of collection. 
	 * 
	 * @param value value to insert into collection
	 */
	void add(Object value);

	/**
	 * Returns true if the collection contains given value. Uses 
	 * equals method to determine if value is contained in collection. 
	 * Giving null as parameter value is allowed.
	 * 
	 * @param value value to find in collection
	 * @return true if collection contains given value; false otherwise
	 */
	boolean contains(Object value);
	
	/**
	 * Removes given value from collection. Uses equals method to 
	 * determine if value is contained in collection and removes one
	 * instance of it.
	 * 
	 *  @param value value to remove from collection
	 *  @return true if value was removed from collection; false otherwise
	 */
	boolean remove(Object value);
	
	/**
	 * Returns the collection in form of array.
	 */
	Object[] toArray();

	/**
	 * Calls processor.process() method on every element of collection. 
	 * 
	 * @param processor processor which method process is used on 
	 * 			each element of collection
	 */
	default void forEach(Processor processor) {
		ElementsGetter getter = createElementsGetter();
		
		getter.processRemaining(processor);
	}
	
	/**
	 * Copies the elements of given collection to original collection. 
	 * Uses local processor class whose method process() adds element 
	 * of other collection to original collection. 
	 * 
	 * @param other collection whose elements are copied to original collection 
	 */
	default void addAll(Collection other) {
		
		/**
		 * Local processor class inside of Collection.addAll(Collection) method.
		 * It is used for adding all elements of other collection to original 
		 * collection.
		 * 
		 * @author Mateo Puhalović
		 * @version 1.0
		 * @see Collection#addAll
		 * 
		 */
		class AddAllProcessor implements Processor {
			
			/**
			 * Copies value of given element of other collection to 
			 * original collection.
			 * 
			 * @param value value of element of other collection which 
			 * 		will be added to original collection 
			 */
			public void process(Object value) {
				add(value);
			}
			
		}

		Processor p = new AddAllProcessor();
		other.forEach(p);	
	}

	/**
	 * Removes all elements from this collection.
	 */
	void clear();

	/**
	 * Creates and returns new {@link ElementsGetter} instance
	 * which can be used for iterating through this collection.
	 * 
	 * @return new ElementsGetter
	 */
	ElementsGetter createElementsGetter();
	
	/**
	 * Adds all elements from given collection which tester accepts
	 * to this collection.
	 * 
	 * @param col collection whose elements are copied to this collection
	 * @param tester tester which is used for accepting elements
	 */
	default void addAllSatisfying(Collection col, Tester tester) {
		ElementsGetter getter = col.createElementsGetter();
		
		getter.processRemaining(value -> {
				if (tester.test(value)) {
					add(value);
				}
		});
	}

	/**
	 * Checks whether given index is in [lowerBorder, upperBorder].
	 * 
	 * @param index index to check
	 * @param lowerBorder lower border of interval in which index is acceptable
	 * @param upperBorder upper border of interval in which index is acceptable
	 * @throws IndexOutOfBoundsException if given index is not in [0, size]
	 */
	public static void checkBounds(int index, int lowerBorder, int upperBorder) throws IndexOutOfBoundsException{
		if (index < lowerBorder || index > upperBorder) {
			throw new IndexOutOfBoundsException("Index is out of bounds!");
		}
	}
	
}
