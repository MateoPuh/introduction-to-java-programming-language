package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * {@link IWebWorker} that represents homepage.
 * 
 * @author Mateo Puhalović
 *
 */
public class Home implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) {
		String bgColor = context.getPersistentParameter("bgcolor");
		
		if(bgColor == null) {
			bgColor = "7F7F7F";
		}
		
		context.setTemporaryParameter("background", bgColor);
		
		try {
			context.getDispatcher().dispatchRequest("/private/pages/home.smscr");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}