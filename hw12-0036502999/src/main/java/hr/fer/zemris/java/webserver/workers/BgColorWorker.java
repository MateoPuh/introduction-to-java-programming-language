package hr.fer.zemris.java.webserver.workers;

import java.io.IOException;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * {@link IWebWorker} that changes the color of the background.
 * 
 * @author Mateo Puhalović
 *
 */
public class BgColorWorker implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) {
		String bgColor = context.getParameter("bgcolor");
		boolean isHex = bgColor == null ? false : bgColor.matches("^[0-9a-fA-F]+$");
		String message = null;
		
		if(bgColor == null || bgColor.length() != 6 || !isHex) {
			message = "Color not changed.";
		} else {
			context.setPersistentParameter("bgcolor", bgColor);
			message = "Color changed.";
		}	
		
		context.setMimeType("text/html");
		try {
			context.write("<html><body>");
			context.write("<head><title>SetColor</title></head>");			
			context.write("<h1>" + message +"</h1>");
			context.write("<a href=\"/index2.html\">Homepage</a>");
			context.write("</body></html>");
		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}
}