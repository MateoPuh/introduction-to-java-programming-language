package hr.fer.zemris.java.custom.scripting.exec;

import java.util.function.BiFunction;

/**
 * This class represents wrapper of Object value and 
 * contains method for adding, subtracting, multiplying, 
 * dividing and comparing with other values.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ValueWrapper {

	/** Value that is wrapped. */
	private Object value;
	
	/**
	 * Creates new {@link ValueWrapper} with initial value.
	 * 
	 * @param value initial value
	 */
	public ValueWrapper(Object value) {
		this.value = value;
	}

	/**
	 * Adds the given value to the internal value.
	 * 
	 * @param incValue given value
	 * @throws IllegalArgumentException if internal value
	 * 	or given value are not in right format
	 */
	public void add(Object incValue) {
		checkArguments(incValue);
		
		this.value = performOperation(
				convertArgument(this.value), 
				convertArgument(incValue), 
				(t, u) -> t.intValue() + u.intValue(),
				(t, u) -> t.doubleValue() + u.doubleValue()
		);
	}
	
	/**
	 * Subtracts the given value from the internal value.
	 * 
	 * @param decValue given value
	 * @throws IllegalArgumentException if internal value
	 * 	or given value are not in right format
	 */
	public void subtract(Object decValue) {
		checkArguments(decValue);
		
		this.value = sub(this.value, decValue);
	}
	
	/**
	 * Multiplyies the given value to the internal value.
	 * 
	 * @param mulValue given value
	 * @throws IllegalArgumentException if internal value
	 * 	or given value are not in right format
	 */
	public void multiply(Object mulValue) {
		checkArguments(mulValue);

		this.value = performOperation(
				convertArgument(this.value), 
				convertArgument(mulValue), 
				(t, u) -> t.intValue() * u.intValue(),
				(t, u) -> t.doubleValue() * u.doubleValue()
		);
	}
	
	/**
	 * Divides the given value from the internal value.
	 * 
	 * @param divValue given value
	 * @throws IllegalArgumentException if internal value
	 * 	or given value are not in right format
	 */
	public void divide(Object divValue) {
		checkArguments(divValue);

		this.value = performOperation(
				convertArgument(this.value), 
				convertArgument(divValue), 
				(t, u) -> t.intValue() / u.intValue(),
				(t, u) -> t.doubleValue() / u.doubleValue()
		);
	}
	
	/**
	 * Performs operation with given value1 and value2. If
	 * they are both Integer format then intFunction is used,
	 * otherwise double function is used.
	 * 
	 * @param value1 first value
	 * @param value2 second value
	 * @param intFunction function with integers
	 * @param doubleFunction function with doubles
	 * @return result of operation
	 * @throws IllegalArgumentException if value1
	 * 	or value2 are not in right format
	 */
	private Object performOperation(Object value1, Object value2, 
			BiFunction<Integer, Integer, Integer> intFunction, 
			BiFunction<Double, Double, Double> doubleFunction) {
		if(value1 instanceof Double) {
			if(value2 instanceof Double) {
				return doubleFunction.apply(
						(Double)value1, (Double)value2);
			}else if(value2 instanceof Integer) {
				return doubleFunction.apply(
						(Double)value1, Double.valueOf((Integer)value2));
			}
			
		} else if(value1 instanceof Integer) {
			if(value2 instanceof Double) {
				return doubleFunction.apply(Double.valueOf((Integer)value1), (Double)value2);
			}else if(value2 instanceof Integer) {
				return intFunction.apply(((Integer)value1), (Integer)value2);
			}
		}
		
		throw new IllegalArgumentException("Type of arguments are not suitable");
	}

	/**
	 * Performs the operation of subtraction and returns the result.
	 * 
	 * @param value1 first value
	 * @param value2 second value
	 * @return result of subtraction
	 */
	private Object sub(Object value1, Object value2) {
		return performOperation(
				convertArgument(value1), 
				convertArgument(value2), 
				(t, u) -> t.intValue() - u.intValue(),
				(t, u) -> t.doubleValue() - u.doubleValue()
		);
	}
	
	/**
	 * Compares internal value with given value.
	 * Returns negative number if internal value
	 * is lesser than given value, positive value
	 * if internal value is grater that given value
	 * and 0 if they are same.
	 * 
	 * @param withValue value to compare internal
	 * 	value to
	 * @return number that indicates relation between
	 * 	internal and given value
	 */
	public int numCompare(Object withValue) {
		checkArguments(withValue);
			
		Object diff = sub(this.value, withValue);
		
		if(diff instanceof Integer) {
			return (Integer)diff;
		}
		
		if(diff instanceof Double) {
			double d = (Double)diff;
			
			if(Math.abs(d) < 1e-6) {
				return 0;
			} else if(d < 0) {
				return -1;
			} else {
				return 1;
			}
		}
		
		throw new IllegalArgumentException("Invalid type of arguments for numCompare.");
	}
	
	/**
	 * If given argument is null, Integer(0) is returned.
	 * If given argument is String, then it is parsed as
	 * Double or Integer, depending on its content.
	 * Otherwise, argument is returned.
	 * 
	 * @param argument argument to convert
	 * @return converted or same argument
	 */
	private Object convertArgument(Object argument) {
		if(argument == null) {
			return Integer.valueOf(0);
		}
		
		if(argument instanceof String) {
			if(((String) argument).contains(".") || ((String) argument).contains("E")) {
				try {
					return Double.parseDouble((String)argument);
				} catch(NumberFormatException e) {
					throw new IllegalArgumentException(e.getLocalizedMessage());
				}
			}
		
			try {
				return Integer.parseInt((String)argument);
			} catch(NumberFormatException e) {
				throw new IllegalArgumentException(e.getLocalizedMessage());
			}
		}
		
		return argument;
	}
	
	/**
	 * Checks the internal value and given argument and
	 * throws {@link IllegalArgumentException} if they are in wrong
	 * format.
	 * 
	 * @param argument argument to check
	 * @throws IllegalArgumentException if internal value or given
	 * 	argument are not in right format
	 */
	private void checkArguments(Object argument) {
		if(!checkArgument(value)) {
			throw new IllegalArgumentException("Type of stored value not suitable for operation.");
		}
		if(!checkArgument(argument)) {
			throw new IllegalArgumentException("Type of argument not suitable for operation.");
		}
	}
	
	/**
	 * Checks whether given argument is instance of
	 * Integer, Double, String or if it is null. If it is
	 * anything else, false is returned.
	 * 
	 * @param argument argument to check
	 * @return true if argument is null or instance of String, Double
	 * 	or Integer; false otherwise
	 */
	private boolean checkArgument(Object argument) {
		return argument == null
				|| argument instanceof Integer
				|| argument instanceof Double
				|| argument instanceof String;
	}
	
	/**
	 * Returns the internal value.
	 * 
	 * @return internal value
	 */
	public Object getValue() {
		return value;
	}
	
	/**
	 * Sets the internal value to the given one.
	 * 
	 * @param value given value
	 */
	public void setValue(Object value) {
		this.value = value;
	}
	
}
