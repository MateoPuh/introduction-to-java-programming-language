package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents base class for all other elements from 
 * hr.fer.zemris.java.custom.elems package. Elements are used 
 * for the representation of expressions.
 * 
 * <p>It provides method for getting representation of element 
 * in String format. For this class, it returns empty String.</p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.elems
 */
public class Element {
	
	/**
	 * Returns representation of this element in String format.
	 * 
	 * @return representation of this element in String format
	 */
	 public String asText() {
		 return new String();
	 }
	 
}
