package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents {@link Element} which represents Integer
 * in an expression. 
 * 
 * <p>It provides method for getting its value.</p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.elems
 */
public class ElementConstantInteger extends Element {

	/** Value of Integer */
	private int value;
	
	/**
	 * Creates new {@link ElementConstantInteger} with given value.
	 * 
	 * @param value value of Integer
	 */
	public ElementConstantInteger(int value) {
		this.value = value;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText() {
		return String.valueOf(value);
	}
	
	/**
	 * Returns value of Integer.
	 * 
	 * @return value of integer
	 */
	public int getValue() {
		return value;
	}
	
}
