package hr.fer.zemris.java.webserver;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

/**
 * This class represents web server. It gets single argument from command line
 * which is path to config file.
 * 
 * @author Mateo Puhalović
 *
 */
public class SmartHttpServer {
	
	private String address;
	private String domainName;
	private int port;
	@SuppressWarnings("unused")
	private int workerThreads;
	private int sessionTimeout;
	private Map<String,String> mimeTypes = new HashMap<String, String>();
	private ServerThread serverThread;
	private ExecutorService threadPool;
	private Path documentRoot;
	private Map<String,IWebWorker> workersMap = new HashMap<String, IWebWorker>();
	private Map<String, SessionMapEntry> sessions = new HashMap<String, SmartHttpServer.SessionMapEntry>();
	private Random sessionRandom = new Random();
	private CleanThread cleanThread;
	
	/**
	 * Creates {@link SmartHttpServer}.
	 * 
	 * @param configFileName path to config file
	 */
	public SmartHttpServer(String configFileName) {
		try {
			loadProperties(configFileName);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		serverThread = new ServerThread();
	}
	
	/**
	 * Loads all properties from config files.
	 * 
	 * @param configFileName path to server config file
	 * @throws IOException if error occurs
	 */
	private void loadProperties(String configFileName) throws IOException {
		Properties props = new Properties();
		props.load(new FileInputStream(configFileName));
		
		address = props.getProperty("server.address");
		domainName = props.getProperty("server.domainName");
		port = Integer.parseInt(props.getProperty("server.port"));
		workerThreads = Integer.parseInt(props.getProperty("server.workerThreads"));
		sessionTimeout = Integer.parseInt(props.getProperty("session.timeout"));	
		documentRoot = Paths.get(props.getProperty("server.documentRoot"));		
		
		loadMimeTypes(props.getProperty("server.mimeConfig"));
		try {
			loadWorkers(props.getProperty("server.workers"));
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * Loads workers from its config file.
	 * 
	 * @param workerFileName path to config file for workers
	 * @throws FileNotFoundException if error occurs
	 * @throws IOException if error occurs
	 * @throws InstantiationException if error occurs
	 * @throws IllegalAccessException if error occurs
	 * @throws ClassNotFoundException if error occurs
	 */
	private void loadWorkers(String workerFileName) throws FileNotFoundException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Properties props = new Properties();
		props.load(new FileInputStream(workerFileName));
		
		Set<Object> keySet = props.keySet();
		for(Object key : keySet) {
			Class<?> referenceToClass = this.getClass().getClassLoader().loadClass(props.getProperty((String)key));
			@SuppressWarnings("deprecation")
			Object newObject = referenceToClass.newInstance();
			IWebWorker iww = (IWebWorker)newObject;

			workersMap.put((String)key, iww);
		}
	}

	/**
	 * Loads mime properties from its config file.
	 * 
	 * @param mimeFileName path to mime types config file
	 * @throws FileNotFoundException if error occurs
	 * @throws IOException if error occurs
	 */
	private void loadMimeTypes(String mimeFileName) throws FileNotFoundException, IOException {
		Properties props = new Properties();
		props.load(new FileInputStream(mimeFileName));
		
		Set<Object> keySet = props.keySet();
		for(Object key : keySet) {
			mimeTypes.put((String)key, props.getProperty((String)key));
		}
	}
	
	/**
	 * Starts server.
	 */
	protected synchronized void start() {
		if(!serverThread.isAlive()) {
			serverThread.start();
		}
		
		threadPool = Executors.newFixedThreadPool(
				Runtime.getRuntime().availableProcessors(), 
				(r) -> new Thread(r)
		);	
		
		cleanThread = new CleanThread();
		cleanThread.setDaemon(true);
		cleanThread.start();
	}

	/**
	 * Stops server.
	 */
	protected synchronized void stop() {
		serverThread.stopThread();
		cleanThread.stopThread();
		threadPool.shutdown();
	}
	
	/**
	 * This thread deletes session parameters which are not valid every minute.
	 * 
	 * @author Mateo Puhalović
	 *
	 */
	private class CleanThread extends Thread {
		private final static int PERIOD_TIME = 60000;
		
		private boolean stopCleaning = false;
		
		@Override 
		public void run() {
			while (!stopCleaning) {
				try {
					Thread.sleep(PERIOD_TIME);
					for (String key : sessions.keySet()) {
						if (sessions.get(key).validUntil < System.currentTimeMillis() / 1000) {
							sessions.remove(key);
						}
					}
				} catch (InterruptedException e) {
				}
			}
		}
		
		public void stopThread() {
			stopCleaning = true;
		}
	}
	
	/**
	 * This thread waits for client's requests and serves them.
	 * 
	 * @author Mateo Puhalović
	 *
	 */
	protected class ServerThread extends Thread {
		private boolean stopServer = false;
		
		@Override
		public void run() {
			try {
				ServerSocket serverSocket = new ServerSocket();
				serverSocket.bind(
						new InetSocketAddress(address, port)
					);
				while(!stopServer) {	
					Socket client = serverSocket.accept();					
					ClientWorker cw = new ClientWorker(client);
					threadPool.submit(cw);
					
					if(Thread.currentThread().isInterrupted()) {
						serverSocket.close();
						break;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		public void stopThread() {
			stopServer = true;
		}
		
	}

	/**
	 * Data structure that represents entry in session map.
	 * 
	 * @author Mateo Puhalović
	 *
	 */
	private static class SessionMapEntry {
		String sid;
		String host;
		long validUntil;
		Map<String,String> map;
	}
	
	/**
	 * Generates random SID as 20 uppercase letters.
	 * 
	 * @return generated SID
	 */
	private String generateRandomSid() {
		String sid = "";
		for(int i = 0; i < 20; i++) {
			sid += (char)(sessionRandom.nextInt('Z' - 'A' + 1) + 'A');
		}
		return sid;
	}
	
	/**
	 * Represents worker for client requests.
	 * 
	 * @author Mateo Puhalović
	 *
	 */
	private class ClientWorker implements Runnable, IDispatcher {
		
		private Socket csocket;
		private PushbackInputStream istream;
		private OutputStream ostream;
		private String version;
		private String method;
		private String host;
		private Map<String,String> params = new HashMap<String, String>();
		private Map<String,String> tempParams = new HashMap<String, String>();
		private Map<String,String> permPrams = new HashMap<String, String>();
		private List<RCCookie> outputCookies = new ArrayList<RCCookie>();
		private String SID;
		private RequestContext context = null;
		
		/**
		 * Creates new {@link ClientWorker}.
		 * 
		 * @param csocket client socket
		 */
		public ClientWorker(Socket csocket) {
			super();
			this.csocket = csocket;
		}
		
		@Override
		public void run() {
			try {
				istream = new PushbackInputStream(csocket.getInputStream());
				ostream = csocket.getOutputStream();
					
				List<String> request = readRequest();
				if(request == null || request.isEmpty()) {
					sendError(ostream, 400, "Bad request");
					return;
				}
				
				String firstLine = request.get(0);
				String[] parts = firstLine.split(" ");
				if(parts.length != 3) {
					throw new IllegalArgumentException("Too many arguments in " + firstLine);
				}
				
				method = parts[0];
				String requestedpath = parts[1];
				version = parts[2];
				
				if(!method.equals("GET") || !(version.equals("HTTP/1.0") || version.equals("HTTP/1.1"))) {
					sendError(ostream, 400, "Bad request");
					return;
				}
				
				for(String s : request) {
					if(s.startsWith("Host:")) {
						host = s.substring("Host:".length()).trim();
						host = host.split(":")[0];
					}
				}
				
				if(host == null) {
					host = domainName;
				}			
				
				SessionMapEntry entry = null;
				String sidCandidate = checkSession(request);
				
				if(sidCandidate != null) {
					entry = sessions.get(sidCandidate);

					if(entry == null 
							|| !entry.host.equals(host)
							|| entry.validUntil < System.currentTimeMillis() / 1000) {
						entry = createSessionEntry();
					} else {
						entry.validUntil = System.currentTimeMillis() / 1000 + sessionTimeout;
					}	
				} else {
					entry = createSessionEntry();
				}			
				
				permPrams = entry.map;
				this.SID = entry.sid;
				
				String[] pathParts = requestedpath.split("\\?");
								
				if(pathParts.length > 2) {
					sendError(ostream, 400, "Bad request");
					return;
				}
				
				String path = pathParts[0];
				String paramString = pathParts.length == 2 ? pathParts[1] : null;
				
				parseParameters(paramString);
								
				internalDispatchRequest(path, true);
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		}
		
		/**
		 * Creates {@link SessionMapEntry}.
		 * 
		 * @return {@link SessionMapEntry}
		 */
		private SessionMapEntry createSessionEntry() {
			SessionMapEntry entry = new SessionMapEntry();
			entry.sid = generateRandomSid();
			entry.validUntil = System.currentTimeMillis() / 1000 + sessionTimeout;
			entry.map = new ConcurrentHashMap<String, String>();
			entry.host = host;
			sessions.put(entry.sid, entry);
			outputCookies.add(new RCCookie("sid", entry.sid, null, host, "/"));
			return entry;
		}
		
		/**
		 * Returns session id candidate.
		 * 
		 * @param request list of lines in request
		 * @return sid candidate or null
		 */
		private String checkSession(List<String> request) {
			try {
				Pattern p = Pattern.compile("^.*sid=\"(.*)\".*");
				for (String line : request) {
					if (line.startsWith("Cookie")) { 
						Matcher m = p.matcher(line);
						if (m.find()) {
							String sidCandidate = m.group(1);
							return sidCandidate;
						}
					}
				}
			} catch (Exception ex) {
			}
			return null;
		}

		/**
		 * Closes client socket.
		 */
		public void closeSocket() {
			while (true) {
				try {
					csocket.close();
					break;
				} catch (IOException ignorable) { 
				}
			}
		}
		
		/**
		 * Extracts extension of given filename.
		 * 
		 * @param filename file name
		 * @return extension
		 */
		private String extractExtension(String filename) {
			int index = filename.lastIndexOf('.');
			
			if(index == -1) {
				return null;
			}
		
			return filename.substring(index + 1);
		}
		
		/**
		 * Parses given parameters in requests.
		 * 
		 * @param paramString string with parameters
		 */
		private void parseParameters(String paramString) {
			if(paramString == null || paramString.isEmpty()) {
				return;
			}
		
			String[] parts = paramString.split("&");
			for(String part : parts) {
				String[] splitted = part.split("=");
				if(splitted.length != 2) {
					throw new IllegalArgumentException();
				}
				params.put(splitted[0], splitted[1]);
			}
		}
		
		private void sendError(OutputStream cos, 
				int statusCode, String statusText) throws IOException {

				cos.write(
					("HTTP/1.1 "+statusCode+" "+statusText+"\r\n"+
					"Server: simple java server\r\n"+
					"Content-Type: text/plain;charset=UTF-8\r\n"+
					"Content-Length: 0\r\n"+
					"Connection: close\r\n"+
					"\r\n").getBytes(StandardCharsets.US_ASCII)
				);
				cos.flush();

			}
		
		/**
		 * Reads request as list of strings.
		 * 
		 * @return request
		 * @throws IOException if error occurs
		 */
		private List<String> readRequest() throws IOException {
			List<String> request = new ArrayList<String>();
			
			byte[] b = readRequestBytes();
			if(b != null) {
				request = List.of((new String(b).split("\n")));
			}
			
			return request;
		}
		
		private byte[] readRequestBytes() throws IOException {
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				int state = 0;
l:				while(true) {
					int b = istream.read();
					if(b==-1) return null;
					if(b!=13) {
						bos.write(b);
					}
					switch(state) {
					case 0: 
						if(b==13) { state=1; } else if(b==10) state=4;
						break;
					case 1: 
						if(b==10) { state=2; } else state=0;
						break;
					case 2: 
						if(b==13) { state=3; } else state=0;
						break;
					case 3: 
						if(b==10) { break l; } else state=0;
						break;
					case 4: 
						if(b==10) { break l; } else state=0;
						break;
					}
				}
				return bos.toByteArray();
			}

		/**
		 * Creates new context if it is not created and returns it.
		 * 
		 * @return context
		 */
		private RequestContext getContext() {
			if(context == null) {
				context = new RequestContext(ostream, params, permPrams, outputCookies, tempParams, this, SID);
				context.setStatusCode(200);
			}
			
			return context;
		}
		
		/**
		 * Processes the urlPath.
		 * 
		 * @param urlPath path
		 * @param directCall flag that indicates if method is directly called
		 * @throws Exception if error occurs
		 */
		public void internalDispatchRequest(String urlPath, boolean directCall) throws Exception {
			if(urlPath.startsWith("/ext/")) {
				String workerPath = urlPath.substring("/ext/".length());
								
				Class<?> referenceToClass = this.getClass().getClassLoader().loadClass("hr.fer.zemris.java.webserver.workers." + workerPath);
				@SuppressWarnings("deprecation")
				Object newObject = referenceToClass.newInstance();
				IWebWorker iww = (IWebWorker)newObject;
				
				iww.processRequest(getContext());
				
				return;
			}
			
			if(urlPath.startsWith("/private/") && directCall) {
				sendError(ostream, 404, "Not found");
			}
			
			if(workersMap.containsKey(urlPath)) {
				workersMap.get(urlPath).processRequest(getContext());
				return;
			}
			
			Path reqPath = documentRoot.resolve(Paths.get(urlPath.substring(1))).normalize();
			
			if(documentRoot.toAbsolutePath().startsWith(reqPath.toAbsolutePath())) {
				sendError(ostream, 403, "Forbidden");
				return;
			}
			
			if(!Files.exists(reqPath) || !Files.isReadable(reqPath) 
					|| Files.isDirectory(reqPath)) {
				sendError(ostream, 404, "Not found");
				return;
			}
			
			String ext = extractExtension(reqPath.getFileName().toString());
			
			String mimeType = "application/octet-stream"; 
			
			if(ext != null) {
				if(mimeTypes.get(ext) != null) {
					mimeType = mimeTypes.get(ext);
				}
			}
			
			RequestContext rc = getContext();
			
			if(ext.equals("smscr")) {
				String documentBody = readFromDisk(reqPath.toString());
									
				new SmartScriptEngine(
						new SmartScriptParser(documentBody).getDocumentNode(),
						rc
				).execute();
				
				closeSocket();

				return;
			}

			rc.setContentLength(Files.size(reqPath));
			rc.setMimeType(mimeType);
			
			try {
				InputStream is = Files.newInputStream(reqPath);
				
				byte[] buff = new byte[4 * 1024];
				while(true) {
					int r = is.read(buff);
					if(r<1) break;
					
					byte[] outputArray = Arrays.copyOfRange(buff, 0, r);
					
					rc.write(outputArray);
				}
			} catch(IOException e) {
				e.printStackTrace();
			}
			
			closeSocket();
		}
		
		@Override
		public void dispatchRequest(String urlPath) throws Exception {
			internalDispatchRequest(urlPath, false);
		}
	}
	
	/**
	 * Reads the file from the disk.
	 * 
	 * @param filename file name
	 * @return document from disk
	 */
	private static String readFromDisk(String filename) {
		try {
			return new String(Files.readAllBytes(Paths.get(filename)), StandardCharsets.UTF_8);
		} catch (IOException ex) {
			System.out.println("Cannot read file: " + ex.getMessage());
			System.exit(1);
		} catch (Exception e1) {
			System.out.println(e1.getLocalizedMessage());
			System.exit(1);
		}
		return null;
	}
	
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("Wrong number of arguments: " + args.length + ", expected: 1");
			return;
		}
		
		SmartHttpServer server = new SmartHttpServer(args[0]);
		server.start();
	}

}
