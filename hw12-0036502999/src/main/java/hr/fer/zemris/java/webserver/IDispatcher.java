package hr.fer.zemris.java.webserver;

/**
 * This class represents dispatcher for {@link SmartHttpServer}.
 * 
 * @author Mateo Puhalović
 *
 */
public interface IDispatcher {
	
	/**
	 * Dispatches the request with given path.
	 * 
	 * @param urlPath path
	 * @throws Exception if error occurs
	 */
	void dispatchRequest(String urlPath) throws Exception;

}
