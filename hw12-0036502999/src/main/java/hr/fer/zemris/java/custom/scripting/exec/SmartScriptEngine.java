package hr.fer.zemris.java.custom.scripting.exec;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Stack;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents engine that executes parsed document.
 * 
 * @author Mateo Puhalović
 *
 */
public class SmartScriptEngine {
	
	private DocumentNode documentNode;
	private RequestContext requestContext;
	private ObjectMultistack multistack = new ObjectMultistack();
	private INodeVisitor visitor;
	
	/**
	 * Creates new {@link SmartScriptEngine}.
	 * 
	 * @param documentNode root node of parsed document
	 * @param requestContext context
	 */
	public SmartScriptEngine(DocumentNode documentNode, RequestContext requestContext) {
		this.documentNode = documentNode;
		this.requestContext = requestContext;
		
		createVisitor();
	}
	
	/**
	 * Creates visitor for parsed tree that executes it.
	 */
	private void createVisitor() {
		visitor = new INodeVisitor() {

			@Override
			public void visitTextNode(TextNode node) {
				try {
					requestContext.write(node.getText());
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(1);
				}
			}

			@Override
			public void visitForLoopNode(ForLoopNode node) {
				String variable = node.getVariable().asText();
				String increment = node.getStepExpression().asText();
				String endExpression = node.getEndExpression().asText();

				multistack.push(variable,
						new ValueWrapper(node.getStartExpression().asText()));
				while(true) {
					iterateChildren(node);
					
					ValueWrapper current = multistack.peek(variable);
					current.add(increment);
					if(current.numCompare(endExpression) > 0) {
						multistack.pop(variable);
						break;
					}
				}
			}

			@Override
			public void visitEchoNode(EchoNode node) {
				Stack<ValueWrapper> stack = new Stack<>();
				for(int i = 1; i < node.getElements().length; i++) {
					Element e = node.getElements()[i];
					if(e instanceof ElementConstantDouble 
							|| e instanceof ElementConstantInteger) {
						stack.push(new ValueWrapper(e.asText()));
					}
					
					if(e instanceof ElementString) {
						stack.push(new ValueWrapper(e.asText()));
					}
					
					if(e instanceof ElementVariable) {
						stack.push(new ValueWrapper(multistack.peek(e.asText()).getValue()));
					}
					
					if(e instanceof ElementOperator) {
						ValueWrapper second = stack.pop();
						ValueWrapper first = stack.pop();
		
						switch(e.asText()) {
						case "+":
							first.add(second.getValue());
							stack.push(first);
							break;
						case "-":
							first.subtract(second.getValue());
							stack.push(first);
							break;
						case "*":
							first.multiply(second.getValue());
							stack.push(first);
							break;
						case "/":
							first.divide(second.getValue());
							stack.push(first);
							break;
						}
					}
					
					if(e instanceof ElementFunction) {
						switch(e.asText()) {
						case "@sin":						
							Number n = (Number)stack.pop().getValue();
							stack.push(new ValueWrapper(Math.sin((Double)n.doubleValue())));
							break;
						case "@decfmt":
							String format = getString(stack);
							DecimalFormat f = new DecimalFormat(format);
							Number n2 = (Number)stack.pop().getValue();
							Double x2 = n2.doubleValue();
							stack.push(new ValueWrapper(f.format(x2)));
							break;
						case "@dup":
							stack.push(new ValueWrapper(stack.peek().getValue()));
							break;
						case "@swap":
							ValueWrapper a = stack.pop();
							ValueWrapper b = stack.pop();
							stack.push(a);
							stack.push(b);
							break;
						case "@setMimeType":
							String x3 = getString(stack);
							requestContext.setMimeType(x3);
							break;
						case "@paramGet":
							ValueWrapper defValue = stack.pop();
							String dv = getString(stack);
							String value = requestContext.getParameter(dv);
							stack.push(new ValueWrapper(value != null? value : defValue.getValue()));
							break;
						case "@pparamGet":
							ValueWrapper defValuep = stack.pop();
							String dvp = getString(stack);
							String valuep = requestContext.getPersistentParameter(dvp);
							stack.push(new ValueWrapper(valuep != null? valuep : defValuep.getValue()));
							break;
						case "@pparamSet":
							String name = getString(stack);
							String value3 = stack.pop().getValue().toString();
							requestContext.setPersistentParameter(name, value3);
							break;
						case "@pparamDel":
							String name4 = getString(stack);
							requestContext.removePersistentParameter(name4);
							break;
						case "@tparamGet":
							ValueWrapper defValuet = stack.pop();
							String dvt = getString(stack);
							String valuet = requestContext.getTemporaryParameter(dvt);
							stack.push(new ValueWrapper(valuet != null? valuet : defValuet.getValue()));
							break;
						case "@tparamSet":
							String name5 = getString(stack);
							String value5 = stack.pop().getValue().toString();
							requestContext.setTemporaryParameter(name5, value5);
							break;
						case "@tparamDel":
							String name6 = getString(stack);
							requestContext.removeTemporaryParameter(name6);
							break;
						}
					}
				}
				
				Stack<ValueWrapper> reverseStack = new Stack<>();
				
				while(!stack.isEmpty()) {
					reverseStack.push(stack.pop());
				}
				
				while(!reverseStack.isEmpty()) {
					try {
						requestContext.write(reverseStack.pop().getValue().toString());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
			}
			
			@Override
			public void visitDocumentNode(DocumentNode node) {
				iterateChildren(node);
			}
			
			private void iterateChildren(Node node) {
				for(int i = 0; i < node.numberOfChildren(); i++) {
					node.getChild(i).accept(this);
				}
			}
			
			private String getString(Stack<ValueWrapper> stack) {
				String s = (String)stack.pop().getValue();
				return s;
			}
		
		};
	}
	
	/**
	 * Executes parsed document.
	 */
	public void execute() {
		documentNode.accept(visitor);
	}

}
