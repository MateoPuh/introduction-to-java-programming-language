package hr.fer.zemris.java.custom.scripting.exec.demo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext;

public class SmartScriptEngineDemo2 {
	
	public static void main(String[] args) {
		String documentBody = readFromDisk("examples/zbrajanje.smscr");
		
		Map<String,String> parameters = new HashMap<String, String>();
		Map<String,String> persistentParameters = new HashMap<String, String>();
		List<RequestContext.RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();

		parameters.put("a", "4");
		parameters.put("b", "2");
		
		new SmartScriptEngine(
				new SmartScriptParser(documentBody).getDocumentNode(),
				new RequestContext(System.out, parameters, persistentParameters, cookies)
		).execute();
	}
	
	private static String readFromDisk(String filename) {
		try {
			return new String(Files.readAllBytes(Paths.get(filename)), StandardCharsets.UTF_8);
		} catch (IOException ex) {
			System.out.println("Cannot read file: " + ex.getMessage());
			System.exit(1);
		} catch (Exception e1) {
			System.out.println(e1.getLocalizedMessage());
			System.exit(1);
		}
		return null;
	}

}
