package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.Arrays;
import java.util.Objects;

import hr.fer.zemris.java.custom.collections.Tester;

/**
 * This class represents Lexical analyzer which performs 
 * lexical analysis on document which consists of tags 
 * (bounded by {$ and $}) and rest of the text.
 * 
 * <p>While analyzing text outside of tags, lexer is in TEXT state
 * and produces singular token of TEXT type.</p>
 * 
 * <p>While analyzing content inside tags, lexer is in TAG state
 * and produces two tag for tag bounds, one for tag name and 
 * a certain number of variable, function, operator and number
 * tokens. </p>.
 * 
 * <p>If any error happens while analyzing given document, 
 * {@link SmartScriptLexerException} is thrown.</p>	
 *  
 * @author Mateo Puhalović
 * @version 1.0
 */
public class SmartScriptLexer {

	/** Backslash character */
	private static final char BACKSLASH = '\\';
	
	/** Quotation mark character */
	private static final char QUOTATION_MARK = '\"';
	
	/** String with everything this lexer considers blank character */
	private static final String BLANK_CHARACTERS = " \n\r\t";
	
	/** Left curly bracket character */
	private static final char LEFT_CURLY_BRACKET = '{';
	
	/** Dollar sign character */ 
	private static final char DOLLAR_SIGN = '$';
	
	/** For-loop tag name in lower case */
	private static final String TAG_NAME_FOR = "for";
	
	/** Echo tag name */
	private static final char TAG_NAME_ECHO = '=';
	
	/** End tag name */
	private static final String TAG_NAME_END = "end";
	
	/** Underscore character */
	private static final char UNDERSCORE = '_';
	
	/** At sign character */
	private static final char AT_SIGN = '@';
	
	/** Left bound of tag */
	private static final String LEFT_BOUND = "{$";
	
	/** Right bound of tag */
	private static final String RIGHT_BOUND = "$}";
	
	/** Dot character */
	private static final char DOT = '.';
	
	/** Minus character */
	private static final char MINUS = '-';
	
	/** String with everything this lexer considers operator */
	private static final String OPERATORS = "+-*/^";
	
	/** Analyzed document, split into characters */
	private char[] data; 
	
	/** Last generated token */
	private SmartScriptToken token; 
	
	/** Index of currently analyzed character */
	private int currentIndex; 
	
	/** Counter used for counting how many token did lexer generated in current state*/
	private int stateCounter = 0;
	
	/** Current lexer's state */
	private SmartScriptLexerState state;
	
	/**
	 * Creates new {@link SmartScriptLexer} with given text which it uses to
	 * generate tokens.
	 * 
	 * @param text text from which tokens will be generated
	 * @throws NullPointerException if given text is null
	 */
	public SmartScriptLexer(String text) throws NullPointerException{ 
		Objects.requireNonNull(text);
		this.data = text.toCharArray();
		this.state = SmartScriptLexerState.TEXT;
	}

	/**
	 * Generates and returns next token. 
	 * 
	 * @return next token
	 * @throws SmartScriptLexerException if error occurs while generating token
	 */
	public SmartScriptToken nextToken() throws SmartScriptLexerException {
		stateCounter++;
		
		// if user asks for next token after returning token with type of EOF, throw exception
		if(currentIndex > data.length) {
			throw new SmartScriptLexerException("Error occured while reading next token after EOF was read!");
		}		
		
		// if end of string was reached, return new token with type of EOF and value null
		if(currentIndex == data.length) {
			token = new SmartScriptToken(SmartScriptTokenType.EOF, null);
			currentIndex ++;

			return token;
		}

		// if lexer is analyzing text outside of tags, generate token of type TEXT
		if(state.equals(SmartScriptLexerState.TEXT)) {
			generateText();
			return token;
		}
		
		// if lexer is inside tags
		if(state.equals(SmartScriptLexerState.TAG)) {
			removeBlanks();
			
			// if this is second token, then it is tag name
			if(stateCounter == 2) {
				generateTagName();
				return token;
			}
			
			// if it begins with letter, it is variable
			if(Character.isLetter(data[currentIndex])) {
				generateVariable();
				return token;
			}
			
			// if it start with a digit or minus before digit, it is number
			if(Character.isDigit(data[currentIndex]) || 
					(Character.valueOf(data[currentIndex]).equals(MINUS)) 
					&& currentIndex + 1 < data.length && Character.isDigit(data[currentIndex + 1])) {
				generateNumber();
				return token;
			}
			
			if(OPERATORS.contains(String.valueOf(data[currentIndex]))) {
				token = new SmartScriptToken(SmartScriptTokenType.OPERATOR, String.valueOf(data[currentIndex]));
				currentIndex++;
				return token;
			}
						
			// if it is not tag name, variable, number or operator
			switch(data[currentIndex]) {
			// if '{' is read, then it is left bound
			// if '$' is read, then it is right bound
			case LEFT_CURLY_BRACKET:
			case DOLLAR_SIGN:
				generateBound();
				break;
			// if '@' is read, then it is function
			case AT_SIGN:
				generateFunction();
				break;
			// if '\"' is read, then it is string
			case QUOTATION_MARK:
				generateString();
				break;
			// else, it is error
			default:
				throw new SmartScriptLexerException("Invalid syntax!");
			}
		} 
		
		return token;
	}
	
	/**
	 * Generates left or right bound of a tag. 
	 * 
	 * @throws SmartScriptLexerException if there is error
	 */
	private void generateBound() throws SmartScriptLexerException{
		removeBlanks();
		
		if(currentIndex >= data.length - 1) {
			throw new SmartScriptLexerException("Invalid tag bounds!");
		}
		
		String bound = String.valueOf(Arrays.copyOfRange
				(data, currentIndex, currentIndex+2));
		
		if (LEFT_BOUND.equals(bound)) {
			token = new SmartScriptToken(SmartScriptTokenType.TAG_LEFT_BOUND, bound);
			currentIndex+=2;
		} else if (RIGHT_BOUND.equals(bound)) {
			token = new SmartScriptToken(SmartScriptTokenType.TAG_RIGHT_BOUND, bound);
			currentIndex+=2;
			setState(SmartScriptLexerState.TEXT);
		} else {
			throw new SmartScriptLexerException("Invalid tag bounds!");
		}
	}
	
	/**
	 * Generates String which begins and ends with ". It can have
	 * quotation marks inside if they are escaped properly, like this: \".
	 * It can also have '\' inside if it is escapes properly, like this: \\.
	 * Any other escaping is not allowed. 
	 * 
	 * @throws SmartScriptLexerException if there is error
	 */
	private void generateString() throws SmartScriptLexerException{
		removeBlanks();

		String value = "";		
		
		if(Character.valueOf(data[currentIndex]).equals(QUOTATION_MARK)){
			currentIndex++;
		} else {
			throw new SmartScriptLexerException("Invalid string!");
		}
		
		while(true) {
			if (currentIndex >= data.length) {
				break;
			}
			
			if(Character.valueOf(data[currentIndex]).equals(BACKSLASH)) {
				currentIndex++;
				
				if(currentIndex < data.length 
						&& ((Character.valueOf(data[currentIndex]).equals(BACKSLASH))
						|| (Character.valueOf(data[currentIndex]).equals(QUOTATION_MARK)))){
					value += data[currentIndex];
					currentIndex++;
					continue;
				} else if(currentIndex < data.length 
						&& Character.valueOf(data[currentIndex]).equals('n')) {
					value += "\n";
					currentIndex++;
					continue;
				} else if(currentIndex < data.length 
						&& Character.valueOf(data[currentIndex]).equals('r')) {
					value += "\r";
					currentIndex++;
					continue;
				} else {
					throw new SmartScriptLexerException("Invalid Escaping!");
				}
			}
			
			if(Character.valueOf(data[currentIndex]).equals(QUOTATION_MARK)) {				
				currentIndex++;
				break;
			}
			
			String s = readInputData((ch) -> {
				return !Character.valueOf((char)ch).equals(BACKSLASH) 
						&& !Character.valueOf((char)ch).equals(QUOTATION_MARK);
			});
			
			if (s.length() == 0) break;
		
			value += s;
		}
		
		token = new SmartScriptToken(SmartScriptTokenType.STRING, value);
	}
	
	/**
	 * Generates tag name. If tag name is "for", "=" or "end" it will
	 * have unique token type, else it will have generic TAG_NAME type.
	 * 
	 * @throws SmartScriptLexerException if there is error
	 */
	private void generateTagName() throws SmartScriptLexerException{
		removeBlanks();
		
		if (currentIndex < data.length 
				&& Character.valueOf(data[currentIndex]).equals(TAG_NAME_ECHO)) {
			token = new SmartScriptToken(SmartScriptTokenType.TAG_NAME_ECHO, String.valueOf(data[currentIndex]));
			currentIndex++;
			return;
		}
		
		String s = generateName();
		
		if(s.length() == 0) {
			throw new SmartScriptLexerException("Invalid tag name");
		}
		
		String lowerCaseString = s.toLowerCase();
		
		if(lowerCaseString.equals(TAG_NAME_FOR)) {
			token = new SmartScriptToken(SmartScriptTokenType.TAG_NAME_FOR, s);
		} else if(lowerCaseString.equals(TAG_NAME_END)) {
			token = new SmartScriptToken(SmartScriptTokenType.TAG_NAME_END, s);
		} else {
			token = new SmartScriptToken(SmartScriptTokenType.TAG_NAME, s);
		}
	}
	
	/**
	 * Generates variable. Valid variable name starts with letter
	 * and then it can have letter, digits and underscore '_'.
	 * 
	 * @throws SmartScriptLexerException if there is error
	 */
	private void generateVariable() throws SmartScriptLexerException{
		token = new SmartScriptToken(SmartScriptTokenType.VARIABLE, generateName());
	}
	
	/**
	 * Generates function. Valid function name start with '@' and
	 * then it must have letter and after letter it can have letters,
	 * digits and underscore '_'.
	 * 
	 * @throws SmartScriptLexerException if there is error
	 */
	private void generateFunction() throws SmartScriptLexerException{
		removeBlanks();
		
		String s = new String();
		
		if(Character.valueOf((data[currentIndex])).equals(AT_SIGN)) {
			s += data[currentIndex];
			currentIndex++;
		} else {
			throw new SmartScriptLexerException("Not valid function name");
		}
		
		s += generateName();
		
		token = new SmartScriptToken(SmartScriptTokenType.FUNCTION, s);
	}
	
	/**
	 * Generates name used for variables, functions and names.
	 * It starts with a letter, and then it can have letter, digits
	 * and underscore '_'.
	 * 
	 * @return generated name
	 * @throws SmartScriptLexerException if there is error
	 */
	private String generateName() throws SmartScriptLexerException{
		removeBlanks();
		
		String s = new String();
		
		if(Character.isLetter(data[currentIndex])) {
			s += data[currentIndex];
			currentIndex++;
		} else {
			throw new SmartScriptLexerException("Invalid variable name!");
		}
		
		s += readInputData((ch) -> {
			return Character.isLetter((char)ch) 
					|| Character.isDigit((char)ch)
					|| Character.valueOf((char)ch).equals(UNDERSCORE);
		});
				
		return s;
	}
	
	/**
	 * Generates token of type TEXT outside of tags. It includes
	 * all character until "{$" or to the end of the text.
	 * If '{' is escaped, like this: "\{", then it is not considered 
	 * to be part of the left bound of tag. Character '\' can also be 
	 * escaped, like this "\\". Any other escaping is invalid.
	 * 
	 * @throws SmartScriptLexerException if there is error
	 */
	private void generateText() throws SmartScriptLexerException {
		String value = "";
		
		while(true) {
			// if end of document is reached, then exit the loop
			if (currentIndex == data.length) {
				break;
			}
			
			// if backslash is read, it is considered escaping
			if(Character.valueOf(data[currentIndex]).equals(BACKSLASH)) {
				currentIndex++;
				
				// if escaping is valid, continue
				if(currentIndex < data.length 
						&& ((Character.valueOf(data[currentIndex]).equals(BACKSLASH))
						|| (Character.valueOf(data[currentIndex]).equals(LEFT_CURLY_BRACKET)))){
					value += data[currentIndex - 1];
					value += data[currentIndex];
					currentIndex++;
					continue;
				// else, throw exception
				} else {
					throw new SmartScriptLexerException("Invalid Escaping!");
				}
			}
			
			// if '{' is read, check if it is part of left tag bound
			if(Character.valueOf(data[currentIndex]).equals(LEFT_CURLY_BRACKET)) {
				
				// if next character is '$' then we read left bound of a tag
				if(currentIndex < data.length - 1 && Character.valueOf(data[currentIndex + 1]).equals(DOLLAR_SIGN)) {
					//currentIndex--;
					setState(SmartScriptLexerState.TAG);
					break;
				// else continue
				} else {
					value += data[currentIndex];
					currentIndex++;
				}
			}
			
			// read until '{' or '\' is read
			String s = readInputData((ch) -> {
				return !Character.valueOf((char)ch).equals(LEFT_CURLY_BRACKET)
						&& !Character.valueOf((char)ch).equals(BACKSLASH);
			});
			
			// if nothing was read in this iteration, then exit the loop
			if (s.length() == 0) break;
			
			value += s;
		}
		
		// if nothing at all was read, then search for another token
		if(value.length() == 0) {
			token = nextToken();
			return;
		}
		
		token = new SmartScriptToken(SmartScriptTokenType.TEXT, value);
	}
	
	/**
	 * Generates number. If it cannot be parsed as a Integer, then it
	 * is parsed as Double.
	 * 
	 * @throws SmartScriptLexerException if there is error
	 */
	private void generateNumber() throws SmartScriptLexerException {
		int multiplier = 1; 

		if(Character.valueOf(data[currentIndex]).equals(MINUS)) {
			multiplier = -1;
			currentIndex++;
		}
		
		String s = readInputData((ch) -> {
			return Character.isDigit((char)ch) || Character.valueOf((char)ch).equals(DOT);
		});
		
		if (currentIndex > 1 && Character.valueOf(data[currentIndex - 1]).equals(DOT)) {
			currentIndex--;
			s = s.substring(0, s.length()-1);
		}
		
		
		try {
			Integer intValue = Integer.parseInt(s);
			token = new SmartScriptToken(SmartScriptTokenType.INTEGER_NUMBER, multiplier * intValue);
		} catch(NumberFormatException ex) {
			try {
				Double value = Double.parseDouble(s);
				token = new SmartScriptToken(SmartScriptTokenType.DOUBLE_NUMBER, multiplier * value);
			} catch(NumberFormatException ex2) {
				throw new SmartScriptLexerException("Number should be in long or double format!");
			}
		}
	}
	
	/**
	 * Reads input data while tester is accepting it and
	 * returns string that it read.
	 * 
	 * @param tester tester used for accepting input data
	 */
	private String readInputData(Tester tester) {
		int indexBeginning = currentIndex;
		
		while (currentIndex < data.length) {
			if (tester.test(data[currentIndex])) {
				currentIndex++;
			}
			else {
				break;
			}
		}
		
		char[] value = Arrays.copyOfRange(data, indexBeginning, currentIndex);
		return String.valueOf(value);
	}
	
	/**
	 * Returns last generated token. Calling this method
	 * will not result in generating new token.
	 * 
	 * @return last generated token
	 */
	public SmartScriptToken getToken() {
		return token;
	}
	
	/**
	 * Sets state of lexer to a given state.
	 * 
	 * @param state new state of lexer
	 * @throws NullPointerException if given state is null
	 */
	public void setState(SmartScriptLexerState state) throws NullPointerException{
		this.state = Objects.requireNonNull(state);
		stateCounter=0;
	}
	
	/**
	 * Removes all blank characters and sets currentIndex to next character
	 * that is not blank or to the end of document.
	 */
	private void removeBlanks() {
		while(currentIndex < data.length 
				&& BLANK_CHARACTERS.contains(Character.toString((data[currentIndex])))) {
			currentIndex ++;
		}
	}

}
