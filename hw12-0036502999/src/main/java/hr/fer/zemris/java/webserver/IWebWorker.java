package hr.fer.zemris.java.webserver;

/**
 * Defines web worker used in {@link SmartHttpServer}.
 * 
 * @author Mateo Puhalović
 *
 */
public interface IWebWorker {
	
	/**
	 * Processes request with given context.
	 * 
	 * @param context context
	 * @throws Exception if error occurs
	 */
	public void processRequest(RequestContext context) throws Exception;

}
