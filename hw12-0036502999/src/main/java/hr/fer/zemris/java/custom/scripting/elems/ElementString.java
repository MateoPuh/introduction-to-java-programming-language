package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents {@link Element} which represents String
 * in an expression. 
 * 
 * <p>It provides method for getting its content.</p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.elems
 */
public class ElementString extends Element {

	/** Content of String */
	private String value;
	
	/** Quotation mark used for beginning and ending String parameter */
	private static final String STRING_QUOTATION_MARK = "\"";
	
	/** Quotation mark character */
	private static final char QUOTATION_MARK = '\"';
	
	/** Backslash used for escaping */
	private static final String STRING_BACKSLASH = "\\";
	
	/** Backslash character */
	private static final char BACKSLASH = '\\';
	
	/**
	 * Creates new {@link ElementString} with given value.
	 * 
	 * @param value String content
	 */
	public ElementString(String value) {
		this.value = value;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText() {
		char[] data = value.toCharArray();
		
		String result = new String();
		
		for(int i = 0; i < data.length; i++) {
			if(Character.valueOf(data[i]).equals(QUOTATION_MARK)
					|| Character.valueOf(data[i]).equals(BACKSLASH)) {
				result += STRING_BACKSLASH;
			}
			result += data[i];
		}		
		return result;
		//return STRING_QUOTATION_MARK + result + STRING_QUOTATION_MARK;
	}
	
	/**
	 * Returns content of a String.
	 * 
	 * @return content of a String
	 */
	public String getValue() {
		return value;
	}

}
