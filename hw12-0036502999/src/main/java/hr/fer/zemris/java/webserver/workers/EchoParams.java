package hr.fer.zemris.java.webserver.workers;

import java.io.IOException;
import java.util.Set;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * {@link IWebWorker} that displays given parameters.
 * 
 * @author Mateo Puhalović
 *
 */
public class EchoParams implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) {
		context.setMimeType("text/html");
		Set<String> names = context.getParameterNames();
		try {
			context.write("<html><body>");
			context.write("<head><title>Tablica parametara</title>\r\n</head>");			
			context.write("</body></html>");
			context.write("<h1>Tablica parametara:</h1>");
			context.write("<table><tbody>");
			
			for(String name : names) {
				context.write("<tr><td>" + name + ": </td><td>" + context.getParameter(name) + "</td></tr>\r\n");
			}
			
			context.write("</tbody></table>");

		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}
}