package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.NoSuchElementException;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;

/**
 * This class represents base class for all other nodes from 
 * hr.fer.zemris.java.custom.nodes package. Nodes are used 
 * for representation of structured document.
 * 
 * <p>It provides methods for adding children, getting child
 * at given index and getting number of children. These methods
 * use {@link ArrayIndexedCollection} collection in its implementation. 
 * This collection is created only when needed. That means that
 * collection will be created only when user adds first child.</p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.nodes
 */
public abstract class Node {
	
	/** collection used as children storage */
	private ArrayIndexedCollection collection;
	
	/**
	 * Adds new child to this nodes internal collection. If 
	 * collection is not created, now it will be.
	 * 
	 * @param child child which will be added to this node
	 */
	public void addChildNode(Node child) {
		if (collection == null) {
			collection = new ArrayIndexedCollection();
		}
		
		collection.add(child);
	}
	
	/**
	 * Returns number of direct children.
	 * 
	 * @return number of direct children
	 */
	public int numberOfChildren() {
		if (collection != null) {
			return collection.size();
		}
		
		return 0;
	}
	
	/**
	 * Returns child at given index or throws exception
	 * if index is invalid.
	 * 
	 * @param index index of child 
	 * @return child at given index
	 * @throws NoSuchElementException if index is invalid
	 */
	public Node getChild(int index) throws NoSuchElementException {
		try {
			return (Node)collection.get(index);
		} catch (IndexOutOfBoundsException ex) {
			throw new NoSuchElementException("There is no child for given index!");
		}
	}
	
	/**
	 * Method that visitor calls when it visits this node.
	 * 
	 * @param visitor visitor
	 */
	public abstract void accept(INodeVisitor visitor);
	
}
