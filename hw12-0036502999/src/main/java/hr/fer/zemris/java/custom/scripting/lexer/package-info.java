﻿/**
 * This package provides classes {@link SmartScriptLexer} and
 * {@link SmartScriptToken} used for performing lexical analysis.
 * It also provides two enums: {@link SmartScriptLexerState}, 
 * which represents state in which SmartScriptLexer can be in and
 * {@link SmartScriptTokenType} which represent type of token that
 * is analyzed.
 * 
 * <p>It performs lexical analysis on document which consists of
 * tags (bounded by {$ and $}) and rest of the text.</p>
 * 
 * <p>If any error happens while analyzing given document, 
 * {@link SmartScriptLexerException} is thrown.</p>	
 *  
 * @author Mateo Puhalović
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.lexer;