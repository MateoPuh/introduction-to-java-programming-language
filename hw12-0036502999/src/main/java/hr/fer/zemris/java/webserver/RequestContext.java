package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * This class represents context of request. It contains method for writing
 * context to given outputStream and inner static class {@link RCCookie} which
 * represents cookie.
 * 
 * @author Mateo Puhalović
 *
 */
public class RequestContext {
	
	/** Default encoding. */
	private static final String DEFAULT_ENCODING = "UTF-8";
	/** Default status. */
	private static final int DEFAULT_STATUS = 200;
	/** Default status. */
	private static final String DEFAULT_STATUS_TEXT = "OK";
	/** Default mime type. */
	private static final String DEFAULT_MIME_TYPE = "text/html";
	
	private OutputStream outputStream;
	private Charset charset;
	private String encoding;
	private int statusCode;
	private String statusText;
	private String mimeType;
	private Long contentLength;
	private Map<String,String> parameters;
	private Map<String,String> temporaryParameters;
	private Map<String,String> persistentParameters;
	private List<RCCookie> outputCookies;
	private boolean headerGenerated;
	private IDispatcher dispatcher;
	private String SID;
		
	/**
	 * Creates new {@link RequestContext}.
	 * 
	 * @param outputStream stream on which context will be written
	 * @param parameters parameters
	 * @param persistentParameters persistent parameters
	 * @param outputCookies cookies
	 */
	public RequestContext(OutputStream outputStream, Map<String, String> parameters,
			Map<String, String> persistentParameters, List<RCCookie> outputCookies) {
		this.outputStream = Objects.requireNonNull(outputStream);
		this.parameters = parameters != null ? parameters : new HashMap<>();
		this.persistentParameters = persistentParameters != null ?	persistentParameters : new HashMap<>();
		this.outputCookies = outputCookies != null ? outputCookies : new ArrayList<>();
		this.temporaryParameters = new HashMap<String, String>();
		
		this.encoding = DEFAULT_ENCODING;
		this.statusCode = DEFAULT_STATUS;
		this.statusText = DEFAULT_STATUS_TEXT;
		this.mimeType = DEFAULT_MIME_TYPE;
	}
	
	/**
	 * Creates new {@link RequestContext}.
	 * 
	 * @param outputStream stream on which context will be written
	 * @param parameters parameters
	 * @param persistentParameters persistent parameters
	 * @param outputCookies cookies
	 * @param temporaryParameters temporary parameters
	 * @param dispatcher dispatcher
	 */
	public RequestContext(OutputStream outputStream, Map<String, String> parameters,
			Map<String, String> persistentParameters, List<RCCookie> outputCookies,
			Map<String,String> temporaryParameters, IDispatcher dispatcher) {
		this(outputStream, parameters, persistentParameters, outputCookies);
		this.temporaryParameters = temporaryParameters != null ? temporaryParameters : new HashMap<String, String>();
		this.dispatcher = dispatcher;
	}
	
	/**
	 * Creates new {@link RequestContext}.
	 * 
	 * @param outputStream stream on which context will be written
	 * @param parameters parameters
	 * @param persistentParameters persistent parameters
	 * @param outputCookies cookies
	 * @param temporaryParameters temporary parameters
	 * @param dispatcher dispatcher
	 * @param SID session id
	 */
	public RequestContext(OutputStream outputStream, Map<String, String> parameters,
			Map<String, String> persistentParameters, List<RCCookie> outputCookies,
			Map<String,String> temporaryParameters, IDispatcher dispatcher, String SID) {
		this(outputStream, parameters, persistentParameters, outputCookies, temporaryParameters, dispatcher);
		this.SID = SID;
	}
	
	/**
	 * This class represents cookie inside of context.
	 * 
	 * @author Mateo Puhalović
	 *
	 */
	public static class RCCookie {
		
		private String name;
		private String value;
		private String domain;
		private String path;
		private Integer maxAge;
		private boolean httpOnly;
				
		/**
		 * Creates new {@link RCCookie}.
		 * 
		 * @param name name
		 * @param value value
		 * @param maxAge max age
		 * @param domain domain
		 * @param path path
		 */
		public RCCookie(String name, String value, Integer maxAge, String domain, String path) {
			this.name = Objects.requireNonNull(name);
			this.value = Objects.requireNonNull(value);
			this.domain = domain;
			this.path = path;
			this.maxAge = maxAge;
		}
		
		/**
		 * Returns name.
		 * 
		 * @return name
		 */
		public String getName() {
			return name;
		}
		
		/**
		 * Returns value.
		 * 
		 * @return value
		 */
		public String getValue() {
			return value;
		}
		
		/**
		 * Returns domain.
		 * 
		 * @return domain
		 */
		public String getDomain() {
			return domain;
		}
		
		/**
		 * Returns path.
		 * 
		 * @return path
		 */
		public String getPath() {
			return path;
		}
		
		/**
		 * Returns maxAge.
		 * 
		 * @return maxAge
		 */
		public Integer getMaxAge() {
			return maxAge;
		}	
		
		/**
		 * Sets cookie httpOnly flag to given value.
		 * 
		 * @param httpOnly httpOnly flag
		 */
		public void setHttpOnly(boolean httpOnly) {
			this.httpOnly = httpOnly;
		}
		
		/**
		 * Returns value of httpOnly flag.
		 * 
		 * @return httpOnly flag
		 */
		public boolean isHttpOnly() {
			return httpOnly;
		}
		
	}

	/** 
	 * Method that retrieves value from parameters map (or null if no association exists).
	 * 
	 * @param name name
	 * @return parameter
	 */
	public String getParameter(String name) {
		return parameters.get(Objects.requireNonNull(name));
	}
	
	/** 
	 * Method that retrieves names of all parameters in parameters map (note, 
	 * this set must be read-only).
	 * 
	 * @return set of names
	 */
	public Set<String> getParameterNames() {
		return Collections.unmodifiableSet(parameters.keySet());
	}
	
	/** 
	 * Method that retrieves value from persistentParameters map (or null if 
	 * no association exists)
	 * 
	 * @param name name
	 * @return persistent parameter
	 */
	public String getPersistentParameter(String name) {
		return persistentParameters.get(Objects.requireNonNull(name));
	}
	
	/** 
	 * Method that retrieves names of all parameters in persistent parameters 
	 * map (note, this set must be readonly).
	 * 
	 * @return set of names
	 */
	public Set<String> getPersistentParameterNames() {
		return Collections.unmodifiableSet(persistentParameters.keySet());
	}
	
	/** 
	 * Method that stores a value to persistentParameters map.
	 * 
	 * @param name name
	 * @param value value
	 */
	public void setPersistentParameter(String name, String value) {
		persistentParameters.put(Objects.requireNonNull(name), value);
	}
	
	/** 
	 * Method that removes a value from persistentParameters map.
	 * 
	 * @param name name
	 */
	public void removePersistentParameter(String name) {
		persistentParameters.remove(Objects.requireNonNull(name));
	}
	
	/** 
	 * Method that retrieves value from temporaryParameters map (or null 
	 * if no association exists).
	 * 
	 * @param name name
	 * @return parameter
	 */
	public String getTemporaryParameter(String name) {
		return temporaryParameters.get(Objects.requireNonNull(name));
	}
	
	/** 
	 * Method that retrieves names of all parameters in temporary parameters map 
	 * (note, this set must be readonly).
	 * 
	 * @return set of temporary names
	 */
	public Set<String> getTemporaryParameterNames() {		
		return Collections.unmodifiableSet(temporaryParameters.keySet());
	}
	
	/** 
	 * Method that retrieves an identifier which is unique for current user session.
	 * 
	 * @return SID
	 */
	public String getSessionID() {
		return SID;
	}
	
	/** 
	 * Method that stores a value to temporaryParameters map.
	 * 
	 * @param name name
	 * @param value value
	 */
	public void setTemporaryParameter(String name, String value) {
		temporaryParameters.put(Objects.requireNonNull(name), value);
	}
	
	/** 
	 * Method that removes a value from temporaryParameters map.
	 * 
	 * @param name
	 */
	public void removeTemporaryParameter(String name) {
		temporaryParameters.remove(Objects.requireNonNull(name));
	}
	
	/**
	 * Writes byte array to outputStram given in constructor.
	 * 
	 * @param data byte array
	 * @return context
	 * @throws IOException if error occurs
	 */
	public RequestContext write(byte[] data) throws IOException {
		generateHeadersIfNeeded();
		
		outputStream.write(data);
		return this;
	}
	
	/**
	 * Writes byte array to outputStram given in constructor.
	 * 
	 * @param data byte array
	 * @param offset offset
	 * @param len length
	 * @return context
	 * @throws IOException if error occurs
	 */
	public RequestContext write(byte[] data, int offset, int len) throws IOException {
		generateHeadersIfNeeded();
		
		outputStream.write(data, offset, len);
		return this;
	}
	
	/**
	 * Writes string to outputStram given in constructor.
	 * 
	 * @param data byte array
	 * @return context
	 * @throws IOException if error occurs
	 */
	public RequestContext write(String text) throws IOException {
		generateHeadersIfNeeded();
		return write(text.getBytes(this.charset));
	}

	/**
	 * Generates headers if header is not already generated.
	 * 
	 * @throws IOException if error occurs
	 */
	private void generateHeadersIfNeeded() throws IOException {
		if(headerGenerated) {
			return;
		}
		
		this.headerGenerated = true;		
		this.charset = Charset.forName(encoding);
		
		StringBuilder sb = new StringBuilder();
		sb.append("HTTP/1.1 " + statusCode + " " + statusText + "\r\n");
		sb.append("Content-Type: " + mimeType);
		
		if(mimeType.startsWith("text/")) {
			sb.append("; charset=" + encoding);
		}
		sb.append("\r\n");
		
		if(contentLength != null) {
			sb.append("Content-Length:" + contentLength + "\r\n");
		}
		
		if(!outputCookies.isEmpty()) {
			for(RCCookie cookie : outputCookies) {
				sb.append("Set-Cookie: " + cookie.getName() + "=\"" + cookie.getValue() + "\"");
				
				if(cookie.getDomain() != null) {
					sb.append("; Domain=" + cookie.getDomain());
				}
				
				if(cookie.getPath() != null) {
					sb.append("; Path=" + cookie.getPath());
				}
				
				if(cookie.getMaxAge() != null) {
					sb.append("; Max-age=" + cookie.getMaxAge());
				}
				
				if(cookie.isHttpOnly()) {
					sb.append("; HttpOnly");
				}
				
				sb.append("\r\n");
			}
		}
		
		sb.append("\r\n");

		outputStream.write(sb.toString().getBytes(StandardCharsets.ISO_8859_1));
	}
	
	/**
	 * Sets context encoding.
	 * 
	 * @param encoding
	 */
	public void setEncoding(String encoding) {
		if(headerGenerated) {
			throw new UnsupportedOperationException("Header already generated!");
		}
		this.encoding = encoding;
	}

	/**
	 * Sets status code.
	 * 
	 * @param statusCode status code
	 */
	public void setStatusCode(int statusCode) {
		if(headerGenerated) {
			throw new UnsupportedOperationException("Header already generated!");
		}
		this.statusCode = statusCode;
	}

	/**
	 * Sets status text.
	 * 
	 * @param statusText status text
	 */
	public void setStatusText(String statusText) {
		if(headerGenerated) {
			throw new UnsupportedOperationException("Header already generated!");
		}
		this.statusText = statusText;
	}

	/**
	 * Sets mime type.
	 * 
	 * @param mimeType mime type
	 */
	public void setMimeType(String mimeType) {
		if(headerGenerated) {
			throw new UnsupportedOperationException("Header already generated!");
		}
		this.mimeType = mimeType;
	}

	/**
	 * Sets content length.
	 * 
	 * @param contentLength content length
	 */
	public void setContentLength(Long contentLength) {
		if(headerGenerated) {
			throw new UnsupportedOperationException("Header already generated!");
		}
		this.contentLength = contentLength;
	}
	
	/**
	 * Adds cookie.
	 * 
	 * @param rCookie cookie
	 */
	public void addRCCookie(RCCookie rCookie) {
		outputCookies.add(rCookie);
	}

	/**
	 * Returns dispatcher.
	 * 
	 * @return dispatcher
	 */
	public IDispatcher getDispatcher() {
		return dispatcher;
	}

}
