package hr.fer.zemris.java.custom.collections;

/**
 * This class represents model of an object capable of 
 * performing some operation on the passed object.
 * Its method process(Object) is used for that reason.
 * 
 * <p>It is an conceptual contract between users, which 
 * will have objects to be processed, and each 
 * concrete Processor which knows how to perform the 
 * selected operation.</p>
 * 
 * 
 * @author Mateo Puhalović
 * @version 1.0
 * 
 */
public interface Processor {
	
	/**
	 * Method that is used to process given object. 
	 * 
	 * @param value object to process
	 */
	void process(Object value);
	
}
