package hr.fer.zemris.java.custom.scripting.parser;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.custom.scripting.elems.*;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexerException;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptToken;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptTokenType;
import hr.fer.zemris.java.custom.scripting.nodes.*;
/**
 * This class represents parser which performs 
 * parsing on document which consists of tags 
 * (bounded by {$ and $}) and rest of the text. 
 * 
 * <p>It uses {@link SmartScriptLexer} for generating tokens 
 * and then {@link Node} and {@link Element} classes for 
 * creating tree construction.</p>
 * 
 * <p>It provides method for returning first node 
 * ({@link DocumentNode} type) which can be used for
 * accessing whole tree construction through its children.</p>
 * 
 * <p>If any error happens while parsing given document, 
 * {@link SmartScriptParserException} is thrown.</p>	
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class SmartScriptParser {

	/** Document which is parsed */
	@SuppressWarnings("unused")
	private String document;
	
	/** Lexer used for generating tokens */
	private SmartScriptLexer lexer;
	
	/** Stack used for generating tree construction */
	private ObjectStack stack;
	
	/** Document node which contains whole tree structure */
	private DocumentNode documentNode;
	
	/**
	 * Creates new {@link SmartScriptParser} with given document
	 * which will be parsed. That document is also used for 
	 * creating {@link SmartScriptLexer}.
	 * 
	 * @param document document which will be parsed
	 * @throws NullPointerException if given document is null
	 */
	public SmartScriptParser(String document) throws SmartScriptParserException {
		if(document == null) {
			throw new SmartScriptParserException("Document should not be null!");
		}
		
		this.document = document;
		lexer = new SmartScriptLexer(document);
		stack = new ObjectStack();		
		parse();
	}
	
	/**
	 * Method that parses given document and constructs tree structure.
	 * 
	 * @throws SmartScriptParserException if there is error
	 */
	private void parse() throws SmartScriptParserException {
		documentNode = new DocumentNode();
		
		stack.push(documentNode);
				
		while(true) {
			SmartScriptToken next = null;
			
			try {
				 next = lexer.nextToken();
			} catch(SmartScriptLexerException e) {
				throw new SmartScriptParserException(e.getMessage());
			}
			
			if(next.getType().equals(SmartScriptTokenType.EOF)) {
				if(stack.size() > 1) {
					throw new SmartScriptParserException(stack.peek() + " tag is not closed!");
				}
				
				break;
			}
			
			if(next.getType().equals(SmartScriptTokenType.TEXT)) {
				TextNode newNode = new TextNode((String)next.getValue());	
				AddChildToLastNode(newNode);
			}
			
			if(next.getType().equals(SmartScriptTokenType.TAG_LEFT_BOUND)) {
				SmartScriptToken tagName = null;
				
				try {
					 tagName = lexer.nextToken();
				} catch(SmartScriptLexerException e) {
					throw new SmartScriptParserException(e.getMessage());
				}
				
				switch(tagName.getType()) {
				case TAG_NAME_FOR:
					ForLoopNode forLoopNode = parseForLoop();
					addToStack(forLoopNode);
					break;
				case TAG_NAME_ECHO:
				case TAG_NAME:
					EchoNode echoNode = parseEchoNode(tagName);
					AddChildToLastNode(echoNode);
					break;
				case TAG_NAME_END:
					EchoNode endNode = parseEchoNode(tagName);
					AddChildToLastNode(endNode);
					
					if(stack.size() > 1) {
						stack.pop();
					} else {
						throw new SmartScriptParserException("End tag closes nothing!");
					}
					break;
				default:
				
				}
			}
		}
	}
	
	/**
	 * Adds given node as a child to the last node
	 * on stack. Last node is result of peek() method.
	 * 
	 * @param node node to add as a child
	 */
	private void AddChildToLastNode(Node node) {
		Node lastNode = (Node)stack.peek();

		lastNode.addChildNode(node);
	}
	
	/**
	 * Adds given node as a child to the last node
	 * and pushes given node to the stack.
	 * 
	 * @param node node to add as a child and push on the stack
	 */
	private void addToStack(Node node) {
		AddChildToLastNode(node);
		
		stack.push(node);
	}
	
	/**
	 * Method used for parsing EchoNode. Given token
	 * is tag name. Other tokens are generated with 
	 * lexer until it reached right tag bound.
	 * 
	 * @param token tag name
	 * @return parsed EchoNode
	 * @throws SmartScriptParserException if there is error
	 */
	private EchoNode parseEchoNode(SmartScriptToken token) throws SmartScriptParserException {
		ArrayIndexedCollection elements = new ArrayIndexedCollection();
		
		elements.add(new ElementVariable((String)token.getValue()));
		
		try {
			while(true) {
				Element newElement = parseEchoParameter();
				elements.add(newElement);
			}
		} catch(SmartScriptParserException ex) {			
			if(lexer.getToken().getType().equals(SmartScriptTokenType.TAG_RIGHT_BOUND)) {
				
				if(elements.size() == 0) {
					throw new SmartScriptParserException("Too few arguments!");
				}
				
				Element[] result = new Element[elements.size()];
				
				for (int i = 0; i < elements.size(); i++) {
					result[i] = (Element)elements.get(i);
				}
				
				return new EchoNode(result);
			} else {
				throw new SmartScriptParserException(ex.getLocalizedMessage());
			}
		}
	}
	
	/**
	 * Method used for parsing parameter for EchoNode.
	 * EchoNode parameter can be variable, string, number (double and integer),
	 * operator and function.
	 * 
	 * @return parsed EchoNode parameter
	 * @throws SmartScriptParserException if there is error
	 */
	private Element parseEchoParameter() throws SmartScriptParserException {
		try {
			SmartScriptToken nextToken = lexer.nextToken();
			if(nextToken.getType().equals(SmartScriptTokenType.VARIABLE)) {
				return new ElementVariable((String)nextToken.getValue());
			} else if(nextToken.getType().equals(SmartScriptTokenType.STRING)) {
				return new ElementString((String)nextToken.getValue());
			} else if(nextToken.getType().equals(SmartScriptTokenType.DOUBLE_NUMBER)) {
				return new ElementConstantDouble((Double)nextToken.getValue());
			} else if(nextToken.getType().equals(SmartScriptTokenType.INTEGER_NUMBER)) {
				return new ElementConstantInteger((Integer)nextToken.getValue());
			} else if(nextToken.getType().equals(SmartScriptTokenType.OPERATOR)) {
				return new ElementOperator((String)nextToken.getValue());
			} else if(nextToken.getType().equals(SmartScriptTokenType.FUNCTION)) {
				return new ElementFunction((String)nextToken.getValue());
			} else {
				throw new SmartScriptParserException(nextToken.getValue().toString() 
						+ " is not a valid For loop argument\n");
			}
		} catch(SmartScriptLexerException ex) {
			throw new SmartScriptParserException(ex.getMessage());
		}
	}
	
	/**
	 * Method used for parsing ForLoopNode. It parses
	 * 3 to 4 parameters and then right closing bound.
	 * If there is error, SmartScriptParserException is
	 * thrown.
	 * 
	 * @return parsed ForLoopNode
	 * @throws SmartScriptParserException if there is error
	 */
	private ForLoopNode parseForLoop() throws SmartScriptParserException {
		SmartScriptToken nextToken = null;
		
		try {
			 nextToken = lexer.nextToken();
		} catch(SmartScriptLexerException e) {
			throw new SmartScriptParserException(e.getMessage());
		}

		if(nextToken.getType().equals(SmartScriptTokenType.VARIABLE)) {
			ElementVariable variable = new ElementVariable((String)nextToken.getValue());
			
			Element startExpression = parseForLoopParameter();	
			Element endExpression = parseForLoopParameter();
			Element stepExpression = null;
			
			// try parsing last argument
			// if there is exception, then try parsing right bound of tag
			try {
				stepExpression = parseForLoopParameter();
			} catch(SmartScriptParserException e) {
				try{
					parseRightBound(lexer.getToken());
					return new ForLoopNode(variable, startExpression, endExpression, stepExpression);
				} catch(SmartScriptParserException e2) {
					throw new SmartScriptParserException(e.getMessage());
				}
			}
			
			parseRightBound(lexer.nextToken());
			return new ForLoopNode(variable, startExpression, endExpression, stepExpression);
			
		} else {
			throw new SmartScriptParserException("Wrong parameter type!");
		}
	}
	
	/**
	 * Parsed right tag bound. If there is error, 
	 * SmartScriptParserException is thrown.
	 * 	
	 * @param token token to parse
	 * @throws SmartScriptParserException if there is error
	 */
	private void parseRightBound(SmartScriptToken token) throws SmartScriptParserException {
		if(!(token.getType().equals(SmartScriptTokenType.TAG_RIGHT_BOUND))) {
			throw new SmartScriptParserException("Cannot parse end of tag!");
		}
	}
	
	/**
	 * Method used for parsing parameter for For-loop starting from second.
	 * Parameter for For-loop can be variable, string, number(double and integer).
	 * 
	 * @return parsed parameter
	 * @throws SmartScriptParserException If there is error
	 */
	private Element parseForLoopParameter() throws SmartScriptParserException {
		try {
			SmartScriptToken nextToken = lexer.nextToken();
			if(nextToken.getType().equals(SmartScriptTokenType.VARIABLE)) {
				return new ElementVariable((String)nextToken.getValue());
			} else if(nextToken.getType().equals(SmartScriptTokenType.STRING)) {
				return new ElementString((String)nextToken.getValue());
			} else if(nextToken.getType().equals(SmartScriptTokenType.DOUBLE_NUMBER)) {
				return new ElementConstantDouble((Double)nextToken.getValue());
			} else if(nextToken.getType().equals(SmartScriptTokenType.INTEGER_NUMBER)) {
				return new ElementConstantInteger((Integer)nextToken.getValue());
			} else {
				throw new SmartScriptParserException(nextToken.getValue().toString() 
						+ " is not a valid For loop argument\n");
			}
		} catch(SmartScriptLexerException ex) {
			throw new SmartScriptParserException(ex.getMessage());
		}
	}
	
	/**
	 * Returns documentNode, which was first put on stack.
	 * 
	 * @return document node
	 */
	public DocumentNode getDocumentNode() {
		return documentNode;
	}
	
}
