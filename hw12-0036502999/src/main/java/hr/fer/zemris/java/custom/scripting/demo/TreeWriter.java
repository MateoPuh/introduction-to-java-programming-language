package hr.fer.zemris.java.custom.scripting.demo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * Program that accepts file name as a single argument from command line. Program
 * parses it into a tree and reproduces its content on System.out.
 * 
 * @author Mateo Puhalović
 *
 */
public class TreeWriter {
	
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("Invalid number of arguments. Expected: 1, got: " + args.length);
			return;
		}
		
		String docBody = null;
		
		try {
			docBody = new String(
					 Files.readAllBytes(Paths.get(args[0])),
					 StandardCharsets.UTF_8
					);
		} catch (IOException ex) {
			System.out.println("Cannot read file: " + ex.getMessage());
			System.exit(1);
		} catch (Exception e1) {
			System.out.println(e1.getLocalizedMessage());
			System.exit(1);
		}
		
		SmartScriptParser p = new SmartScriptParser(docBody);
		WriterVisitor visitor = new WriterVisitor();
		
		p.getDocumentNode().accept(visitor);
	}
	
	private static class WriterVisitor implements INodeVisitor {

		@Override
		public void visitTextNode(TextNode node) {
			System.out.print(node.getText());
			writeChildren(node);
		}

		@Override
		public void visitForLoopNode(ForLoopNode node) {
			System.out.print(node.toString());
			writeChildren(node);
		}

		@Override
		public void visitEchoNode(EchoNode node) {
			System.out.print(node.toString());
			writeChildren(node);
		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			writeChildren(node);
		}
		
		private void writeChildren(Node node) {
			for(int i = 0; i < node.numberOfChildren(); i++) {
				node.getChild(i).accept(this);
			}
		}
		
	}

}
