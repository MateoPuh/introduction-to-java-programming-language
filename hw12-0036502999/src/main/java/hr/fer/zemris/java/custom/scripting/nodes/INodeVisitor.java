package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Represents Visitor objects for {@link Node}.
 * 
 * @author Mateo Puhalović
 *
 */
public interface INodeVisitor {
	
	/**
	 * Method that is called when visitor visits {@link TextNode}.
	 * 
	 * @param node text node
	 */
	public void visitTextNode(TextNode node);
	
	/**
	 * Method that is called when visitor visits {@link ForLoopNode}.
	 * 
	 * @param node for loop node
	 */
	public void visitForLoopNode(ForLoopNode node);
	
	/**
	 * Method that is called when visitor visits {@link EchoNode}.
	 * 
	 * @param node echo node
	 */
	public void visitEchoNode(EchoNode node);
	
	/**
	 * Method that is called when visitor visits {@link DocumentNode}.
	 * 
	 * @param node document node
	 */
	public void visitDocumentNode(DocumentNode node);


}
