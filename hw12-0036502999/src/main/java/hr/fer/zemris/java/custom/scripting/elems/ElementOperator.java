package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents {@link Element} which represents operator
 * in an expression. 
 * 
 * <p>It provides method for getting its symbol.</p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.elems
 */
public class ElementOperator extends Element {

	/** Operators symbol */
	private String symbol;
	
	/**
	 * Creates new {@link ElementOperator} with given symbol.
	 * 
	 * @param symbol symbol of operator
	 */
	public ElementOperator(String symbol) {
		this.symbol = symbol;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText() {
		return symbol;
	}

	/**
	 * Returns symbol of operator.
	 * 
	 * @return symbol of operator
	 */
	public String getSymbol() {
		return symbol;
	}	
	
}
