package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents {@link Element} which represents function
 * in an expression. 
 * 
 * <p>It provides method for getting its name.</p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.elems
 */
public class ElementFunction extends Element {

	/** Name of function */
	private String name;
	
	/**
	 * Creates new {@link ElementFunction} with given
	 * name.
	 * 
	 * @param name name of function
	 */
	public ElementFunction(String name) {
		this.name = name;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText() {
		return name;
	}

	/**
	 * Returns name of a function.
	 * 
	 * @return name of a function
	 */
	public String getName() {
		return name;
	}
	
}
