package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;

/**
 * This class represents {@link Node} used for representing a 
 * command which generates some textual output dynamically
 * in a structured document.
 * 
 * <p>It provides methods for getting its elements which are 
 * represented with {@link ElementVariable} class.</p>
 *  
 * @author Mateo Puhalović
 * @version 1.0
 * @see hr.fer.zemris.java.custom.nodes
 */
public class EchoNode extends Node {

	/** Left bound of for node */
	private static final String LEFT_BOUND = "{$";

	/** Right bound of for node */
	private static final String RIGHT_BOUND = "$}";
	
	/** Elements of echo node */
	private Element[] elements;

	/**
	 * Creates new {@link EchoNode} with given elements.
	 * 
	 * @param elements elements for new EchoNode
	 */
	public EchoNode(Element[] elements) {
		this.elements = elements;
	}
	
	/**
	 * Returns elements used in this node.
	 * 
	 * @return elements of this node
	 */
	public Element[] getElements() {
		return elements;
	}
	
	/**
	 * Returns string in format: 
	 * "{$ = element[0] element[1] ... element[size-1] $}".
	 * 
	 * @return formatted string
	 */
	@Override
	public String toString() {
		String result = LEFT_BOUND ;
		
		for (Element e : elements) {
			String text = e.asText();
			
			if(e instanceof ElementString) {
				text = "\"" + text;
				text += text + "\"";
			}
			result += " " + e.asText();
		}
		
		return result + " " + RIGHT_BOUND; 
	}

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitEchoNode(this);		
	}
	
}
