package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * {@link IWebWorker} that adds two given numbers: a and b.
 * 
 * @author Mateo Puhalović
 *
 */
public class SumWorker implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) {
		context.setMimeType("text/html");

		int a = 1;
		int b = 2;
		try {
			a = Integer.parseInt(context.getParameter("a"));		
		}catch(NullPointerException | NumberFormatException ignorable) {
		}
		
		try {
			b = Integer.parseInt(context.getParameter("b"));
		}catch(NullPointerException | NumberFormatException ignorable) {
		}

		int result = a + b;
		context.setTemporaryParameter("zbroj", String.valueOf(result));
		context.setTemporaryParameter("varA", String.valueOf(a));
		context.setTemporaryParameter("varB", String.valueOf(b));
		
		context.setTemporaryParameter("imgName", result % 2 == 0 ? "/images/mustang.png" : "/images/jon_snow.png");
		try {
			context.getDispatcher().dispatchRequest("/private/pages/calc.smscr");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}