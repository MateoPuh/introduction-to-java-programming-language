package hr.fer.zemris.java.p12.dao;

import java.util.List;

import hr.fer.zemris.java.hw14.polls.Poll;
import hr.fer.zemris.java.hw14.polls.PollOption;

/**
 * Sučelje prema podsustavu za perzistenciju podataka.
 * 
 * @author marcupic
 *
 */
public interface DAO {
	
	/**
	 * Returns list of all {@link Poll} objects in POLLS table.
	 * 
	 * @return List of polls
	 */
	List<Poll> getPolls();
	
	/**
	 * Returns {@link Poll} with given id.
	 * 
	 * @param pollId poll id
	 * @return poll with given id
	 */
	Poll getPoll(long pollId);
	
	/**
	 * Returns list of all {@link PollOption} objects with given
	 * poll id.
	 * 
	 * @param pollId poll id
	 * @return list of poll options for given poll
	 */
	List<PollOption> getPollOptions(long pollId);
	
	/**
	 * Increments {@link PollOption}'s votesCount.
	 * 
	 * @param optionId option id
	 */
	void increment(long optionId);
	
}