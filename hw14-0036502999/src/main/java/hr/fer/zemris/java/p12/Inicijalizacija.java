package hr.fer.zemris.java.p12;

import java.beans.PropertyVetoException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.NoSuchElementException;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

@WebListener
public class Inicijalizacija implements ServletContextListener {
	
	private static final String DB_SETTINGS = "WEB-INF/dbsettings.properties";
	private static final String POLL_OPTIONS_BANDS = "WEB-INF/glasanje-definicija.txt";
	private static final String POLL_OPTIONS_MOVIES = "WEB-INF/glasanje2-definicija.txt";

	private static final String CREATE_POLLS = "CREATE TABLE Polls" + 
			" (id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," + 
			" title VARCHAR(150) NOT NULL," + 
			" message CLOB(2048) NOT NULL" + 
			")";
	
	private static final String CREATE_POLL_OPTIONS = "CREATE TABLE PollOptions" + 
			" (id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," + 
			" optionTitle VARCHAR(100) NOT NULL," + 
			" optionLink VARCHAR(150) NOT NULL," + 
			" pollID BIGINT," + 
			" votesCount BIGINT," + 
			" FOREIGN KEY (pollID) REFERENCES Polls(id)" + 
			")";

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		Properties props = new Properties();
		try {
			props.load(new FileInputStream(sce.getServletContext().getRealPath(DB_SETTINGS)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		String host = props.getProperty("host");
		String port = props.getProperty("port");
		String dbName = props.getProperty("name");
		String user = props.getProperty("user");
		String password = props.getProperty("password");
		
		if(host == null || port == null || dbName == null
				|| user == null || password == null) {
			throw new NoSuchElementException("Missing property.");
		}
		
		String connectionURL = "jdbc:derby://" + host + ":" + port + "/" 
				+ dbName ;

		ComboPooledDataSource cpds = new ComboPooledDataSource();
		try {
			cpds.setDriverClass("org.apache.derby.jdbc.ClientDriver");
		} catch (PropertyVetoException e1) {
			throw new RuntimeException("Pogreška prilikom inicijalizacije poola.", e1);
		}
		cpds.setJdbcUrl(connectionURL);
		cpds.setUser(user);
		cpds.setPassword(password);
		
		sce.getServletContext().setAttribute("hr.fer.zemris.dbpool", cpds);
		
		try {
			createPollTables(cpds);
			populateTables(cpds, sce);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * Method used for populating polls and pollOptions tables.
	 * 
	 * @param cpds combo pooled data source
	 * @param sce context
	 * @throws SQLException if error occurs
	 */
	private void populateTables(ComboPooledDataSource cpds, ServletContextEvent sce) throws SQLException {
		Connection con = null;
		try {
			con = cpds.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		PreparedStatement pst = null;
		
		try {
			pst = con.prepareStatement("SELECT COUNT(*) from Polls");
			ResultSet rset = pst.executeQuery();
			
			rset.next();
			
			if(rset.getLong(1) == 0) {
				createPoll(cpds, 
						"Glasanje za omiljeni bend:", 
						"Od sljedećih bendova, koji Vam je bend najdraži? "
						+ "Kliknite na link kako biste glasali!",
						sce.getServletContext().getRealPath(POLL_OPTIONS_BANDS));
				createPoll(cpds, 
						"Glasanje za omiljeni film:",
						"Kliknite na link kako biste glasali za omiljeni film!",
						sce.getServletContext().getRealPath(POLL_OPTIONS_MOVIES));
			}
			
			try {
				
			} finally {
				try { rset.close(); } catch(Exception ignorable) {}
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		} finally {
			try { pst.close(); } catch(Exception ignorable) {}
		}
		
		try { con.close(); } catch(Exception ignorable) {}
	}
	
	private void createPollOptions(ComboPooledDataSource cpds, String filename,
			long pollId) throws IOException, SQLException {
		Connection con = null;
		try {
			con = cpds.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		Path path = Paths.get(filename);
		
		if (!Files.exists(path)) {
			throw new IOException(filename + " does not exist");
		}
		
		PreparedStatement pst = null;

		try {
			pst = con.prepareStatement(
				"INSERT INTO PollOptions (optionTitle, optionLink, pollID, votesCount) values (?,?,?,?)", 
				Statement.RETURN_GENERATED_KEYS);
			for (String line : Files.readAllLines(path)) {
				String[] parts = line.split("\t");			
				
				pst.setString(1, parts[0]);
				pst.setString(2, parts[1]);
				pst.setLong(3, pollId);
				pst.setLong(4, 0);
				
				pst.executeUpdate(); 
				
				ResultSet rset = pst.getGeneratedKeys();
				
				try { rset.close(); } catch(Exception ignorable) {}
			}
		} finally {
			try { pst.close(); } catch(Exception ignorable) {}
		}
		
		try { con.close(); } catch(Exception ignorable) {}
	}
	
	/**
	 * Method used for creating a poll.
	 * 
	 * @param cpds combo pooled DataSource
	 * @param title title
	 * @param message message
	 * @param filename filename
	 * @throws SQLException if error occurs
	 * @throws IOException if error occurs
	 */
	private void createPoll(ComboPooledDataSource cpds, String title, 
			String message, String filename) throws SQLException, IOException {
		Connection con = null;
		try {
			con = cpds.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		PreparedStatement pst = null;
		long id = -1;
		
		try {
			pst = con.prepareStatement(
					"INSERT INTO Polls (title, message) values (?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, title);
			pst.setString(2, message);
						
			pst.executeUpdate(); 
			
			ResultSet rset = pst.getGeneratedKeys();
			
			try {
				if(rset != null && rset.next()) {
					id = rset.getLong(1);
				}
			} finally {
				try { rset.close(); } catch(Exception ignorable) {}
			}
		} finally {
			try { pst.close(); } catch(Exception ignorable) {}
		}
		
		try { con.close(); } catch(Exception ignorable) {}
		
		if(id == -1) {
			throw new SQLException();
		}
				
		createPollOptions(cpds, filename, id);
	}
	
	/**
	 * Method that creates poll tables if they do not exist.
	 * 
	 * @param cpds combo pooled data source
	 * @throws SQLException if error occurs
	 */
	private void createPollTables(ComboPooledDataSource cpds) throws SQLException {
		Connection con = null;
		try {
			con = cpds.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(1);
		}

		ResultSet rsPolls = con.getMetaData().getTables(null, null, "POLLS", null);
		
		PreparedStatement pst = null;
		try {
			if(!rsPolls.next())
			{
				System.out.println("Creating table polls...");
				try {
					pst = con.prepareStatement(CREATE_POLLS);
					pst.execute();
				} finally {
					pst.close();
				}
				System.out.println("Table polls created");
			}
		} finally {
			try { rsPolls.close(); } catch(Exception ignorable) {}
		}
		
		ResultSet rsPollOptions = con.getMetaData().getTables(null, null, "POLLOPTIONS", null);

		try {
			if(!rsPollOptions.next())
			{
				System.out.println("Creating table pollOptions...");
				try {
					pst = con.prepareStatement(CREATE_POLL_OPTIONS);
					pst.execute();
				} finally {
					try { pst.close(); } catch(Exception ignorable) {}
				}
				System.out.println("Table pollOptions created");
			}
		} finally {
			try { rsPollOptions.close(); } catch(Exception ignorable) {}
		}
		
		try { con.close(); } catch(Exception ignorable) {}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ComboPooledDataSource cpds = (ComboPooledDataSource)sce.getServletContext().getAttribute("hr.fer.zemris.dbpool");
		if(cpds!=null) {
			try {
				DataSources.destroy(cpds);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}