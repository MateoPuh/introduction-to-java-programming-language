package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.polls.PollOption;
import hr.fer.zemris.java.p12.dao.DAOProvider;

/**
 * {@link HttpServlet} that is used for displaying result of voting to user.
 * It is mapped to "/glasanje-rezultati".
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="glasanje-rezultati", urlPatterns={"/servleti/glasanje-rezultati"})
public class GlasanjeRezultatiServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Integer pollId = null;
		try{
			pollId = Integer.parseInt(req.getParameter("pollID"));
		} catch(NumberFormatException | NullPointerException e) {
			ServletUtils.sendError(req, resp, "Parameter pollID should not be null or in non-integer format!");
			return;
		}
		
		List<PollOption> results = DAOProvider.getDao().getPollOptions(pollId);
						
		Collections.sort(results, new Comparator<PollOption>() {
			 public int compare(PollOption b1, PollOption b2) {
				 return (int)(b2.getVotesCount() - b1.getVotesCount()); 
			 }
		});
		
		req.setAttribute("results", results);
			
		List<PollOption> winners = new ArrayList<PollOption>();

		if(!results.isEmpty()) {
			long winnerVotes = results.get(0).getVotesCount();
			
			if(winnerVotes != 0) {
				for(PollOption result : results) {
					if(result.getVotesCount() != winnerVotes) {
						break;
					}
					
					winners.add(result);
				}
			}
		}
		
		req.setAttribute("winners", winners);
		req.setAttribute("poll", DAOProvider.getDao().getPoll(pollId));
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
	}

}
