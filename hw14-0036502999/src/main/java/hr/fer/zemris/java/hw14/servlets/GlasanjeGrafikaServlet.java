package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;

import hr.fer.zemris.java.hw14.polls.PollOption;
import hr.fer.zemris.java.p12.dao.DAOProvider;

/**
 * {@link HttpServlet} that is used for generating pie chart for voting results.
 * It is mapped to "/servleti/glasanje-grafika".
 * 
 * @author Mateo Puhalović
 * @version 2.0
 */
@WebServlet(name="glasanje-grafika", urlPatterns={"/servleti/glasanje-grafika"})
public class GlasanjeGrafikaServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final int WIDTH = 500;
	private static final int HEIGHT = 400;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Integer pollId = null;
		try{
			pollId = Integer.parseInt(req.getParameter("pollID"));
		} catch(NumberFormatException | NullPointerException e) {
			ServletUtils.sendError(req, resp, "Parameter pollID should not be null or in non-integer format!");
			return;
		}

		List<PollOption> pollOptions = DAOProvider.getDao().getPollOptions(pollId);
						
		Collections.sort(pollOptions, new Comparator<PollOption>() {
			 public int compare(PollOption b1, PollOption b2) {
				 return (int)(b2.getVotesCount() - b1.getVotesCount()); 
			 }
		});
		
		resp.setContentType("image/png");
		OutputStream os = resp.getOutputStream();
		
		DefaultPieDataset result = new DefaultPieDataset();
		
		for(PollOption r : pollOptions) {
			result.setValue(r.getTitle(), r.getVotesCount());
		}

		JFreeChart chart = ChartFactory.createPieChart3D(
				 null, 
				 result, 
				 true, 
				 false, 
				 false
				 );
		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.9f);
		chart.setBorderVisible(true); 
	       	     
	    ChartFactory.createPieChart("Voting results", result);
	    ChartUtils.writeChartAsPNG(os, chart, WIDTH, HEIGHT);	
	 }

}