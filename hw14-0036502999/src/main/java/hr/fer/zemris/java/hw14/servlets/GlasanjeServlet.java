package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.polls.PollOption;
import hr.fer.zemris.java.p12.dao.DAOProvider;

/**
 * {@link HttpServlet} that is used for displaying list of band to used
 * and allowing them to vote for favorite band. It is mapped to "/glasanje".
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="glasanje", urlPatterns={"/servleti/glasanje"})
public class GlasanjeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Integer pollId = null;
		try{
			pollId = Integer.parseInt(req.getParameter("pollID"));
		} catch(NumberFormatException | NullPointerException e) {
			ServletUtils.sendError(req, resp, "Parameter pollID should not be null or in non-integer format!");
			return;
		}
		
		List<PollOption> pollOptions = DAOProvider.getDao().getPollOptions(pollId);
						
		Collections.sort(pollOptions, new Comparator<PollOption>() {
			 public int compare(PollOption b1, PollOption b2) {
				 return (int)(b2.getId() - b1.getId()); 
			 }
		});
		
		req.setAttribute("poll", DAOProvider.getDao().getPoll(pollId));
		req.setAttribute("pollOptions", pollOptions);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
	}

}
