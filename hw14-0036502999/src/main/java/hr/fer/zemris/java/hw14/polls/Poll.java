package hr.fer.zemris.java.hw14.polls;

/**
 * Represents a entity in POLLS table.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class Poll {
	
	private long id;
	private String title;
	private String message;

	/**
	 * Creates new {@link Poll} with given id, title and message.
	 * 
	 * @param id poll id
	 * @param title title 
	 * @param message message
	 */
	public Poll(long id, String title, String message) {
		this.id = id;
		this.title = title;
		this.message = message;
	}

	/**
	 * Returns poll's id.
	 * 
	 * @return poll id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Returns poll's title.
	 * 
	 * @return poll title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns poll's message.
	 * 
	 * @return poll message
	 */
	public String getMessage() {
		return message;
	}

}
