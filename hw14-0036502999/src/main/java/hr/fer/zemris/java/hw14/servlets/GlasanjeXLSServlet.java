package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import hr.fer.zemris.java.hw14.polls.PollOption;
import hr.fer.zemris.java.p12.dao.DAOProvider;

/**
 * {@link HttpServlet} that is used for generating excel document with results 
 * of voting.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
@WebServlet(name="glasanje-xls", urlPatterns={"/servleti/glasanje-xls"})
public class GlasanjeXLSServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final String FILENAME = "rezultati.xls";
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Integer pollId = null;
		try{
			pollId = Integer.parseInt(req.getParameter("pollID"));
		} catch(NumberFormatException | NullPointerException e) {
			ServletUtils.sendError(req, resp, "Parameter pollID should not be null or in non-integer format!");
			return;
		}
		
		List<PollOption> pollOptions = DAOProvider.getDao().getPollOptions(pollId);
						
		Collections.sort(pollOptions, new Comparator<PollOption>() {
			 public int compare(PollOption b1, PollOption b2) {
				 return (int)(b2.getVotesCount() - b1.getVotesCount()); 
			 }
		});

		HSSFWorkbook hwb=new HSSFWorkbook();
		
		HSSFSheet sheet =  hwb.createSheet("Rezultati");

		for(int i = 0; i < pollOptions.size(); i++) {
			PollOption r = pollOptions.get(i);
			HSSFRow rowhead= sheet.createRow(i + 1);
			rowhead.createCell(0).setCellValue(r.getTitle());
			rowhead.createCell(1).setCellValue(r.getVotesCount());
		}
		
		resp.setHeader("Content-Disposition", "attachment; filename=\"" 
				+ FILENAME + "\"");
		resp.setContentType("application/vnd.ms-exce");
		
		try {
			hwb.write(resp.getOutputStream());
			resp.getOutputStream().flush();
		} catch ( Exception ex ) {
			ex.printStackTrace();
			ServletUtils.sendError(req, resp, "Could not generate document. Please try again later!");
		} finally {
			hwb.close();
		}
	}

}