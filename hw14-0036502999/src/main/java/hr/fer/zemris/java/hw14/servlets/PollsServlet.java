package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.polls.Poll;
import hr.fer.zemris.java.p12.dao.DAOProvider;

/**
 * {@link HttpServlet} mapped on "/servleti/index.html" url. It
 * shows list of available polls and allows user to open page for
 * voting by clicking on poll's title.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
@WebServlet("/servleti/index.html")
public class PollsServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		List<Poll> polls = DAOProvider.getDao().getPolls();
		req.setAttribute("polls", polls);
		
		req.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(req, resp);
	}
	
}