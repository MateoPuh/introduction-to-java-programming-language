package hr.fer.zemris.java.hw14.polls;

/**
 * Represents entity in POLLOPTIONS table.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class PollOption {
	
	private long id;
	private String title;
	private String link;
	private long pollId;
	private long votesCount;
	
	/**
	 * Creates new {@link PollOption} with given id, title, link, poll id and
	 * count of votes.
	 * 
	 * @param id poll option id
	 * @param title title
	 * @param link link
	 * @param pollId poll id
	 * @param votesCount votes count
	 */
	public PollOption(long id, String title, String link, long pollId, long votesCount) {
		this.id = id;
		this.title = title;
		this.link = link;
		this.pollId = pollId;
		this.votesCount = votesCount;
	}

	/**
	 * Returns option's id.
	 * 
	 * @return option id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Returns option's title.
	 * 
	 * @return option title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns option's link.
	 * 
	 * @return option link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * Returns id of poll this option is part of.
	 * 
	 * @return poll id
	 */
	public long getPollId() {
		return pollId;
	}

	/**
	 * Returns count of votes.
	 * 
	 * @return votes count
	 */
	public long getVotesCount() {
		return votesCount;
	}

}
