package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.p12.dao.DAOProvider;

/**
 * {@link HttpServlet} that is used for voting for favorite band.
 * It is mapped to "/servleti/glasanje-glasaj".
 * 
 * @author Mateo Puhalović
 * @version 2.0
 */
@WebServlet(name="glasanje-glasaj", urlPatterns={"/servleti/glasanje-glasaj"})
public class GlasanjeGlasajServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Integer optionId = null;
		Integer pollId = null;
		
		try {
			optionId = Integer.parseInt(req.getParameter("id"));
			pollId = Integer.parseInt(req.getParameter("pollID"));
		} catch(NumberFormatException | NullPointerException e) {
			ServletUtils.sendError(req, resp, "Parameters id and pollID should not be null or in non-integer format!");
			return;
		}
		
		DAOProvider.getDao().increment(optionId);
		
		resp.sendRedirect(req.getContextPath() 
				+ "/servleti/glasanje-rezultati?pollID=" + pollId);
	}

}
