<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<body bgcolor=powderblue>
		<ul>
			<c:forEach var="poll" items="${polls}">
				<li><a href="<%=request.getContextPath()%>/servleti/glasanje?pollID=${ poll.id }">${ poll.title }</a></li>
			</c:forEach>
		</ul>
	</body>
</html>