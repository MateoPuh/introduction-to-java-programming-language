<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
String color = (String)session.getAttribute("bgColor");
if(color == null){
	color = "FFFFFF";
}
%>

<html>
	<body  bgcolor="#<%=color%>">
 		
 		<h1>${poll.title }</h1>
 		<p>${poll.message }</p>
 		<ol>
 
			<c:forEach var="o" items="${pollOptions}">
				<li><a href="<%=request.getContextPath()%>/servleti/glasanje-glasaj?pollID=${ poll.id }&id=${o.id}">${o.title}</a></li>
			</c:forEach>

 		</ol>
	</body>
</html>
