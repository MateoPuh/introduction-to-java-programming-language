<%@page import="java.util.Random"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
String color = (String)session.getAttribute("bgColor");
if(color == null){
	color = "FFFFFF";
}
%>

<html>
	<body bgcolor="#<%=color%>">
		<p>${message}</p>
	</body>
</html>