package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ValueWrapperTest {
	
	@Test
	public void test() {
		ValueWrapper vv1 = new ValueWrapper(Boolean.valueOf(true));
		assertThrows(IllegalArgumentException.class, () -> vv1.add(Integer.valueOf(5))); // ==> throws, since current value is boolean
		ValueWrapper vv2 = new ValueWrapper(Integer.valueOf(5));
		assertThrows(IllegalArgumentException.class, () -> vv2.add(Boolean.valueOf(true))); // ==> throws, since the argument value is boolean
	}
	
	@Test
	public void test2() {
		ValueWrapper v1 = new ValueWrapper(null);
		assertTrue(v1.getValue() == null);
		ValueWrapper v2 = new ValueWrapper(null);
		v1.add(v2.getValue()); // v1 now stores Integer(0); v2 still stores null.
		assertTrue(v1.getValue() instanceof Integer);
		assertEquals(0, v1.getValue());
		assertTrue(v2.getValue() == null);

		ValueWrapper v3 = new ValueWrapper("1.2E1");
		ValueWrapper v4 = new ValueWrapper(Integer.valueOf(1));
		assertTrue(v3.getValue() instanceof String);
		assertEquals("1.2E1", v3.getValue());
		assertTrue(v4.getValue() instanceof Integer);
		assertEquals(1, v4.getValue());

		v3.add(v4.getValue()); // v3 now stores Double(13); v4 still stores Integer(1).
		assertTrue(v3.getValue() instanceof Double);
		assertEquals(13, (Double)v3.getValue(), 1E-6);
		assertTrue(v4.getValue() instanceof Integer);
		assertEquals(1, v4.getValue());
		
		ValueWrapper v5 = new ValueWrapper("12");
		ValueWrapper v6 = new ValueWrapper(Integer.valueOf(1));
		assertTrue(v5.getValue() instanceof String);
		assertEquals("12", v5.getValue());
		assertTrue(v6.getValue() instanceof Integer);
		assertEquals(1, v6.getValue());
		v5.add(v6.getValue()); // v5 now stores Integer(13); v6 still stores Integer(1).
		assertTrue(v5.getValue() instanceof Integer);
		assertEquals(13, v5.getValue());
		assertTrue(v6.getValue() instanceof Integer);
		assertEquals(1, v6.getValue());
		
		ValueWrapper v7 = new ValueWrapper("Ankica");
		ValueWrapper v8 = new ValueWrapper(Integer.valueOf(1));
		assertTrue(v7.getValue() instanceof String);
		assertEquals("Ankica", v7.getValue());
		assertTrue(v8.getValue() instanceof Integer);
		assertEquals(1, v8.getValue());
		assertThrows(IllegalArgumentException.class, () ->v7.add(v8.getValue())); // throws RuntimeExceptio
	}
	
	@Test
	public void testIntegerAndNull() {
		ValueWrapper v1 = new ValueWrapper(Integer.valueOf(1));
		ValueWrapper v2 = new ValueWrapper(null);
		v1.add(v2.getValue()); 
		assertTrue(v1.getValue() instanceof Integer);
		assertEquals(1, v1.getValue());
		assertTrue(v2.getValue() == null);
		
		ValueWrapper v3 = new ValueWrapper(null);
		ValueWrapper v4 = new ValueWrapper("a");
		assertThrows(IllegalArgumentException.class, () -> v3.add(v4.getValue())); 
	}
	
	@Test
	public void testIntegerAndDouble() {
		ValueWrapper v1 = new ValueWrapper(Integer.valueOf(1));
		ValueWrapper v2 = new ValueWrapper(Double.valueOf(1.5));
		v1.add(v2.getValue()); 
		assertTrue(v1.getValue() instanceof Double);
		assertEquals(2.5, v1.getValue());
		assertEquals(1.5, (Double)v2.getValue(), 1E-6);
	}
	
	@Test
	public void testDoubleAndDouble() {
		ValueWrapper v1 = new ValueWrapper(Double.valueOf(2.5));
		ValueWrapper v2 = new ValueWrapper(Double.valueOf(1.5));
		v1.add(v2.getValue()); 
		assertTrue(v1.getValue() instanceof Double);
		assertEquals(4, (Double)v1.getValue(), 1E-6);
		assertEquals(1.5, (Double)v2.getValue(), 1E-6);
	}

}
