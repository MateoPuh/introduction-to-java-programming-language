package hr.fer.zemris.java.hw07.observer2;

import hr.fer.zemris.java.hw07.observer1.IntegerStorage;

/**
 * This interface represents Observer in Observer design 
 * pattern. When value in Subject ({@link IntegerStorage})
 * is changed, it calls {@link IntegerStorageObserver#valueChanged}
 * method which is implemented in Concrete Observers.
 * 
 * @author Mateo Puhalović
 * @version 1.1
 *
 */
public interface IntegerStorageObserver {

	/**
	 * When value in Subject ({@link IntegerStorage}) is changed,
	 * this method is called. It defines set of actions that are 
	 * performed on Subject.
	 * 
	 * @param istorage {@link IntegerStorageChange} that encapsulates
	 * 		reference to the storage, value before change and current value
	 */
	public void valueChanged(IntegerStorageChange istorage);
	
}
