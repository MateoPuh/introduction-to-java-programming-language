package hr.fer.zemris.java.hw07.observer2;

/**
 * Concrete Observer ({@link IntegerStorageObserver})
 * which prints square value of new value.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class SquareValue implements IntegerStorageObserver {

	@Override
	public void valueChanged(IntegerStorageChange istorage) {
		int value = istorage.getValueAfter();
		
		System.out.println("Provided new value: " + value
		+ ", square is " + value * value);
	}

}
