package hr.fer.zemris.java.hw07.demo2;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This class represents collection of prime numbers.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class PrimesCollection implements Iterable<Integer> {
	
	/** Size of collection. */
	private int size;
	
	/**
	 * Creates new {@link PrimesCollection} with given size.
	 * 
	 * @param size size of collection
	 */
	public PrimesCollection(int size) {
		if(size <= 0) {
			throw new IllegalArgumentException("Invalid size.");
		}
		
		this.size = size;
	}

	/**
	 * Determines whether given number is prime number.
	 * 
	 * @param n number to check whether it is prime
	 * @return true if given number is prime; false otherwise
	 */
	private boolean isPrime(int n) {
		if(n <= 0) {
			return false;
		}
		
		if(n == 1) {
			return true;
		}
		
	    for(int i = 2; i < n; i++) {
	        if(n % i == 0) {
	            return false;
	        }
	    }
	    return true;
	}
	
	/**
	 * Generates prime number that is next in relation
	 * to given number.
	 * 
	 * @param lastPrime last prime number
	 * @return next prime number
	 */
	private int generateNextPrime(int lastPrime) {
		int i = lastPrime + 1;
		
		while(true) {
			if(isPrime(i)) {
				return i;
			}
			i++;
		}
	}
	
	@Override
	public Iterator<Integer> iterator() {
		return new Iterator<Integer>() {

			private int currentIndex = 0;
			private int lastPrime = 1;
			
			@Override
			public boolean hasNext() {
				return currentIndex < size;
			}

			@Override
			public Integer next() {
				if(!hasNext()) {
			        throw new NoSuchElementException();
				}
				
				lastPrime = generateNextPrime(lastPrime);
				currentIndex++;
				return lastPrime;
			}
			
		};
	}
	
}
