package hr.fer.zemris.java.hw07.observer1;

/**
 * Concrete Observer that prints double value (i.e. “value * 2”) 
 * of the current value which is stored in Subject {@link IntegerStorage},
 * but only first n times since its registration with the Subject (n
 * is given in constructor).
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class DoubleValue implements IntegerStorageObserver {

	/** Number of times this observer will print double value. */
	private int n;
	
	/** Current number of times this observer printed double value. */
	private int numOfChanges = 0;
	
	/**
	 * Creates new {@link DoubleValue} with given n, number
	 * of times that double value will be printed.
	 * 
	 * @param n number of times double value will be printed
	 */
	public DoubleValue(int n) {
		if(n <= 0) {
			throw new IllegalArgumentException("Invalid value of n: " + n);
		}
		
		this.n = n;
	}
	
	@Override
	public void valueChanged(IntegerStorage istorage) {
		int value = istorage.getValue();
		System.out.println("Double value: " + value * 2);
		numOfChanges++;		
		
		if(numOfChanges == n) {
			istorage.removeObserver(this);
		}
	}

}
