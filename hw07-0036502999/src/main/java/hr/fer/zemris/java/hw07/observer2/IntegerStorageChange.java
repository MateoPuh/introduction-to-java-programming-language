package hr.fer.zemris.java.hw07.observer2;

/**
 * This class encapsulates a reference to the {@link IntegerStorage}, 
 * the value of stored integer before the change has occurred, 
 * and the new value of currently stored integer.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class IntegerStorageChange {
	
	/** Reference to the {@link IntegerStorage} */
	private IntegerStorage iStorage;
	
	/** Value of stored integer before change. */
	private int valueBefore;
	
	/** Value of currently stored integer. */
	private int valueAfter;
	
	/**
	 * Creates new {@link IntegerStorageChange} with given reference
	 * to the {@link IntegerStorage}, value it stored before change and 
	 * value that is currently stored.
	 * 
	 * @param iStorage reference to the storage
	 * @param valueBefore value before change
	 * @param valueAfter value currently stored
	 */
	public IntegerStorageChange(IntegerStorage iStorage, int valueBefore, int valueAfter) {
		this.iStorage = iStorage;
		this.valueBefore = valueBefore;
		this.valueAfter = valueAfter;
	}

	/**
	 * Returns reference to the storage.
	 * 
	 * @return reference to the storage
	 */
	public IntegerStorage getIStorage() {
		return iStorage;
	}

	/**
	 * Returns value before change.
	 * 
	 * @return value before change
	 */
	public int getValueBefore() {
		return valueBefore;
	}

	/**
	 * Returns currently stored value.
	 * 
	 * @return currently stored value
	 */
	public int getValueAfter() {
		return valueAfter;
	}

}
