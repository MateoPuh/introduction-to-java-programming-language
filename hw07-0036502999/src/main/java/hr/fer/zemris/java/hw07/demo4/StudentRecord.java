package hr.fer.zemris.java.hw07.demo4;

/**
 * This class represents one record of student's data.
 * It contains student's jmbag, first name, last name, 
 * midterm score, finals score, score on laboratory
 * experiments and final grade.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class StudentRecord {

	private String jmbag;
	private String lastName;
	private String firstName;
	private double scoreMidterm;
	private double scoreFinal;
	private double scoreLab;
	private int grade;
	
	/**
	 * Creates new {@link StudentRecord} with given 
	 * jmbag, lastName, firstName, midterm score, finals score,
	 * laboratory experiments score and final grade.
	 * 
	 * @param jmbag student's jmbag
	 * @param lastName student's last name
	 * @param firstName student's first name
	 * @param scoreMidterm student's score on midterm
	 * @param scoreFinal student's score on finals
	 * @param scoreLab student's score on laboratory 
	 * 		experiments
	 * @param grade student's final grade
	 */
	public StudentRecord(String jmbag, String lastName, String firstName, double scoreMidterm, double scoreFinal,
			double scoreLab, int grade) {
		this.jmbag = jmbag;
		this.lastName = lastName;
		this.firstName = firstName;
		this.scoreMidterm = scoreMidterm;
		this.scoreFinal = scoreFinal;
		this.scoreLab = scoreLab;
		this.grade = grade;
	}

	/**
	 * Returns student's jmbag.
	 * 
	 * @return student's jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * Returns student's last name.
	 * 
	 * @return student's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Returns student's first name.
	 * 
	 * @return student's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Returns student's total score.
	 * 
	 * @return student's total score
	 */
	public double getTotalScore() {
		return scoreFinal + scoreLab + scoreMidterm;
	}
	
	/**
	 * Returns student's midterm score.
	 * 
	 * @return student's midterm score
	 */
	public double getScoreMidterm() {
		return scoreMidterm;
	}

	/**
	 * Returns student's final score.
	 * 
	 * @return student's final score
	 */
	public double getScoreFinal() {
		return scoreFinal;
	}

	/**
	 * Returns student's laboratory experiment score.
	 * 
	 * @return student's laboratory experiment score
	 */
	public double getScoreLab() {
		return scoreLab;
	}

	/**
	 * Returns student's grade.
	 * 
	 * @return student's grade
	 */
	public int getGrade() {
		return grade;
	}

	/**
	 * Returns {@link StudentRecord} in String format.
	 */
	public String toString() {
		return jmbag + "\t" + lastName + "\t" + firstName + "\t" + scoreMidterm + "\t"
				+ scoreFinal + "\t" + scoreLab + "\t" + grade;
	}
	
}
