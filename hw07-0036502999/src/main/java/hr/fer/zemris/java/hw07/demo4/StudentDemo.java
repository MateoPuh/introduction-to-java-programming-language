package hr.fer.zemris.java.hw07.demo4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class is used for reading student records
 * from studenti.txt file and printing filtered {@link List}s 
 * and {@link Map}s of students using Java {@link Stream} API.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class StudentDemo {
	
	/** Path to studenti.txt */
	private static final String FILEPATH = "./studenti.txt";
	
	/** Number of arguments in one line of student record. */
	private static final int NUM_OF_FIELDS = 7;
	
	/** Best grade. */
	private static final int TOP_GRADE = 5;
	
	/** Lowest grade. */
	private static final int LOWEST_GRADE = 1;

	public static void main(String[] args) {
		List<String> lines;
		try {
			lines = Files.readAllLines(Paths.get(FILEPATH));
			List<StudentRecord> records = convert(lines);
			calculateAndPrintResults(records);
		} catch (IOException e) {
			System.out.println("Error occured while reading " + FILEPATH);
			e.printStackTrace();
		}	
	}
	
	/**
	 * Calculates results and prints them on standard output
	 * with title "Zadatak x." for each task.
	 * 
	 * @param records records of students
	 */
	private static void calculateAndPrintResults(List<StudentRecord> records) {
		long broj = vratiBodovaViseOd25(records);
		long broj5 = vratiBrojOdlikasa(records);
		List<StudentRecord> odlikasi = vratiListuOdlikasa(records);
		List<StudentRecord> odlikasiSortirano = vratiSortiranuListuOdlikasa(records);
		List<String> nepolozeniJMBAGovi = vratiPopisNepolozenih(records);
		Map<Integer, List<StudentRecord>> mapaPoOcjenama = razvrstajStudentePoOcjenama(records);
		Map<Integer, Integer> mapaPoOcjenama2 = vratiBrojStudenataPoOcjenama(records);
		Map<Boolean, List<StudentRecord>> prolazNeprolaz = razvrstajProlazPad(records);
		
		printFrame(1);
		System.out.println(broj);
		
		printFrame(2);
		System.out.println(broj5);
		
		printFrame(3);
		for(StudentRecord odlikas : odlikasi) {
			System.out.println(odlikas);
		}
		
		printFrame(4);
		for(StudentRecord odlikas : odlikasiSortirano) {
			System.out.println(odlikas);
		}
		
		printFrame(5);
		for(String nepolozen : nepolozeniJMBAGovi) {
			System.out.println(nepolozen);
		}
		
		printFrame(6);
		for(Entry<Integer, List<StudentRecord>> ocjena : mapaPoOcjenama.entrySet()) {
			System.out.println(ocjena.getKey());
			
			for(StudentRecord student : ocjena.getValue()) {
				System.out.println("\t" + student);
			}
		}
		
		printFrame(7);
		for(Entry<Integer, Integer> ocjena : mapaPoOcjenama2.entrySet()) {
			System.out.println(ocjena.getKey() + " : " + ocjena.getValue());
		}
		
		printFrame(8);
		for(Entry<Boolean, List<StudentRecord>> prolaz : prolazNeprolaz.entrySet()) {
			System.out.println(prolaz.getKey() ? "Studenti koji su prosli:" : "Studenti koji nisu prosli:");
			
			for(StudentRecord student : prolaz.getValue()) {
				System.out.println("\t" + student);
			}
		}
		
	}
	
	/**
	 * Prints the title and frame of task.
	 * 
	 * @param taskNumber Number of task to print.
	 */
	private static void printFrame(int taskNumber) {
		System.out.println("Zadatak " + taskNumber);
		System.out.println("=========");
	}
	
	/**
	 * Converts list of String to list of {@link StudentRecord}s.
	 * 
	 * @param lines list of strings
	 * @return list of {@link StudentRecord}s
	 * @throws IllegalArgumentException if error occurs
	 */
	private static List<StudentRecord> convert(List<String> lines){
		List<StudentRecord> students = new ArrayList<StudentRecord>();
		
		for(String line : lines) {
			// skip empty lines
			if(line.trim().isEmpty()) {
				continue;
			}
			
			String[] args = line.split("\\s+");
			if(args.length != NUM_OF_FIELDS) {
				throw new IllegalArgumentException("Line " + line + " is invalid.");
			}
			
			try {
				students.add(
						new StudentRecord(
								args[0], args[1], args[2], 
								Double.parseDouble(args[3]), 
								Double.parseDouble(args[4]), 
								Double.parseDouble(args[5]), 
								Integer.parseInt(args[6])
						)
				);
			} catch(Exception e) {
				throw new IllegalArgumentException("Error occured while parsing student record: " 
						+ e.getLocalizedMessage());
			}
		}
		
		return students;
	}
	
	/**
	 * Returns number of students with total score greater than 25.
	 * 
	 * @param records list of all students
	 * @return number of students with total score greater than 25
	 */
	public static long vratiBodovaViseOd25(List<StudentRecord> records) {
		long broj = records.stream()
				.map(r -> r.getScoreMidterm() + r.getScoreFinal() + r.getScoreLab())
				.filter(s -> s > 25)
				.count();
		return broj;
	}
	
	/**
	 * Returns number of students whose grade is 5.
	 * 
	 * @param records list of all students
	 * @return number of students whose grade is 5
	 */
	public static long vratiBrojOdlikasa(List<StudentRecord> records) {
		long broj = records.stream()
				.map(r -> r.getGrade())
				.filter(g -> g == TOP_GRADE)
				.count();
		return broj;
	}
	
	/**
	 * Returns list of students whose grade is 5.
	 * 
	 * @param records list of all students
	 * @return list of students whose grade is 5
	 */
	public static List<StudentRecord> vratiListuOdlikasa(List<StudentRecord> records) {
		List<StudentRecord> students = records.stream()
				.filter(s -> s.getGrade() == TOP_GRADE)
				.collect(Collectors.toList());
		return students;
	}
	
	/**
	 * Returns list of students whose grade is 5, sorted by 
	 * their total score.
	 * 
	 * @param records list of all students
	 * @return sorted list of students whose grade is 5
	 */
	public static List<StudentRecord> vratiSortiranuListuOdlikasa(List<StudentRecord> records) {
		List<StudentRecord> students = records.stream()
				.filter(s -> s.getGrade() == TOP_GRADE)
				.sorted((s1, s2) -> Double.compare(s1.getTotalScore(), s2.getTotalScore())).collect(Collectors.toList());
		return students;
	}
	
	/**
	 * Returns list of jmbags of students whose grade is 1.
	 * 
	 * @param records list of all students
	 * @return list of jmbags of students whose grade is 1
	 */
	public static List<String> vratiPopisNepolozenih(List<StudentRecord> records) {
		List<String> jmbags = records.stream()
				.filter(s -> s.getGrade() == LOWEST_GRADE)
				.map(s -> s.getJmbag())
				.collect(Collectors.toList());
		return jmbags;
	}
	
	/**
	 * Returns map in which keys are grades and values are
	 * list of students with that grade.
	 * 
	 * @param records list of all students
	 * @return maps whose keys are grades
	 */
	public static Map<Integer, List<StudentRecord>> razvrstajStudentePoOcjenama(List<StudentRecord> records) {
		Map<Integer, List<StudentRecord>> studentsByGrades = records.stream()
				.collect(Collectors.groupingBy(StudentRecord::getGrade));
		
		return studentsByGrades;
	}
	
	/**
	 * Returns map in which keys are grades and values are 
	 * number of students with that grade.
	 * 
	 * @param records list of all students
	 * @return maps whose keys are grades
	 */
	public static Map<Integer, Integer> vratiBrojStudenataPoOcjenama(List<StudentRecord> records) {
		Map<Integer, Integer> studentsByGrades = 
		records.stream()
		.collect(Collectors.toMap(
				StudentRecord::getGrade, 
				s -> 1,
				(s1, s2) -> s1 + s2));
				
		
		return studentsByGrades;
	}
	
	/**
	 * Returns map whose keys are boolean values that indicate whether 
	 * student has passed subject and values are list of student who
	 * passed or did not passed subject.
	 * 
	 * @param records list of all students
	 * @return map whose keys are boolean value that indicate whether student
	 * 		has passed subject
	 */
	public static Map<Boolean, List<StudentRecord>> razvrstajProlazPad(List<StudentRecord> records) {
		Map<Boolean, List<StudentRecord>> studentsByGrades = records.stream()
				.collect(Collectors.groupingBy(s -> s.getGrade() > 1));
		
		return studentsByGrades;
	}

}
