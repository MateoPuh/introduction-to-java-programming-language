package hr.fer.zemris.java.hw07.demo2;

/**
 * This class demonstrates usage of {@link PrimesCollection}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class PrimesDemo1 {

	/**
	 * Demonstrates usage of {@link PrimesCollection}.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		PrimesCollection primesCollection = new PrimesCollection(5); // 5: how many of them
		for(Integer prime : primesCollection) {
			System.out.println("Got prime: "+prime);
		}
	}
	
}
