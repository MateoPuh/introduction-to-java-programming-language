package hr.fer.zemris.java.hw07.observer1;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents storage of Integer objects.
 * 
 * <p>This class represents Subject in Observer design
 * pattern. When value is changed in this class, method
 * {@link IntegerStorageObserver#valueChanged(IntegerStorage)}
 * is called on all registered Observers. </p> 
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class IntegerStorage {
	
	/** Value that is stored in this storage. */
	private int value;
	
	/** List of Concrete Observers. */
	private List<IntegerStorageObserver> observers; 
	
	/**
	 * Creates new {@link IntegerStorage} with given value to store.
	 * 
	 * @param initialValue given value to store.
	 */
	public IntegerStorage(int initialValue) {
		this.value = initialValue;
		observers = new ArrayList<IntegerStorageObserver>();
	}

	/**
	 * Adds Concrete Observer ({@link IntegerStorageObserver})
	 * to internal list of Observers.
	 * 
	 * @param observer observer to add in internal list
	 */
	public void addObserver(IntegerStorageObserver observer) {
		observers.add(observer);
	}

	/**
	 * Removes Observer ({@link IntegerStorageObserver}) from 
	 * internal list of Observers.
	 * 
	 * @param observer observer to remove from internal list
	 */
	public void removeObserver(IntegerStorageObserver observer) {
		observers.remove(observer);
	}
	
	/**
	 * Removes all Observers from internal list of Observers.
	 */
	public void clearObservers() {
		observers = new ArrayList<IntegerStorageObserver>();
	}

	/**
	 * Returns value that is stored.
	 * 
	 * @return value that is stored.
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets new value that will be stored here and calls 
	 * {@link IntegerStorageObserver#valueChanged(IntegerStorage)}
	 * from internal all Observers in internal list.
	 * 
	 * @param value new value to store
	 */
	public void setValue(int value) {
		// Only if new value is different than the current value:
		if(this.value!=value) {
			// Update current value
			this.value = value;
			// Notify all registered observers
			if(observers!=null) {
				List<IntegerStorageObserver> observersCopy = new ArrayList<IntegerStorageObserver>(observers);
				
				for(IntegerStorageObserver observer : observersCopy) {
					observer.valueChanged(this);
				}
			}
		}
	}
	
}
