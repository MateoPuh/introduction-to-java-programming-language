package hr.fer.zemris.java.hw07.observer1;

/**
 * Concrete Observer that prints number of times 
 * that value has been changed.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ChangeCounter implements IntegerStorageObserver {

	/** Number of times that value was changed. */
	private int numOfChanges = 0;
	
	@Override
	public void valueChanged(IntegerStorage istorage) {
		numOfChanges++;
		System.out.println("Number of value changes since tracking: " + numOfChanges);
	}

}
