package hr.fer.zemris.java.custom.scripting.exec;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * This class represents Stack-like structure that
 * is actually map whose keys are strings and whose
 * values are stacks that are internally managed.
 * 
 * <p> Complexity of methods push, pop and peek is O(1).</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ObjectMultistack {
	
	private static Map<String, MultistackEntry> map;
	
	/**
	 * This class represents node in a list that is 
	 * used as a stack in {@link ObjectMultistack} class.
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 *
	 */
	private static class MultistackEntry {
		private ValueWrapper value;
		private MultistackEntry next;
		
		/**
		 * Creates new {@link MultistackEntry} with given value
		 * and next node.
		 * 
		 * @param value value of node
		 * @param next next node
		 */
		public MultistackEntry(ValueWrapper value, MultistackEntry next) {
			super();
			this.value = value;
			this.next = next;
		}
	}
	
	/**
	 * Creates new {@link ObjectMultistack}.
	 * 
	 */
	public ObjectMultistack() {
		map = new HashMap<String, ObjectMultistack.MultistackEntry>();
	}
	
	/**
	 * Pushes the value on stack with given keyName.
	 * 
	 * @param keyName key of target stack
	 */
	public void push(String keyName, ValueWrapper valueWrapper) {
		MultistackEntry stack = map.get(keyName);
		MultistackEntry newEntry = new MultistackEntry(valueWrapper, stack);
		map.put(keyName, newEntry);
	}
	
	/**
	 * Pops the value on stack with given keyName.
	 * 
	 * @param keyName key of target stack
	 * @return value poped from target stack
	 * @throws NoSuchElementException if stack is empty
	 * 	or it does not exist
	 */
	public ValueWrapper pop(String keyName) {
		MultistackEntry stack = map.get(keyName);
		if(!isEmpty(keyName)) {
			ValueWrapper value = stack.value;
			stack = stack.next;
			map.put(keyName, stack);
			return value;
		} else {
			throw new NoSuchElementException("There is no entry with given key");
		}
	}
	
	/**
	 * Peeks the value on stack with given keyName.
	 * 
	 * @param keyName key of target stack
	 * @return value peeked from target stack
	 * @throws NoSuchElementException if stack is empty
	 * 	or it does not exist
	 */
	public ValueWrapper peek(String keyName) {
		MultistackEntry stack = map.get(keyName);
		if(!isEmpty(keyName)) {
			return stack.value;
		} else {
			throw new NoSuchElementException("There is no entry with given key");
		}
	}
	
	/**
	 * Returns true if stack is empty for given keyName.
	 * 	
	 * @param keyName key of target stack
	 * @return true if stack with given keyName is empty;
	 * 	false otherwise
	 */
	public boolean isEmpty(String keyName) {
		MultistackEntry stack = map.get(keyName);
		return stack == null;
	}

}
