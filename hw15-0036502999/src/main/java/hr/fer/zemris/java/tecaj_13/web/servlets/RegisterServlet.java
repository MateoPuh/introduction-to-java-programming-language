package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Servlet used for registering new user.
 * 
 * @author Mateo Puhalović
 *
 */
@WebServlet("/servleti/register")
public class RegisterServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/pages/Register.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		req.setCharacterEncoding("UTF-8");

		RegisterForm f = new RegisterForm();
		try {
			f.fillFromHttpRequest(req);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			resp.sendRedirect(req.getContextPath() + "/servleti/register");
		}
			
		f.validate();
		
		if(f.hasErrors()) {
			req.setAttribute("form", f);
			req.getRequestDispatcher("/WEB-INF/pages/Register.jsp").forward(req, resp);
			return;
		}
		
		BlogUser user = new BlogUser();
		f.fillInUser(user);
		
		DAOProvider.getDAO().saveBlogUser(user);

		resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/main");
	}
	
}