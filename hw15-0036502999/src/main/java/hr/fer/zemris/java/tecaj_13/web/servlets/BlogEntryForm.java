package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Form used for making new form and editing form.
 * 
 * @author Mateo Puhalović
 *
 */
public class BlogEntryForm {
	
	private String id;
	private String title;
	private String text;

	private BlogUser user;
	
	Map<String, String> errors = new HashMap<>();

	public String getError(String name) {
		return errors.get(name);
	}
	
	public boolean hasErrors() {
		return !errors.isEmpty();
	}
	
	public boolean hasError(String name) {
		return errors.containsKey(name);
	}
	
	public void fillFromHttpRequest(HttpServletRequest req) throws NoSuchAlgorithmException {
		this.title = prepare(req.getParameter("title"));
		this.text = prepare(req.getParameter("text"));
		this.id = req.getParameter("id");
		this.user = DAOProvider.getDAO().getBlogUser((Long)req.getSession().getAttribute("current.user.id"));
	}

	public void fillFromEntry(BlogEntry entry) {
		this.title = entry.getTitle();
		this.text = entry.getText();
		this.id = String.valueOf(entry.getId());
	}
	
	public void fillToEntry(BlogEntry entry) {
		if(id != null && !id.isEmpty()) {
			entry.setId(Long.parseLong(id));
		}
		entry.setText(text);
		entry.setTitle(title);
		entry.setCreator(user);
	}
	
	public void validate() {
		errors.clear();
				
		if(this.title.isEmpty()) {
			errors.put("title", "Title is required!");
		}
		
		if(this.text.isEmpty()) {
			errors.put("text", "Text is required!");
		}
	}
	
	private String prepare(String s) {
		if(s==null) return "";
		return s.trim();
	}

	public String getTitle() {
		return title;
	}

	public String getText() {
		return text;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setUser(BlogUser user) {
		this.user = user;
	}

	public BlogUser getUser() {
		return user;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
