package hr.fer.zemris.java.tecaj_13.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This class represents entity of blog user with
 * attributes id, firstName, lastName, nick, email
 * hashed password and entries.
 * 
 * @author Mateo Puhalović
 *
 */
@Entity
@Table(name="blog_users")
public class BlogUser {
	
	private Long id;
	private String firstName;
	private String lastName;
	private String nick;
	private String email;
	private String passwordHash;
	private List<BlogEntry> entries;
	
	/**
	 * Returns id.
	 * 
	 * @return id
	 */
	@Id @GeneratedValue
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets id.
	 * 
	 * @param id id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Returns first name.
	 * 
	 * @return first name
	 */
	@Column(nullable=false, length=40)
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Sets first name.
	 * 
	 * @param firstName first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * Returns last name.
	 * 
	 * @return last name
	 */
	@Column(nullable=false, length=50)
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Sets last name.
	 * 
	 * @param lastName last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * Returns nick.
	 * 
	 * @return nick
	 */
	@Column(nullable=false, length=20, unique=true)
	public String getNick() {
		return nick;
	}
	
	/**
	 * Sets nick.
	 * 
	 * @param nick nick
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}
	
	/**
	 * Returns email.
	 * 
	 * @return email
	 */
	@Column(nullable=false, length=50)
	public String getEmail() {
		return email;
	}
	
	/**
	 * Sets email.
	 * 
	 * @param email email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Returns hashed password.
	 * 
	 * @return hashed password
	 */
	@Column(nullable=false, length=128)
	public String getPasswordHash() {
		return passwordHash;
	}
	
	/**
	 * Sets hashed password.
	 * 
	 * @param passwordHash hashed password
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	/**
	 * Returns entries.
	 * 
	 * @return entries
	 */
	@OneToMany(mappedBy = "creator", cascade = { CascadeType.ALL })
	public List<BlogEntry> getEntries() {
		return entries;
	}

	/**
	 * Sets entries.
	 * 
	 * @param entries entries
	 */
	public void setEntries(List<BlogEntry> entries) {
		this.entries = entries;
	}
	
}
