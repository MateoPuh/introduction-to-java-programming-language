package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Form used in login.
 * 
 * @author Mateo Puhalović
 *
 */
public class LoginForm {
    	
	private String nick;
	private String password;
	private String passwordHash;

	private BlogUser user;
	
	Map<String, String> errors = new HashMap<>();

	/**
	 * Returns error for given attribute.
	 * 
	 * @param name
	 * @return
	 */
	public String getError(String name) {
		return errors.get(name);
	}
	
	/**
	 * Returns true if errors exist.
	 * 
	 * @return true if errors exists
	 */
	public boolean hasErrors() {
		return !errors.isEmpty();
	}
	
	/**
	 * True if error exists for given attribute.
	 * 
	 * @param name attribute
	 * @return true if error exists
	 */
	public boolean hasError(String name) {
		return errors.containsKey(name);
	}
	
	/**
	 * Fills from request.
	 * 
	 * @param req request
	 * @throws NoSuchAlgorithmException if error occurs
	 */
	public void fillFromHttpRequest(HttpServletRequest req) throws NoSuchAlgorithmException {
		this.nick = prepare(req.getParameter("nick"));
		this.password = prepare(req.getParameter("password"));

		MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
		messageDigest.update(prepare(req.getParameter("password")).getBytes());
	    byte[] digested = messageDigest.digest();
		this.passwordHash = bytetohex(digested);
	}
	
	public static String bytetohex(byte[] bytearray) {
		Objects.requireNonNull(bytearray);
		
		StringBuilder sb = new StringBuilder();
		for(byte b : bytearray) {
            sb.append(String.format("%02x", b));
		}
		
		return sb.toString();
	}

	/**
	 * Fills from user.
	 * 
	 * @param user user
	 */
	public void fillFromUser(BlogUser user) {
		this.nick = user.getNick();
		this.passwordHash = null;
	}
	
	/**
	 * Validates form.
	 */
	public void validate() {
		errors.clear();
				
		if(this.nick.isEmpty()) {
			errors.put("nick", "Nickname is required!");
		}
		
		if(password.isEmpty()) {
			errors.put("password", "Password is required!");
		}
		
		user = DAOProvider.getDAO().getBlogUser(nick);

		if(user == null) {
			errors.put("nick", "Nickname doesn't exist!");
			return;
		}
		
		if(!user.getPasswordHash().equals(passwordHash)) {
			errors.put("password", "Invalid username or password!");
		}
	}
	
	/**
	 * Prepares attribute.
	 * 
	 * @param s attribute
	 * @return attribute
	 */
	private String prepare(String s) {
		if(s==null) return "";
		return s.trim();
	}
	
	/**
	 * Returns nick.
	 * 
	 * @return nick
	 */
	public String getNick() {
		return nick;
	}
	
	/**
	 * Sets nick.
	 * 
	 * @param nick nick
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}
	
	/**
	 * Returns password.
	 * 
	 * @return password.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets password
	 * 
	 * @param password password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Return user.
	 * 
	 * @return user
	 */
	public BlogUser getUser() {
		return user;
	}
	
}
