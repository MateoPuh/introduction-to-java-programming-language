package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;

/**
 * Servlet used for adding new entry or editing entry.
 * 
 * @author Mateo Puhalović
 *
 */
@WebServlet("/servleti/newOrEdit")
public class NewEntryServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		BlogEntryForm f = new BlogEntryForm();
		
		try {
			f.fillFromHttpRequest(req);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			resp.sendRedirect(req.getContextPath() + "/servleti/main");
			return;
		}
			
		f.validate();
		
		if(f.hasErrors()) {
			req.setAttribute("form", f);
			
			req.getRequestDispatcher("/WEB-INF/pages/NewOrEditEntry.jsp").forward(req, resp);
			return;
		}
				
		BlogEntry entry = new BlogEntry();
		
		String entryID = req.getParameter("id");
		
		if(entryID != null && !entryID.isEmpty()) {
			entry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(entryID));
			if(!entry.getCreator().getId().equals(req.getSession().getAttribute("current.user.id"))) {
				ServletUtils.sendError(req, resp, "You are not allowed to edit entry with id " + entry.getId() + ".");
				return;
			}
		}
		
		f.fillToEntry(entry);
		
		entry.setLastModifiedAt(new Date());
		
		if(entryID == null || entryID.isEmpty()) {
			entry.setCreatedAt(new Date());
			DAOProvider.getDAO().saveBlogEntry(entry);
		} 
		
		resp.sendRedirect(req.getContextPath() + "/servleti/main");
	}	

}
