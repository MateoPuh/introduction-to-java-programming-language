package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Form used in registration.
 * 
 * @author Mateo Puhalović
 *
 */
public class RegisterForm {
    
	private static String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
	
	private String id;
	private String firstName;
	private String lastName;
	private String nick;
	private String email;
	private String password;
	private String passwordHash;
	Map<String, String> errors = new HashMap<>();

	/**
	 * Returns error for given attribute.
	 * 
	 * @param name name of attribute
	 * @return error 
	 */
	public String getError(String name) {
		return errors.get(name);
	}
	
	/**
	 * Return true if there are errors in form.
	 * 
	 * @return true if errors exist; false otherwise
	 */
	public boolean hasErrors() {
		return !errors.isEmpty();
	}
	
	/**
	 * Returns true if error exists for given attribute.
	 * 
	 * @param name name of attribute
	 * @return true if error exists; false otherwise
	 */
	public boolean hasError(String name) {
		return errors.containsKey(name);
	}
	
	/**
	 * Fills form from {@link HttpServletRequest}.
	 * 
	 * @param req request
	 * @throws NoSuchAlgorithmException if error occurs
	 */
	public void fillFromHttpRequest(HttpServletRequest req) throws NoSuchAlgorithmException {
		this.id = prepare(req.getParameter("id"));
		this.firstName = prepare(req.getParameter("firstName"));
		this.lastName = prepare(req.getParameter("lastName"));
		this.nick = prepare(req.getParameter("nick"));
		this.email = prepare(req.getParameter("email"));
		this.password = prepare(req.getParameter("password"));
		
		MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
		messageDigest.update(prepare(req.getParameter("password")).getBytes());
	    byte[] digested = messageDigest.digest();
		this.passwordHash = bytetohex(digested);
	}
	
	private static String bytetohex(byte[] bytearray) {
		Objects.requireNonNull(bytearray);
		
		StringBuilder sb = new StringBuilder();
		for(byte b : bytearray) {
            sb.append(String.format("%02x", b));
		}
		
		return sb.toString();
	}

	/**
	 * Fills form from {@link BlogUser}.
	 * 
	 * @param user user
	 */
	public void fillFromUser(BlogUser user) {
		if(user.getId()==null) {
			this.id = "";
		} else {
			this.id = user.getId().toString();
		}
		
		this.email = user.getEmail();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.nick = user.getNick();
		this.password = "";
	}

	/**
	 * Fills user with data from form.
	 * 
	 * @param user user
	 */
	public void fillInUser(BlogUser user) {
		if(this.id.isEmpty()) {
			user.setId(null);
		} else {
			user.setId(Long.valueOf(this.id));
		}
		
		user.setEmail(email);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setPasswordHash(passwordHash);
		user.setNick(nick);
	}
	
	/**
	 * Validates form.
	 */
	public void validate() {
		errors.clear();
		
		if(!this.id.isEmpty()) {
			try {
				Long.parseLong(this.id);
			} catch(NumberFormatException ex) {
				errors.put("id", "Id is invalid.");
			}
		}
		
		if(this.firstName.isEmpty()) {
			errors.put("firstName", "First name is required!");
		}
		
		if(this.lastName.isEmpty()) {
			errors.put("lastName", "Last name is required!");
		}
		
		if(this.nick.isEmpty()) {
			errors.put("nick", "Nickname is required!");
		}

		if(this.email.isEmpty()) {
			errors.put("email", "Email is required!");
		} else {
			if(!email.matches(EMAIL_REGEX)) {
				errors.put("email", "Email is not valid.");
			}
		}
		
		if(password.isEmpty()) {
			errors.put("password", "Password is required!");
		}
		
		BlogUser user = DAOProvider.getDAO().getBlogUser(nick);

		if(user != null) {
			errors.put("nick", "Nickname already exist!");
			return;
		}
	}
	
	/**
	 * Prepares attribute.
	 * 
	 * @param s attribute
	 * @return prepared attribute
	 */
	private String prepare(String s) {
		if(s==null) return "";
		return s.trim();
	}
	
	/**
	 * Returns id.
	 * 
	 * @return id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets id.
	 * 
	 * @param id id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Returns first name.
	 * 
	 * @return first name
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Sets first name.
	 * 
	 * @param firstName first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * Returns last name.
	 * 
	 * @return last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets alst name.
	 * 
	 * @param lastName last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * Returns nick.
	 * 
	 * @return nick
	 */
	public String getNick() {
		return nick;
	}
	
	/**
	 * Sets nick.
	 * 
	 * @param nick nick
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}
	
	/**
	 * Returns email.
	 * 
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Sets email.
	 * 
	 * @param email email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Returns hashed password.
	 * 
	 * @return hash password
	 */
	public String getPasswordHash() {
		return passwordHash;
	}
	
	/**
	 * Sets hashed password.
	 * 
	 * @param passwordHash password
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	
}
