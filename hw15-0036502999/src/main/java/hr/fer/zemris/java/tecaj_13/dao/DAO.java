package hr.fer.zemris.java.tecaj_13.dao;

import java.util.List;

import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

public interface DAO {

	/**
	 * Dohvaća entry sa zadanim <code>id</code>-em. Ako takav entry ne postoji,
	 * vraća <code>null</code>.
	 * 
	 * @param id ključ zapisa
	 * @return entry ili <code>null</code> ako entry ne postoji
	 * @throws DAOException ako dođe do pogreške pri dohvatu podataka
	 */
	public BlogEntry getBlogEntry(Long id) throws DAOException;
	
	/**
	 * Returns {@link BlogUser} with given id;
	 * 
	 * @param id user id
	 * @return user or null if user doesn't exist
	 * @throws DAOException if error occurs
	 */
	public BlogUser getBlogUser(Long id) throws DAOException;
	
	/**
	 * Returns {@link BlogUser} with given nick;
	 * 
	 * @param nick user nick
	 * @return user or null if user doesn't exist
	 * @throws DAOException if error occurs
	 */
	public BlogUser getBlogUser(String nick) throws DAOException;
	
	/**
	 * Saves new user.
	 * 
	 * @param user {@link BlogUser}
	 */
	public void saveBlogUser(BlogUser user);
	
	/**
	 * Saves new entry.
	 * 
	 * @param entry {@link BlogEntry}
	 */
	public void saveBlogEntry(BlogEntry entry);
	
	/**
	 * Saves new comment.
	 * 
	 * @param blogComment {@link BlogComment}
	 */
	public void SaveBlogComment(BlogComment blogComment);
	
	/**
	 * Returns all users.
	 * 
	 * @return list of users
	 */
	public List<BlogUser> getUsers();
	
	/**
	 * Returns all blog entries for user with given nick.
	 * 
	 * @param nick user nick
	 * @return list of entries for given user
	 */
	public List<BlogEntry> getBlogEntries(String nick);
		
}