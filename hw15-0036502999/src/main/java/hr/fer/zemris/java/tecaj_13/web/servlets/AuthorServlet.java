package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Servlet used showing blog entries, creating new entries and 
 * editing existing entries.
 * 
 * @author Mateo Puhalović
 *
 */
@WebServlet("/servleti/author/*")
public class AuthorServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			if(!checkParams(req, resp)) {
				return;
			}
		} catch(Exception e) {
			e.printStackTrace();
			return;
		}
		
		String paths[] = req.getPathInfo().substring(1).split("/");
		BlogUser user = DAOProvider.getDAO().getBlogUser(paths[0]);
		
		req.setAttribute("nick", paths[0]);
		
		if(paths.length == 1) {
			req.setAttribute("entries", DAOProvider.getDAO().getBlogEntries(paths[0]));
			req.getRequestDispatcher("/WEB-INF/pages/Author.jsp").forward(req, resp);
			return;
		}
		
		if(paths.length == 2) {
			switch(paths[1]) {
			case "new":
				try {
					showNewEntry(req, resp, user);
				} catch(Exception e) {
					ServletUtils.sendError(req, resp, "Error occured. Please, try again later.");
				}
				break;
			case "edit":
				try {
					editEntry(req, resp, user);
				} catch(Exception e) {
					ServletUtils.sendError(req, resp, "Error occured. Please, try again later.");
				}
				break;
			default:
				try {
					showEntry(req, resp, user, paths[1]);
				} catch(Exception e) {
					ServletUtils.sendError(req, resp, "Error occured while retrieving entry with id " + paths[1] + ".");
				}
			}
		}
	}
	
	/**
	 * Checks given parameters.
	 * 
	 * @param req request
	 * @param resp response
	 * @param user user
	 * @throws ServletException if error occurs
	 * @throws IOException if error occurs
	 */
	private boolean checkParams(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(req.getPathInfo() == null) {
			ServletUtils.sendError(req, resp, "Invalid url. No nickname provided.");
			return false;
		}
		String pathInfo = req.getPathInfo().substring(1);
		
		if(pathInfo == null || pathInfo.isEmpty()) {
			ServletUtils.sendError(req, resp, "Invalid url. No nickname provided.");
			return false;
		}
		
		String paths[] = pathInfo.split("/");
		
		if(paths.length == 0) {
			ServletUtils.sendError(req, resp, "Invalid url. No nickname provided.");
			return false;
		}
		
		if(paths.length > 2) {
			ServletUtils.sendError(req, resp, "Invalid url. Only nickname and id should be provided.");
			return false;
		}
		
		BlogUser user = DAOProvider.getDAO().getBlogUser(paths[0]);
		
		if(user == null) {
			ServletUtils.sendError(req, resp, "Nickname " + paths[0] + "  doesn't exists.");
			return false;
		}
		
		return true;
	}
	
	/**
	 * Shows page with entry.
	 * 
	 * @param req request
	 * @param resp response
	 * @param user user
	 * @throws ServletException if error occurs
	 * @throws IOException if error occurs
	 */
	private void showEntry(HttpServletRequest req, HttpServletResponse resp, BlogUser user,
			String entryID) throws ServletException, IOException {
		BlogEntry blogEntry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(entryID));
		if(blogEntry == null) {
			ServletUtils.sendError(req, resp, "Entry with id " + entryID + " doesn't exist.");
			return;
		}
		
		if(!blogEntry.getCreator().equals(user)) {
			ServletUtils.sendError(req, resp, "User with nick " + user.getNick() + " doesn't own entry with id " + entryID + ".");
			return;
		}
		
		req.setAttribute("blogEntry", blogEntry);
		req.getRequestDispatcher("/WEB-INF/pages/Entry.jsp").forward(req, resp);
	}
	
	/**
	 * Shows page for editing entry.
	 * 
	 * @param req request
	 * @param resp response
	 * @param user user
	 * @throws ServletException if error occurs
	 * @throws IOException if error occurs
	 */
	private void editEntry(HttpServletRequest req, HttpServletResponse resp, 
			BlogUser user) throws ServletException, IOException {
		Long userID = (Long)req.getSession().getAttribute("current.user.id");
		if(userID == null || !userID.equals(user.getId())) {
			ServletUtils.sendError(req, resp, "You don't have persmission to access this page.");
			return;
		}
		
		String entryID = req.getParameter("eid");
		
		BlogEntry blogEntry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(entryID));
		if(blogEntry == null) {
			ServletUtils.sendError(req, resp, "Entry with id " + entryID + " doesn't exist.");
			return;
		}
		
		BlogEntryForm f = new BlogEntryForm();
		
		f.fillFromEntry(blogEntry);
		
		req.setAttribute("form", f);
		req.getRequestDispatcher("/WEB-INF/pages/NewOrEditEntry.jsp").forward(req, resp);
		return;
	}
	
	/**
	 * Shows page for crating new entry.
	 * 
	 * @param req request
	 * @param resp response
	 * @param user user
	 * @throws ServletException if error occurs
	 * @throws IOException if error occurs
	 */
	private void showNewEntry(HttpServletRequest req, HttpServletResponse resp,
			BlogUser user) throws ServletException, IOException {
		Long userID = (Long)req.getSession().getAttribute("current.user.id");
		if(userID == null || !userID.equals(user.getId())) {
			ServletUtils.sendError(req, resp, "You don't have persmission to access this page.");
			return;
		}
		
		req.getRequestDispatcher("/WEB-INF/pages/NewOrEditEntry.jsp").forward(req, resp);
		return;
	}

}
