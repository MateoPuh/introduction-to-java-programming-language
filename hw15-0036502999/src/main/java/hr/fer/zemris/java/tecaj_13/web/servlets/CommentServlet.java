package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Servlet used for adding new comment.
 * 
 * @author Mateo Puhalović
 *
 */
@WebServlet("/servleti/comment")
public class CommentServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		String entryID = req.getParameter("entry");
		String userID = req.getParameter("user");
		String comment = req.getParameter("comment");

		if(entryID == null || entryID.isEmpty()) {
			ServletUtils.sendError(req, resp, "EntryID should be provided on comment.");
			return;
		}
		
		BlogComment blogComment = new BlogComment();

		BlogEntry entry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(entryID));
		if(entry == null) {
			ServletUtils.sendError(req, resp, "Valid entryID should be provided on comment.");
			return;
		}
		
		blogComment.setBlogEntry(entry);
		
		if(userID != null && !userID.isEmpty()) {
			BlogUser user = DAOProvider.getDAO().getBlogUser(Long.parseLong(userID));
			blogComment.setUsersEMail(user.getEmail());
		} else {
			blogComment.setUsersEMail("Anonymous");	
		}
		
		blogComment.setMessage(comment);
		blogComment.setPostedOn(new Date());
		
		DAOProvider.getDAO().SaveBlogComment(blogComment);
		
		resp.sendRedirect(req.getContextPath() + "/servleti/main");
	}	

}
