package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Servlet used for logging in.
 * 
 * @author Mateo Puhalović
 *
 */
@WebServlet("/servleti/login")
public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");

		LoginForm f = new LoginForm();
		try {
			f.fillFromHttpRequest(req);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			resp.sendRedirect(req.getContextPath() + "/servleti/main");
		}
			
		f.validate();
		
		if(f.hasErrors()) {
			req.setAttribute("form", f);
			
			req.getRequestDispatcher("main").forward(req, resp);
			resp.sendRedirect(req.getContextPath() + "/servleti/main");

			//req.getRequestDispatcher("/WEB-INF/pages/.jsp").forward(req, resp);
			return;
		}
		
		BlogUser user = f.getUser();
				
		req.getSession().setAttribute("current.user.id", user.getId());
		req.getSession().setAttribute("current.user.fn", user.getFirstName());
		req.getSession().setAttribute("current.user.ln", user.getLastName());
		req.getSession().setAttribute("current.user.nick", user.getNick());
						
		resp.sendRedirect(req.getContextPath() + "/servleti/main");
	}	

}
