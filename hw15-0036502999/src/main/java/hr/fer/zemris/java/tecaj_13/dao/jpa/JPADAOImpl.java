package hr.fer.zemris.java.tecaj_13.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import hr.fer.zemris.java.tecaj_13.dao.DAO;
import hr.fer.zemris.java.tecaj_13.dao.DAOException;
import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

public class JPADAOImpl implements DAO {

	@Override
	public BlogEntry getBlogEntry(Long id) throws DAOException {
		BlogEntry blogEntry = JPAEMProvider.getEntityManager().find(BlogEntry.class, id);
		return blogEntry;
	}
	
	@Override
	public void saveBlogUser(BlogUser user) {
		EntityManager em = JPAEMProvider.getEntityManager();
		em.persist(user);
	}

	@Override
	public BlogUser getBlogUser(String nick) throws DAOException {
		try {
			BlogUser user = (BlogUser)JPAEMProvider.getEntityManager()
					.createQuery("select u from BlogUser as u where u.nick=:nick")
					.setParameter("nick", nick).getSingleResult();
			return user;
		} catch(NoResultException ignorable) {
		}
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BlogUser> getUsers() {
		EntityManager em = JPAEMProvider.getEntityManager();
		return (List<BlogUser>) em.createQuery("select u from BlogUser u").getResultList();
	}

	@Override
	public List<BlogEntry> getBlogEntries(String nick) {
		return getBlogUser(nick).getEntries();
	}

	@Override
	public BlogUser getBlogUser(Long id) throws DAOException {
		BlogUser user = JPAEMProvider.getEntityManager().find(BlogUser.class, id);
		return user;
	}

	@Override
	public void saveBlogEntry(BlogEntry entry) {
		EntityManager em = JPAEMProvider.getEntityManager();
		em.persist(entry);		
	}

	@Override
	public void SaveBlogComment(BlogComment blogComment) {
		EntityManager em = JPAEMProvider.getEntityManager();
		em.persist(blogComment);		
	}

}