<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<style type="text/css">
* {box-sizing: border-box}

/* Add padding to containers */
.container {
  padding: 16px;
}

.error {
		   font-weight: bold;
		   font-size: 1em;
		   color: #FF0000;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit/register button */
.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity:1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}
</style>
</head>
  <body>

  <c:choose>
    <c:when test="${not empty sessionScope['current.user.id']}">
      <h3>Already logged in as ${sessionScope['current.user.fn']} ${sessionScope['current.user.ln']}</h3>
      <a href="<%=request.getContextPath()%>/servleti/logout">Logout</a>
      <p><a href="<%=request.getContextPath()%>/servleti/main">Homepage</a></p>
    </c:when>
    <c:otherwise>
    	<div>
    		<p></p>
    	</div>
	    <form method="post" action="<%=request.getContextPath()%>/servleti/register">
		  <div class="container">
		    <h1>Register</h1>
		    <hr>
		    
		    <div>
			 <c:if test="${form.hasError('firstName')}">
			 <div class="error"><c:out value="${form.getError('firstName')}"/></div>
			 </c:if>
			 <div>
			  <label for="firstName"><b>First Name</b></label>
		      <input type="text" placeholder="Enter First Name" name="firstName" value="${form.firstName}" required>
			 </div>
			</div>
			
			<div>
			 <c:if test="${form.hasError('lastName')}">
			 <div class="error"><c:out value="${form.getError('lastName')}"/></div>
			 </c:if>
			 <div>
			  <label for="lastName"><b>Last Name</b></label>
		      <input type="text" placeholder="Enter Last Name" name="lastName" value="${form.lastName}" required>
			 </div>
			</div>
		    
		    <div>
			 <c:if test="${form.hasError('nick')}">
			 <div class="error"><c:out value="${form.getError('nick')}"/></div>
			 </c:if>
			 <div>
			  <label for="nick"><b>Nick</b></label>
		      <input type="text" placeholder="Enter Nick" name="nick" value="${form.nick}" required>
			 </div>
			</div>
		
			<div>
			 <c:if test="${form.hasError('email')}">
			 <div class="error"><c:out value="${form.getError('email')}"/></div>
			 </c:if>
			 <div>
			  <label for="email"><b>Email</b></label>
		      <input type="text" placeholder="Enter Email" name="email" value="${form.email}" required>
			 </div>
			</div>	
		
		   <div>
			 <c:if test="${form.hasError('password')}">
			 <div class="error"><c:out value="${form.getError('password')}"/></div>
			 </c:if>
			 <div>
			  <label for="password"><b>Password</b></label>
		      <input type="password" placeholder="Enter Password" name="password" required>
			 </div>
			</div>
		
		    <hr>
		
		    <button type="submit" class="registerbtn">Register</button>
		  </div>
		
		  <div class="container signin">
		    <p>Already have an account? <a href="<%=request.getContextPath()%>/servleti/main">Sign in</a>.</p>
		  </div>
		</form>
   
    </c:otherwise>
  </c:choose>

  </body>
</html>
