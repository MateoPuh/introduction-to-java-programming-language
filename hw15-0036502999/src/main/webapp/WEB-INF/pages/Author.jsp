<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<style type="text/css">
* {box-sizing: border-box}

/* Add padding to containers */
.container {
  padding: 16px;
}

.error {
		   font-weight: bold;
		   font-size: 1em;
		   color: #FF0000;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit/register button */
.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity:1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}

.centered {
  text-align: center;
}

</style>
</head>
  <body>

  <c:choose>
    <c:when test="${not empty sessionScope['current.user.id']}">
      <div class="signin">
      <p>You are logged in as:<p> <h3>${sessionScope['current.user.fn']} ${sessionScope['current.user.ln']}</h3>
      <a href="<%=request.getContextPath()%>/servleti/logout">Logout</a>
      </div>
    </c:when>
    <c:otherwise>
    	<div class="signin"> 
		    	<h3>You are not logged in.</h3>
		    </div>
    </c:otherwise>
  </c:choose>
	
	<c:if test="${sessionScope['current.user.nick'].equals(nick)}">
     <h1><a href="<%=request.getContextPath()%>/servleti/author/${nick}/new">New</a></h1>
     </c:if>
     
<h3>Titles:</h3>
  <ul>
  <c:forEach items="${entries}" var="entry">
  	<li><a href="<%=request.getContextPath()%>/servleti/author/${nick}/${entry.id}">${entry.title}</a></li>
  </c:forEach>
  </ul>

  </body>
</html>
