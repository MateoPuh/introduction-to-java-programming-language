<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<style type="text/css">
* {box-sizing: border-box}

/* Add padding to containers */
.container {
  padding: 16px;
}

.error {
		   font-weight: bold;
		   font-size: 1em;
		   color: #FF0000;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

textarea {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit/register button */
.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity:1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}

.centered {
  text-align: center;
}

</style>
</head>
  <body>

  <c:choose>
    <c:when test="${not empty sessionScope['current.user.id']}">
	<div class="signin">
      <p>You are logged in as:<p> <h3>${sessionScope['current.user.fn']} ${sessionScope['current.user.ln']}</h3>
      <a href="<%=request.getContextPath()%>/servleti/logout">Logout</a>
      </div>
    	<form id="newEntry" method="post" action="<%=request.getContextPath()%>/servleti/newOrEdit">
		  <div class="container">
		  <c:choose>
		  <c:when test="${empty form.id}">
		  		    <h1>New Entry:</h1>
		  
		  </c:when>
		  <c:otherwise>
		  		    <h1>Edit Entry:</h1>
		  
		  </c:otherwise>
		  </c:choose>
		    <hr>
		    
		    <input type="hidden" name="id" value="${form.id}"> 
		    
		    <div>
			 <c:if test="${form.hasError('title')}">
			 <div class="error"><c:out value="${form.getError('title')}"/></div>
			 </c:if>
			 <div>
			  <label for="title"><b>Title</b></label>
		      <input type="text" placeholder="Enter Title" name="title" value="${form.title}" required>
			 </div>
			</div>
				
		
		   <div>
			 <c:if test="${form.hasError('text')}">
			 <div class="error"><c:out value="${form.getError('text')}"/></div>
			 </c:if>
			 <div>
			  <label for="text"><b>Text</b></label>
		      <textarea rows="10" cols="50" placeholder="Enter Text" name="text" required>${form.text}</textarea>
			 </div>
			</div>
		
		    <hr>
		
		    <button type="submit" class="registerbtn">Submit</button>
		  </div>
		
		</form>
    </c:when>
    <c:otherwise>
    <div class="signin"> 
	   	<h3>You are not logged in.</h3>
	</div>
    </c:otherwise>
  </c:choose>

  </body>
</html>
