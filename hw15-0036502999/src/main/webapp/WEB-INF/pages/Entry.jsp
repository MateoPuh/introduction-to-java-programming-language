<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<style type="text/css">
* {box-sizing: border-box}

/* Add padding to containers */
.container {
  padding: 16px;
}

.error {
		   font-weight: bold;
		   font-size: 1em;
		   color: #FF0000;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit/register button */
.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity:1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}

.centered {
  text-align: center;
}

</style>
</head>
  <body>

	<c:choose>
    <c:when test="${not empty sessionScope['current.user.id']}">
      <div class="signin">
      <p>You are logged in as:<p> <h3>${sessionScope['current.user.fn']} ${sessionScope['current.user.ln']}</h3>
      <a href="<%=request.getContextPath()%>/servleti/logout">Logout</a>
      </div>
    </c:when>
    <c:otherwise>
		  <div class="container">
		    <div class="signin"> 
		    	<h3>You are not logged in.</h3>
		    </div>
	</c:otherwise>
	</c:choose>
  
  <c:choose>
    <c:when test="${blogEntry==null}">
      Nema unosa!
    </c:when>
    <c:otherwise>
    <h3>${nick}'s blog (${blogEntry.createdAt}):</h3>
    <c:if test="${!blogEntry.createdAt.equals(blogEntry.lastModifiedAt)}">
    <h4>Last modified at: ${blogEntry.lastModifiedAt }</h4>
    </c:if>
    <hr>
     <c:if test="${sessionScope['current.user.id'] == blogEntry.getCreator().getId()}">
     <h1><a href="<%=request.getContextPath()%>/servleti/author/${nick}/edit?eid=${blogEntry.id}">edit</a></h1>
     </c:if>
      <h2><c:out value="${blogEntry.title}"/></h2>
      <p><c:out value="${blogEntry.text}"/></p>
      <hr>
      <c:choose>
      <c:when test="${!blogEntry.comments.isEmpty()}">
      <h3>Comments:</h3>
      <ul>
      <c:forEach var="e" items="${blogEntry.comments}">
        <li><div style="font-weight: bold">[Korisnik=<c:out value="${e.usersEMail}"/>] <c:out value="${e.postedOn}"/></div><div style="padding-left: 10px;"><c:out value="${e.message}"/></div></li>
      </c:forEach>
      </ul>
      </c:when>
      <c:otherwise>
      <p>No comments...</p>
      </c:otherwise>
      </c:choose>
      <hr>
      <form method="post" action="<%=request.getContextPath()%>/servleti/comment">
		  <div class="container">
		   
		   <input type="hidden" name="entry" value="${blogEntry.id}"> 
		   <input type="hidden" name="user" value="${sessionScope['current.user.id']}"> 
		   
		   <label for="comment"><b>Comment</b></label>
		    <input type="text" placeholder="Enter Comment" name="comment" required>
			 
		    <button type="submit" class="registerbtn">Comment</button>
		  </div>

		</form>
    </c:otherwise>
  </c:choose>

  </body>
</html>
