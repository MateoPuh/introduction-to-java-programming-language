package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * This class represents general collection of objects which
 * are called elements. It contains various methods for manipulating 
 * elements in collection which all collections should have, such 
 * as methods for adding and removing elements.
 * 
 * <p>Classes that extend Collection class should implement methods that
 * they want to use, expect for methods isEmpty() and addAll(Collection).</p>
 * 
 * <p>Static method checkBounds(int, int, int) can be used for checking
 * if index is inside allowed range.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Collection {
	
	/**
	 * Creates new collection.
	 */
	protected Collection() {
		
	}
	
	/**
	 * Returns true if there are no elements in collection.
	 * 
	 * @return true is collection is empty; false otherwise
	 */
	public boolean isEmpty() {		
		return (size() <= 0); 
	}

	/**
	 * Returns number of elements in collection.
	 * 
	 * @return size of collection
	 */
	public int size() {
		return 0;
	}
	
	/**
	 * Insert the element with given value to the end of collection. 
	 * 
	 * @param value value to insert into collection
	 */
	public void add(Object value) {
		
	}

	/**
	 * Returns true if the collection contains given value. Uses 
	 * equals method to determine if value is contained in collection. 
	 * Giving null as parameter value is allowed.
	 * 
	 * @param value value to find in collection
	 * @return true if collection contains given value; false otherwise
	 */
	boolean contains(Object value) {
		return false;
	}
	
	/**
	 * Removes given value from collection. Uses equals method to 
	 * determine if value is contained in collection and removes one
	 * instance of it.
	 * 
	 *  @param value value to remove from collection
	 *  @return true if value was removed from collection; false otherwise
	 */
	public boolean remove(Object value) {
		return false;
	}
	
	/**
	 * Returns the collection in form of array.
	 */
	public Object[] toArray(){
		throw new UnsupportedOperationException();
	}

	/**
	 * Calls processor.process() method on every element of collection. 
	 * 
	 * @param processor processor which method process is used on 
	 * 			each element of collection
	 */
	public void forEach(Processor processor) {
		
	}
	
	/**
	 * Copies the elements of given collection to original collection. 
	 * Uses local processor class whose method process() adds element 
	 * of other collection to original collection. 
	 * 
	 * @param other collection whose elements are copied to original collection 
	 */
	public void addAll(Collection other) {
		
		/**
		 * Local processor class inside of Collection.addAll(Collection) method.
		 * It is used for adding all elements of other collection to original 
		 * collection.
		 * 
		 * @author Mateo Puhalović
		 * @version 1.0
		 * @see Collection#addAll
		 * 
		 */
		class AddAllProcessor extends Processor {
			
			private Collection collection;
			
			/**
			 * Creates new AddAllProcessor with given collection to which 
			 * new elements will be added.
			 * 
			 * @param collection collection to which elements are copied 
			 * @throws NullPointerException if given collection is null
			 */
			public AddAllProcessor(Collection collection) throws NullPointerException {
				this.collection = Objects.requireNonNull(collection, "Given collection should not be null!");
			}
			
			/**
			 * Copies value of given element of other collection to 
			 * original collection.
			 * 
			 * @param value value of element of other collection which 
			 * 		will be added to original collection 
			 */
			public void process(Object value) {
				collection.add(value);
			}
			
		}

		Processor p = new AddAllProcessor(this);
		other.forEach(p);	
	}

	/**
	 * Removes all elements from this collection.
	 */
	public void clear() {
		
	}

	/**
	 * Checks whether given index is in [lowerBorder, upperBorder].
	 * 
	 * @param index index to check
	 * @param lowerBorder lower border of interval in which index is acceptable
	 * @param upperBorder upper border of interval in which index is acceptable
	 * @throws IndexOutOfBoundsException if given index is not in [0, size]
	 */
	protected static void checkBounds(int index, int lowerBorder, int upperBorder) throws IndexOutOfBoundsException{
		if (index < lowerBorder || index > upperBorder) {
			throw new IndexOutOfBoundsException("Index is out of bounds!");
		}
	}
}
