package hr.fer.zemris.java.hw02;

import java.util.Objects;

/**
 *  This class represents a complex number. Complex number is represented by
 *  its real and imaginary part.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ComplexNumber {

	private double real;
	private double imaginary;
	
	/**
	 * Creates new complex number with given real and imaginary part.
	 * 
	 * @param real real part of complex number
	 * @param imaginary imaginary part of complex number
	 */
	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}
	
	/**
	 * Creates new complex number with given real part. Imaginary part
	 * is set to 0;
	 * 
	 * @param real real part of complex number
	 * @return new complex number
	 */
	public static ComplexNumber fromReal(double real) {
		return new ComplexNumber(real, 0);
	}
	
	/**
	 * Creates new complex number with given imaginary part. Real part
	 * is set to 0;
	 * 
	 * @param imaginary imaginary part of complex number
	 * @return new complex number
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		return new ComplexNumber(0, imaginary);
	}
	
	/**
	 * Creates new complex number from given magnitude and angle.
	 * 
	 * @param magnitude magnitude of complex number
	 * @param angle angle of complex number
	 * @return new complex number
	 */
	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
		return new ComplexNumber(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
	}
	
	/**
	 * Returns new complex number that is parsed from given string. Method 
	 * Double.parseDouble(String) is used for parsing numbers.
	 * 
	 * <p>Given string can contain pure real number: x, pure imaginary number: yi,
	 * or real and imaginary part: x+yi. String given in yi+x format cannot be parsed.
	 * If "i" is before imaginary number it cannot be parsed. So, numbers like "i2" or 
	 * "i4.567" will not be parsed. Placing "+" sign befora real part is acceptable, 
	 * however placing multiple signs is not acceptable. So, numbers like "+-2" or
	 * "3++4i" will not be parsed.</p> 
	 * 
	 * @param s complex number in string format
	 * @return parsed complex number
	 * @throws NumberFormatException if numbers are in wrong format
	 * @throws NullPointerException if given string is null
	 */
	public static ComplexNumber parse(String s) throws NumberFormatException, NullPointerException {			
		Objects.requireNonNull(s, "Cannot parse null value!");
		
		char[] charArray = s.toCharArray();
		
		// index of ending of first number
		int firstEnd = -1;
		
		// finding the real part of complex number
		for (int i = 0, length = charArray.length; i < length; i++) {
			if ((charArray[i] == '+' || charArray[i] == '-') && i > 0) {
				firstEnd = i -1;
			} 
			
			// if 'i' is in string that means there is imaginary part
			if (charArray[i] == 'i') {
				
				// if there is 'i' in string, it should be on last position
				if (i != length - 1) {
					throw new NumberFormatException(); 
				}
				
				double second;
				double first;
				
				// if only 'i' is written, without number, then imaginary part is 1 (or -1)
				if (i == 0 || charArray[i-1] == '+') {
					second = 1;
				} else if (charArray[i-1] == '-') {
					second = -1;
				} else if (firstEnd == -1) {
					second = Double.parseDouble(s.substring(0, s.length() - 1));
				} else {
					second = Double.parseDouble(s.substring(firstEnd + 1, s.length() - 1));
				}
				
				if (firstEnd == -1) {
					first = 0;
				} else {
					first = Double.parseDouble(s.substring(0, firstEnd+1));
				}
				
				return new ComplexNumber(first, second);
			}
		}
		
		if (firstEnd == -1) {
			return new ComplexNumber(Double.parseDouble(s), 0);
		} else {
			throw new NumberFormatException();
		}
	}
	
	/**
	 * Returns real part of complex number.
	 * 
	 * @return real part of complex number
	 */
	public double getReal() {
		return real;
	}
	
	/**
	 * Returns imaginary part of complex number.
	 * 
	 * @return imaginary part of complex number
	 */
	public double getImaginary() {
		return imaginary;
	}
	
	/**
	 * Returns magnitude of complex number.
	 * It is calculated by equation:
	 * magnitude = sqrt(real^2 + imaginary^2).
	 * 
	 * @return magnitude of complex number
	 */
	public double getMagnitude() {
		return Math.sqrt(Math.pow(real, 2) + Math.pow(imaginary, 2));
	}
	
	/**
	 * Returns angle of complex number in radians in range [0, 2 * PI].
	 * It is calculated by equation: 
	 * angle = atan(imaginary / real);
	 * 
	 * @return angle of complex number
	 */
	public double getAngle() {
		double angle = Math.atan(imaginary / real);

		if (real < 0) {
			angle += Math.PI;
		}
		
		return angle < 0 ? 2 * Math.PI + angle : angle;
	}
	
	/**
	 * Returns new complex number which is result of addition of two 
	 * complex numbers. 
	 * Given complex number is added to the original one.
	 * 
	 * @param c other complex number 
	 * @return result of addition
	 */
	public ComplexNumber add(ComplexNumber c) {
		return new ComplexNumber(real + c.getReal(), imaginary + c.getImaginary());
	}
	
	/**
	 * Returns new complex number which is result of subtraction of two 
	 * complex numbers. 
	 * Given complex number is subtracted from the original one.
	 * 
	 * @param c other complex number 
	 * @return result of subtraction
	 */
	public ComplexNumber sub(ComplexNumber c) {
		return new ComplexNumber(real - c.getReal(), imaginary - c.getImaginary());
	}
	
	/**
	 * Returns new complex number which is result of multiplication of two 
	 * complex numbers. 
	 * Original complex number is multiplied by given complex number.
	 * 
	 * @param c other complex number 
	 * @return result of multiplication
	 */
	public ComplexNumber mul(ComplexNumber c) {
		return new ComplexNumber(real * c.getReal() - imaginary * c.getImaginary(), 
				real * c.getImaginary() + imaginary * c.getReal());
	}
	
	/**
	 * Returns new complex number which is result of division of two 
	 * complex numbers. 
	 * Original complex number is divided by given complex number.
	 * 
	 * @param c other complex number 
	 * @return result of division
	 */
	public ComplexNumber div(ComplexNumber c) {
		return new ComplexNumber(
				(real * c.getReal() + imaginary * c.getImaginary())/(Math.pow(c.getReal(), 2) + Math.pow(c.getImaginary(), 2)), 
				(imaginary * c.getReal() - real * c.getImaginary())/((Math.pow(c.getReal(), 2) + Math.pow(c.getImaginary(), 2))));
	}
	
	/**
	 * Return complex number which is result of exponentiation of
	 * complex number to the exponent of given power.
	 * 
	 * @param n power of exponent
	 * @return result of exponentiation
	 * @throws IllegalArgumentException if power of exponent is less than 0
	 */
	public ComplexNumber power(int n) throws IllegalArgumentException{
		if (n < 0) {
			throw new IllegalArgumentException("Power of exponent should be >= 0!");
		}
		
		return fromMagnitudeAndAngle(Math.pow(getMagnitude(),  n), n * getAngle());
	}
	
	/**
	 * Return complex numbers which are nth root of complex number (n is 
	 * number given by user).
	 * 
	 * @param n index of root 
	 * @return roots of complex number
	 * @throws IllegalArgumentException if given index is less or equal to 0
	 */
	public ComplexNumber[] root(int n) throws IllegalArgumentException {
		if (n <= 0) {
			throw new IllegalArgumentException("Index of root should be > 0!");
		}
		
		ComplexNumber[] result = new ComplexNumber[n];
				
		for (int i = 0; i < n; i++) {
			result[i] = fromMagnitudeAndAngle(Math.pow(getMagnitude(),  1.0 / n), (getAngle() + (double)i * 2.0 * Math.PI) / n);
		}
		
		return result;
	}

	/**
	 * Return complex number in string format.
	 */
	public String toString() {
		return real + String.valueOf((imaginary >= 0) ? "+" + imaginary : imaginary) + "i";
	}

	@Override
	public int hashCode() {
		return Objects.hash(imaginary, real);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ComplexNumber))
			return false;
		ComplexNumber other = (ComplexNumber) obj;
		return Math.abs(this.imaginary - other.imaginary) < 1e-6 && Math.abs(this.real - other.real) < 1e-6;
	}
	
	
}
