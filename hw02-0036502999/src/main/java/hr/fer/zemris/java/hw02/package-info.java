﻿/**
 * This package provides ComplexNumber class which
 * can be used for dealing with complex number.
 * It contains various methods for mathematical 
 * operations with complex numbers. It aloows user
 * to parse complex number from string and to get
 * complex number from magnitude and angle.
 * 
 *  
 * @author Mateo Puhalović
 * @version 1.0
 */
package hr.fer.zemris.java.hw02;