package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.Objects;

/**
 * This class represents resizable array-backed collection.
 * This collection allows duplicate elements and doesn't allow
 * null refrences. 
 * 
 * <p> Implementation of this collection is based on resizable array
 * which means that it doubles its capacity every time it becomes full. </p>
 * 
 * <p> That is the reasons why complexity of add(Object) method is O(n) in 
 * worst case, but its average complexity is Θ(1). Average complexity of
 * method get(int) is also Θ(1). Average complexity of methods contains(Object), 
 * clear(), remove(int), remove(Object), addAll(Collection), insert(Object, int),
 * indexOf(Object) is Θ(n). </p>
 * 
 * <p> The capacity of this collection will initialy be set to default capacity
 * of 16 if capacity is not specified. It can be initialy filled with elements
 * of other collection if that collection is specified. </p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ArrayIndexedCollection extends Collection {
	
	private int size;
	private Object[] elements;
	private int capacity;
	
	/**
	 * Default capacity of collection
	 */
	private static final int DEFAULT_CAPACITY = 16;
	
	/**
	 * Creates an empty collection with a default capacity of 16.
	 */
	public ArrayIndexedCollection() {
		this(DEFAULT_CAPACITY);
	}
	
	/**
	 * Creates an empty collection with a given initial capacity.
	 * 
	 * @param initialCapacity initial capacity of collection
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		this(new Collection(), initialCapacity);
	}
	
	/**
	 * Creates an collection filled with elements of given collection. 
	 * Its capacity will be set to default capacity of 16 or to size of collection if
	 * default capacity is not enough to fit all elements in.
	 * 
	 * @param collection collection which elements will be copied to new collection 
	 */
	public ArrayIndexedCollection(Collection collection) {
		this (collection, DEFAULT_CAPACITY);
	}
	
	/**
	 * Creates an collection filled with elements of given collection.
	 * Its capacity will be set to given initial capacity or to size of collection if
	 * given capacity is not enough to fit all elements in.
	 * 
	 * @param collection collection which elements will be copied to new collection 
	 * @param initialCapacity initial capacity of collection
	 * @throws NullPointerException if given collection is null
	 */
	public ArrayIndexedCollection(Collection collection, int initialCapacity) 
			throws NullPointerException{
	    Objects.requireNonNull(collection, "Given collection should not be null!");

		if (initialCapacity < 1) {
			throw new IllegalArgumentException("Capacity should be more than 1!");
		}
		
		capacity = (initialCapacity > collection.size()) ? initialCapacity : collection.size(); 
		elements = new Object[capacity];
		
		addAll(collection);
	}
	
	@Override
	public int size() {
		return size;
	}
	
	/**
	 * Insert the element with given value to the end of collection. If 
	 * array is full before adding new element, it will first double its size
	 * and then proceed to add new element. 
	 * 
	 *  <p> Average complexity of this method is Θ(1). 
	 *  It is O(n) in worst case, which is only when array is doubling its size. </p>
	 * 
	 * @param value value to insert into collection
	 * @throws NullPointerException if given value is null
	 */
	@Override
	public void add(Object value) throws NullPointerException {
	    Objects.requireNonNull(value, "Null value cannot be added to this collection!");

	    insert(value, size);	    
	}

	/**
	 * Doubles the capacity of array by creating the new array
	 * with double the capacity of original one and copying all 
	 * the elements in new array.
	 */
	private void doubleArrayCapacity() {
    	capacity *= 2;
    	Object[] newElements = Arrays.copyOf(elements, capacity);
    	elements = newElements;
	}
	
	@Override
	public boolean contains(Object value) {
		if (value == null) {
			return false;
		}
		
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(value)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Removes given value from collection. Uses equals method to 
	 * determine if value is contained in collection. Since this collection
	 * can have duplicate values, this implementation removes first instance
	 * of element with given value.
	 * 
	 *  @param value value to remove from collection
	 *  @return true if value was removed from collection; false otherwise
	 */
	@Override
	public boolean remove(Object value) {
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(value)) {
				try {
					remove(i);
					return true;
				} catch (IndexOutOfBoundsException ex) {
					return false;
				}
			}
		}
		
		return false;
	}

	/**
	 * Removes element at given index of collection. It moves part of array that 
	 * starts at (index + 1) 1 index lower than they were before.
	 * 
	 * @param index index of element to remove
	 * @throws IndexOutOfBoundsException if given index is not in [0, size - 1]
	 */
	public void remove(int index) throws IndexOutOfBoundsException {
		checkBounds(index);
		
		for (int i = index; i < size - 1; i++) {
			elements[i] = elements[i + 1];
		}
		
		elements[size-1] = null;
		size --;	
	}
	
	/**
	 * Returns index of element with given value. Uses method equals 
	 * for finding which element has given value. Since collection can
	 * have duplicate elements, this implementation returns index of 
	 * first element with given value.
	 * 
	 * @param value value whose index is being searched
	 * @return index of element with given value; -1 if not found
	 */
	public int indexOf(Object value) {
		if (value == null) {
			return -1;
		}
		
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(value)) {
				return i;
			}
		}
		
		return -1;
	}
	
	/**
	 * Returns element at given index. 
	 * 
	 * @param index index element that is searched for
	 * @return element at given index
	 * @throws IndexOutOfBoundsException if given index is not in [0, size - 1]
	 */
	public Object get(int index) throws IndexOutOfBoundsException {
		checkBounds(index);
				
		return elements[index];
	}
	
	@Override
	public Object[] toArray(){
		Object[] array = Arrays.copyOf(elements, size);
		return array;
	}

	
	@Override
	public void forEach(Processor processor) {
		for (int i = 0; i < size; i++) {
			processor.process(elements[i]);
		}
	}

	@Override
	public void clear() {
		for (int i = 0; i < size - 1; i++) {
			elements[i] = null;
		}
		
		size = 0;
	}
	
	/**
	 * Inserts element with given value at the given position. 
	 * 
	 * @param value value to insert in collection
	 * @param position position on which to insert new element
	 * @throws IndexOutOfBoundsException if given position is not in [0, size]
	 * @throws NullPointerException if given value is null
	 */
	public void insert(Object value, int position) throws IndexOutOfBoundsException, NullPointerException {
		checkBounds(position, 0, size);		
	    Objects.requireNonNull(value, "Null value cannot be added to this collection!");

		if (size + 1 > capacity) {
			doubleArrayCapacity();
		}
		
		for (int i = size; i > position; i--) {
			elements[i] = elements[i - 1];
		}
		
		elements[position] = value;
		size++;
	}

	/**
	 * Checks if given index is in [0, size]. Uses Collection.checkBounds(int, int, int) method
	 * with values 0 and size - 1;
	 * 
	 * @param index index to check 
	 * @throws IndexOutOfBoundsException if given index is not in [0, size - 1]
	 * @see Collection#checkBounds(int, int, int)
	 */
	private void checkBounds(int index) throws IndexOutOfBoundsException{
		checkBounds(index, 0, size - 1);
	}
	
}
