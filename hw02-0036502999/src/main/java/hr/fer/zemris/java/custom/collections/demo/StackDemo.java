package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * This class demonstrates usage of ObjectStack class. 
 * ObjectStack is used for evaluating expression in 
 * postfix representation which user gives as
 * command-line argument.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class StackDemo {

	/** Sign that represents addition operation. */
	private static final String PLUS = "+";
	
	/** Sign that represents subtraction operation. */
	private static final String MINUS = "-";
	
	/** Sign that represents multiplication operation. */
	private static final String MULTIPLICATION_SIGN = "*";
	
	/** Sign that represents division operation. */
	private static final String DIVISION_SIGN = "/";
	
	/** Sign that represents modulo operation. */
	private static final String MODULO = "%";


	/**
	 * This method is first run after running the program. 
	 * It takes one argument on command-line which is then
	 * evaluated as expression in postfix representation.
	 * If number of arguments is not 1, user is informed about
	 * it and program quits.
	 * 
	 * @param args command-line argument
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Wrong number of arguments!");
			System.exit(1);;
		}

		ObjectStack stack = new ObjectStack();
		
		String[] arguments = args[0].split("\\s+");
		
		for (String arg : arguments) {	
			try {
				int number = Integer.parseInt(arg);
				stack.push(number);
			} catch(NumberFormatException ex) {
				try {
					int number2 = (int)stack.pop();
					int number1 = (int)stack.pop();
					
					int result = calculate(number1, number2, arg);
					
					stack.push(result);
				} catch(EmptyStackException ex2) {
					System.out.println("Invalid expression!");
					System.exit(1);
				} catch(IllegalArgumentException ex2) {
					System.out.println("Invalid operation!");
					System.exit(1);
				}
			}
		}
		
		if (stack.size() != 1) {
			System.out.println("Invalid expression!");
			System.exit(1);
		} else {
			int result = (int)stack.pop();
			System.out.println("Expression evaluates to " + result + ".");
		}
	}
	
	/**
	 * This method calculates expression given to it by arguments
	 * number1, number2 and operation. If operation cannot be parsed
	 * this method throws IllegalArgumentException. 
	 * 
	 * @param number1 first argument
	 * @param number2 second argument
	 * @param operation operation to apply on arguments
	 * @return result of operation
	 * @throws IllegalArgumentException if operation cannot be parsed
	 */
	public static int calculate(int number1, int number2, String operation) 
			throws IllegalArgumentException {
		
		switch(operation.trim()) {
		case PLUS:
			return number1 + number2;
		case MINUS:
			return number1 - number2;
		case DIVISION_SIGN:
			return number1 / number2;
		case MULTIPLICATION_SIGN:
			return number1 * number2;
		case MODULO:
			return number1 % number2;
		default:
			throw new IllegalArgumentException();
		}
			
	}
	
}
