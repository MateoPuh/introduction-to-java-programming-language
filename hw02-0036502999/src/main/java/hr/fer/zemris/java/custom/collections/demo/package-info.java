﻿/**
 * This package provides demo class which shows
 * usage of ObjectStack class. ObjectStack is used
 * for evaluating expression in postfix representation.
 * 
 *  
 * @author Mateo Puhalović
 * @version 1.0
 */
package hr.fer.zemris.java.custom.collections.demo;