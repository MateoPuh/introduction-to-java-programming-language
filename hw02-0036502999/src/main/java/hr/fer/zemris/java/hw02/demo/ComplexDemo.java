package hr.fer.zemris.java.hw02.demo;

import hr.fer.zemris.java.hw02.ComplexNumber;

/**
 * This class demonstrates usage of ComplexNumber class.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ComplexDemo {

	/**
	 * This method is first run after running the program. 
	 * It creates few complex numbers and performs few mathematical
	 * operations on those numbers.
	 */
	public static void main(String[] args) {
		ComplexNumber c1 = new ComplexNumber(2, 3);
		ComplexNumber c2 = ComplexNumber.parse("2.5-3i");
		ComplexNumber c3 = c1.add(ComplexNumber.fromMagnitudeAndAngle(2, 1.57))
		.div(c2).power(3).root(2)[1];
		System.out.println(c3);
	}
	
}
