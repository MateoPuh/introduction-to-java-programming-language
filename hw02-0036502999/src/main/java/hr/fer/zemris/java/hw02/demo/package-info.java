﻿/**
 * This package provides demonstration of ComplexNumber class.
 * 
 *  
 * @author Mateo Puhalović
 * @version 1.0
 */
package hr.fer.zemris.java.hw02.demo;