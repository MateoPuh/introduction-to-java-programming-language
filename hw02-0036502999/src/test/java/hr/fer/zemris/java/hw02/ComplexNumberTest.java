package hr.fer.zemris.java.hw02;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class ComplexNumberTest {
	
	@Test
	public void constructorTest() {
		ComplexNumber c = new ComplexNumber(1, 1);
		
		assertEquals(1, c.getReal());
		assertEquals(1, c.getImaginary());
	}
	
	@Test
	public void fromMagnitudeAndAngleTest() {
		ComplexNumber c = ComplexNumber.fromMagnitudeAndAngle(1, Math.PI / 3);

		assertEquals(1, c.getMagnitude());
		assertEquals(Math.PI / 3, c.getAngle());
	}
	
	@Test
	public void fromRealTest() {
		ComplexNumber c = ComplexNumber.fromReal(1);
		
		assertEquals(1, c.getReal());
		assertEquals(0, c.getImaginary());
	}

	@Test
	public void fromImaginaryTest() {
		ComplexNumber c = ComplexNumber.fromImaginary(1);
		
		assertEquals(0, c.getReal());
		assertEquals(1, c.getImaginary());
	}
	
	@Test
	public void parseTest() {
		ComplexNumber c = ComplexNumber.parse("3.51");
		
		assertEquals(3.51, c.getReal());
		assertEquals(0, c.getImaginary());

		c = ComplexNumber.parse("-3.17");
		
		assertEquals(-3.17, c.getReal());
		assertEquals(0, c.getImaginary());
		
		c = ComplexNumber.parse("-2.71i");
		
		assertEquals(0, c.getReal());
		assertEquals(-2.71, c.getImaginary());
		
		c = ComplexNumber.parse("-2.71-3.15i");
		
		assertEquals(-2.71, c.getReal());
		assertEquals(-3.15, c.getImaginary());
		
		c = ComplexNumber.parse("i");
		
		assertEquals(0, c.getReal());
		assertEquals(1, c.getImaginary());
		
		c = ComplexNumber.parse("+i");
		
		assertEquals(0, c.getReal());
		assertEquals(1, c.getImaginary());
		
		c = ComplexNumber.parse("-i");
		
		assertEquals(0, c.getReal());
		assertEquals(-1, c.getImaginary());
		
		c = ComplexNumber.parse("1+i");
		
		assertEquals(1, c.getReal());
		assertEquals(1, c.getImaginary());
	}
	
	@Test
	public void parseNullExceptionTest() {
		assertThrows(NullPointerException.class, () -> {
			ComplexNumber.parse(null);
		});;
	}
	
	@Test
	public void parseExceptionTest1() {
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber.parse("i351");
		});;
	}
	
	@Test
	public void parseExceptionTest2() {
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber.parse("-i35.1");
		});;
	}
	
	@Test
	public void parseExceptionTest3() {
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber.parse("-+2.71");
		});;
	}
	
	@Test
	public void parseExceptionTest4() {
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber.parse("-2.71+-3.15i");
		});;
	}

	@Test
	public void addTest() {
		ComplexNumber c = ComplexNumber.parse("-2.5-3.5i");
		ComplexNumber c2 = ComplexNumber.parse("2-3i");

		ComplexNumber c3 = c.add(c2);
		
		assertEquals(-0.5, c3.getReal());
		assertEquals(-6.5, c3.getImaginary());
	}
	
	@Test
	public void subTest() {
		ComplexNumber c = ComplexNumber.parse("-2.5-3.5i");
		ComplexNumber c2 = ComplexNumber.parse("2-3i");

		ComplexNumber c3 = c.sub(c2);
		
		assertEquals(-4.5, c3.getReal());
		assertEquals(-0.5, c3.getImaginary());
	}
	
	@Test
	public void mulTest() {
		ComplexNumber c = ComplexNumber.parse("-2.5-3.5i");
		ComplexNumber c2 = ComplexNumber.parse("2-2i");

		ComplexNumber c3 = c.mul(c2);
		
		assertEquals(-12, c3.getReal());
		assertEquals(-2, c3.getImaginary());
	}
	
	@Test
	public void divTest() {
		ComplexNumber c = ComplexNumber.parse("-2.5-3.5i");
		ComplexNumber c2 = ComplexNumber.parse("2-2i");

		ComplexNumber c3 = c.div(c2);
		
		assertEquals(0.25, c3.getReal());
		assertEquals(-1.5, c3.getImaginary());
	}
	
	@Test
	public void powerTest() {
		ComplexNumber c = ComplexNumber.parse("1-2i");

		ComplexNumber c2 = c.power(6);
		
		assertEquals(117, c2.getReal(), 1e-8);
		assertEquals(-44, c2.getImaginary(), 1e-3);
	}
	
	@Test
	public void powerExceptionTest() {
		assertThrows(IllegalArgumentException.class, () -> {
			ComplexNumber c = ComplexNumber.parse("1-2i");
			c.power(-1);
		});;
	}
	
	@Test
	public void rootTest() {
		ComplexNumber c= ComplexNumber.parse("-5+12i");

		ComplexNumber[] roots = c.root(2);
		
		assertEquals(2, roots.length);
		
		ComplexNumber test1 = ComplexNumber.parse("2+3i");
		ComplexNumber test2 = ComplexNumber.parse("-2-3i");
		
		assertEquals(test1, roots[0]);
		assertEquals(test2, roots[1]);
	}
	
	@Test
	public void rootExceptionTest() {
		assertThrows(IllegalArgumentException.class, () -> {
			ComplexNumber c = ComplexNumber.parse("1-2i");
			c.root(0);
		});;
	}
	
	@Test
	public void angleTest() {
		ComplexNumber c1 = ComplexNumber.parse("1+i");
		ComplexNumber c2 = ComplexNumber.parse("-1+i");
		ComplexNumber c3 = ComplexNumber.parse("-1-i");
		ComplexNumber c4 = ComplexNumber.parse("1-i");

		assertEquals(0.25, c1.getAngle() / Math.PI);
		assertEquals(0.75, c2.getAngle() / Math.PI);
		assertEquals(1.25, c3.getAngle() / Math.PI);
		assertEquals(1.75, c4.getAngle() / Math.PI);
	}
	
	@Test
	public void toStringTest() {
		ComplexNumber c = ComplexNumber.parse("1-2i");
		ComplexNumber c2 = ComplexNumber.parse(c.toString());
		
		assertEquals(c2, c);
	}
	
}
