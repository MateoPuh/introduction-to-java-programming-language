package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class ArrayIndexedCollectionTest {
	
	private static final int SIZE_OF_COLLECTION = 10;
	
	private ArrayIndexedCollection createEmptyCollection() {
		ArrayIndexedCollection col = new ArrayIndexedCollection();
		return col;
	}
	
	private ArrayIndexedCollection createNonEmptyCollection() {
		ArrayIndexedCollection col = new ArrayIndexedCollection();
		
		for (int i = 0; i < SIZE_OF_COLLECTION; i++) {
			col.add(Integer.valueOf(i));
		}
		
		return col;
	}
	
	@Test
	public void firstConstructorTest() {
		ArrayIndexedCollection col = new ArrayIndexedCollection();
		
		assertNotNull(col);
	}
	
	@Test
	public void secondConstructorTest() {
		ArrayIndexedCollection col = new ArrayIndexedCollection(10);
		
		assertNotNull(col);
	}
	
	@Test
	public void thirdConstructorTest() {
		ArrayIndexedCollection other = createNonEmptyCollection();
		
		ArrayIndexedCollection col = new ArrayIndexedCollection(other);
		
		assertEquals(SIZE_OF_COLLECTION, col.size());
	}
	
	@Test
	public void fourthConstructorTest() {
		ArrayIndexedCollection other = createNonEmptyCollection();
		
		ArrayIndexedCollection col = new ArrayIndexedCollection(other, 10);
		
		assertEquals(SIZE_OF_COLLECTION, col.size());
	}
	
	
	@Test
	public void addTestNull() {
		assertThrows(NullPointerException.class, () -> {
			ArrayIndexedCollection col = createEmptyCollection();
			col.add(null);
		});
	}
	
	@Test
	public void addTest() {
		ArrayIndexedCollection col = createEmptyCollection();
		col.add(1);
		
		assertEquals(1, col.size());
	}
	
	@Test
	public void getTestExceptionTooBig() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ArrayIndexedCollection col = createEmptyCollection();
			col.add(1);
			
			col.get(2);
		});;
	}
	
	@Test
	public void getTestExceptionNegative() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ArrayIndexedCollection col = createEmptyCollection();
			col.add(1);
			
			col.get(-1);
		});;
	}
	
	@Test
	public void getTest() {
		ArrayIndexedCollection col = createEmptyCollection();
		
		col.add(0);
		col.add(1);
		col.add(2);
		col.add(3);
		
		assertEquals(0, col.get(0));
		assertEquals(3, col.get(col.size()-1));		
	}
	
	@Test
	public void clearTest() {
		ArrayIndexedCollection col = createNonEmptyCollection();
		
		assertEquals(SIZE_OF_COLLECTION, col.size());
		
		col.clear();
		
		assertEquals(0, col.size());		
	}
	
	@Test
	public void insertBeginningTest() {
		ArrayIndexedCollection col = createNonEmptyCollection();
		
		assertEquals(SIZE_OF_COLLECTION, col.size());
		
		assertEquals(0, col.get(0));
		assertEquals(1, col.get(1));
		assertEquals(2, col.get(2));

		col.insert(10, 0);	
		
		assertEquals(10, col.get(0));
		assertEquals(0, col.get(1));
		assertEquals(1, col.get(2));
		
		assertEquals(SIZE_OF_COLLECTION + 1, col.size());
	}

	@Test
	public void insertEndTest() {
		ArrayIndexedCollection col = createNonEmptyCollection();
		
		assertEquals(SIZE_OF_COLLECTION, col.size());
		
		assertEquals(7, col.get(7));
		assertEquals(8, col.get(8));
		assertEquals(9, col.get(9));
		
		col.insert(100, col.size());
		
		assertEquals(8, col.get(8));
		assertEquals(9, col.get(9));
		assertEquals(100, col.get(10));
		
		assertEquals(SIZE_OF_COLLECTION + 1, col.size());
	}
	
	@Test
	public void insertExceptionTestTooBig() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ArrayIndexedCollection col = createNonEmptyCollection();
			col.insert(100, col.size()+1);
		});;
	}
	
	@Test
	public void insertExceptionTestNegative() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ArrayIndexedCollection col = createNonEmptyCollection();
			col.insert(100, -1);
		});;
	}
	
	@Test
	public void indexOfTest() {
		ArrayIndexedCollection col = createNonEmptyCollection();
		
		assertEquals(3, col.indexOf(3));
		assertEquals(-1, col.indexOf(100));
	}
	
	@Test
	public void removeIndexExceptionTestTooBig() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ArrayIndexedCollection col = createNonEmptyCollection();
			col.remove(100);
		});;
	}
	
	@Test
	public void removeIndexExceptionTestNegative() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ArrayIndexedCollection col = createNonEmptyCollection();
			col.remove(-1);
		});;
	}
	
	@Test
	public void removeIndexTest() {
		ArrayIndexedCollection col = createNonEmptyCollection();
		
		assertEquals(0, col.get(0));
		
		col.remove(0);
		
		assertEquals(1, col.get(0));
		
		assertEquals(SIZE_OF_COLLECTION - 1, col.size());
	}
	
	@Test
	public void removeValueTest() {
		ArrayIndexedCollection col = createEmptyCollection();
		
		assertFalse(col.remove(Integer.valueOf(1)));
		
		col.add(Integer.valueOf(1));
		
		assertTrue(col.remove(Integer.valueOf(1)));
	}
	
	@Test
	public void isEmptyTest() {
		ArrayIndexedCollection col = createEmptyCollection();
		
		assertTrue(col.isEmpty());
		
		col.add(1);
		
		assertFalse(col.isEmpty());
	}
	
	@Test
	public void containsTest() {
		ArrayIndexedCollection col = createEmptyCollection();
		
		assertFalse(col.contains(1));
		
		col.add(1);
		
		assertTrue(col.contains(1));
	}
	
	@Test
	public void toArrayTest() {
		ArrayIndexedCollection col = createEmptyCollection();
		
		Object[] array1 = col.toArray();
		
		assertEquals(0, array1.length);
		
		col.add(1);
		col.add(2);

		Object[] array2 = col.toArray();
		
		assertEquals(2, array2.length);
		assertEquals(1, array2[0]);
		assertEquals(2, array2[1]);
	}

	@Test
	public void addAllTest() {
		ArrayIndexedCollection other = createNonEmptyCollection();
		ArrayIndexedCollection col = createEmptyCollection();
		
		assertEquals(0, col.size());
		
		col.addAll(other);
		
		assertEquals(SIZE_OF_COLLECTION, col.size());
		assertEquals(0, col.get(0));
	}
}
