package hr.fer.zemris.java.gui.calc.buttons;

import java.awt.Color;
import java.util.Objects;
import java.util.function.DoubleBinaryOperator;

import javax.swing.JButton;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

/**
 * {@link JButton} that represents binary operation. 
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class BinaryOperationButton {
	
	/** Button that is represented by this class. */
	private JButton operationButton;
	
	/**
	 * Creates new {@link BinaryOperationButton} with given {@link CalcModel}, name
	 * of operation and {@link DoubleBinaryOperator} operator.
	 * 
	 * @param calcModel model 
	 * @param name operation name
	 * @param operator operation
	 * @throws NullPointerException if given model or operation is null
	 */
	public BinaryOperationButton(CalcModel calcModel, String name, DoubleBinaryOperator operator) {
		Objects.requireNonNull(calcModel);
		Objects.requireNonNull(operator);

		this.operationButton = new JButton(Objects.requireNonNull(name));
		operationButton.setBackground(Color.LIGHT_GRAY);
		operationButton.setVisible(true);
		operationButton.addActionListener((e) -> {
			// calculate if there is pending operation and set it as value
			if(calcModel.isActiveOperandSet()) {
				double result = calcModel.getPendingBinaryOperation().applyAsDouble(
						calcModel.getActiveOperand(), calcModel.getValue());
				calcModel.setValue(result);
			}
			
			// set current value as operand and set pending operation
			calcModel.setActiveOperand(calcModel.getValue());
			calcModel.setPendingBinaryOperation(operator);
			calcModel.clear();
		});
	}

	/**
	 * Returns {@link JButton} represented by this class.
	 * 
	 * @return button
	 */
	public JButton getOperationButton() {
		return operationButton;
	}

}
