package hr.fer.zemris.java.gui.calc.buttons;

import java.awt.Color;

import javax.swing.JButton;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Contains static utility methods for buttons in 
 * {@link Calculator}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ButtonsUtil {
	
	/**
	 * Creates {@link JButton} with given text and boolean argument
	 * which indicated whether font on button is big or small.
	 * 
	 * @param name text of button
	 * @param bigFont is font big 
	 * @return created {@link JButton}
	 */
	public static JButton createButton(String name, boolean bigFont) {
		JButton button = new JButton(name);
		button.setBackground(Color.LIGHT_GRAY);
		button.setVisible(true);
		if(bigFont) {
			button.setFont(button.getFont().deriveFont(30f));
		}
		
		return button;
	}	

}
