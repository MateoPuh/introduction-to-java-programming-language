package hr.fer.zemris.java.gui.layouts;

/**
 * Exception which is thrown whenever error occurs in {@link CalcLayout}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class CalcLayoutException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates new CalcLayoutException.
	 */
	public CalcLayoutException() {
		
	}
	
	/**
	 * Creates new CalcLayoutException with a given message.
	 * 
	 * @param message exception message
	 */
	public CalcLayoutException(String message) {
		super(message);
	}
	
	/**
	 * Creates new CalcLayoutException with a given cause.
	 * 
	 * @param cause exception cause
	 */
	public CalcLayoutException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates new CalcLayoutException with a given message and cause.
	 * 
	 * @param message exception message
	 * @param cause exception cause
	 */
	public CalcLayoutException(String message, Throwable cause) {
		super(message, cause);
	}

}
