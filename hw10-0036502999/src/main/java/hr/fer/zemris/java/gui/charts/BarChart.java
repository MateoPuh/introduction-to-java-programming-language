package hr.fer.zemris.java.gui.charts;

import java.util.List;

/**
 * Represents bar chart that contains list of {@link XYValue}, description
 * of x values, description of y values, minimal y value, maximal y value, 
 * gap between y values.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class BarChart {
		
	private List<XYValue> values;
	private String xDesc;
	private String yDesc;
	private int yMin;
	private int yMax;
	private int gap;
	
	/**
	 * Creates new {@link BarChart} with given list of {@link XYValue}, description of x
	 * values, description of y values, minimal y, maximal y, and gap between y values.
	 * 
	 * @param values list of {@link XYValue}
	 * @param xDesc description of x values
	 * @param yDesc description of y values
	 * @param yMin minimal y
	 * @param yMax maximal y
	 * @param gap gap between y values
	 */
	public BarChart(List<XYValue> values, String xDesc, String yDesc, int yMin, int yMax, int gap) {
		if(yMin < 0) {
			throw new IllegalArgumentException("Minimal y should be non-negative.");
		}
		if(yMax <= yMin) {
			throw new IllegalArgumentException("Maximal y should be greater than minimal y.");
		}
				
		for(XYValue value : values) {
			if(value.getY() < yMin) {
				throw new IllegalArgumentException("Value (" + value.getX() + " ," + value.getY() 
				+ ") contains y property lesser than minimal y.");
			}
		}
		
		this.values = values;
		this.xDesc = xDesc;
		this.yDesc = yDesc;
		this.yMin = yMin;
		this.yMax = yMax;
		this.gap = gap;
	}

	/**
	 * Returns list of {@link XYValue}.
	 * 
	 * @return list of {@link XYValue}
	 */
	public List<XYValue> getValues() {
		return values;
	}

	/**
	 * Returns description of x values.
	 * 
	 * @return description of x values
	 */
	public String getxDesc() {
		return xDesc;
	}

	/**
	 * Returns description of y values.
	 * 
	 * @return description of y values
	 */
	public String getyDesc() {
		return yDesc;
	}

	/**
	 * Returns minimal value of y.
	 * 
	 * @return minimal y value
	 */
	public int getyMin() {
		return yMin;
	}

	/**
	 * Returns maximal y value.
	 * 
	 * @return maximal y value
	 */
	public int getyMax() {
		return yMax;
	}

	/**
	 * Returns gap between y values.
	 * 
	 * @return gap between y values
	 */
	public int getGap() {
		return gap;
	}

}
