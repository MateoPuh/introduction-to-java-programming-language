package hr.fer.zemris.java.gui.calc.buttons;

import java.awt.event.ActionListener;
import java.util.Objects;
import java.util.function.DoubleUnaryOperator;

import javax.swing.JButton;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

/**
 * {@link JButton} that represents unary operation. It contains
 * two names of operations and two operations, which can be switched
 * by providing boolean variable to {@link #setName(boolean)} and
 * {@link #setListener(boolean)} methods.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class UnaryOperationButton {
	
	/** Button that this class represents. */
	private JButton operationButton;
	/** First name. */
	private String name1;
	/** Second name. */
	private String name2;
	/** First action listener. */
	private ActionListener listener1;
	/** Second action listener. */
	private ActionListener listener2;
		
	/**
	 * Creates new {@link UnaryOperationButton} with given {@link CalcModel}, name of 
	 * first operation, first operator, name of second operation and second operator.
	 * 
	 * @param calcModel model that will contain this button
	 * @param name1 name of first operation
	 * @param operator1 first operation
	 * @param name2 name of second operation
	 * @param operator2 second operation
	 * @throws NullPointerException if given model or operators are null
	 */
	public UnaryOperationButton(CalcModel calcModel, String name1, DoubleUnaryOperator operator1, 
			String name2, DoubleUnaryOperator operator2) {
		Objects.requireNonNull(calcModel);
		
		this.name1 = name1;
		this.listener1 = (e) -> {
			calcModel.setValue(Objects.requireNonNull(operator1).applyAsDouble(calcModel.getValue()));
		};
		this.name2 = name2;
		this.listener2 = (e) -> {
			calcModel.setValue(Objects.requireNonNull(operator2).applyAsDouble(calcModel.getValue()));
		};
		
		this.operationButton = ButtonsUtil.createButton(Objects.requireNonNull(name1), false);
	}

	/**
	 * Returns {@link JButton} that this class represents.
	 * 
	 * @return button
	 */
	public JButton getOperationButton() {
		operationButton.setText(name1);
		operationButton.addActionListener(listener1);
				
		return operationButton;
	}
	
	/**
	 * Sets name of operation. If given boolean argument is false, 
	 * name of first operation is set, if argument is true, name of
	 * second operation is set.
	 * 
	 * @param inverted inverted
	 */
	public void setName(boolean inverted) {
		operationButton.setText(getName(inverted));
	}
	
	/**
	 * Sets action listener. If given boolean argument is false, 
	 * name of first operation is set, if argument is true, name of
	 * second operation is set.
	 * 
	 * @param inverted inverted
	 */
	public void setListener(boolean inverted) {
		ActionListener[] listeners = operationButton.getActionListeners();
		
		for(int i = 0; i < listeners.length; i++) {
			operationButton.removeActionListener(listeners[i]);
		}
		
		operationButton.addActionListener(getListener(inverted));
	}
	
	/**
	 * Returns name of operation. If given boolean argument is false, 
	 * name of first operation is returned, if argument is true, name of
	 * second operation is returned.
	 * 
	 * @param inverted inverted
	 * @return name of operation
	 */
	public String getName(boolean inverted) {
		return inverted ? name2 : name1;
	}
	
	/**
	 * Returns operation. If given boolean argument is false, 
	 * first operation is returned, if argument is true, second
	 * operation is returned.
	 * 
	 * @param inverted inverted
	 * @return operation
	 */
	public ActionListener getListener(boolean inverted) {
		return inverted ? listener2 : listener1;
	}

}
