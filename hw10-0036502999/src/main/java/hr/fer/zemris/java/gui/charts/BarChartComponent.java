package hr.fer.zemris.java.gui.charts;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.List;

import javax.swing.JComponent;

/**
 * {@link JComponent} of {@link BarChart} that creates graph
 * and paints it.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class BarChartComponent extends JComponent {

	private static final long serialVersionUID = 1L;

	/** Gap between description of x and values of x. */
	private static final int HORIZONTAL_DESCRIPTION_GAP = 25;
	/** Gap between description of y and values of y. */
	private static final int VERTICAL_DESCRIPTION_GAP = 5;
	/** Gap between values on axis and axis. */
	private static final int NUMBER_GAP = 5;
	/** Height of arrow. */
	private static final int ARROW_HEIGHT = 10;
	/** Width of arrow. */
	private static final int ARROW_WIDTH = 10;
	/** Gap between arrow and last bar. */
	private static final int HORIZONTAL_END_GAP = 5;
	/** Gap between arrow and last bar. */
	private static final int VERTICAL_END_GAP = 10;
	
	private int horizontalOffset;
	private int verticalOffset;
	private int xAxisOffset;
	private int yAxisOffset;
	private BarChart barChart;
	private Graphics2D g2d;
	private FontMetrics fm;
	
	/**
	 * Creates new {@link BarChartComponent} for given {@link BarChart}.
	 * 
	 * @param barChart bar chart
	 */
	public BarChartComponent(BarChart barChart) {
		this.barChart = barChart;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		g2d = (Graphics2D)g;
		g2d.setColor(Color.GRAY);
		
		AffineTransform defaultAt = g2d.getTransform();
		g2d.setFont(new Font(g2d.getFont().getFontName(), Font.PLAIN, 15)); 
		fm = g.getFontMetrics();

		// x description
		g2d.drawString(barChart.getxDesc(), 
				getWidth() / 2 - fm.stringWidth(barChart.getxDesc()) / 2, 
				getHeight() - fm.getDescent());
		
		// rotate
		AffineTransform at = new AffineTransform();
		at.rotate(-Math.PI / 2);
		g2d.setTransform(at);
		
		// y description
		g2d.drawString(barChart.getyDesc(), 
				-getHeight()/2 - fm.stringWidth(barChart.getyDesc()) / 2, 
				+fm.getAscent());
		
		// transform to default
		g2d.setTransform(defaultAt);
		
		horizontalOffset = fm.getHeight() + HORIZONTAL_DESCRIPTION_GAP;
	    verticalOffset = fm.getHeight() + VERTICAL_DESCRIPTION_GAP;
		
		drawAxes();
		drawGraph();
		
		super.paintComponent(g);
	}
	
	/**
	 * Draws x and y axis.
	 */
	private void drawAxes() {
		g2d.setColor(Color.GRAY);
	    g2d.setStroke(new BasicStroke(1.5f));

	    xAxisOffset = fm.getHeight() + horizontalOffset + NUMBER_GAP;
	    yAxisOffset = fm.getHeight() + verticalOffset + NUMBER_GAP;
	    
	    // x axis
		g2d.drawLine(yAxisOffset, 
				getHeight() - xAxisOffset, 
				getWidth(), 
				getHeight() - xAxisOffset);

		// arrow head
		int arrowHead1X0 = getWidth() - ARROW_HEIGHT;
		int arrowHead1Y0 = getHeight() - xAxisOffset - ARROW_WIDTH/2;
		
		g2d.fillPolygon(new int[]{arrowHead1X0, arrowHead1X0, getWidth()}, 
				new int[] {arrowHead1Y0, arrowHead1Y0 + ARROW_WIDTH, arrowHead1Y0 + ARROW_WIDTH/2}, 3);

		// y axis
		g2d.drawLine(xAxisOffset, 0, xAxisOffset, getHeight() - yAxisOffset);
		
		// arrow head
		int arrowHead2X0 = xAxisOffset - ARROW_WIDTH/2;
		int arrowHead2Y0 = 0;
		
		g2d.fillPolygon(new int[]{arrowHead2X0, arrowHead2X0 + ARROW_WIDTH/2, arrowHead2X0 + ARROW_WIDTH}, 
				new int[] {arrowHead2Y0 + ARROW_HEIGHT, arrowHead2Y0, arrowHead2Y0 + ARROW_HEIGHT},						
				3);
	}
	
	/**
	 * Draws grid, values and bar graphs.
	 */
	private void drawGraph() {
		g2d.setStroke(new BasicStroke(1.5f));
	    
	    List<XYValue> values = barChart.getValues();

	    // last x on x axis
	 	int xEnd = getWidth() - ARROW_WIDTH - HORIZONTAL_END_GAP;	 	
	 	// width of one graph division
	 	double xWidth = ((double)(xEnd - xAxisOffset))/values.size();
	 	double x = xAxisOffset;

	 	// last x on x axis
		int yEnd = getHeight() - ARROW_WIDTH - VERTICAL_END_GAP;
			
		// number of y values
		int yNum = (int)Math.ceil(((double)(barChart.getyMax() - barChart.getyMin()))/barChart.getGap());
			
		// width of one graph division
		double yWidth = (yEnd - xAxisOffset)/(double)yNum;
			
		double y = getHeight() - xAxisOffset;
		
		for(int i = 0; i < values.size(); i++) {
			x += xWidth/2;
			g2d.setColor(Color.BLACK);
			
			String value = String.valueOf(values.get(i).getX());
			
			g2d.drawString(value, (int)x - fm.stringWidth(value) / 2, 
					getHeight() - horizontalOffset);
			
			x += xWidth/2;

			g2d.setColor(Color.LIGHT_GRAY);
			g2d.drawLine((int)x, 0, (int)x, getHeight() - yAxisOffset);			
		}
		
		for(int i = 0; i <= yNum; i++) {
			g2d.setColor(Color.BLACK);
			String value = String.valueOf(barChart.getyMin() + i * barChart.getGap());
			g2d.drawString(value, verticalOffset - fm.stringWidth(value) / 2, 
					(int)y + fm.getDescent()/2);
			y -= yWidth;
			if(i == yNum) {
				break;
			}
			g2d.setColor(Color.LIGHT_GRAY);
			g2d.drawLine(yAxisOffset, (int)y, getWidth(), (int)y);			
		}
		
		x = xAxisOffset;

		for(int i = 0; i < values.size(); i++) {
			int yValue = values.get(i).getY();
			
			double height = (int)((yValue - barChart.getyMin())*yWidth / barChart.getGap());
			drawBar(x + 1, height, xWidth);
			x += xWidth;
		}	
	}
	
	/**
	 * Draws single bar.
	 * 
	 * @param x x coordinate
	 * @param height height of bar
	 * @param xWidth width of bar
	 */
	private void drawBar(double x, double height, double xWidth) {
		g2d.setColor(Color.ORANGE);
		g2d.fillRect((int)x, (int)(getHeight() - xAxisOffset - height), (int)xWidth - 1, (int)(height));			
	}
	
}
