package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

/**
 * Program that opens two lists of prime numbers {@link PrimListModelTest}.
 * and {@link JButton} that generates prime number.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class PrimDemo extends JFrame {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates new {@link PrimDemo}.
	 */
	public PrimDemo() {
		initGUI();	    
	}
	
	/**
	 * Initializes content of {@link PrimDemo}.
	 */
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		
		PrimListModel model = new PrimListModel();
	    JList<Integer> list1 = new JList<Integer>(model);
	    JList<Integer> list2 = new JList<Integer>(model);
		
		JButton button = new JButton("next");
	    button.addActionListener((e) -> {
	    	model.next();
	    });
			    
		JPanel listPanel = new JPanel(new GridLayout(1, 0));
		
		listPanel.add(new JScrollPane(list1));
		listPanel.add(new JScrollPane(list2));
	    
	    cp.add(listPanel, BorderLayout.CENTER);
		cp.add(button, BorderLayout.PAGE_END);
	}
	
	/**
	 * This method is first run after program is started.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			PrimDemo p = new PrimDemo();
		    p.setSize(260, 200);
			p.setVisible(true);
		});	
	}

}
