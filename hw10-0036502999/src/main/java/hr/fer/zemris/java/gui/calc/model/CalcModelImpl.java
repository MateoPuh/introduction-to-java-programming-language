package hr.fer.zemris.java.gui.calc.model;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.DoubleBinaryOperator;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * {@link CalcModel} implementation that models {@link Calculator}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class CalcModelImpl implements CalcModel {

	private boolean isEditable;
	private boolean isNegative;
	private String value;
	private double numericValue;
	private Double activeOperand;
	private DoubleBinaryOperator pendingOperation;
	
	List<CalcValueListener> listeners;
	
	/**
	 * Creates {@link CalcModelImpl}
	 */
	public CalcModelImpl() {
		init();
		
		listeners = new CopyOnWriteArrayList<CalcValueListener>();
	}
	
	@Override
	public void addCalcValueListener(CalcValueListener l) {
		listeners.add(l);
	}

	@Override
	public void removeCalcValueListener(CalcValueListener l) {
		listeners.remove(l);
	}

	@Override
	public double getValue() {
		return numericValue * (isNegative ? -1 : 1);
	}

	@Override
	public void setValue(double value) {		
		this.numericValue = value;
		this.value = String.valueOf(value);
		this.isEditable = false;
		
		notifiyAllListeners();
	}

	@Override
	public boolean isEditable() {
		return isEditable;
	}

	@Override
	public void clear() {
		init();
		
		notifiyAllListeners();
	}

	private void init() {
		this.value = "";
		this.numericValue = 0;
		this.isEditable = true;
		this.isNegative = false;
	}
	
	@Override
	public void clearAll() {
		clear();
		this.activeOperand = null;
		this.pendingOperation = null;	
		
		notifiyAllListeners();
	}

	@Override
	public void swapSign() throws CalculatorInputException {
		if(!isEditable) {
			throw new CalculatorInputException("Model is not editable!");
		}
		
		isNegative = !isNegative;
		
		notifiyAllListeners();
	}

	@Override
	public void insertDecimalPoint() throws CalculatorInputException {
		if(!isEditable) {
			throw new CalculatorInputException("Model is not editable!");
		}
		
		if(value.contains(".")) {
			throw new CalculatorInputException("Decimal point already exist!");
		}
		
		if(value.isEmpty()) {
			throw new CalculatorInputException("Decimal point cannot be added when there is no numer!");
		}
		
		
		value += ".";
		notifiyAllListeners();
	}

	@Override
	public void insertDigit(int digit) throws CalculatorInputException, IllegalArgumentException {
		if(!isEditable) {
			throw new CalculatorInputException("Model not editable!");
		}
		
		String newValue = "";
		
		if(!value.equals("0")) {
			newValue += value;
		}
		
		newValue += String.valueOf(digit);
		
		try {
			numericValue = Double.parseDouble(newValue);
			value = newValue;
			
			notifiyAllListeners();
		} catch (NumberFormatException e) {
			throw new CalculatorInputException("Invalid number format!");
		}
	}

	@Override
	public boolean isActiveOperandSet() {
		return activeOperand != null;
	}

	@Override
	public double getActiveOperand() throws IllegalStateException {
		if(activeOperand == null) {
			throw new IllegalStateException("Active operand is not set!");
		}
		
		return activeOperand.doubleValue();
	}

	@Override
	public void setActiveOperand(double activeOperand) {
		this.activeOperand = activeOperand;	
		
		notifiyAllListeners();
	}

	@Override
	public void clearActiveOperand() {
		this.activeOperand = null;	
		
		notifiyAllListeners();
	}

	@Override
	public DoubleBinaryOperator getPendingBinaryOperation() {
		return pendingOperation;
	}

	@Override
	public void setPendingBinaryOperation(DoubleBinaryOperator op) {
		this.pendingOperation = op;
		
		notifiyAllListeners();
	}
	
	@Override
	public String toString() {
		String result = "";
		
		String absValue = value;
		if(value.startsWith("-")) {
			absValue = value.substring(1);
		}
		
		double resultValue = numericValue;
		//resultValue *= isNegative? -1 : 1;
		
		if(resultValue < 0) {
			result += isNegative ? "" : "-";
			result += value.isEmpty() ? "0" : absValue;
		} else {
			result += isNegative ? "-" : "";
			result += value.isEmpty() ? "0" : absValue;
		}
		
		return result;
	}
	
	private void notifiyAllListeners() {
		for(CalcValueListener listener : listeners) {
			listener.valueChanged(this);
		}
	}

}
