package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * {@link ListModel} that models list of prime numbers.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class PrimListModel implements ListModel<Integer> {

	private List<Integer> primNumbers;
	private int last;
	private List<ListDataListener> listeners;

	/**
	 * Creates new {@link PrimListModelTest} and initializes it's content
	 * as list that contains first prime number (1).
	 */
	public PrimListModel() {
		last = 1;
		primNumbers = new ArrayList<Integer>();
		primNumbers.add(last);
		listeners = new CopyOnWriteArrayList<ListDataListener>();
	}
	
	/**
	 * Calculates next prime number.
	 * 
	 * @return next prime number
	 */
	public int next() {
		int next = last + 1;
		
		while(true) {
			if(isPrime(next)) {
				last = next;
				primNumbers.add(last);
				notifyListeners();
				
				return next;
			}
			
			next++;
		}
	}
	
	/**
	 * Notifies all listeners about change in content of this list.
	 */
	private void notifyListeners() {
		ListDataEvent le = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, getSize());
		for (int i = 0; i < listeners.size(); i++) {
			((ListDataListener) listeners.get(i)).contentsChanged(le);
		}
	}
	
	/**
	 * Returns true if given number is prime number.
	 * 
	 * @param number number to check
	 * @return true if given number is prime; false otherwise
	 */
	private boolean isPrime(int number) {
	    for(int i = 2; i < number; i++) {
	        if(number%i == 0)
	            return false;
	    }
	    
	    return true;
	}

	@Override
	public int getSize() {
		return primNumbers.size();
	}

	@Override
	public Integer getElementAt(int index) {
		return primNumbers.get(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		listeners.add(l);
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		listeners.remove(l);
	}

}
