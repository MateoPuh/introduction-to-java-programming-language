package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Program that takes one command line argument: path to file.
 * File should contain (each in it's own line): description of x,
 * description of y, list of values, minimal y, maximal y, gap
 * between y values.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class BarChartDemo extends JFrame {

	private static final long serialVersionUID = 1L;

	private BarChart model;
	private String path;
	
	/**
	 * Creates new {@link BarChartDemo} for given {@link BarChart} model.
	 * 
	 * @param model bar chart model
	 */
	public BarChartDemo(BarChart model, String path) {
		setLocation(20, 50);
		setSize(700, 500);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		
		this.model = model;
		this.path = path;
		
		initGUI();
	}

	/**
	 * Initializes the content on {@link BarChartDemo}.
	 */
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());

		JLabel label = new JLabel(path);
		label.setBackground(Color.GREEN);
		label.setOpaque(true);
		
		cp.add(label, BorderLayout.NORTH);
		cp.add(new BarChartComponent(model), BorderLayout.CENTER);
	}
	
	/**
	 * Reads first numLines of lines from a file.
	 * 
	 * @param path path of file
	 * @param numLines number of lines
	 * @return first n lines of file
	 * @throws IOException if error occurs
	 */
	private static List<String> readFirst(Path path, int numLines) throws IOException {
	    try (final Stream<String> lines = Files.lines(path)) {
	        return lines.limit(numLines).collect(Collectors.toList());
	    }
	}
	
	/**
	 * This method is first run after program is run. It takes one argument: 
	 * path to file, and reads first 6 lines and creates {@link BarChartDemo}
	 * based on read information.
	 * 
	 * @param args command line argument
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("Pogrešan broj argumenata.");
			System.exit(1);
		}
		
		try {
			List<String> lines = readFirst(Paths.get(args[0]), 6);
			List<XYValue> values = new ArrayList<XYValue>();
			
			String[] splitted = lines.get(2).split(" ");
			
			for(String value : splitted) {
				String[] splittedValues = value.split(",");
				
				if(splittedValues.length != 2) {
					System.out.println("Invalid argument: " + value);
					System.exit(1);
				}
				
				values.add(new XYValue(Integer.parseInt(splittedValues[0]), Integer.parseInt(splittedValues[1])));
			}				
			
			BarChart barChart = new BarChart(values, lines.get(0), lines.get(1),
						Integer.parseInt(lines.get(3)), Integer.parseInt(lines.get(4)), 
						Integer.parseInt(lines.get(5)));
			
			SwingUtilities.invokeLater(() -> {
				JFrame frame = new BarChartDemo(barChart, args[0]);
				frame.setVisible(true);
			});
		} catch(IOException | NumberFormatException e) {
			System.out.println("Error occured:" + e.getLocalizedMessage());
			System.exit(1);
		}
	}
}