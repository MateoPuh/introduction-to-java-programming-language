package hr.fer.zemris.java.gui.layouts;

import java.util.Objects;

/**
 * Represents constraint for {@link CalcLayout} class. It contains
 * two read-only integer properties: row and column, which represent
 * row and column of position.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class RCPosition {
	
	private int row;
	private int column;
	
	/**
	 * Creates new {@link RCPosition} with given row and column
	 * property.
	 * 
	 * @param row row
	 * @param column column
	 */
	public RCPosition(int row, int column) {
		this.row = row;
		this.column = column;
	}

	/**
	 * Returns row of {@link RCPosition}.
	 * 
	 * @return row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Returns column of {@link RCPosition}.
	 * 
	 * @return column
	 */
	public int getColumn() {
		return column;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(column, row);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof RCPosition))
			return false;
		RCPosition other = (RCPosition) obj;
		return column == other.column && row == other.row;
	}
	
}
