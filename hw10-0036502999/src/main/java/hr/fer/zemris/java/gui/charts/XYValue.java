package hr.fer.zemris.java.gui.charts;

import java.util.Objects;

/**
 * Represents one bar in {@link BarChart}. It contains two read-only
 * propetries: x and y value.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class XYValue {

	private int x;
	private int y;
	
	/**
	 * Creates new {@link XYValue} with given x and y.
	 * 
	 * @param x x value
	 * @param y y value
	 */
	public XYValue(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Returns x value.
	 * 
	 * @return x value
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Returns y value.
	 * 
	 * @return y value
	 */
	public int getY() {
		return y;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof XYValue))
			return false;
		XYValue other = (XYValue) obj;
		return x == other.x && y == other.y;
	}
	
}
