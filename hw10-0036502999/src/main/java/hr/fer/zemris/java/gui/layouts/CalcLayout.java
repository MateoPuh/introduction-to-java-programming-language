package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.util.HashMap;
import java.util.Map;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Layout manager for {@link Calculator}. It contains 5 rows and 
 * 7 columns. Numeration of rows and columns start at 1. It has
 * 31 components. Component added on position (1, 1) also covers
 * positions (1, 2) to (1, 5).
 * 
 * <p>If any error occurs, {@link CalcLayoutException} is thrown.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class CalcLayout  implements java.awt.LayoutManager2{

	/** Number of rows. */
	private static final int ROWS = 5;
	/** Number of columns. */
	private static final int COLUMNS = 7;
	/** Width of first component. */
	private static final int WIDTH_OF_FIRST = 5;
	/** Default padding between components. */
	private static final int DEFAULT_GAP = 0;
	
	private int gap;
	private double minWidth;
	private double minHeight;
	private double maxWidth;
	private double maxHeight;
	private double preferredWidth;
	private double preferredHeight;
	private HashMap<Component, RCPosition> constraintsMap;
	
	/**
	 * Creates new {@link CalcLayout} with given gap between components.
	 * 
	 * @param gap gap between components
	 */
	public CalcLayout(int gap) {
		this.gap = gap;
		this.constraintsMap = new HashMap<Component, RCPosition>();
	}
	
	/**
	 * Creates new {@link CalcLayout} with no gap between components.
	 */
	public CalcLayout() {
		this(DEFAULT_GAP);
	}
	
	@Override
	public void addLayoutComponent(String name, Component comp) {
		throw new UnsupportedOperationException("Method is not supported.");
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		constraintsMap.remove(comp);
	}

	/**
	 * Sets minimal, maximal and preferred sizes for width and height.
	 * 
	 * @param parent container with components
	 */
	private void setSizes(Container parent) {
        int nComps = parent.getComponentCount();
        
        preferredWidth = 0;
        preferredHeight = 0;
        minWidth = 0;
        minHeight = 0;
        maxWidth = Integer.MAX_VALUE;
        maxHeight = Integer.MAX_VALUE;
        
        double maxPreferredWidth = 0;
        double maxPreferredHeight = 0;

        for (int i = 0; i < nComps; i++) {
            Component c = parent.getComponent(i);
            RCPosition pos = constraintsMap.get(c);
            
            if (c.isVisible()) {
                maxPreferredWidth = Math.max(c.getPreferredSize().width, maxPreferredWidth);
                maxPreferredHeight = Math.max(c.getPreferredSize().height, maxPreferredHeight);
                
                minWidth = Math.max(c.getMinimumSize().width, minWidth);
                minHeight = Math.max(c.getMinimumSize().height, minHeight);
                
                maxWidth = Math.min(c.getMaximumSize().width, maxWidth);
                maxHeight = Math.min(c.getMaximumSize().height, maxHeight);
                
                if(pos.getColumn() == 1 && pos.getRow() == 1) {
                	maxPreferredWidth = transformFromFirst(maxPreferredWidth);
                	maxPreferredHeight = transformFromFirst(maxPreferredHeight);
            		minWidth = transformFromFirst(minWidth);
            		minHeight = transformFromFirst(minHeight);
            		maxWidth = transformFromFirst(maxWidth);
            		maxHeight = transformFromFirst(maxHeight);
            	}
            }
        }
        
        preferredWidth = maxPreferredWidth * COLUMNS + gap * (COLUMNS - 1);
        preferredHeight = maxPreferredHeight * ROWS + gap * (ROWS - 1);
    }
	
	private double transformFromFirst(double number) {
		number -= (WIDTH_OF_FIRST - 1) * gap;
		number /= WIDTH_OF_FIRST;
		return number;
	}
	
	@Override
	public Dimension preferredLayoutSize(Container parent) {
		setSizes(parent);
        Insets insets = parent.getInsets();

		return new Dimension((int)(preferredWidth + insets.right + insets.left),
				(int)(preferredHeight + insets.bottom + insets.top));
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		setSizes(parent);
        Insets insets = parent.getInsets();

		return new Dimension((int)(minWidth + insets.right + insets.left),
				(int)(minHeight + insets.bottom + insets.top));
	}

	@Override
	public void layoutContainer(Container parent) {
        int nComps = parent.getComponentCount();
        Insets insets = parent.getInsets(); 
        Dimension size = parent.getSize();
        
        int totalWidth = size.width - insets.left - insets.right;
        int totalHeight = size.height - insets.bottom -insets.top;
        
        double componentWidth = (totalWidth - (COLUMNS - 1) * gap)/(double)COLUMNS;
        double componentHeight = (totalHeight - (ROWS - 1) * gap)/(double)ROWS;
        
        for (int i = 0; i < nComps; i++) {
        	Component c = parent.getComponent(i);
        	if (c.isVisible()) {
                RCPosition pos = constraintsMap.get(c);    
                
                if(pos.getColumn() == 1 && pos.getRow() == 1) {
                	c.setBounds(insets.left, insets.top, 
                			(int)componentWidth * WIDTH_OF_FIRST + gap * (WIDTH_OF_FIRST - 1), 
                			(int)componentHeight);
                    continue;
                }
                
                c.setBounds((pos.getColumn() - 1) * (gap + (int)componentWidth) + insets.left, 
                		(pos.getRow() - 1) * (gap + (int)componentHeight) + insets.top, 
                		(int)componentWidth, 
                		(int)componentHeight);
        	}
        }
		
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {		
		if(constraints == null) {
			return;
		}
		if(!(constraints instanceof RCPosition)) {
			throw new UnsupportedOperationException("Invalid constraint type. It should be RCPosition.");
		}
				
	    RCPosition rcPosition = (RCPosition) constraints;
	      
	    if(rcPosition.getRow() < 1 
	    		|| rcPosition.getColumn() < 1
	    		|| rcPosition.getRow() > ROWS
	    		|| rcPosition.getColumn() > COLUMNS) {
	    	throw new CalcLayoutException("Constraint (r,s): "
	    			+ "r should be between 1 and 5 and s should be between 1 and 7.");
	    }
	      
	    if(rcPosition.getRow() == 1 
	    		&& rcPosition.getColumn() > 1 
	    		&& rcPosition.getColumn() < COLUMNS - 1) {
	    	throw new CalcLayoutException("Attempting to use forbidden constraints: (1,2) to (1,5).");
	    }	
	    
	    if(positionExists(rcPosition)) {
	    	throw new CalcLayoutException("Constraint already added.");
	    }
	    
	    constraintsMap.put(comp, rcPosition);
	}

	private boolean positionExists(RCPosition rcPosition) {
		for(Map.Entry<Component, RCPosition> entry : constraintsMap.entrySet()) {
			if(entry.getValue().equals(rcPosition)) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public Dimension maximumLayoutSize(Container target) {
		setSizes(target);
        Insets insets = target.getInsets();

		return new Dimension((int)(maxWidth + insets.right + insets.left),
				(int)(maxHeight + insets.bottom + insets.top));
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0;
	}

	@Override
	public void invalidateLayout(Container target) {
	}

}
