package hr.fer.zemris.java.gui.layouts.demo;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

public class Demo1 extends JFrame {

	private static final long serialVersionUID = 1L;

	public Demo1() {
		super();
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Prozor1");
		setLocation(20, 20);
		setSize(500, 200);
		initGUI();
		
	}
	
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		
		JPanel p = new JPanel(new CalcLayout(3));
		
//		p.add(new JLabel("x"), "1,1");
//		p.add(new JLabel("y"), "2,3");
//		p.add(new JLabel("z"), "2,7");
//		p.add(new JLabel("w"), "4,2");
//		p.add(new JLabel("a"), "4,5");
//		p.add(new JLabel("b"), "4,7");

		p.add(new JLabel("x"), new RCPosition(1,1));
		p.add(new JLabel("y"), new RCPosition(2,3));
		p.add(new JLabel("z"), new RCPosition(2,7));
		p.add(new JLabel("w"), new RCPosition(4,2));
		p.add(new JLabel("a"), new RCPosition(4,5));
		p.add(new JLabel("b"), new RCPosition(4,7));
		
		cp.add(p, BorderLayout.PAGE_END);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Demo1 prozor = new Demo1();
				prozor.setVisible(true);
			}
		});
	}
}


