package hr.fer.zemris.java.gui.calc.display;

import javax.swing.JLabel;

import hr.fer.zemris.java.gui.calc.Calculator;
import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalcValueListener;

/**
 * Represents {@link CalcValueListener} for Display of 
 * {@link Calculator}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class DisplayListener implements CalcValueListener {

	private JLabel display;
	
	/**
	 * Creates {@link DisplayListener} for given display.
	 * 
	 * @param display display whose listener is created
	 */
	public DisplayListener(JLabel display) {
		this.display = display;
	}
	
	@Override
	public void valueChanged(CalcModel model) {
		display.setText(model.toString());		
	}

}
