package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.calc.buttons.BinaryOperationButton;
import hr.fer.zemris.java.gui.calc.buttons.ButtonsUtil;
import hr.fer.zemris.java.gui.calc.buttons.DigitButton;
import hr.fer.zemris.java.gui.calc.buttons.UnaryOperationButton;
import hr.fer.zemris.java.gui.calc.display.DisplayListener;
import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalcModelImpl;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * {@link JFrame} that represents Calculator. It uses {@link CalcLayout}
 * layout as component layout manager. It contains buttons with unary
 * operator button, digit buttons and binary operator buttons.
 *  
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Calculator extends JFrame {

	private static final long serialVersionUID = 1L;

	private CalcModel calcModel;
	private Container cp;
	private boolean inverted;
	private Stack<Double> stack;
	private List<UnaryOperationButton> unaryOperationButtons;
	
	/**
	 * Creates new {@link Calculator}.
	 */
	public Calculator() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		calcModel = new CalcModelImpl();
		stack = new Stack<Double>();
		unaryOperationButtons = new ArrayList<UnaryOperationButton>();
		inverted = false;
		initGUI();
		pack();
	}
	
	/**
	 * Initializes {@link Calculator}. Creates all buttons, display and checkbox
	 * for inverting unary operators.
	 */
	private void initGUI() {
		cp = getContentPane();
			
		JLabel display = new JLabel(calcModel.toString(), SwingConstants.RIGHT);
		display.setFont(display.getFont().deriveFont(30f));
		display.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		display.setBackground(Color.YELLOW);
		display.setOpaque(true);
		
		DisplayListener displayListener = new DisplayListener(display);
		calcModel.addCalcValueListener(displayListener);
		
		JCheckBox inv = new JCheckBox("Inv");
		inv.setOpaque(true);
		inv.addActionListener((e) -> {
			inverted = !inverted;
			updateUnary();
		});
		
		cp.setLayout(new CalcLayout(3));
		cp.add(display, new RCPosition(1,1));
		cp.add(inv, new RCPosition(5, 7));
		
		setDigitButtons();
		setUnaryFunctions();
		setBinaryFunctions();
		setButtons();
	}

	/**
	 * Sets digit buttons.
	 */
	private void setDigitButtons() {
		int digit = 1;
		
		for(int i = 2; i <= 5; i++) {
			for(int j = 3; j <= 5; j++) {
				if(i == 5 && j == 4) {
					return;
				}
				
				DigitButton digitButton = new DigitButton(calcModel, digit++%10);
				cp.add(digitButton.getDigitButton(), new RCPosition(i, j));
			}
		}
	}
	
	/**
	 * Sets 1/x, sin, log, cos, ln, tan and ctg buttons.
	 */
	private void setUnaryFunctions() {		
		UnaryOperationButton oneDividedByX = 
				new UnaryOperationButton(calcModel, "1/x", (x) -> {return 1/x;},
						"1/x", (x) -> {return 1/x;}); 
		UnaryOperationButton sin = 
				new UnaryOperationButton(calcModel, "sin", Math::sin,
						"arcsin", Math::asin); 
		UnaryOperationButton log = 
				new UnaryOperationButton(calcModel, "log", Math::log10,
						"10^x", (x) -> {return Math.pow(10, x);}); 
		UnaryOperationButton cos = 
				new UnaryOperationButton(calcModel, "cos", Math::cos,
						"arccos", Math::acos); 
		UnaryOperationButton ln = 
				new UnaryOperationButton(calcModel, "ln", Math::log,
						"e^x", Math::exp); 
		UnaryOperationButton tan = 
				new UnaryOperationButton(calcModel, "tan", Math::tan,
						"arctan", Math::atan); 
		UnaryOperationButton ctg = 
				new UnaryOperationButton(calcModel, "ctg", (x) -> {return 1/Math.tan(x);},
						"arcctg", (x) -> {return Math.PI / 2 - Math.atan(x);}); 
				
		unaryOperationButtons.add(oneDividedByX);
		unaryOperationButtons.add(sin);
		unaryOperationButtons.add(log);
		unaryOperationButtons.add(cos);
		unaryOperationButtons.add(ln);
		unaryOperationButtons.add(tan);
		unaryOperationButtons.add(ctg);

		int cnt = 0;
		for(int i = 2; i <= 5; i++) {
			for(int j = 1; j <= 2; j++) {
				if(i == 5 && j == 1) {
					continue;
				}
				cp.add(unaryOperationButtons.get(cnt++).getOperationButton(), new RCPosition(i, j));
			}
		}
	}
		
	/**
	 * Updates values on unary operator buttons.
	 */
	private void updateUnary() {
		for(UnaryOperationButton button : unaryOperationButtons) {
			button.setName(inverted);
			button.setListener(inverted);
		}
	}
	
	/**
	 * Sets power ("x^n"), divide ("/"), multiply ("*"), subtract ("-") and 
	 * add ("+") button.
	 */
	private void setBinaryFunctions() {
		BinaryOperationButton xOnN = 
				new BinaryOperationButton(calcModel, "x^n", Math::pow);
		BinaryOperationButton divide = 
				new BinaryOperationButton(calcModel, "/", (x, y) -> {return x/y;});
		BinaryOperationButton multiply = 
				new BinaryOperationButton(calcModel, "*", (x, y) -> {return x*y;});
		BinaryOperationButton subtract = 
				new BinaryOperationButton(calcModel, "-", (x, y) -> {return x-y;});
		BinaryOperationButton add = 
				new BinaryOperationButton(calcModel, "+", (x, y) -> {return x+y;});
		
		cp.add(xOnN.getOperationButton(), new RCPosition(5, 1));
		cp.add(divide.getOperationButton(), new RCPosition(2, 6));
		cp.add(multiply.getOperationButton(), new RCPosition(3, 6));
		cp.add(subtract.getOperationButton(), new RCPosition(4, 6));
		cp.add(add.getOperationButton(), new RCPosition(5, 6));
	}
	
	/**
	 * Sets equals ("="), decimal point ("."), swap sign ("+/-"), clear ("clr"),
	 * reset ("reset"), push ("push") and pop ("pop") buttons. 
	 */
	private void setButtons() {
		JButton equals = ButtonsUtil.createButton("=", false);
		equals.addActionListener((e) -> {
			double result = calcModel.getPendingBinaryOperation().applyAsDouble(
					calcModel.getActiveOperand(), calcModel.getValue());
			calcModel.setValue(result);
		});
		
		JButton decimalPoint = ButtonsUtil.createButton(".", false);
		decimalPoint.addActionListener((e) -> {
			calcModel.insertDecimalPoint();
		});
		
		JButton swapSign = ButtonsUtil.createButton("+/-", false);
		swapSign.addActionListener((e) -> {
			calcModel.swapSign();
		});
		
		JButton clr = ButtonsUtil.createButton("clr", false);
		clr.addActionListener((e) -> {
			calcModel.clear();
		});
		
		JButton res = ButtonsUtil.createButton("reset", false);
		res.addActionListener((e) -> {
			calcModel.clearAll();
		});
		
		JButton push = ButtonsUtil.createButton("push", false);
		push.addActionListener((e) -> {
			stack.add(calcModel.getValue());
		});
		
		JButton pop = ButtonsUtil.createButton("pop", false);
		pop.addActionListener((e) -> {
			calcModel.setValue(stack.pop());
		});
		
		cp.add(equals, new RCPosition(1, 6));
		cp.add(decimalPoint, new RCPosition(5, 5));
		cp.add(swapSign, new RCPosition(5, 4));
		cp.add(clr, new RCPosition(1, 7));
		cp.add(res, new RCPosition(2, 7));
		cp.add(push, new RCPosition(3, 7));
		cp.add(pop, new RCPosition(4, 7));
	}
	
	/**
	 * This method is first run after program is started. It open
	 * {@link Calculator}.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new Calculator().setVisible(true);
		});	
	}
		
}