package hr.fer.zemris.java.gui.calc.buttons;

import java.util.Objects;

import javax.swing.JButton;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

/**
 * {@link JButton} that represent digit between 0 and 9.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class DigitButton {
	
	/** Minimal digit. */
	private static int MIN_NUM = 0;
	/** Maximal digit. */
	private static int MAX_NUM = 9;
	
	/** Button that is represented by this class. */
	private JButton digitButton;
	
	/**
	 * Creates new {@link DigitButton} with given {@link CalcModel} and digit.
	 * 
	 * @param calcModel {@link CalcModel} which will contain this button
	 * @param digit digit that button will represent
	 * @throws IllegalArgumentException if given digit is not in [0, 9]
	 */
	public DigitButton(CalcModel calcModel, int digit) throws IllegalArgumentException {
		if(digit < MIN_NUM && digit > MAX_NUM) {
			throw new IllegalArgumentException("Digit should be between 0 and 9.");
		}
		
		Objects.requireNonNull(calcModel);
		
		this.digitButton = ButtonsUtil.createButton(String.valueOf(digit), true);
		digitButton.addActionListener((e) -> {
			calcModel.insertDigit(digit);
		});
	}

	/**
	 * Returns {@link JButton} that is represented by this class.
	 * 
	 * @return button
	 */
	public JButton getDigitButton() {
		return digitButton;
	}

}
