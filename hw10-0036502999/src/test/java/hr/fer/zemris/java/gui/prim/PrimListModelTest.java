package hr.fer.zemris.java.gui.prim;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PrimListModelTest {
	
	@Test
	public void testNext() {
		PrimListModel model = new PrimListModel();
		
		assertEquals(2, model.next());
		assertEquals(3, model.next());
		assertEquals(5, model.next());
		assertEquals(7, model.next());
	}

}
