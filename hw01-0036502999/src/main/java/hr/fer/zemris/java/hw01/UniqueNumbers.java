package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * Program used for working with ordered binary trees. It is possible to add node, 
 * see if value is already in ordered binary tree, find out the size of tree and print
 * the content of the tree.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class UniqueNumbers {
	
	/**
	 * Value that denotes the end of interaction with user.
	 */
	public static final String KRAJ = "kraj"; 
	
	/**
	 * Class that represents node in ordered binary tree.
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 */
	static class TreeNode {
		TreeNode left;
		TreeNode right;
		int value;
	}
	
	/**
	 * This is the main method which asks user to enter arguments for 
	 * addNode method. It alse makes use of containsValue and treeSize methods, 
	 * as well as printTree method.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		TreeNode head = null;
		
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.print("Unesite broj > ");
			
			if (sc.hasNextInt()) {
				int number = Integer.parseInt(sc.next());	
				
				if (!containsValue(head, number)) {
					head = addNode(head, number);
					System.out.println("Dodano.");
				} else {
					System.out.println("Broj već postoji. Preskačem.");	
				}
				
			} else {
				String next = sc.next();
				
				if (next.trim().equals(KRAJ)) {
					sc.close();
				
					System.out.print("Ispis od najmanjeg: ");
					printTree(head, true);
					System.out.println();
					System.out.print("Ispis od najveceg: ");
					printTree(head, false);
					
					break;
				}
				
				System.out.println(next + " nije cijeli broj.");
			}	
		} while(true);
	}
	
	/**
	 * This method is used for adding node with given value in ordered binary tree 
	 * defined by given head node.
	 * 
	 * @param head head node of ordered binary tree
	 * @param value value of node which is added to ordered binary tree
	 * @return head node
	 */
	public static TreeNode addNode(TreeNode head, int value) {
		
		if (head == null) {
			head = new TreeNode();
			head.value = value;
			
			return head;
		}
		
		if (value < head.value) {
			
			if (head.left == null) {
				head.left = new TreeNode();
				head.left.value = value;
			} else {
				addNode(head.left, value);
			}
			
		} else if (value > head.value) {
			
			if (head.right == null) {
				head.right = new TreeNode();
				head.right.value = value;
			} else {
				addNode(head.right, value);
			}
	
		} 
		
		return head;
	}

	/**
	 * This method is used for calculating size of ordered binary tree 
	 * defined by given head node.
	 * 
	 * @param head head node of ordered binary tree
	 * @return size of ordered binary tree
	 */
	public static int treeSize(TreeNode head) {
		
		if (head == null) {
			return 0;
		}
		
		return 1 + treeSize(head.left) + treeSize(head.right);
	}
	
	/**
	 * This method is used for finding out if value is contained in ordered binary tree
	 * defined by given head node.
	 * 
	 * @param head head node of ordered binary tree
	 * @param value value to find inside of ordered binary tree
	 * @return true if ordered binary tree contains value; false otherwise
	 */
	public static boolean containsValue(TreeNode head, int value) {
		
		if (head == null) {
			return false;
		}
		
		if (value < head.value) {
		
			if (head.left == null) {
				return false;
			} else {
				return containsValue(head.left, value);
			}
			
		} else if (value > head.value) {
			
			if (head.right == null) {
				return false;
			} else {
				return containsValue(head.right, value);
			}
		
		} else {
			return true;
		}	
	}
	
	/**
	 * This method is used for printing the content of ordered binary tree 
	 * defined by given head node.
	 * 
	 * @param head head node of ordered binary tree
	 * @param smallestFirst if true, the smallest value will be printed first; 
	 * 		if false, the highest value will be printed first
	 */
	public static void printTree(TreeNode head, boolean smallestFirst) {
		
		if (head == null) {			
			return;
		}
		
		if (smallestFirst) {
			
			printTree(head.left, smallestFirst);
			
			System.out.print(head.value + " ");
			
			printTree(head.right, smallestFirst); 
			
		} else {
			
			printTree(head.right, smallestFirst);
			
			System.out.print(head.value + " ");
			
			printTree(head.left, smallestFirst); 
			
		}	
	}
}