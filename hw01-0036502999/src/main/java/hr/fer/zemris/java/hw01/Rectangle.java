package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * Program used for calculating the area and perimeter of rectangle.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Rectangle {
	
	/**
	 * This is the main method which uses command line arguments if they are given, 
	 * otherwise it asks user to enter arguments. 
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		double length;
		double height;
		
		if (args.length == 2) {			
			try {
				length = Double.parseDouble(args[0]);
				
				if (length < 0) {
					printNegativeError();
				} else {
					try {
						height = Double.parseDouble(args[1]);
						
						if (height < 0) {
							printNegativeError();
						} else {		
							printSuccessfulMessage(length, height);
						}
					} catch (NumberFormatException ex) {
						printNotNumberError(args[1]);
					}
				}
			} catch (NumberFormatException ex) {
				printNotNumberError(args[0]);
			}
			
		} else if (args.length == 0) {
			try (Scanner sc = new Scanner(System.in)) {
			
				length = getInput("Unesite širinu > ", sc);
				height = getInput("Unesite visinu > ", sc);
			
				printSuccessfulMessage(length, height);
			}
		} else {
			System.out.println("Unesen pogrešan broj argumenata.");
		}
	}
	
	/**
	 * This method is used for printing error message if argument 
	 * cannot be interpreted as a number.
	 * 
	 * @param arg argument that cannot be interpreted as error
	 */
	private static void printNotNumberError(String arg) {
		System.out.println(arg + " se ne može protumačiti kao broj.");
	}
	
	/**
	 * This method is used for printing error message if given number
	 * is negative.
	 * 
	 */
	private static void printNegativeError() {
		System.out.println("Unijeli ste negativnu vrijednost.");
	}
	
	/**
	 * This method is used for printing area and perimeter of rectangle. 
	 * 
	 * @param length length of rectangle
	 * @param height height of rectangle
	 */
	private static void printSuccessfulMessage(double length, double height) {
		System.out.println("Pravokutnik širine " + Double.toString(length) 
				+ " i visine " + Double.toString(height) 
				+ " ima površinu " + Double.toString(getArea(length, height)) 
				+ " i opseg " + Double.toString(getPerimeter(length, height)) + ".");
	}
	
	/**
	 * This method is used for calculating the area of rectangle. 
	 * 
	 * @param length length of rectangle
	 * @param height height of rectangle
	 * @return area of rectangle	 
	 * @throws IllegalArgumentException if given length of height are negative
	 */
	public static double getArea(double length, double height) throws IllegalArgumentException {
		
		if (length < 0) {
			throw new IllegalArgumentException("Length of rectangle cannot be negative");
		}
		
		if (height < 0) {
			throw new IllegalArgumentException("Height of rectangle cannot be negative");
		}
		
		return length * height;
	}
	
	/**
	 * This method is used for calculating the perimeter of rectangle.
	 * 
	 * @param length length of rectangle
	 * @param height height of rectangle
	 * @return perimeter of rectangle
	 * @throws IllegalArgumentException if given length of height are negative
	 */
	public static double getPerimeter(double length, double height) throws IllegalArgumentException {
		
		if (length < 0) {
			throw new IllegalArgumentException("Length of rectangle cannot be negative");
		}
		
		if (height < 0) {
			throw new IllegalArgumentException("Height of rectangle cannot be negative");
		}
		
		return 2 * length + 2 * height;
	}
	
	/**
	 * This method is used for getting input from user.
	 * 
	 * @param message message display to user
	 * @param sc scanner used for getting input
	 * @return number the user has given
	 */
	private static double getInput(String message, Scanner sc) {	
		double number;
		
		do {
			System.out.print(message);
			
			String next;
			next = sc.next();
			try {						
				number = Double.parseDouble(next);	
				if (number < 0) {
					printNegativeError();
				} else {
					return number;
				}
			} catch (NumberFormatException ex) {
				printNotNumberError(next);
			}			
		} while(true);	
	}
}
