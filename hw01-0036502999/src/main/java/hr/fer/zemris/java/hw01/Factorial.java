package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * Program used for calculating factorial of given number.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Factorial {
	
	/**
	 * Value that denotes the end of interaction with user.
	 */
	public static final String KRAJ = "kraj"; 
	
	/**
	 * This is the main method which asks user to enter arguments for 
	 * factorial method. 
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		int number;
		
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.print("Unesite broj > ");
			
			if (sc.hasNextInt()) {
				number = Integer.parseInt(sc.next());	
				
				if (number < 3 || number > 20) {
					System.out.println(number + " nije broj u dozvoljenom rasponu.");
				} else {
					int result = factorial(number);
					System.out.println(number + "! = " + result);				
				}
			
			} else {
				String next = sc.next();
				
				if (next.trim().equals(KRAJ)) {
					System.out.println("Doviđenja.");
					sc.close();
					break;
				}
				
				System.out.println(next + " nije cijeli broj.");
			}
		} while(true);
	}
	
	/**
	 * This method is used for calculating factorial of given non-negative number.
	 * 
	 * @param number number whose factorial is calculated
	 * @return factorial of given number
	 * 	 
	 * @throws IllegalArgumentException if given number is negative
	 */
	public static int factorial(int number) throws IllegalArgumentException {
		if (number < 0) {
			throw new IllegalArgumentException("Broj mora biti nenegativan!");
		}
		
		int result = 1;
		
		for (int i = 1; i <= number; i++) {
			result *= i;
		}
		
		return result;
	}
}
