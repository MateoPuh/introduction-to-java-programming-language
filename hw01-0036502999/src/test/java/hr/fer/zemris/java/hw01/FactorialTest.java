package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class FactorialTest {
	
	@Test
	public void factorialTest() {
		assertEquals(120, Factorial.factorial(5));
	}
	
	/* Testing if factorial method works if 0 is given to it (edge case) */
	@Test
	public void factorialZero() {
		assertEquals(1, Factorial.factorial(0));
	}
	
	/* Testing if factorial method throws IllegalArgumentException after giving it negative value */
	@Test 
	public void factorialException() {
		assertThrows(IllegalArgumentException.class, () -> {
			 Factorial.factorial(-1);
		});
	}

}
