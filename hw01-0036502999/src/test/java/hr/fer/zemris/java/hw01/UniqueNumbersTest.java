package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw01.UniqueNumbers.TreeNode;

public class UniqueNumbersTest {
	
	/* Testing the addNode method when tree is empty */
	@Test
	public void addNodeHead() {
		TreeNode head = null;
		head = UniqueNumbers.addNode(head, 1);
		
		assertEquals(1, head.value);		
	}
	
	/* Testing the addNode method when node is added to the left side */
	@Test
	public void addNodeLeft() {
		TreeNode head = null;
		head = UniqueNumbers.addNode(head, 2);
		
		UniqueNumbers.addNode(head, 1);

		assertEquals(1, head.left.value);
	}
	
	/* Testing the addNode method when node is added to the right side */
	@Test
	public void addNodeRight() {
		TreeNode head = null;
		head = UniqueNumbers.addNode(head, 1);
		
		UniqueNumbers.addNode(head, 2);
		
		assertEquals(2, head.right.value);		
	}

	/* Testing the treeSize method on empty tree */
	@Test
	public void treeSizeEmpty() {
		TreeNode head = null;
		
		assertEquals(0, UniqueNumbers.treeSize(head));	
	}
	
	@Test
	public void treeSizeTest() {
		TreeNode head = null;
		head = UniqueNumbers.addNode(head, 2);
		
		head = UniqueNumbers.addNode(head, 1);
		head = UniqueNumbers.addNode(head, 1);
		head = UniqueNumbers.addNode(head, 3);
		head = UniqueNumbers.addNode(head, 4);
		
		assertEquals(4, UniqueNumbers.treeSize(head));	
	}
	
	/* Testing the containsValue on empty tree */
	@Test
	public void containsValueEmpty() {
		TreeNode head = null;
		
		assertEquals(false, UniqueNumbers.containsValue(head, 1));	
	}
	
	@Test
	public void containsValueTest() {
		TreeNode head = null;
		
		head = UniqueNumbers.addNode(head, 2);
		
		head = UniqueNumbers.addNode(head, 1);
		head = UniqueNumbers.addNode(head, 1);
		head = UniqueNumbers.addNode(head, 3);
		head = UniqueNumbers.addNode(head, 4);
		
		assertEquals(true, UniqueNumbers.containsValue(head, 1));
		assertEquals(true, UniqueNumbers.containsValue(head, 2));
		assertEquals(true, UniqueNumbers.containsValue(head, 3));
		assertEquals(true, UniqueNumbers.containsValue(head, 4));
		assertEquals(false, UniqueNumbers.containsValue(head, 5));
	}
}
