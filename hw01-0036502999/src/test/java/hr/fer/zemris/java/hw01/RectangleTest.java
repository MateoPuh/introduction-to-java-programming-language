package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class RectangleTest {
	
	@Test
	public void areaTest() {
		assertEquals(20, Rectangle.getArea(4, 5));
	}
	
	@Test
	public void areaTestDouble() {
		assertEquals(22, Rectangle.getArea(4.0, 5.5));
	}
	
	/* Testing if area method works if 0 is given to it as a length (edge case) */
	@Test
	public void areaZero() {
		assertEquals(0, Rectangle.getArea(0, 5));
	}
	
	/* Testing if area method throws IllegalArgumentException after giving it negative value for a length */
	@Test 
	public void areaExceptionLength() {
		assertThrows(IllegalArgumentException.class, () -> {
			 Rectangle.getArea(-1, 5);
		});
	}
	
	/* Testing if area method throws IllegalArgumentException after giving it negative value for a height */
	@Test 
	public void areaExceptionHeight() {
		assertThrows(IllegalArgumentException.class, () -> {
			 Rectangle.getArea(5, -1);
		});
	}
	
	@Test
	public void perimeterTest() {
		assertEquals(18, Rectangle.getPerimeter(4, 5));
	}
	
	/* Testing if perimeter method works if 0 is given to it as a length (edge case) */
	@Test
	public void perimeterZero() {
		assertEquals(10, Rectangle.getPerimeter(0, 5));
	}
	
	/* Testing if perimeter method throws IllegalArgumentException after giving it negative value for a length */
	@Test 
	public void perimeterExceptionLength() {
		assertThrows(IllegalArgumentException.class, () -> {
			 Rectangle.getPerimeter(-1, 5);
		});
	}
	
	/* Testing if perimeter method throws IllegalArgumentException after giving it negative value for a height */
	@Test 
	public void perimeterExceptionHeight() {
		assertThrows(IllegalArgumentException.class, () -> {
			 Rectangle.getPerimeter(5, -1);
		});
	}

}
