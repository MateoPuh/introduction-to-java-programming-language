package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class DictionaryTest {
	
	@Test
	public void testEmpty() {
		Dictionary<Integer, String> dict = new Dictionary<>();
		
		assertTrue(dict.isEmpty());
		assertEquals(dict.size(), 0);
	}
	
	@Test
	public void testPutAngGet() {
		Dictionary<Integer, String> dict = new Dictionary<>();

		dict.put(1, "test1");
		dict.put(5, "test2");
		
		assertFalse(dict.isEmpty());
		assertEquals(dict.size(), 2);
		
		assertEquals("test1", dict.get(1));
		assertEquals("test2", dict.get(5));
	}
	
	@Test
	public void testClearAndGet() {
		Dictionary<Integer, String> dict = new Dictionary<>();

		dict.put(20, "test1");
		dict.put(5, "test2");
		
		assertFalse(dict.isEmpty());
		assertEquals(dict.size(), 2);
		
		assertEquals(null, dict.get(1));
		
		dict.clear();
		
		assertTrue(dict.isEmpty());
		assertEquals(dict.size(), 0);
	}
	
	@Test
	public void testPutOverride() {
		Dictionary<Integer, String> dict = new Dictionary<>();

		dict.put(20, "test1");
		dict.put(5, "test2");
		
		assertFalse(dict.isEmpty());
		assertEquals(dict.size(), 2);
		
		dict.put(5, "test3");
		
		assertFalse(dict.isEmpty());
		assertEquals(dict.size(), 2);
		
		assertEquals("test3", dict.get(5));
	}
	
	@Test
	public void testGet() {
		Dictionary<Integer, String> dict = new Dictionary<>();

		dict.put(20, "test1");
		dict.put(5, "test2");
		
		assertFalse(dict.isEmpty());
		assertEquals(dict.size(), 2);
		
		assertEquals(null, dict.get("štefanija"));
	}
	
	@Test 
	public void testPutException() {
		Dictionary<Integer, String> dict = new Dictionary<>();

		assertThrows(NullPointerException.class, ()-> dict.put(null, "štef"));
	}
}
