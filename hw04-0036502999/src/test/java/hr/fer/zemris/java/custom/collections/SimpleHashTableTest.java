package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;

public class SimpleHashTableTest {
	
	@Test
	public void testConstructorException() {
		assertThrows(IllegalArgumentException.class, () -> new SimpleHashTable<Integer, Integer>(0));
	}
	
	@Test 
	public void testEmpty() {
		var table = new SimpleHashTable<Integer, String>();

		assertEquals(0, table.size());
		assertTrue(table.isEmpty());
	}
	
	@Test
	public void testPutAndSize() {
		var table = new SimpleHashTable<Integer, String>();
		 
		table.put(1, "mateo1");
		table.put(205, null);
		
		assertEquals(2, table.size());
		assertFalse(table.isEmpty());
	}
	
	@Test
	public void testPutException() {
		var table = new SimpleHashTable<Integer, String>();
		
		assertThrows(NullPointerException.class, () -> table.put(null, "štefi"));
	}
	
	@Test
	public void testPutOverride() {
		var table = new SimpleHashTable<Integer, String>();
		 
		table.put(1, "mateo1");
		table.put(205, "test");
		table.put(205, "novi test");
		
		assertEquals(2, table.size());
		assertFalse(table.isEmpty());
		
		assertEquals("novi test", table.get(205));
	}
	
	@Test
	public void testGet() {
		var table = new SimpleHashTable<String, String>();
		
		table.put("prvi", "vrijednost1");
		table.put("test", "test");
		table.put("treci", null);
		
		assertEquals("vrijednost1", table.get("prvi"));
		assertEquals(null, table.get("ivana"));
		assertEquals(null, table.get("treci"));
		assertEquals(null, table.get(5.6));
	}
	
	@Test
	public void testContainsKey() {
		var table = new SimpleHashTable<String, String>();
		
		table.put("prvi", "vrijednost1");
		table.put("test", "test");
		table.put("treci", null);
		
		assertFalse(table.containsKey(null));
		assertTrue(table.containsKey("prvi"));
		assertFalse(table.containsKey("stoti"));
		assertFalse(table.containsKey(5.6));
	}
	
	@Test
	public void testContainsValue() {
		var table = new SimpleHashTable<String, String>();
				
		table.put("prvi", "vrijednost1");
		table.put("test", "test");
		table.put("treci", null);
		
		assertTrue(table.containsValue(null));
		assertTrue(table.containsValue("test"));
		assertFalse(table.containsValue("java"));
		assertFalse(table.containsValue(5.6));
	}
	
	@Test
	public void testRemove() {
		var table = new SimpleHashTable<Integer, String>();
		 
		table.put(1, "mateo1");
		table.put(205, "test");
		table.put(205, "novi test");
		
		assertEquals(2, table.size());
		assertFalse(table.isEmpty());
		
		table.remove(205);
		
		assertEquals(1, table.size());
		assertFalse(table.isEmpty());
		
		assertEquals(null, table.get(205));
		
		table.remove(null);
		table.remove(5.6);
		table.remove(3);
		
		assertEquals(1, table.size());
		assertFalse(table.isEmpty());		
	}
	
	@Test
	public void testToString() {
		var table = new SimpleHashTable<String, String>();

		table.put("prvi", "proba1");
		table.put("drugi", "štefi");
				
		assertTrue(table.toString().equals("[prvi=proba1, drugi=štefi]") 
				|| table.toString().equals("[drugi=štefi, prvi=proba1]"));
	}
	
	@Test
	public void testOverfilling() {
		var table = new SimpleHashTable<String, String>(4);

		table.put("prvi", "proba1");
		table.put("drugi", "štefi");
		table.put("treci", "ante"); // overfilling should happen here
		table.put("cetvrti", "šimun");
		table.put("peti", "stipe");
		table.put("sesti", "stipe"); // and here
		table.put("sedmi", "stipe");
		table.put("osmi", "stipe");
		table.put("deveti", "stipe");
	
		assertEquals(9, table.size());
		
		assertEquals("proba1", table.get("prvi"));
		assertEquals("štefi", table.get("drugi"));
		assertEquals("ante", table.get("treci"));
		assertEquals("šimun", table.get("cetvrti"));
		assertEquals("stipe", table.get("peti"));
		assertEquals("stipe", table.get("sesti"));
		assertEquals("stipe", table.get("sedmi"));
		assertEquals("stipe", table.get("osmi"));
		assertEquals("stipe", table.get("deveti"));
	}
	
	@Test
	public void testClear() {
		var table = new SimpleHashTable<String, String>(4);

		table.put("prvi", "proba1");
		table.put("drugi", "štefi");
		table.put("treci", "ante"); // overfilling should happen here
		table.put("cetvrti", "šimun");
		table.put("peti", "stipe");
		table.put("sesti", "stipe"); // and here
		table.put("sedmi", "stipe");
		table.put("osmi", "stipe");
		table.put("deveti", "stipe");
	
		assertEquals(9, table.size());
		
		table.clear();
		
		assertEquals(0, table.size());
		assertTrue(table.isEmpty());
	}
	
	@Test
	public void testNext() {
		var table = new SimpleHashTable<String, String>();

		table.put("prvi", "proba1");
		table.put("drugi", "štefi");
		table.put("treci", "ante"); 
		table.put("cetvrti", "šimun");
		
		Iterator<SimpleHashTable.TableEntry<String, String>> iter = table.iterator();
		
		table.containsKey(iter.next().getKey());
		table.containsKey(iter.next().getKey());
		table.containsValue(iter.next().getValue());
		table.containsValue(iter.next().getValue());
		
		assertThrows(NoSuchElementException.class, () -> iter.next());
	}
	
	@Test
	public void testHasNext() {
		var table = new SimpleHashTable<String, String>();

		table.put("prvi", "proba1");
		table.put("drugi", "štefi");
		table.put("treci", "ante"); 
		table.put("cetvrti", "šimun");
		
		Iterator<SimpleHashTable.TableEntry<String, String>> iter = table.iterator();

		assertTrue(iter.hasNext());
		assertTrue(iter.hasNext());
		assertTrue(iter.hasNext());
		iter.next();
		assertTrue(iter.hasNext());
		iter.next();
		assertTrue(iter.hasNext());
		iter.next();
		assertTrue(iter.hasNext());
		assertTrue(iter.hasNext());
		assertTrue(iter.hasNext());
		assertTrue(iter.hasNext());
		iter.next();
		
		assertFalse(iter.hasNext());
		assertFalse(iter.hasNext());
		assertFalse(iter.hasNext());
	}
	
	@Test
	public void testRemoveIterator() {
		var table = new SimpleHashTable<String, String>();

		table.put("prvi", "proba1");
		table.put("drugi", "štefi");
		table.put("treci", "ante"); 
		table.put("cetvrti", "šimun");
		
		Iterator<SimpleHashTable.TableEntry<String, String>> iter = table.iterator();

		while(iter.hasNext()) {
			if(iter.next().getKey().equals("prvi")) {
				iter.remove();
			}
		}
		
		assertEquals(3, table.size());
		assertFalse(table.containsKey("prvi"));
	}
	
	@Test 
	public void testIteratorException() {
		var table = new SimpleHashTable<String, String>();

		table.put("prvi", "proba1");
		table.put("drugi", "štefi");
		table.put("treci", "ante"); 
		table.put("cetvrti", "šimun");
		
		Iterator<SimpleHashTable.TableEntry<String, String>> iter = table.iterator();
		
		iter.next();
		
		table.put("test", "test");
		
		assertThrows(ConcurrentModificationException.class, () -> iter.hasNext());
		assertThrows(ConcurrentModificationException.class, () -> iter.next());
		assertThrows(ConcurrentModificationException.class, () -> iter.remove());
	}
	
}
