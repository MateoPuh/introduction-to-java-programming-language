package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class Problem1Test {

	@Test
	public void testArrayIndexedCollection() {
		var arrayIndexedCol = new ArrayIndexedCollection<String>();
		
		Collection<String> col = arrayIndexedCol;
		
		assertTrue(col.isEmpty());
		
		// add method
		col.add("štefi");
		col.add("marko");
		col.add("test");
		
		// remove method
		assertFalse(col.remove(null));
		assertFalse(col.remove(5));
		assertTrue(col.remove("test"));
		
		// contains method
		assertFalse(col.contains(5));
		assertFalse(col.contains(null));
		assertTrue(col.contains("štefi"));
		
		// size method
		assertEquals(2, col.size());
		
		// clear method
		col.clear();
		assertTrue(col.isEmpty());
		assertEquals(0, col.size());
		
		col.add("štefi");
		col.add("ivana");
		
		List<String> list = new ArrayIndexedCollection<String>();
		
		// addAll method
		list.addAll(col);
		
		assertEquals(2, list.size());
		
		assertFalse(list.contains(5));
		assertFalse(list.contains(null));
		assertTrue(list.contains("štefi"));

		// insert method
		list.insert("mateo", 2);
		assertThrows(NullPointerException.class, () -> list.insert(null, 0));
		assertThrows(IndexOutOfBoundsException.class, () -> list.insert("mateo", 50));

		// get method
		assertEquals("mateo", list.get(2));
		assertThrows(IndexOutOfBoundsException.class, () -> list.get(50));
		
		// indexOf method
		assertEquals(-1, list.indexOf(6));
		assertEquals(-1, list.indexOf("ante"));
		assertEquals(2, list.indexOf("mateo"));
		
		// remove method
		list.remove(2);
		assertEquals(-1, list.indexOf("mateo"));
		assertThrows(IndexOutOfBoundsException.class, () -> list.remove(50));
	}
	
	@Test
	public void testLinkedListIndexedCollection() {
		var linkedListIndexedCol = new LinkedListIndexedCollection<String>();
		
		Collection<String> col = linkedListIndexedCol;
		
		assertTrue(col.isEmpty());
		
		// add method
		col.add("štefi");
		col.add("marko");
		col.add("test");
		
		// remove method
		assertFalse(col.remove(null));
		assertFalse(col.remove(5));
		assertTrue(col.remove("test"));
		
		// contains method
		assertFalse(col.contains(5));
		assertFalse(col.contains(null));
		assertTrue(col.contains("štefi"));
		
		// size method
		assertEquals(2, col.size());
		
		// clear method
		col.clear();
		assertTrue(col.isEmpty());
		assertEquals(0, col.size());
		
		col.add("štefi");
		col.add("ivana");
		
		List<String> list = new LinkedListIndexedCollection<String>();
		
		// addAll method
		list.addAll(col);
		
		assertEquals(2, list.size());
		
		assertFalse(list.contains(5));
		assertFalse(list.contains(null));
		assertTrue(list.contains("štefi"));

		// insert method
		list.insert("mateo", 2);
		assertThrows(NullPointerException.class, () -> list.insert(null, 0));
		assertThrows(IndexOutOfBoundsException.class, () -> list.insert("mateo", 50));

		// get method
		assertEquals("mateo", list.get(2));
		assertThrows(IndexOutOfBoundsException.class, () -> list.get(50));
		
		// indexOf method
		assertEquals(-1, list.indexOf(6));
		assertEquals(-1, list.indexOf("ante"));
		assertEquals(2, list.indexOf("mateo"));
		
		// remove method
		list.remove(2);
		assertEquals(-1, list.indexOf("mateo"));
		assertThrows(IndexOutOfBoundsException.class, () -> list.remove(50));
	}
	
	@Test
	public void testObjectStack() {
		ObjectStack<List<Integer>> stack = new ObjectStack<>();
		
		List<Integer> list1 = new LinkedListIndexedCollection<Integer>();
		
		list1.add(5);
		list1.add(6);
		list1.add(7);

		List<Integer> list2 = new ArrayIndexedCollection<Integer>();

		list2.add(1);
		list2.add(2);
		
		assertTrue(stack.isEmpty());
		assertEquals(0, stack.size());
		
		stack.push(list1);
		stack.push(list2);
		
		assertFalse(stack.isEmpty());
		assertEquals(2, stack.size());
		
		List<Integer> listFromStack = stack.pop();
		
		assertTrue(listFromStack.contains(1));
		assertTrue(listFromStack.contains(2));
		
		assertEquals(1, stack.size());

		List<Integer> listFromStack2 = stack.peek();

		assertTrue(listFromStack2.contains(5));
		assertTrue(listFromStack2.contains(6));
		assertTrue(listFromStack2.contains(7));
		
		assertEquals(1, stack.size());
		
		stack.clear();
		
		assertEquals(0, stack.size());

		assertThrows(EmptyStackException.class, () -> stack.pop());
	}
	
}
