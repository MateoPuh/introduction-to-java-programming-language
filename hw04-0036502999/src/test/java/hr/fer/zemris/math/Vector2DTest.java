package hr.fer.zemris.math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class Vector2DTest {
	
	/** Maximal difference between two doubles to consider them equal */
	private static final double EPSILON = 1e-6;
	
	@Test
	public void testNewVector() {
		Vector2D vector1 = new Vector2D(1, -1);
		Vector2D vector2 = new Vector2D(0, -2);

		assertEquals(1, vector1.getX(), EPSILON);
		assertEquals(-1, vector1.getY(), EPSILON);
		assertEquals(0, vector2.getX(), EPSILON);
		assertEquals(-2, vector2.getY(), EPSILON);
	}
	
	@Test
	public void testTranslate() {
		Vector2D vector1 = new Vector2D(1, -1);
		Vector2D vector2 = new Vector2D(4, -2);

		vector1.translate(vector2);
		
		assertEquals(5, vector1.getX(), EPSILON);
		assertEquals(-3, vector1.getY(), EPSILON);
	}
	
	@Test
	public void testTranslated() {
		Vector2D vector1 = new Vector2D(1, -1);
		Vector2D vector2 = new Vector2D(4, -2);

		Vector2D vector3 = vector1.translated(vector2);
		
		assertEquals(1, vector1.getX(), EPSILON);
		assertEquals(-1, vector1.getY(), EPSILON);
		assertEquals(5, vector3.getX(), EPSILON);
		assertEquals(-3, vector3.getY(), EPSILON);
	}
	
	@Test
	public void testRotate() {
		Vector2D vector1 = new Vector2D(1, -1);

		vector1.rotate(Math.PI / 2);
		
		assertEquals(1, vector1.getX(), EPSILON);
		assertEquals(1, vector1.getY(), EPSILON);
	}
	
	/* If x == 0 it is special case because it calculates angles differently */
	@Test
	public void testRotateSpecialCase() {
		Vector2D vector1 = new Vector2D(0, 1);

		vector1.rotate(Math.PI / 2);
		
		assertEquals(-1, vector1.getX(), EPSILON);
		assertEquals(0, vector1.getY(), EPSILON);
		
		Vector2D vector2 = new Vector2D(0, -1);

		vector2.rotate(Math.PI / 2);
		
		assertEquals(1, vector2.getX(), EPSILON);
		assertEquals(0, vector2.getY(), EPSILON);
	}
	
	@Test
	public void testRotated() {
		Vector2D vector1 = new Vector2D(1, -1);

		Vector2D vector2 = vector1.rotated(Math.PI / 4);
		
		assertEquals(1, vector1.getX(), EPSILON);
		assertEquals(-1, vector1.getY(), EPSILON);
		assertEquals(Math.sqrt(2), vector2.getX(), EPSILON);
		assertEquals(0, vector2.getY(), EPSILON);
	}
	
	@Test
	public void testScale() {
		Vector2D vector1 = new Vector2D(1, -1);

		vector1.scale(5);
		
		assertEquals(5, vector1.getX(), EPSILON);
		assertEquals(-5, vector1.getY(), EPSILON);
	}
	
	@Test
	public void testScaled() {
		Vector2D vector1 = new Vector2D(1, -1);
		
		Vector2D vector2 = vector1.scaled(5);
		
		assertEquals(1, vector1.getX(), EPSILON);
		assertEquals(-1, vector1.getY(), EPSILON);
		assertEquals(5, vector2.getX(), EPSILON);
		assertEquals(-5, vector2.getY(), EPSILON);
	}
	
	@Test
	public void testCopy() {
		Vector2D vector1 = new Vector2D(1, -1);
		
		Vector2D vector2 = vector1.copy();
		vector2.scale(5);
		
		assertEquals(1, vector1.getX(), EPSILON);
		assertEquals(-1, vector1.getY(), EPSILON);
		assertEquals(5, vector2.getX(), EPSILON);
		assertEquals(-5, vector2.getY(), EPSILON);
	}
	
	@Test
	public void testEquals() {
		var v1 = new Vector2D(1, 1);
		var v2 = new Vector2D(1, 1);
		
		assertTrue(v1.equals(v2));		
	}
}
