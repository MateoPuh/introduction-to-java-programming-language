package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * This class represents linked list-backed collection of objects. 
 * This collection allows duplicate elements and doesn't allow null refrences. 
 * 
 * <p> Implementation of this collection is based on linked list whose nodes
 * have reference to previous and next node, which allows more efficient 
 * algorithms since it can be searched in two directions. </p>
 * 
 * <p> That is the reasons why complexity of add(Object) method is O(1). Complexity 
 * of clear() method is also O(1). Average complexity of methods get(int), remove(int) 
 * and insert(Object, int) is Θ(n), but it is never greater than n/2+1. Average complexity 
 * of methods contains(Object), remove(Object), addAll(Collection) and indexOf(Object) is Θ(n). </p>
 * 
 * <p> This collection can initialy be filled with elements of other collection if 
 * that collection is specified. </p>
 * 
 * @param <T> The type of objects stored in this collection
 * 
 * @author Mateo Puhalović
 * @version 1.1
 *
 */
public class LinkedListIndexedCollection<T> implements List<T> {

	private int size;
	private ListNode first;
	private ListNode last;
	
	private long modificationCount;
	
	/**
	 * This class represents Node in list. It has reference to 
	 * next node, previous node and reference to value storage.
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 *
	 */
	private  class ListNode {
		ListNode nextNode;
		ListNode previousNode;
		T value;
		
		/**
		 * Creates new ListNode with given references to next node, previous node 
		 * and reference to value storage.
		 * 
		 * @param nextNode reference to next node
		 * @param previousNode reference to previous node
		 * @param value reference to value storage
		 */
		public ListNode(ListNode nextNode, ListNode previousNode, T value) {
			this.nextNode = nextNode;
			this.previousNode = previousNode;
			this.value = value;
		}
	}
	
	/**
	 * {@link ElementsGetter} class used for {@link LinkedListIndexedCollection}.
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 *
	 */
	private class LinkedListElementsGetters implements ElementsGetter<T> {

		private LinkedListIndexedCollection<T> collection;
		private ListNode currentNode;
		
		private long savedModificationCount;
		
		/**
		 * Creates new ArrayElementsGetter for given collection.
		 * 
		 * @param collection collection which will be used in {@link ElementsGetter}
		 */
		public LinkedListElementsGetters(LinkedListIndexedCollection<T> collection) {
			this.collection = collection;
			savedModificationCount = collection.modificationCount;
			this.currentNode = collection.first;
		}
		
		@Override
		public boolean hasNextElement() {
			return currentNode != null;
		}

		@Override
		public T getNextElement() throws NoSuchElementException, ConcurrentModificationException {
			if (savedModificationCount != collection.modificationCount) {
				throw new ConcurrentModificationException("Content of collection changed while using this ElementsGetter!");
			}
			
			if(this.hasNextElement()) {
				T result = currentNode.value;
				currentNode = currentNode.nextNode;
				return result;
			} else {
				throw new NoSuchElementException("There is no next element!");
			}
		}
		
	}
	
	/**
	 * Creates empty collection.
	 */
	public LinkedListIndexedCollection() {
		this(null);
	}
	
	/**
	 * Creates collection with copied elements from given collection.
	 * 
	 * @param collection collection whose elements are copied to new collection
	 */
	public LinkedListIndexedCollection(Collection<T> collection) {
		first = null;
		last = null;
		
		if (collection != null) {
			addAll(collection);
		}
	}
	
	/**
	 * Insert the element with given value to the end of collection. 
	 * It doesn't allow for null values to be added to collection. 
	 * Complexity of this method is O(1). 
	 * 
	 * @param value value to insert into collection
	 * @throws NullPointerException if given value is null
	 */
	@Override
	public void add(T value) throws NullPointerException {
	    Objects.requireNonNull(value, "Null value cannot be added to this collection!");
		
		insert(value, size);
	}
	
	/**
	 * Returns the object at given index. The complexity of this method 
	 * is never greater than n/2+1.
	 * 
	 * @param index index of object that will be taken
	 * @return Object at given index
	 * @throws IndexOutOfBoundsException if given index is not in in [0, size - 1]
	 */
	public T get(int index) throws IndexOutOfBoundsException {
		checkBounds(index);
		
		if (index > size/2) {
			ListNode node = last;
			
			for (int currentIndex = size - 1; currentIndex > index; currentIndex--) {
				node = node.previousNode;
			}
			
			return node.value;
		} else {
			ListNode node = first;
			
			for (int currentIndex = 0; currentIndex < index; currentIndex++) {
				node = node.nextNode;
			}
			
			return node.value;
		}
	}
	
	@Override
	public void forEach(Processor<? super T> processor) {
		ListNode node = first;
		
		for (int currentIndex = 0; currentIndex != size; currentIndex++) {
			if (node == null) {
				break;
			}
			processor.process(node.value);
			node = node.nextNode;
		}
	}
	
	@Override
	public int size() {
		return size;
	}
	
	@Override
	public void clear() {	
		first = null;
		last = null;
		modificationCount++;
		size = 0;
	}
	
	/**
	 * Returns index of element with given value. Uses method equals 
	 * for finding which element has given value. Since collection can
	 * have duplicate elements, this implementation returns index of 
	 * first element with given value.
	 * 
	 * @param value value whose index is being searched
	 * @return index of element with given value; -1 if not found
	 */
	public int indexOf(Object value) {
		if (value == null) {
			return -1;
		}
		
		ListNode node = first;
				
		for (int currentIndex = 0; currentIndex != size; currentIndex++) {
			if (node == null) {
				break;
			}
			
			if (node.value == value) {
				return currentIndex;
			}	
			node = node.nextNode;
		}
		
		return -1;	
	}

	/**
	 * Removes element at given index of collection. It moves part of array that 
	 * starts at (index + 1) 1 index lower than they were before.
	 * 
	 * @param index index of element to remove
	 * @throws IndexOutOfBoundsException if given index is not in [0, size - 1]
	 */
	public void remove(int index) throws IndexOutOfBoundsException {
		checkBounds(index);
		
		ListNode node = first;
				
		for (int currentIndex = 0; currentIndex != size; currentIndex++) {
			if (currentIndex == index) {	
				removeNode(node);
				return;
			}
			node = node.nextNode;
		}	
	}
	
	/**
	 * Method that removes node by changing references of previous and next node.
	 * 
	 * @param node node to remove
	 */
	private void removeNode(ListNode node) {
		if (node.previousNode != null) {
			node.previousNode.nextNode = node.nextNode;
		} else {
			first = node.nextNode;
		}
		
		if (node.nextNode != null) {
			node.nextNode.previousNode = node.previousNode;
		} else {
			last = node.previousNode;
		}
		
		modificationCount++;
		size--;
	}
	
	@Override
	public boolean remove(Object value) {
		ListNode node = first;
		
		for (int currentIndex = 0; currentIndex != size; currentIndex++) {
			if (node == null) {
				break;
			}
			
			if (node.value == value) {
				removeNode(node);
				return true;
			}	
			node = node.nextNode;
		}
		
		return false;
	}

	@Override
	public boolean contains(Object value) {
		if (value == null) {
			return false;
		}
		
		return (indexOf(value) != -1);
	}
	
	@Override
	public Object[] toArray(){
		Object[] array = new Object[size];
		ListNode node = first;
		
		for (int currentIndex = 0; currentIndex < size; currentIndex++) {
			array[currentIndex] = node.value;
			node = node.nextNode;
		}
		
		return array;
	}
	
	/**
	 * Inserts element with given value at the given position. 
	 * 
	 * @param value value to insert in collection
	 * @param position position on which to insert new element
	 * @throws IndexOutOfBoundsException if given position is not in [0, size]
	 * @throws NullPointerException if given value is null
	 */
	@Override
	public void insert(T value, int position) throws IndexOutOfBoundsException, NullPointerException {
		Collection.checkBounds(position, 0, size);
	    Objects.requireNonNull(value, "Null value cannot be added to this collection!");
	    
	    /* since this algorithm insert new node after the one on the given position,
	     * inserting on the beginning is special case
	     */
	    if (position == 0) {
			ListNode newNode = new ListNode(first, null, value);
			first = newNode;
			
			if (last == null) {
				last = newNode;
			}
			
			modificationCount++;
			size++;
			return;
	    }

	    ListNode node;

		if (position > size/2) {
			node = last;
			
			for (int currentIndex = size; currentIndex > position; currentIndex--) {
				node = node.previousNode;
			}			
			
		} else {
			node = first;
			
			for (int currentIndex = 0; currentIndex < position - 1; currentIndex++) {
				node = node.nextNode;
			}
		}
		
		ListNode newNode = new ListNode(node.nextNode, node, value);
		
		if (node.nextNode != null) {
			node.nextNode.previousNode = newNode;
		} else {
			last = newNode;
		}
		
		node.nextNode = newNode;
		
		modificationCount++;
		size++;
	}
	
	/**
	 * Checks if given index is in [0, size - 1]. Uses Collection.checkBounds(int, int, int) method
	 * with values 0 and size - 1;
	 * 
	 * @param index index to check 
	 * @throws IndexOutOfBoundsException if given index is not in [0, size - 1]
	 * @see Collection#checkBounds(int, int, int)
	 */
	private void checkBounds(int index) throws IndexOutOfBoundsException{
		Collection.checkBounds(index, 0, size - 1);
	}

	@Override
	public ElementsGetter<T> createElementsGetter() {
		return new LinkedListElementsGetters(this);
	}
}


