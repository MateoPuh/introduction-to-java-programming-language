package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * This interface represents object whose task is to return elements of
 * {@link Collection}, one by one. It contains two methods: hasNextElement 
 * and getNextElement. 
 * 
 * <p>Method hasNextElement informs user if there is next element in collection. 
 * Method getNextElement returns next element in collection, starting from first and
 * then iterating through whole collection.</p>
 * 
 * <p>Users are expected to not change the content of collection while using
 * ElementsGetter. However, if user changes content of collection while using 
 * ElementsGetter, he/she won't be able to use it anymore. </p>
 * 
 * @param <T> The type of objects through which the ElementsGetter iterates
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public interface ElementsGetter<T> {
	
	/**
	 * Returns true if there is next element in this 
	 * iteration through collection.
	 * 
	 * @return true if there is next element; false otherwise
	 */
	boolean hasNextElement();
	
	/**
	 * Returns next element in iteration if he exists.
	 * 
	 * @return next element in iteration
	 * @throws NoSuchElementException if there is no next element
	 * @throws ConcurrentModificationException if content of collection
	 * 		is changed while using ElementsGetter
	 */
	T getNextElement() throws NoSuchElementException, ConcurrentModificationException;
	
	/**
	 * Uses process method from given {@link Processor} to
	 * process all remaining elements in collection.
	 * 
	 * @param p Processor whose process method is used to process
	 * 		elements
	 */
	default void processRemaining(Processor<? super T> p) {
		while(this.hasNextElement()) {
			p.process(this.getNextElement());
		}
	}
	
}
