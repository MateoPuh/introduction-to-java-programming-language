package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * This class represents simple hash table which stores entries 
 * in form of a {@link TableEntry} class. Entry consists of a 
 * key and value pair.
 * 
 * <p>Keys in entries are unique, unchangeable and non-null, while
 * values can be null and can be changed.</p>
 * 
 * <p>Overflow problem is solved by putting entries in a list for
 * each slot in a table.</p>
 * 
 * <p>The size of a table will be set to 16 if size is not specified.
 * Otherwise, size will be set to smallest power of 2 greater than or
 * equal to specified size.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 * @param <K>The type of key
 * @param <V>The type of value
 */
public class SimpleHashTable<K, V>  implements Iterable<SimpleHashTable.TableEntry<K,V>> {

	/** Default capacity. */
	private static final int DEFAULT_CAPACITY = 16;
	
	/** Left bracket used in toString method. */
	private static final String LEFT_BRACKET = "[";
	
	/** Right bracket used in toString method. */
	private static final String RIGHT_BRACKET = "]";
	
	/** Equals sign used in toString method. */
	private static final String EQUALS = "=";
	
	/** Comma sign used in toString method. */
	private static final String COMMA = ",";
	
	/** percentage that indicates that table is overfilled. */
	private static final double OVERFILLED_PERCENTAGE = 0.75;

	private TableEntry<K,V>[] table;
	private int size;
	private int capacity;
	private long modificationCount;
	
	/**
	 * This class represents one entry of a {@link SimpleHashTable} 
	 * collection. It consists of key and value pair. 
	 * 
	 * <p>Key of an entry is unique, unchangeable and cannot be null, 
	 * while value can be changed and can be null.</p>
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 *
	 * @param <K> The type of key 
	 * @param <V> The type of value
	 */
	public static class TableEntry<K, V> {
		private K key;
		private V value;
		private TableEntry<K, V> next;
		
		/**
		 * Creates new {@link TableEntry} with given key 
		 * and value.
		 * 
		 * @param key value of an entry
		 * @param value value of an entry
		 * @throws NullPointerException if given key is null
		 */
		public TableEntry(K key, V value) throws NullPointerException {
			this.key = Objects.requireNonNull(key);
			this.value = value;
		}

		/**
		 * Returns key of an entry.
		 * 
		 * @return key of an entry
		 */
		public K getKey() {
			return key;
		}
		
		/**
		 * Returns value of an entry.
		 * 
		 * @return value of an entry
		 */
		public V getValue() {
			return value;
		}

		/**
		 * Sets value of an entry to the given value.
		 * 
		 * @param value value to set
		 */
		public void setValue(V value) {
			this.value = value;
		}	
	}
	
	/**
	 * This class implements {@link Iterator} and is used 
	 * for iterating through {@link SimpleHashTable}.
	 * It provides methods for getting next entry, removing 
	 * last retrieved entry and finding out if next entry exists. 
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 *
	 */
	public class IteratorImpl implements Iterator<TableEntry<K, V>> {
		private int nextIndex;
		private TableEntry<K, V> current;
		private int nextSlotIndex;
		private long savedModificationCount;
		private int tableSize;

		/**
		 * Creates new {@link Iterator} for {@link SimpleHashTable}.
		 */
		public IteratorImpl() {
			savedModificationCount = modificationCount;
			this.tableSize = size;
		}
		
		@Override
		public boolean hasNext() {
			if(savedModificationCount != modificationCount) {
				throw new ConcurrentModificationException("HashTable was modified during iteration!");
			}
			
			return nextIndex < tableSize;
		}
		
		@Override
		public TableEntry<K, V> next() {
			if(!hasNext()) {
				throw new NoSuchElementException();
			}
			
			// if current entry is first entry in table
			// or if current entry is last in its slot
			// then find first entry in next non-empty slot
			if(current == null || current.next == null) {
				for(int i = nextSlotIndex; i < capacity; i++) {
					if(table[i] != null) {
						current = table[i];
						nextIndex++;
						nextSlotIndex=i+1;
						return current;
					}
				}
				// if end of table is reached
				throw new NoSuchElementException();
			// else take next entry in current slot
			} else {
				current = current.next;
				nextIndex++;
				return current;
			}			
		}
		
		@Override
		public void remove() {
			if(savedModificationCount != modificationCount) {
				throw new ConcurrentModificationException("HashTable was modified during iteration!");
			}
			
			// if next was never called throw IllegalArgumentException
			if(nextIndex == 0) {
				throw new IllegalArgumentException("Nothing to remove!");
			}
			
			// if entry is already removed throw IllegalArgumentException
			if(!SimpleHashTable.this.containsKey(current.getKey())) {
				throw new IllegalArgumentException("Error while removing non-existing entry!");
			}
				
			// remove entry from table and increase savedModificationCount 
			// to match it with modification count
			SimpleHashTable.this.remove(current.getKey());
			savedModificationCount++;
		}

	}
	
	/**
	 * Creates new {@link SimpleHashTable} with default capacity
	 * of 16.
	 */
	public SimpleHashTable() {
		this(DEFAULT_CAPACITY);
	}
	
	/**
	 * Creates new {@link SimpleHashTable} with smallest power of 
	 * 2 which is greater than or equal to given initial capacity.
	 * 
	 * @param capacity capacity used for initializing hash table
	 * @throws IllegalArgumentException if given capacity is less than 1
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashTable(int capacity) throws IllegalArgumentException {
		if(capacity < 1) {
			throw new IllegalArgumentException("Capacity of hash table should be bigger than 1!");
		}
				
		this.capacity = roundToPowerOfTwo(capacity); 
		this.table = new TableEntry[this.capacity];
	}
	
	/**
	 * Inserts new {@link TableEntry} with given key and value to the
	 * {@link SimpleHashTable}. if there is already entry with given
	 * key, his value is replaced with given one.
	 * Complexity of this method is O(1). 
	 * 
	 * @param key key of an entry
	 * @param value value of an entry
	 * @throws NullPointerException if given key is null
	 */
	public void put(K key, V value) throws NullPointerException{
		TableEntry<K, V> entry = new TableEntry<K, V>(Objects.requireNonNull(key), value);
		int slot = Math.abs(key.hashCode()) % capacity;
				
		// if slot is empty, put this entry in slot
		if(table[slot] == null) {
			table[slot] = entry;
			size++;
			modificationCount++;

			if(isOverfilled()) {
				doubleTheCapacity();
			}
			
			return;
		} 
				
		var current = table[slot];
		
		// find entry with same key in this slot or add to the end of the slot
		while(true) {
			if(current.getKey().equals(key)) {
				current.setValue(value);
				return;
			}
			
			if(current.next == null) {
				current.next = entry;
				size++;
				modificationCount++;
				
				if(isOverfilled()) {
					doubleTheCapacity();
				}
				
				return;
			}
			
			current = current.next;
		}
	}
	
	/**
	 * Returns value of {@link TableEntry} whose key is equal to the 
	 * given one. Returns null if entry with given key does 
	 * not exist. Complexity of this method is O(1). 
	 * 
	 * @param key key of an entry
	 * @return value of an entry; null if entry does not exist
	 */
	public V get(Object key) {
		if(key == null) {
			return null;
		}
		
		// try to find entry in its slot
		TableEntry<K, V> e = findInSlot(key);
		if(e != null) {
			return e.getValue();
		}
		
		// return null if entry is not found
		return null;		
	}
	
	/**
	 * Returns number of entries stores in this
	 * {@link SimpleHashTable}.
	 * 
	 * @return size of hash table
	 */
	public int size() {
		return size;
	}
	
	/**
	 * Returns true if {@link SimpleHashTable} contains entry with 
	 * given key. Complexity of this method is O(1). 
	 * 
	 * @param key key of an entry
	 * @return true if entry with given key exists; 
	 * 			false otherwise
	 */
	public boolean containsKey(Object key) {
		if(key == null) {
			return false;
		}
		
		// try to find entry in its slot
		TableEntry<K, V> e = findInSlot(key);
		if(e != null) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Returns true if {@link TableEntry} with given value is contained 
	 * in {@link SimpleHashTable}. Complexity of this method is O(n). 
	 * 
	 * @param value value of an entry
	 * @return true if hash table contains given value;
	 * 		false otherwise
	 */
	public boolean containsValue(Object value) {
		Iterator<SimpleHashTable.TableEntry<K, V>> iter = iterator();
		
		while(iter.hasNext()) {
			var current = iter.next();
			
			// if given value is null and value of current entry is null return true
			if(current.getValue() == null && value == null) {
				return true;
			}
			
			// if value of current entry is equal to given value return true
			if(current.getValue() != null && current.getValue().equals(value)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Removes entry with a given key from {@link SimpleHashTable}. 
	 * Complexity of this method is O(1). 
	 * 
	 * @param key key of an entry to remove
	 */
	public void remove(Object key) {
		if(key == null) {
			return;
		}
		
		int slot = Math.abs(key.hashCode()) % capacity;

		// if slot is empty, return 
		if(table[slot] == null) {
			return;
		} 
			
		var current = table[slot];
		
		// removing first from the slot
		if(current.getKey().equals(key)) {
			table[slot] = current.next;
			size--;
			modificationCount++;
			return;
		}
		
		// find entry with given key and remove it or return if end of slot is reached
		while(true) { 
			// if this is last entry in slot return
			if(current.next == null) {
				return;
			}
			
			// if next slot has given key
			// then set reference next of this entry to reference next of next entry
			if(current.next.getKey().equals(key)) {
				current.next = current.next.next;
				size--;
				modificationCount++;
				return;
			}
					
			current = current.next;
		}	
	}
	
	/**
	 * Returns true if {@link SimpleHashTable} is empty.
	 * 
	 * @return true if table is empty; 
	 * 	false otherwise
	 */
	public boolean isEmpty() {
		return size == 0;
	}
	
	/**
	 * Removes all entries from table.
	 */
	@SuppressWarnings("unchecked")
	public void clear() {
		this.table = new TableEntry[this.capacity];
		size = 0;
	}
	
	/**
	 * Returns formatted String for {@link SimpleHashTable} 
	 * class. Format of a String is "[key1=value1, key2=value2, ..., key{n}=value{n}]".
	 * 
	 * @return formatted string
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(LEFT_BRACKET);
		
		Iterator<SimpleHashTable.TableEntry<K, V>> iter = iterator();
		
		// if entry is first, dont put ", " before it
		if(iter.hasNext()) {
			var current = iter.next();
			sb.append(current.getKey() + EQUALS + current.getValue());
		}
		
		// for each entry append ", key=value" to the string
		while(iter.hasNext()) {
			var current = iter.next();
			sb.append(COMMA + " " + current.getKey() + EQUALS + current.getValue());
		}
		
		sb.append(RIGHT_BRACKET);
		return sb.toString();
	}
	
	@Override
	public Iterator<TableEntry<K, V>> iterator() {
		return new IteratorImpl();
	}
	
	/**
	 * Returns true if more than 75% of capacity
	 * of {@link SimpleHashTable} is filled.
	 * 
	 * @return true if table is overfilled; false otherwise
	 */
	private boolean isOverfilled() {
		return (double)size/capacity >= OVERFILLED_PERCENTAGE;
	}
	
	/**
	 * Doubles the capacity of {@link SimpleHashTable} by
	 * creating new array with double the size of original one
	 * and adding all entries to it.
	 */
	@SuppressWarnings("unchecked")
	private void doubleTheCapacity() {
		if(!isOverfilled()) {
			return;
		}
		
		// current table
		TableEntry<K,V>[] oldTable = this.table;
		//new table
		this.table = new TableEntry[this.capacity * 2];
		this.capacity *= 2;
		
		for(int i = 0; i < capacity/2; i++) {
			// if slot is empty, continue 
			if(oldTable[i] == null) {
				continue;
			} 
				
			var current = oldTable[i];

			// add all entries from current slot to the new table
			while(true) {
				// decrease size because put will increase it
				size--;
				put(current.getKey(), current.getValue());
				
				if(current.next == null) {
					break;
				}
						
				current = current.next;
			}	
		}
	}
	
	/**
	 * Returns entry with given key from its slot. 
	 * 
	 * @param key key of an entry
	 * @return entry with given key; null otherwise
	 */
	private TableEntry<K, V> findInSlot(Object key) {
		int slot = Math.abs(key.hashCode()) % capacity;

		// if slot is empty, return null
		if(table[slot] == null) {
			return null;
		} 
			
		var current = table[slot];
		
		// find entry with given key or return null if end of slot is reached
		while(true) {
			if(current.getKey().equals(key)) {
				return current;
			}
				
			if(current.next == null) {
				return null;
			}
					
			current = current.next;
		}	
	}
	
	/**
	 * Calculates smallest power of 2 greater than or equal to
	 * given number.
	 * 
	 * @param number number used for calculating 
	 * @return smallest power of 2 greater than or equal to
	 * 			given number
	 * @throws IllegalArgumentException if given number is less than 1
	 */
	private static int roundToPowerOfTwo(int number) throws IllegalArgumentException {
		if(number < 1) {
			throw new IllegalArgumentException("Capacity of hash table should be bigger than 1!");
		}
		// calculates base - 2 log of given number
		double log = Math.log(number) / Math.log(2);
		
		// returns 2^N where N is smallest integer greater than or equal to logs
		return (int)Math.ceil(Math.pow(2, Math.ceil(log)));		
	}
	
}