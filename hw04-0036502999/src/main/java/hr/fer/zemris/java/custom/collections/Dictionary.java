package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * This class represents collection made of (Key, Value) pairs
 * called entries. Keys in entries are unique, non-null and 
 * unchangeable, while values can be null and can be changed 
 * once entry is created. 
 * 
 * <p>It uses {@link ArrayIndexedCollection} in its implementation 
 * which stores instances of {@link Entry} class. </p>
 * 
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 * @param <K> The type of a key in (Key, Value) entry
 * @param <V> The type of a value in (Key, Value) entry
 */
public class Dictionary<K,V> {
	
	private ArrayIndexedCollection<Entry> collection;
	
	/**
	 * This class represents one entry of a {@link Dictionary} 
	 * collection. It consists of key and value pairs in which
	 * keys are unique, non-null and unchangeable. 
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 *
	 * @param <K> The type of key in (Key, Value) entry
	 * @param <V> The type of value in (Key, Value) entry
	 */
	private class Entry {
		
		private K key;
		private V value;
		
		/**
		 * Creates new Entry for {@link Dictionary}.
		 * 
		 * @param key key in (Key, Value) entry
		 * @param value value in (Key, Value) entry
		 * @throws NullPointerException if given key is null
		 */
		public Entry(K key, V value) throws NullPointerException{
			this.key = Objects.requireNonNull(key);
			this.value = value;
		}

		/**
		 * Returns key of an (Key, Value) entry.
		 * 
		 * @return key of an entry
		 */
		public K getKey() {
			return key;
		}
		
		/**
		 * Returns value of an (Key, Value) entry.
		 * 
		 * @return value of an entry
		 */
		public V getValue() {
			return value;
		}

		/**
		 * Sets value of an (Key, Value) entry.
		 * 
		 * @param value value to set in an entry
		 */
		public void setValue(V value) {
			this.value = value;
		}
		
	}

	/**
	 * Creates new dictionary.
	 */
	public Dictionary() {
		this.collection = new ArrayIndexedCollection<Entry>();
	}

	/**
	 * Returns true if dictionary is empty, false otherwise.
	 * 
	 * @return true if dictionary is empty; false otherwise
	 */
	public boolean isEmpty() {
		return collection.isEmpty();
	}

	/**
	 * Returns number of entries in dictionary.
	 * 
	 * @return size of a dictionary
	 */
	public int size() {
		return collection.size();
	}
	
	/**
	 * Removes all entries from dictionary.
	 */
	public void clear() {
		collection.clear();
	}
	
	/**
	 * Adds entry to dictionary. If there is already entry with 
	 * same key in dictionary it will be replaced by this one.
	 * 
	 * @param key key of a new entry
	 * @param value value of a new entry
	 * @throws NullPointerException if given key is null
	 */
	public void put(K key, V value) {
		Objects.requireNonNull(key);
		
		Entry e = getEntry(key);
		if(e != null) {
			e.setValue(value);
			return;
		}
				
		collection.add(new Entry(key, value));
	}

	/**
	 * Returns value of an entry defined by given key. If there is
	 * no entry with given key in dictionary null will be returned. 
	 * 
	 * @param key key of an entry
	 * @return value of an entry
	 * @throws NullPointerException if given key is null
	 */
	public V get(Object key) {
		Objects.requireNonNull(key);
		
		Entry e = getEntry(key);
		if(e != null) {
			return e.getValue();
		}
		
		return null;
	}
	
	/**
	 * Returns {@link Entry} with given key.
	 * 
	 * @param key key of an entry
	 * @return entry with given key; null if entry does not exist
	 * @throws NullPointerException if given key is null
	 */
	private Entry getEntry(Object key) {
		Objects.requireNonNull(key);
		ElementsGetter<Entry> getter = collection.createElementsGetter();
		
		while(getter.hasNextElement()) {
			Entry e = getter.getNextElement();
			
			if (e.getKey().equals(key)) {
				return e;
			}
		}
		
		return null;
	}
	
}
