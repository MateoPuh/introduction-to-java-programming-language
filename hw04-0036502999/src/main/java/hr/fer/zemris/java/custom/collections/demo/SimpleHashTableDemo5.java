package hr.fer.zemris.java.custom.collections.demo;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

import hr.fer.zemris.java.custom.collections.SimpleHashTable;

/**
 * This class demonstrates usage of {@link SimpleHashTable}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class SimpleHashTableDemo5 {

	/**
	 * This method is first run when program is run.
	 * It demonstrates usage of {@link SimpleHashTable}
	 * and its Iterator.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		// create collection:
		SimpleHashTable<String,Integer> examMarks = new SimpleHashTable<>(2);
		// fill data:
		examMarks.put("Ivana", 2);
		examMarks.put("Ante", 2);
		examMarks.put("Jasna", 2);
		examMarks.put("Kristina", 5);
		examMarks.put("Ivana", 5); // overwrites old grade for Ivana
		
		Iterator<SimpleHashTable.TableEntry<String,Integer>> iter = examMarks.iterator();
		try {
			while(iter.hasNext()) {
				SimpleHashTable.TableEntry<String,Integer> pair = iter.next();
				if(pair.getKey().equals("Ivana")) {
					examMarks.remove("Ivana");
				}
			}
		} catch(ConcurrentModificationException e) {
			System.out.println(e.getMessage());
		}
	}
	
}
