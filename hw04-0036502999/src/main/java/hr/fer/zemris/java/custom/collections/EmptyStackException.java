package hr.fer.zemris.java.custom.collections;

/**
 * This class represents an exception which is used in ObjectStack
 * class. It is thrown when user tries to pop or peek stack when 
 * stack is empty.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 */
public class EmptyStackException extends RuntimeException {
	
	private static final long serialVersionUID = 1L; 
	
	/**
	 * Creates new EmptyStackException.
	 */
	public EmptyStackException() {
		
	}
	
	/**
	 * Creates new EmptyStackException with a given message.
	 * 
	 * @param message exception message
	 */
	public EmptyStackException(String message) {
		super(message);
	}
	
	/**
	 * Creates new EmptyStackException with a given cause.
	 * 
	 * @param cause exception cause
	 */
	public EmptyStackException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates new EmptyStackException with a given message and cause.
	 * 
	 * @param message exception message
	 * @param cause exception cause
	 */
	public EmptyStackException(String message, Throwable cause) {
		super(message, cause);
	}

}
