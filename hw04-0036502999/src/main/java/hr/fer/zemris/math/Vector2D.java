package hr.fer.zemris.math;

import java.util.Objects;

/**
 * This class represents vector in 2D space. It
 * is represented by two coordinates: x and y, which 
 * are real numbers.
 * 
 * <p>It contains methods for translating, rotating 
 * and scaling 2D vectors.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Vector2D {

	/** Maximal difference between two doubles to consider them equal */
	private static final double EPSILON = 1e-6;
	
	private double x;
	private double y;
	
	/**
	 * Creates new {@link Vector2D} with given x and y
	 * coordinates.
	 * 
	 * @param x x coordinate of 2D vector
	 * @param y y coordinate of 2D vector
	 */
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Returns x coordinate of 2D vector.
	 * 
	 * @return x coordinate
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * Returns y coordinate of 2D vector.
	 * 
	 * @return y coordinate
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * Translates the vector by given offset.
	 * 
	 * @param offset offset which is used to translate vector
	 */
	public void translate(Vector2D offset) {
		this.x += offset.x;
		this.y += offset.y;
	}
	
	/**
	 * Returns new 2D vector which is result
	 * of translation of this vector by given offset.
	 * 
	 * @param offset offset which is used to translate vector
	 * @return new vector which is translated
	 */
	public Vector2D translated(Vector2D offset) {
		return new Vector2D(this.x + offset.x, this.y + offset.y);
	}
	
	/**
	 * Rotates the vector by given angle.
	 * 
	 * @param angle angle of rotation of vector
	 */
	public void rotate(double angle) {
		// if x == 0 y/x causes exception, so x = PI/2
		// if y < 0 then x = -PI/2, else it is PI/2
		double theta = (this.x != 0) ? Math.atan(this.y / this.x) 
				: (this.y > 0) ? (Math.PI / 2) : -(Math.PI / 2);
		double magnitude = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
		
		this.x = magnitude * Math.cos(theta + angle);
		this.y = magnitude * Math.sin(theta + angle);
	}
	
	/**
	 * Returns new vector which is result of rotating 
	 * this vector by given angle.
	 * 
	 * @param angle angle of rotation
	 * @return new vector which is rotated
	 */
	public Vector2D rotated(double angle) {
		Vector2D vector = new Vector2D(this.x, this.y);
		vector.rotate(angle);
		
		return vector;
	}
	
	/**
	 * Scales the vector by given scaler.
	 * 
	 * @param scaler scaler which is used for scaling
	 */
	public void scale(double scaler) {
		this.x *= scaler;
		this.y *= scaler;
	}
	
	/**
	 * Returns new vector which is result of 
	 * scaling this vector by given scaler.
	 * 
	 * @param scaler scaler used for scaling
	 * @return new vector which is scaled
	 */
	public Vector2D scaled(double scaler) {
		Vector2D vector = new Vector2D(this.x, this.y);
		vector.scale(scaler);
		
		return vector;
	}
	
	/**
	 * Returns the copy of this vector.
	 * 
	 * @return copy of this vector
	 */
	public Vector2D copy() {
		return new Vector2D(this.x, this.y);
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vector2D))
			return false;
		Vector2D other = (Vector2D) obj;
		return Math.abs(x - other.x) < EPSILON
				&& Math.abs(y - other.y) < EPSILON;
	}
	
}
