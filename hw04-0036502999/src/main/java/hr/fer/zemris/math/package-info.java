﻿/**
 * This package provides class which can be used
 * for performing various mathematical operations
 * on vectors in 2D.
 *  
 * @author Mateo Puhalović
 * @version 1.0
 */
package hr.fer.zemris.math;