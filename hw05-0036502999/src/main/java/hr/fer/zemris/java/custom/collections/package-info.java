﻿/**
 * This package provides two generic interfaces which can be used
 * for manipulating collections: Collection and List. Collection
 * provides methods for manipulating elements of collection, such as
 * methods for adding and removing elements. List extends Collection 
 * and offers additional methods. 
 * 
 * <p> There are two collection implementations: ArrayIndexedCollection 
 * which implements resizable array-backed collection of objects
 * and LinkedListIndexedCollection which implements linked list-backed 
 * collection of objects. </p>
 * 
 * <p> Two additional collection, which operate with entries consisting of
 * key and value pairs, included in this package are Dictionary, 
 * which represents collection of entries in form of List and 
 * SimpleHashMap which represents collection of entries in form of
 * HashTable. </p>
 * 
 * <p> This package also includes ElementsGetter class which is used
 * as an iterator in Collections. </p>
 * 
 * <p> It also includes ObjectStack and EmptyStackException
 * classes. ObjectStack class is used as stack and acts as 
 * an interface between user and Collection, providing usual
 * methods used in stack. EmptyStackException is thrown if
 * user tries to pop object when stack is empty. </p>
 * 
 * <p> Processor class is also provided in this package. It 
 * represents model of an object capable of performing some 
 * operation on the passed object. </p>
 * 
 * <p> Tester class is also provided in this package. It 
 * represents object whose task is to test whether the 
 * object it received is acceptable or not. </p>
 *  
 * @author Mateo Puhalović
 * @version 2.0
 */
package hr.fer.zemris.java.custom.collections;