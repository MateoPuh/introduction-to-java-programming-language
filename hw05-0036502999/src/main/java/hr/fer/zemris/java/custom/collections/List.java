package hr.fer.zemris.java.custom.collections;

/**
 * This interface extends {@link Collection} and provides
 * extra methods which are used in {@link ArrayIndexedCollection} 
 * and {@link LinkedListIndexedCollection}.
 * 
 * @param <T> The type of objects stored in List
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public interface List<T> extends Collection<T> {

	/**
	 * Returns element at given index. 
	 * 
	 * @param index index of element that is searched for
	 * @return element at given index
	 * @throws IndexOutOfBoundsException if given index is not in [0, size - 1]
	 */
	T get(int index) throws IndexOutOfBoundsException;
	
	/**
	 * Inserts element with given value at the given position. 
	 * 
	 * @param value value to insert in collection
	 * @param position position on which to insert new element
	 * @throws IndexOutOfBoundsException if given position is not in [0, size]
	 * @throws NullPointerException if given value is null
	 */
	void insert(T value, int position) throws IndexOutOfBoundsException, NullPointerException;
	
	/**
	 * Returns index of element with given value. Uses method equals 
	 * for finding which element has given value. Since collection can
	 * have duplicate elements, this implementation returns index of 
	 * first element with given value.
	 * 
	 * @param value value whose index is being searched
	 * @return index of element with given value; -1 if not found
	 */
	int indexOf(Object value);
	
	/**
	 * Removes element at given index of collection. It moves part of array that 
	 * starts at (index + 1) 1 index lower than they were before.
	 * 
	 * @param index index of element to remove
	 * @throws IndexOutOfBoundsException if given index is not in [0, size - 1]
	 */
	void remove(int index) throws IndexOutOfBoundsException;
	
}
