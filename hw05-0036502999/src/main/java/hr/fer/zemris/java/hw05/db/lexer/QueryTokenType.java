package hr.fer.zemris.java.hw05.db.lexer;

/**
 * Enumeration of {@link QueryToken} types.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public enum QueryTokenType {

	/** String inside a quotation marks. */
	STRING,
	
	/** String with no quoatation marks. */
	ATTRIBUTE,
	
	/** Word "and". Case insensitive. */
	KEY_WORD_AND,
	
	/** Operator for comparison. */
	OPERATOR,
	
	/** End of query. */
	EOF
	
}
