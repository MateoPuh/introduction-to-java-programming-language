package hr.fer.zemris.java.custom.collections;

/**
 * This interface represents object whose task is to test whether the 
 * object it received is acceptable or not. It has one method: test(Object)
 * which is used for testing received objects.
 *
 * @param <T> The type of objects which will be tested
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public interface Tester<T> {
	
	/**
	 * This method is used for testing received objects. It returns
	 * true if object is acceptable.
	 * 
	 * @param obj object to test whether is acceptable or not
	 * @return true if object is acceptable; false otherwise
	 */
	boolean test(T obj);

}
