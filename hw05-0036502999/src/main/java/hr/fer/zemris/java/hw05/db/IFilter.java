package hr.fer.zemris.java.hw05.db;

/**
 * This class represents filter for {@link StudentRecord} objects.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
@FunctionalInterface
public interface IFilter {
	
	/**
	 * Returns true if filter accepts the student record.
	 * 
	 * @param record student record to filter
	 * @return true if record is accepted; false otherwise
	 */
	public boolean accepts(StudentRecord record);
	
}
