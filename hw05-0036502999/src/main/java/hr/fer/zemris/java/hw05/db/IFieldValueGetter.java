package hr.fer.zemris.java.hw05.db;

/**
 * This interface is used for getting specific field
 * in {@link StudentDatabase}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
@FunctionalInterface
public interface IFieldValueGetter {
	
	/**
	 * Returns value of specific field from given {@link StudentRecord}.
	 * 
	 * @param record student record
	 * @return value of a specific field in record
	 */
	public String get(StudentRecord record);
	
}
