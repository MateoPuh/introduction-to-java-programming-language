package hr.fer.zemris.java.hw05.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * This class represents database of {@link StudentRecord} objects.
 * 
 * <p>It receives database in constructor in form of String array in which
 * each line represents one row. Additionally, it creates an index 
 * for jmbag attribute in for of {@link Map}.</p>
 * 
 * <p>It does not allow for duplicate jmbag and grades outside of
 * range [1, 5]. </p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class StudentDatabase {

	/** Minimal number of tokens in a row. */
	private static final int MIN_NUM_OF_TOKENS = 4;
	
	/** Minimal grade. */
	private static final int MIN_GRADE = 1;
	
	/** Maximal grade. */
	private static final int MAX_GRADE = 5;
	
	/** List of students. Represents database. */
	List<StudentRecord> students;
	
	/** Index for jmbag attribute. */
	Map<String, StudentRecord> indexedStudents;
	
	/**
	 * Creates new {@link StudentDatabase} with given
	 * String array. It checks validity of given data
	 * (meaning it check if jmbags are unique and if
	 * grades are in range [1, 5]).
	 * 
	 * @param database String array that represents rows of database
	 * @throws NullPointerException if given array is null
	 * @throws IllegalArgumentException if any field in a row is invalid 
	 */
	public StudentDatabase(String[] database) throws IllegalArgumentException {
		Objects.requireNonNull(database);
		
		students = new ArrayList<>();	
		indexedStudents = new HashMap<>();
		
		for(String row : database) {
			String[] data = row.split("\\s+");
						
			// there can be 4 or more tokens in row
			// more than 4 tokens are for students with multiple last names
			if(data.length < MIN_NUM_OF_TOKENS) {
				throw new IllegalArgumentException("Wrong number of attributes in row!");
			}
			
			// check if given jmbag already exists
			if(indexedStudents.get(data[0]) != null) {
				throw new IllegalArgumentException("Student with jmbag " + data[0] + " already exists!");
			}
			
			try {
				// grade is last token in a row
				int grade = Integer.parseInt(data[data.length - 1]);
				
				// check if grade is between 1 and 5
				if(grade < MIN_GRADE || grade > MAX_GRADE) {
					throw new IllegalArgumentException("Grade " + grade + " is not in valid range [1, 5]!");
				}
				
				// last name is second token
				String lastName = data[1];
				
				// add other student's last names if they exists
				for (int i = 2; i < data.length - 2; i++) {
					lastName += " " + data[i];
				}
				
				StudentRecord student = new StudentRecord(data[0], data[data.length - 2], lastName, grade);
				students.add(student);
				indexedStudents.put(data[0], student);
			} catch(NumberFormatException e) {
				throw new IllegalArgumentException(e.getMessage());
			}	
		}
	}
	
	/**
	 * Returns {@link StudentRecord} with given jmbag.
	 * 
	 * @param jmbag jmbag of a student
	 * @return student with given jmbag
	 */
	public StudentRecord forJMBAG(String jmbag) {
		return indexedStudents.get(jmbag);
	}
	
	/**
	 * Filters the database with given {@link IFilter} and 
	 * returns List of {@link StudentRecord} objects.
	 * 
	 * @param filter filter used for filtering database
	 * @return list of students
	 */
	public List<StudentRecord> filter(IFilter filter){
		return students.stream().filter(filter::accepts).collect(Collectors.toList());
	}
	
}
