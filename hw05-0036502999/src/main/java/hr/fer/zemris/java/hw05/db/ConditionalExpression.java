package hr.fer.zemris.java.hw05.db;

/**
 * This class represents the conditional expression.
 * It uses {@link IFieldValueGetter} to get value of 
 * a given field, {@link IComparisonOperator} to get 
 * comparator and String literal which is String that
 * field is compared to.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ConditionalExpression {

	/** Gets the specific field. */
	private IFieldValueGetter getter;
	
	/** Value that field is compared to. */
	private String literal;
	
	/** Comparator used for comparing field and given value.*/
	private IComparisonOperator comparator;

	/**
	 * Creates new {@link ConditionalExpression} with given
	 * {@link IFieldValueGetter}, {@link IComparisonOperator}
	 * and String value that field is compared to.
	 * 
	 * @param getter gets the specific field
	 * @param literal value that field is compared to
	 * @param comparator comparator used for comparing 
	 */
	public ConditionalExpression(IFieldValueGetter getter, String literal, IComparisonOperator comparator) {
		this.getter = getter;
		this.literal = literal;
		this.comparator = comparator;
	}

	/** 
	 * Returns {@link IFieldValueGetter} getter of field.
	 * 
	 * @return getter of field
	 */
	public IFieldValueGetter getGetter() {
		return getter;
	}

	/**
	 * Returns value that field is compared to.
	 * 
	 * @return value for comparison
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns comparator used for comparing.
	 * 
	 * @return comparator
	 */
	public IComparisonOperator getComparator() {
		return comparator;
	}
	
}
