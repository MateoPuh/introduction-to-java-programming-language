package hr.fer.zemris.java.hw05.db.lexer;

import java.util.Arrays;
import java.util.Objects;

import hr.fer.zemris.java.hw05.db.QueryParser;

/**
 * This class represents Lexer used for generating {@link QueryToken} objects
 * for {@link QueryParser}. 
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class QueryLexer {

	/** Key word and. */
	private static final String AND = "AND";
	
	/** Quotation mark. */
	private static final String QUOTATION_MARK = "\"";
	
	/** Exclamation mark. */
	private static final String EXCLAMATION_MARK = "!";
			
	/** Equals operator (=). */
	private static final String EQUALS = "=";
		
	/** Less operator (<). */
	private static final String LESS = "<";
	
	/** Greater operator (>). */
	private static final String GREATER = ">";
	
	/** LIKE operator. */
	private static final String LIKE = "LIKE";
	
	/** Query as array of characters. */
	private char[] query;
	
	/** Last generated token. */
	private QueryToken token;
	
	/** Index of currently analyzed character. */
	private int currentIndex;
	
	/**
	 * Creates new {@link QueryLexer} with given
	 * query as text.
	 * 
	 * @param text query text
	 */
	public QueryLexer(String text) {
		Objects.requireNonNull(text);
		this.query = text.toCharArray();
	}
	
	/**
	 * Returns last generated {@link QueryToken}.
	 * 
	 * @return last generated token
	 */
	public QueryToken getQueryToken() {
		return token;
	}
	
	/**
	 * Generates token.
	 * 
	 * @return new generated token
	 */
	public QueryToken nextQueryToken() {
		removeBlanks();
		
		// if current index is bigger that length of query 
		// then either generate EOF token
		// or throw exception if EOF is already generated
		if(currentIndex >= query.length) {
			if(token != null && token.getType().equals(QueryTokenType.EOF)) {
				throw new QueryLexerException("Error: Attempting to generate QueryToken after EOF!");
			} else {
				token = new QueryToken(QueryTokenType.EOF, null);
				return token;
			}
		}
		
		// if character on current index is quotation mark then generate string 
		if(String.valueOf(query[currentIndex]).equals(QUOTATION_MARK)) {
			QueryToken t = generateStringInQuotes();
						
			if(t != null) {
				return t;
			} else {
				throw new QueryLexerException("Cannot generate string.");
			}
		}
		
		// if character on current index is exclamation mark then try to generate not-equals operator
		// or throw exception
		if(String.valueOf(query[currentIndex]).equals(EXCLAMATION_MARK)) {
			if(currentIndex + 1 < query.length && String.valueOf(query[currentIndex + 1]).equals("=")) {
				token = new QueryToken(QueryTokenType.OPERATOR, 
						String.valueOf(Arrays.copyOfRange(query, currentIndex, currentIndex+2)));
				currentIndex += 2;
				return token;
			}
			
			throw new QueryLexerException("Cannot generate operator.");
		}
		
		// if character on current index is equals sign then generate equals operator
		if(String.valueOf(query[currentIndex]).equals(EQUALS)) {
			token = new QueryToken(QueryTokenType.OPERATOR, 
					String.valueOf(Arrays.copyOfRange(query, currentIndex, currentIndex+1)));
			currentIndex++;
			return token;
		}
		
		// if character on current index is less or greater sign
		// then first try to see if equals is after them
		// if not generate less or greater
		if((LESS + GREATER).contains(String.valueOf(query[currentIndex]))) {
			if(currentIndex + 1 < query.length && String.valueOf(query[currentIndex + 1]).equals(EQUALS)) {
				token = new QueryToken(QueryTokenType.OPERATOR, 
						String.valueOf(Arrays.copyOfRange(query, currentIndex, currentIndex+2)));
				currentIndex += 2;
				return token;
			}
			
			token = new QueryToken(QueryTokenType.OPERATOR, 
					String.valueOf(Arrays.copyOfRange(query, currentIndex, currentIndex+1)));
			currentIndex ++;
			return token;
		}
		
		// if character on current index is letter generate attribute
		if(Character.isLetter(query[currentIndex])) {
			QueryToken t = generateAttribute();
			
			if(t == null) {
				throw new QueryLexerException("Cannot generate attribute.");
			} else {
				return t;
			}
		}
		
		throw new QueryLexerException("Cannot generate token");
	}
	
	/**
	 * Generates token of type attribute or returns null
	 * 
	 * @return token or null
	 */
	private QueryToken generateAttribute() {
		removeBlanks();
		int startIndex = currentIndex;
		
		if(currentIndex < query.length && Character.isLetter(query[currentIndex])) {
			currentIndex++;
		}
		
		while(currentIndex < query.length 
				&& (Character.isLetter(query[currentIndex]) || Character.isDigit(query[currentIndex]))) {
			currentIndex++;
		}
		
		if(startIndex < currentIndex) {
			String s = String.valueOf(Arrays.copyOfRange(query, startIndex, currentIndex));
			
			if(s.equalsIgnoreCase(AND)) {
				token = new QueryToken(QueryTokenType.KEY_WORD_AND, s);
			} else if(s.equals(LIKE)) {
				token = new QueryToken(QueryTokenType.OPERATOR, s);
			} else {
				token = new QueryToken(QueryTokenType.ATTRIBUTE, s);
			}
			
			return token;
		} else {
			return null;
		}
	}
	
	/**
	 * Generates token of string type or null.
	 * 
	 * @return token or null
	 */
	private QueryToken generateStringInQuotes() {
		int startIndex = currentIndex;
		
		if(currentIndex < query.length && String.valueOf(query[currentIndex]).equals(QUOTATION_MARK)) {
			currentIndex++;
			startIndex++;
		}
		
		while(currentIndex < query.length) { 
			if(String.valueOf(query[currentIndex]).equals(QUOTATION_MARK)) {
				token = new QueryToken(QueryTokenType.STRING, 
						String.valueOf(Arrays.copyOfRange(query, startIndex, currentIndex)));
				currentIndex++;
				return token;
			}
			
			currentIndex++;
		}
		
		return null;
	}
	
	/**
	 * Removes blanks from query text until next non-blank character.
	 */
	private void removeBlanks() {
		while(currentIndex < query.length && Character.isWhitespace(query[currentIndex])) {
			currentIndex++;
		}
	}
	
}
