package hr.fer.zemris.java.hw05.db;

import java.util.Objects;

/**
 * This class contains concrete implementations of 
 * {@link IComparisonOperator} interface. 
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ComparisonOperators {

	/** Wildcard * that represents any number of characters. */
	private static final String WILDCARD = "\\*";
	
	/** 
	 * Compares first value to the second with "less" comparator (<). 
	 * 
	 * @throws NullPointerException if any of given values are null
	 */
	public final static IComparisonOperator LESS = (v1, v2) -> (Objects.requireNonNull(v1).compareTo(Objects.requireNonNull(v2))) < 0;
	
	/** 
	 * Compares first value to the second with "less or equals" comparator (<=).
	 * 
	 * @throws NullPointerException if any of given values are null
	 */
	public final static IComparisonOperator LESS_OR_EQUALS = (v1, v2) -> (Objects.requireNonNull(v1).compareTo(Objects.requireNonNull(v2))) <= 0;
	
	/** 
	 * Compares first value to the second with "greater" comparator (>). 
	 * 
	 * @throws NullPointerException if any of given values are null
	 */
	public final static IComparisonOperator GREATER = (v1, v2) -> (Objects.requireNonNull(v1).compareTo(Objects.requireNonNull(v2))) > 0;
	
	/** 
	 * Compares first value to the second with "greater or equal" comparator (>=).
	 * 
	 * @throws NullPointerException if any of given values are null
	 */
	public final static IComparisonOperator GREATER_OR_EQUALS = (v1, v2) -> (Objects.requireNonNull(v1).compareTo(Objects.requireNonNull(v2))) >= 0;
	
	/** 
	 * Compares first value to the second with "equals" comparator (=). 
	 * 
	 * @throws NullPointerException if any of given values are null
	 */
	public final static IComparisonOperator EQUALS = (v1, v2) -> (Objects.requireNonNull(v1).compareTo(Objects.requireNonNull(v2))) == 0;
	
	/** 
	 * Compares first value to the second with "not equals" comparator (!=). 
	 * 
	 * @throws NullPointerException if any of given values are null 
	 */
	public final static IComparisonOperator NOT_EQUALS = (v1, v2) -> (Objects.requireNonNull(v1).compareTo(Objects.requireNonNull(v2))) != 0;
	
	/** 
	 * Compares first value to the second with "LIKE" comparator. 
	 * LIKE comparator compares first value to the regular expression
	 * given as a second value. That regular expression can contain
	 * wildcard * only once. 
	 * 
	 * @throws NullPointerException if any of given values are null
	 */
	public final static IComparisonOperator LIKE = (v1, v2) -> {
		Objects.requireNonNull(v1);
		String[] parts = (Objects.requireNonNull(v2)).split(WILDCARD, -1);
		
		if(parts.length > 2) {
			throw new IllegalArgumentException(v2 + " contains more than 1 wildcard!");
		}

		if(parts.length == 1) {
			// if there is no wildcard, compare them with equals
			return ComparisonOperators.EQUALS.satisfied(v1, v2);
		} else {
			// if there is wildcard value should begin with part before * and 
			// end with part after the *
			// and it should be bigger than the regex + 1
			return v1.startsWith(parts[0]) && (v1).endsWith(parts[1]) 
					&& v2.length() <= v1.length() + 1;
		}
	};

	
}
