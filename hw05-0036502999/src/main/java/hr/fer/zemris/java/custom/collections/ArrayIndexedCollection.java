package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * This class represents resizable array-backed collection.
 * This collection allows duplicate elements and doesn't allow
 * null references. 
 * 
 * <p> Implementation of this collection is based on resizable array
 * which means that it doubles its capacity every time it becomes full. </p>
 * 
 * <p> That is the reasons why complexity of add(Object) method is O(n) in 
 * worst case, but its average complexity is Θ(1). Average complexity of
 * method get(int) is also Θ(1). Average complexity of methods contains(Object), 
 * clear(), remove(int), remove(Object), addAll(Collection), insert(Object, int),
 * indexOf(Object) is Θ(n). </p>
 * 
 * <p> The capacity of this collection will initialy be set to default capacity
 * of 16 if capacity is not specified. It can be initialy filled with elements
 * of other collection if that collection is specified. </p>
 * 
 * @param <T> The type of objects stored in this collection
 *  
 * @author Mateo Puhalović
 * @version 1.1
 *
 */
public class ArrayIndexedCollection<T> implements List<T> {
	
	private int size;
	private T[] elements;
	private int capacity;
	
	private long modificationCount;
	
	/**
	 * {@link ElementsGetter} class used for {@link ArrayIndexedCollection}.
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 *
	 */
	private class ArrayElementsGetter implements ElementsGetter<T> {

		private int currentIndex;
		private ArrayIndexedCollection<T> collection;
		private long savedModificationCount;
		
		/**
		 * Creates new ArrayElementsGetter for given collection.
		 * 
		 * @param collection collection which will be used in {@link ElementsGetter}
		 */
		public ArrayElementsGetter(ArrayIndexedCollection<T> collection) {
			this.collection = collection;
			savedModificationCount = collection.modificationCount;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNextElement() {
			return currentIndex < collection.size();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public T getNextElement() throws NoSuchElementException, ConcurrentModificationException {
			if (savedModificationCount != collection.modificationCount) {
				throw new ConcurrentModificationException("Content of collection changed while using this ElementsGetter!");
			}
			
			if(this.hasNextElement()) {
				return collection.get(currentIndex++);
			} else {
				throw new NoSuchElementException("There is no next element!");
			}
		}
		
	}
	
	/**
	 * Default capacity of collection
	 */
	private static final int DEFAULT_CAPACITY = 16;
	
	/**
	 * Creates an empty collection with a default capacity of 16.
	 */
	public ArrayIndexedCollection() {
		this(DEFAULT_CAPACITY);
	}
	
	/**
	 * Creates an empty collection with a given initial capacity.
	 * 
	 * @param initialCapacity initial capacity of collection
	 */
	@SuppressWarnings("unchecked")
	public ArrayIndexedCollection(int initialCapacity) {
		if (initialCapacity < 1) {
			throw new IllegalArgumentException("Capacity should be more than 1!");
		}
		
		capacity = initialCapacity;
		elements = (T[])new Object[initialCapacity];	
	}
	
	/**
	 * Creates an collection filled with elements of given collection. 
	 * Its capacity will be set to default capacity of 16 or to size of collection if
	 * default capacity is not enough to fit all elements in.
	 * 
	 * @param collection collection which elements will be copied to new collection 
	 */
	public ArrayIndexedCollection(Collection<T> collection) {
		this (collection, DEFAULT_CAPACITY);
	}
	
	/**
	 * Creates an collection filled with elements of given collection.
	 * Its capacity will be set to given initial capacity or to size of collection if
	 * given capacity is not enough to fit all elements in.
	 * 
	 * @param collection collection which elements will be copied to new collection 
	 * @param initialCapacity initial capacity of collection
	 * @throws NullPointerException if given collection is null
	 */
	public ArrayIndexedCollection(Collection<T> collection, int initialCapacity) 
			throws NullPointerException{
	    this((initialCapacity > collection.size()) ? initialCapacity : collection.size());
		
		Objects.requireNonNull(collection, "Given collection should not be null!");
		addAll(collection);
	}
	
	@Override
	public int size() {
		return size;
	}
	
	/**
	 * Insert the element with given value to the end of collection. If 
	 * array is full before adding new element, it will first double its size
	 * and then proceed to add new element. 
	 * 
	 *  <p> Average complexity of this method is Θ(1). 
	 *  It is O(n) in worst case, which is only when array is doubling its size. </p>
	 * 
	 * @param value value to insert into collection
	 * @throws NullPointerException if given value is null
	 */
	@Override
	public void add(T value) throws NullPointerException {
	    Objects.requireNonNull(value, "Null value cannot be added to this collection!");

	    insert(value, size);	    
	}

	/**
	 * Doubles the capacity of array by creating the new array
	 * with double the capacity of original one and copying all 
	 * the elements in new array.
	 */
	private void doubleArrayCapacity() {
    	capacity *= 2;
    	elements = (T[])Arrays.copyOf(elements, capacity);
	}
	
	@Override
	public boolean contains(Object value) {
		if (value == null) {
			return false;
		}
		
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(value)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Removes given value from collection. Uses equals method to 
	 * determine if value is contained in collection. Since this collection
	 * can have duplicate values, this implementation removes first instance
	 * of element with given value.
	 * 
	 *  @param value value to remove from collection
	 *  @return true if value was removed from collection; false otherwise
	 */
	@Override
	public boolean remove(Object value) {
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(value)) {
				try {
					remove(i);
					return true;
				} catch (IndexOutOfBoundsException ex) {
					return false;
				}
			}
		}
		
		return false;
	}

	@Override
	public void remove(int index) throws IndexOutOfBoundsException {
		checkBounds(index);
		
		for (int i = index; i < size - 1; i++) {
			elements[i] = elements[i + 1];
		}
		modificationCount++;
		elements[size-1] = null;
		size --;	
	}
	
	@Override
	public int indexOf(Object value) {
		if (value == null) {
			return -1;
		}
		
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(value)) {
				return i;
			}
		}
		
		return -1;
	}
	
	@Override
	public T get(int index) throws IndexOutOfBoundsException {
		checkBounds(index);
				
		return elements[index];
	}
	
	@Override
	public Object[] toArray(){
		Object[] array = Arrays.copyOf(elements, size);
		return array;
	}

	@Override
	public void clear() {
		for (int i = 0; i < size - 1; i++) {
			elements[i] = null;
		}
		modificationCount++;
		size = 0;
	}
	
	@Override
	public void insert(T value, int position) throws IndexOutOfBoundsException, NullPointerException {
		Collection.checkBounds(position, 0, size);		
	    Objects.requireNonNull(value, "Null value cannot be added to this collection!");

		if (size + 1 > capacity) {
			doubleArrayCapacity();
		}
		
		for (int i = size; i > position; i--) {
			elements[i] = elements[i - 1];
		}
		
		elements[position] = value;
		modificationCount++;
		size++;
	}

	@Override
	public ElementsGetter<T> createElementsGetter() {
		return new ArrayElementsGetter(this);
	}
	
	/**
	 * Checks if given index is in [0, size]. Uses Collection.checkBounds(int, int, int) method
	 * with values 0 and size - 1;
	 * 
	 * @param index index to check 
	 * @throws IndexOutOfBoundsException if given index is not in [0, size - 1]
	 * @see Collection#checkBounds(int, int, int)
	 */
	private void checkBounds(int index) throws IndexOutOfBoundsException{
		Collection.checkBounds(index, 0, size - 1);
	}
	
}
