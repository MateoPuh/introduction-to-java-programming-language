package hr.fer.zemris.java.hw05.db;

import java.util.List;
import java.util.Objects;

/**
 * This class represents the {@link IFilter} which
 * uses list of {@link ConditionalExpression} objects
 * to filter the given record.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class QueryFilter implements IFilter {

	/** List of {@link ConditionalExpression} objects. */ 
	List<ConditionalExpression> expressions;
	
	/**
	 * Creates new {@link QueryFilter} with given
	 * list of {@link ConditionalExpression} objects.
	 * 
	 * @param expressions lsit of expressions
	 * @throws NullPointerException given list is null
	 */
	public QueryFilter(List<ConditionalExpression> expressions) {
		this.expressions = Objects.requireNonNull(expressions);
	}
	
	/**
	 * Filters the record based on internal list of 
	 * expressions. Returns true if record satisfy 
	 * all of the expressions.
	 * 
	 * @return true if accepted; false otherwise
	 */
	@Override
	public boolean accepts(StudentRecord record) {
		for(ConditionalExpression e : expressions) {
			if(!e.getComparator().satisfied(e.getGetter().get(record), e.getLiteral())) {
				return false;
			}
		}
		
		return true;
	}

}
