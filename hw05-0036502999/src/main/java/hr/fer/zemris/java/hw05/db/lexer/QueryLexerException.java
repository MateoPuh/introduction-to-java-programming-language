package hr.fer.zemris.java.hw05.db.lexer;

/**
 * Exception thrown for all errors in {@link QueryLexer}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class QueryLexerException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates new {@link QueryLexerException}.
	 */
	public QueryLexerException() {
		
	}
	
	/**
	 * Creates new {@link QueryLexerException} with given message.
	 */
	public QueryLexerException(String message) {
		super(message);
	}
	
	/**
	 * Creates new {@link QueryLexerException} with given cause.
	 */
	public QueryLexerException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates new {@link QueryLexerException} with given message and cause.
	 */
	public QueryLexerException(String message, Throwable cause) {
		super(message, cause);
	}

}
