package hr.fer.zemris.java.hw05.db;

/**
 * This class contains concrete implementations 
 * of {@link IFieldValueGetter} interface.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class FieldValueGetters {

	/** Gets the value of student's first name. */
	public final static IFieldValueGetter FIRST_NAME = r -> r.getFirstName();
	
	/** Gets the value of student's last name. */
	public final static IFieldValueGetter LAST_NAME = r -> r.getLastName();
	
	/** Gets the value of student's jmbag. */
	public final static IFieldValueGetter JMBAG = r -> r.getJmbag();

}
