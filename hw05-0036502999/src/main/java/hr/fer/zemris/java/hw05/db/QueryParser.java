package hr.fer.zemris.java.hw05.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw05.db.lexer.QueryLexer;
import hr.fer.zemris.java.hw05.db.lexer.QueryLexerException;
import hr.fer.zemris.java.hw05.db.lexer.QueryToken;
import hr.fer.zemris.java.hw05.db.lexer.QueryTokenType;

/**
 * This class represents parser of query statements.
 * It gets query (without "query" keyword) through constructor.
 * It uses {@link QueryLexer} to generate tokens and then uses them
 * to find out if query is direct (meaning it has only one expression
 * which is expression with jmbag field and equals comparator), to 
 * get the string in the direct query and to get query in form of
 * list of {@link ConditionalExpression} objects.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class QueryParser {

	/** Jmbag attribute. */
	private static final String JMBAG = "jmbag";
	
	/** LastName attribute. */
	private static final String LAST_NAME = "lastName";
	
	/** FirstName attribute. */
	private static final String FIRST_NAME = "firstName";
	
	/** Equals operator (=). */
	private static final String EQUALS = "=";
	
	/** Not-equals operator (!=). */
	private static final String NOT_EQUALS = "!=";
	
	/** Less operator (<). */
	private static final String LESS = "<";
	
	/** Less-or-equals operator (<=). */
	private static final String LESS_OR_EQUALS = "<=";
	
	/** Greater operator (>). */
	private static final String GREATER = ">";
	
	/** Greater-or-equals operator (>=). */
	private static final String GREATER_OR_EQUALS = ">=";
	
	/** LIKE operator. */
	private static final String LIKE = "LIKE";
	
	/** Query that is parsed. */
	private String query;

	/**
	 * Creates new {@link QueryParser} with given query
	 * as a String.
	 * 
	 * @param query query to parse
	 * @throws NullPointerException if given query is null
	 */
	public QueryParser(String query) {
		this.query = Objects.requireNonNull(query);
	}
	
	/**
	 * Returns true if query is direct (meaning it has only one expression
	 * which is expression with jmbag field and equals comparator).
	 * 
	 * @return true if query is direct; false otherwise
	 */
	public boolean isDirectQuery() {
		try {
			getQueriedJMBAG();
			return true;
		} catch(IllegalStateException e) {
			return false;
		}
	}
	
	/**
	 * Returns the string used in the direct query. Throws
	 * {@link IllegalStateException} if query is not direct.
	 * 
	 * @return string used in the direct query
	 * @throws IllegalStateException if given query is not direct
	 */
	public String getQueriedJMBAG() throws IllegalStateException {
		QueryLexer lexer = new QueryLexer(query);				
		
		try {
			// if first token is attribute jmbag
			if(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.ATTRIBUTE, JMBAG))) {
				// if second token is operator equals
				if(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.OPERATOR, EQUALS))) {
					// if third token is string
					if(lexer.nextQueryToken().getType().equals(QueryTokenType.STRING)) {
						String s = (String)lexer.getQueryToken().getValue();
						// if string is last token
						// return that string
						if(lexer.nextQueryToken().getType().equals(QueryTokenType.EOF)) {
							return s;
						}
					}
				}
			}
		} catch(QueryLexerException e) {
			throw new IllegalStateException("Query is not direct!");
		}
		
		// if query is not direct, throw exception
		throw new IllegalStateException("Query is not direct!");
	}

	/**
	 * Returns query in form of list of {@link ConditionalExpression} objects.
	 * Throws {@link IllegalStateException} if syntax is not valid (meaning
	 * the expression is not in form : (attribute operator "string") connected 
	 * with key word "and") or if attribute or operator is illegal.
	 * 
	 * @return list of conditional expressions representing query
	 * @throws IllegalStateException if syntax is invalid or if attribute or operator 
	 * 		are illegal
	 */
	public List<ConditionalExpression> getQuery() throws IllegalStateException {
		QueryLexer lexer = new QueryLexer(query);
		List<ConditionalExpression> list = new ArrayList<ConditionalExpression>();
		
		// expression should be in form: attribute operator string
		while(true) {
			try {
				if(lexer.nextQueryToken().getType().equals(QueryTokenType.ATTRIBUTE)) {
					IFieldValueGetter getter;
							
					// legal attributes are: jmbag, lastName, firstName
					switch((String)lexer.getQueryToken().getValue()) {
					case JMBAG:
						getter = FieldValueGetters.JMBAG;
						break;
					case LAST_NAME:
						getter = FieldValueGetters.LAST_NAME;
						break;
					case FIRST_NAME:
						getter = FieldValueGetters.FIRST_NAME;
						break;
					default:
						throw new IllegalStateException("Invalid attribute.");
					}
					
					if(lexer.nextQueryToken().getType().equals(QueryTokenType.OPERATOR)) {
						IComparisonOperator comparator;
							
						// legal operators are: =, !=, <, <=, >, >=, LIKE
						switch((String)lexer.getQueryToken().getValue()) {
						case EQUALS:
							comparator = ComparisonOperators.EQUALS;
							break;
						case NOT_EQUALS:
							comparator = ComparisonOperators.NOT_EQUALS;
							break;
						case GREATER:
							comparator = ComparisonOperators.GREATER;
							break;
						case LESS:
							comparator = ComparisonOperators.LESS;
							break;
						case GREATER_OR_EQUALS:
							comparator = ComparisonOperators.GREATER_OR_EQUALS;
							break;
						case LESS_OR_EQUALS:
							comparator = ComparisonOperators.LESS_OR_EQUALS;
							break;
						case LIKE:
							comparator = ComparisonOperators.LIKE;
							break;
						default:
							throw new IllegalStateException("Invalid operator.");
						}
						
						if(lexer.nextQueryToken().getType().equals(QueryTokenType.STRING)) {	
							list.add(new ConditionalExpression(
									getter, (String)lexer.getQueryToken().getValue(), comparator));
						
							// if key word "and" is after the expression continue looping
							if(lexer.nextQueryToken().getType().equals(QueryTokenType.KEY_WORD_AND)) {
								continue;
							// if end of query is reached return list
							} else if(lexer.getQueryToken().getType().equals(QueryTokenType.EOF)) {
								return list;
							}
						}
					}
				}
			} catch(QueryLexerException e) {
				throw new IllegalStateException("Cannot parse query: " + e.getMessage());
			}
			
			throw new IllegalStateException("Invalid syntax");
		}
	}
	
}
