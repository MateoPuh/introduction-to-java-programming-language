package hr.fer.zemris.java.hw05.db;

import java.util.Objects;

/**
 * This class represents one row in {@link StudentDatabase}.
 * It represents student with jmbag, first name, last name and 
 * final grade.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class StudentRecord {

	/** Student's jmbag. */
	private String jmbag;
	
	/** Student's first name. */
	private String firstName;
	
	/** Student's last name. */
	private String lastName;
	
	/** Student's final grade. */
	private int finalGrade;
	
	/**
	 * Creates new {@link StudentRecord} with given jmbag, first name, 
	 * last name and final grade.
	 * 
	 * @param jmbag student's jmbag
	 * @param firstName student's first name
	 * @param lastName student's last name
	 * @param finalGrade student's final grade
	 * @throws NullPointerException if given parameter is null
	 */
	public StudentRecord(String jmbag, String firstName, String lastName, int finalGrade) {
		this.jmbag = Objects.requireNonNull(jmbag);
		this.firstName = Objects.requireNonNull(firstName);
		this.lastName = Objects.requireNonNull(lastName);
		this.finalGrade = finalGrade;
	}
	
	/**
	 * Returns student's jmbag.
	 * 
	 * @return stundet's jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * Returns student's first name.
	 * 
	 * @return student's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Returns student's last name.
	 * 
	 * @return student's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Returns student's final grade.
	 * 
	 * @return student's final grade
	 */
	public int getFinalGrade() {
		return finalGrade;
	}

	@Override
	public int hashCode() {
		return Objects.hash(jmbag);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof StudentRecord))
			return false;
		StudentRecord other = (StudentRecord) obj;
		return Objects.equals(jmbag, other.jmbag);
	}
	
}
