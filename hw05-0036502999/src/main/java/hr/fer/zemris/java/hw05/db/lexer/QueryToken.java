package hr.fer.zemris.java.hw05.db.lexer;

import java.util.Objects;

/**
 * This class represents token that is generated in 
 * {@link QueryLexer}. It can be of {@link QueryTokenType} type
 * and it can have String value.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class QueryToken {

	/** Type of token. */
	private QueryTokenType type;
	
	/** Value of token. */
	private String value;
	
	/**
	 * Creates new {@link QueryToken} with given type and value.
	 * 
	 * @param type type of token
	 * @param value value of token
	 */
	public QueryToken(QueryTokenType type, String value) {
		this.type = type;
		this.value = value;
	}

	/**
	 * Returns type of token.
	 * 
	 * @return type of token
	 */
	public QueryTokenType getType() {
		return type;
	}

	/**
	 * Returns value of token.
	 * 
	 * @return value of token
	 */
	public String getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof QueryToken))
			return false;
		QueryToken other = (QueryToken) obj;
		return type == other.type && Objects.equals(value, other.value);
	}

}
