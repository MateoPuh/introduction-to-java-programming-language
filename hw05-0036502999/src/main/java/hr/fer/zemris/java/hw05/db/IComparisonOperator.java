package hr.fer.zemris.java.hw05.db;

/**
 * This functional interface represents comparison operator
 * with method satisfied which compares two given values.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
@FunctionalInterface
public interface IComparisonOperator {
	
	/**
	 * Compares two given values.
	 * 
	 * @param value1 first value
	 * @param value2 second value
	 * @return true if values satisfy comparison; false otherwise
	 */
	public boolean satisfied(String value1, String value2);

}
