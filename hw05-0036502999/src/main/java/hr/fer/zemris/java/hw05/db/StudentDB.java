package hr.fer.zemris.java.hw05.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.Scanner;

/**
 * This class loads the database from file and given user
 * terminal to write queries and then prints user table
 * with filtered database.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class StudentDB {

	/** Number of spaces in table columns. */
	private static final int OFFSET = 2;
	
	/** Divider between columns. */
	private static final String ROW_DIVIDER = "|";
	
	/** Divider between columns on edge. */
	private static final String EDGE_DIVIDER = "+";
	
	/** Elements that edge of table is made of. */
	private static final String EDGE_ELEMENT = "=";
	
	/** String that represents terminal. */
	private static final String TERMINAL = ">";
	
	/** Query key word. */
	private static final String QUERY = "query";
	
	/** Exit key word. */
	private static final String EXIT = "exit";
	
	/** Path to file with database. */
	private static final String FILENAME = "./src/main/resources/database.txt";

	/**
	 * This method is first run when program is started.
	 * It loads database from specified file and offers terminal
	 * to user in which he can enter queries and exit the terminal.
	 * Terminal prints table with filtered database on console.
	 * 
	 * @param args command line arguments.
	 */
	public static void main(String[] args) {
		try{
			// load file as a list of lines
			List<String> lines = Files.readAllLines(
				 Paths.get(FILENAME),
				 StandardCharsets.UTF_8
				);
			
			// cast list to array
			String[] entries = new String[lines.size()];
			
			for(int i = 0; i < lines.size(); i++) {
				entries[i] = lines.get(i);
			}
			
			// create new database
			StudentDatabase db = new StudentDatabase(entries);
			
			Scanner sc = new Scanner(System.in);
			
			while(true) {
				System.out.print(TERMINAL);
				if(sc.hasNext()) {
					String line = sc.nextLine().trim();
					
					// if user entered query key word
					if(line.startsWith(QUERY)) {
						String query = line.substring(QUERY.length()).trim();
						
						QueryParser parser = new QueryParser(query);

						String recordsSelected = "Records selected: ";
						try {
							// is query is direct then use index
							if(parser.isDirectQuery()) {
								System.out.println("Using index for record retrieval.");
								StudentRecord r = db.forJMBAG(parser.getQueriedJMBAG());
								printTable(r);
								recordsSelected += 1;
							} else {
								// else filter database with given query
								List<StudentRecord> students = new ArrayList<StudentRecord>();
								for(StudentRecord r : db.filter(new QueryFilter(parser.getQuery()))) {
									students.add(r);
								}
								
								printTable(students);
								recordsSelected += students.size();
							}
							
							System.out.println(recordsSelected);
						} catch(Exception e) {
							System.out.println("Error: " + e.getMessage());
						}
					// if user entered exit key word then exit
					} else if(line.equals(EXIT)) {
						System.out.println("Goodbye!");
						sc.close();
						System.exit(0);
					} else {
						// if user entered invalid keyword
						System.out.println("Invalid command " + line);
					}
				}
			}
			
		} catch(IOException e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Prints table with a single student.
	 * 
	 * @param student student to print in table
	 */
	private static void printTable(StudentRecord student) {
		List<StudentRecord> students = new ArrayList<StudentRecord>();
		students.add(student);
		printTable(students);
	}
	
	/**
	 * Prints list of students in a table.
	 * 
	 * @param students list of students
	 */
	private static void printTable(List<StudentRecord> students) {
		if(students.size() == 0) {
			return;
		}
		
		OptionalInt maxJmbag = students.stream().map(s -> s.getJmbag()).mapToInt(jmbag -> jmbag.length()).max();
		OptionalInt maxLastName = students.stream().map(s -> s.getLastName()).mapToInt(l -> l.length()).max();
		OptionalInt maxFirstName = students.stream().map(s -> s.getFirstName()).mapToInt(f -> f.length()).max();
		OptionalInt maxGrade = students.stream().map(s -> String.valueOf(s.getFinalGrade())).mapToInt(l -> l.length()).max();
				
		printEdge(maxJmbag.getAsInt(), maxLastName.getAsInt(), maxFirstName.getAsInt(), maxGrade.getAsInt());
		for(StudentRecord s : students) {
			printRow(maxJmbag.getAsInt(), maxLastName.getAsInt(), maxFirstName.getAsInt(), maxGrade.getAsInt(), s);
		}
		printEdge(maxJmbag.getAsInt(), maxLastName.getAsInt(), maxFirstName.getAsInt(), maxGrade.getAsInt());
	}
	
	/**
	 * Prints edge of a table (upper of lower).
	 * 
	 * @param maxJmbag number of spaces for jmbag field
	 * @param maxLastName number of spaces for lastName field
	 * @param maxFirstName number of spaces for firstName field
	 * @param maxGrade number of spaces for grade field
	 */
	private static void printEdge(int maxJmbag, int maxLastName, int maxFirstName, int maxGrade) {
		System.out.print(EDGE_DIVIDER);
		printEdgeColumn(maxJmbag);
		System.out.print(EDGE_DIVIDER);
		printEdgeColumn(maxLastName);
		System.out.print(EDGE_DIVIDER);
		printEdgeColumn(maxFirstName);
		System.out.print(EDGE_DIVIDER);
		printEdgeColumn(maxGrade);
		System.out.println(EDGE_DIVIDER);
	}
	
	/**
	 * Prints one column on edge.
	 * 
	 * @param max number of spaces for column
	 */
	private static void printEdgeColumn(int max) {
		for(int i = 0; i < max + OFFSET; i++) {
			System.out.print(EDGE_ELEMENT);
		}
	}
	
	/**
	 * Prints one row of the table.
	 * 
	 * @param maxJmbag number of spaces for jmbag field
	 * @param maxLastName number of spaces for lastName field
	 * @param maxFirstName number of spaces for firstName field
	 * @param maxGrade number of spaces for grade field
	 * @param s student
	 */
	private static void printRow(int maxJmbag, int maxLastName, int maxFirstName, int maxGrade, StudentRecord s) {
		System.out.print(ROW_DIVIDER);
		printColumn(maxJmbag, s.getJmbag());
		System.out.print(ROW_DIVIDER);
		printColumn(maxLastName, s.getLastName());
		System.out.print(ROW_DIVIDER);
		printColumn(maxFirstName, s.getFirstName());
		System.out.print(ROW_DIVIDER);
		printColumn(maxGrade, String.valueOf(s.getFinalGrade()));
		System.out.println(ROW_DIVIDER);
	}
	
	/**
	 * Prints one column in a table.
	 * 
	 * @param maxField number of spaces for column
	 * @param field field to print
	 */
	private static void printColumn(int maxField, String field) {
		System.out.print(" ");
		
		for(int i = 0; i < maxField + OFFSET - 1 ; i++) {
			System.out.print(i >= field.length() ? " " : field.charAt(i));
		}
	}
}
