package hr.fer.zemris.lsystems.impl.lexer;

/**
 * Enumeration of {@link Token} types.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public enum TokenType {

	/** String of characters. */
	STRING,
	
	/** One character. */
	CHARACTER,
	
	/** Number in double format. */
	NUMBER,
	
	/** Number in hex format. */
	HEX_NUMBER,
	
	/** End of file. */
	EOF
	
}
