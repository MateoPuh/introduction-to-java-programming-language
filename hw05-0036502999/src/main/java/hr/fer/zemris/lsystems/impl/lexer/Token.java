package hr.fer.zemris.lsystems.impl.lexer;

/**
 * This class represents token that is generated in 
 * {@link Lexer} class. It has a {@link TokenType} 
 * type and a value.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Token {

	/** Type of token. */
	private TokenType type;
	
	/** Value of a token. */
	private Object value;
	
	/**
	 * Creates new {@link Token} with given {@link TokenType}
	 * and value.
	 * 
	 * @param type type of token
	 * @param value value of token
	 */
	public Token(TokenType type, Object value) {
		this.type = type;
		this.value = value;
	}

	/**
	 * Returns type of a token.
	 * 
	 * @return type of token
	 */
	public TokenType getType() {
		return type;
	}

	/**
	 * Value of a token.
	 * 
	 * @return value of a token
	 */
	public Object getValue() {
		return value;
	}
	
	
}
