package hr.fer.zemris.lsystems.impl.demo;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilderProvider;
import hr.fer.zemris.lsystems.gui.LSystemViewer;
import hr.fer.zemris.lsystems.impl.LSystemBuilderImpl;

public class Glavni6 {

	private static final String FILENAME = "./src/main/resources/koch2.txt";
	
	public static void main(String[] args) {
		LSystemViewer.showLSystem(createKochCurve(LSystemBuilderImpl::new));
	}
	
	private static LSystem createKochCurve(LSystemBuilderProvider provider) {
		try {
			// load file as a list of lines
			List<String> lines = Files.readAllLines(
				 Paths.get(FILENAME),
				 StandardCharsets.UTF_8
				);
			
			// cast list to array
			String[] data = new String[lines.size()];
			
			for(int i = 0; i < lines.size(); i++) {
				data[i] = lines.get(i);
			}
			
			return provider.createLSystemBuilder().configureFromText(data).build();
		} catch(Exception e) {
			System.out.println(e);
			System.exit(1);
		}
		return null;
	}
	
}
