package hr.fer.zemris.lsystems.impl.parser;

import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.impl.lexer.Lexer;
import hr.fer.zemris.lsystems.impl.lexer.LexerException;
import hr.fer.zemris.lsystems.impl.lexer.Token;
import hr.fer.zemris.lsystems.impl.lexer.TokenType;

/**
 * This class represents parser used in {@link LSystemBuilder#configureFromText(String[])}
 * method. It parses given input Strings and configures the {@link LSystemBuilder}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ConfigurationParser {

	/** Lexer used for generating tokens. */
	private Lexer lexer;
	
	/** Input Strings. */
	private String[] text;
	
	/** {@link LSystemBuilder} that is configured */
	private LSystemBuilder lSystemBuilder;
	
	/**
	 * Creates new {@link ConfigurationParser} with given input Strings and 
	 * {@link LSystemBuilder}.
	 * 
	 * @param text input strings
	 * @param lSystemBuilder LSystemBuilder that will be configured
	 */
	public ConfigurationParser(String[] text, LSystemBuilder lSystemBuilder) {
		if(text == null) {
			throw new ConfigurationParserException("Given text is null!");
		}
		this.text = text;
		this.lSystemBuilder = lSystemBuilder;
	}
	
	/**
	 * Parses the input Strings and congfigures the given {@link LSystemBuilder}.
	 */
	public void parse() {
		for(String s : text) {
			// empty string are allowed
			if(s.equals("")) {
				continue;
			}
			
			lexer = new Lexer(s);
			
			try {
				Token token = lexer.nextToken();
				if(token.getType().equals(TokenType.STRING)) {
					String firstToken = (String)token.getValue();
					
					if(lexer.nextToken().getType().equals(TokenType.NUMBER)) {
						double x = (Double)lexer.getToken().getValue();
						
						switch(firstToken) {
						case "origin":
							if(lexer.nextToken().getType().equals(TokenType.NUMBER)) {
								double y = (Double)lexer.getToken().getValue();
								
								if(lexer.nextToken().getType().equals(TokenType.EOF)) {
									lSystemBuilder.setOrigin(x, y);
									continue;
								}
							}

							throw new ConfigurationParserException("Invalid origin configuration!");

						case "angle":
							if(lexer.nextToken().getType().equals(TokenType.EOF)) {
								lSystemBuilder.setAngle(x);
								continue;
							}
							
							throw new ConfigurationParserException("Invalid angle configuration!");
							
						case "unitLength":
							if(lexer.nextToken().getType().equals(TokenType.EOF)) {
								lSystemBuilder.setUnitLength(x);
								continue;
							}
						
							throw new ConfigurationParserException("Invalid unitLength configuration!");
							
						case "unitLengthDegreeScaler":
							if(lexer.nextToken().getType().equals(TokenType.CHARACTER)
									&& Character.valueOf((Character)lexer.getToken().getValue()).equals('/')) {
								if(lexer.nextToken().getType().equals(TokenType.NUMBER)) {
									double unitLengthDegreeScaler = x / (Double)lexer.getToken().getValue();
									
									if(lexer.nextToken().getType().equals(TokenType.EOF)) {
										lSystemBuilder.setUnitLengthDegreeScaler(unitLengthDegreeScaler);
										
										
										continue;
									}
								}
							}
							
							throw new ConfigurationParserException("Invalid unitLengthDegreeScaler configuration!");

						}
						
					}
					
					if(lexer.getToken().getType().equals(TokenType.CHARACTER) || lexer.getToken().getType().equals(TokenType.STRING)) {
						String string = String.valueOf(lexer.getToken().getValue());
						
						
						switch(firstToken.trim()) {
						case "axiom":
							if(lexer.nextToken().getType().equals(TokenType.EOF)) {
								lSystemBuilder.setAxiom(String.valueOf(string));
																
								continue;
							}
							throw new ConfigurationParserException("Invalid axiom configuration!");
						}
					}
					
					if(lexer.getToken().getType().equals(TokenType.CHARACTER)) {
						char c = (Character)lexer.getToken().getValue();
						
						
						switch(firstToken.trim()) {

						case "command":
							if(lexer.nextToken().getType().equals(TokenType.STRING)) {
								String command = (String)lexer.getToken().getValue();
								
								if(lexer.nextToken().getType().equals(TokenType.EOF)) {
									lSystemBuilder.registerCommand(c, command); 
									continue;
								}
								
								if(lexer.getToken().getType().equals(TokenType.NUMBER)
										|| lexer.getToken().getType().equals(TokenType.HEX_NUMBER)) {
									String commandParam = String.valueOf(lexer.getToken().getValue());
									if(lexer.nextToken().getType().equals(TokenType.EOF)) {
										command += " " + commandParam;
										lSystemBuilder.registerCommand(c, command); 
										continue;
									}
								} 
								
								if(lexer.getToken().getType().equals(TokenType.STRING)) {
									String commandParam = (String)lexer.getToken().getValue();
									if(lexer.nextToken().getType().equals(TokenType.EOF)) {
										command += " " + commandParam;
										lSystemBuilder.registerCommand(c, command); 
										continue;
									}
								} 
							}
							throw new ConfigurationParserException("Invalid command configuration!");
						
						case "production":
							if(lexer.nextToken().getType().equals(TokenType.STRING)) {
								String production = (String)lexer.getToken().getValue();
																
								lSystemBuilder.registerProduction(c, production);
								continue;
							}
							throw new ConfigurationParserException("Invalid production configuration!");
						}
					}
					
				}
				else {
					throw new ConfigurationParserException("Invalid command.");
				}
			} catch(LexerException e) {
				throw new ConfigurationParserException(e.getMessage());
			}
		}
	}
	
}
