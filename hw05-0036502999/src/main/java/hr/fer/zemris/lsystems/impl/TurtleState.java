package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

import hr.fer.zemris.math.Vector2D;

/**
 * Represents state of a turtle in "turtle graphics". 
 * In this process, the turtle is imagined to walk 
 * from bottom left corner to right side of a screen
 * and leaving trail behind her which represents curve
 * made by particular L-system.
 * 
 * <p>State represents position of a turtle, direction 
 * she is facing, effective length of a next move and
 * color of a line that she draws.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class TurtleState {
	
	/** Position of a turtle. */
	private Vector2D position;
	
	/** Direction of turtle's next move. */
	private Vector2D direction;
	
	/** Color of a line that will be drawn. */
	private Color color;
	
	/** Effective length of next movement. */
	private double movementLength;

	/**
	 * Creates new {@link TurtleState} with a given position, direction,
	 * color and effective movement length.
	 * 
	 * @param position turtle's position
	 * @param direction direction of turtle's next move
	 * @param color color of a line that will be drawn
	 * @param movementLength effective length of a next move
	 */
	public TurtleState(Vector2D position, Vector2D direction, Color color, double movementLength) {
		this.position = position;
		this.direction = direction;
		this.color = color;
		this.movementLength = movementLength;
	}

	/**
	 * Returns a copy of a current state.
	 * 
	 * @return copy of current state
	 */
	public TurtleState copy() {
		return new TurtleState(position.copy(), direction.copy(), color, movementLength);
	}
	
	/**
	 * Returns turtle's position in this state.
	 * 
	 * @return position of a turtle
	 */
	public Vector2D getPosition() {
		return position;
	}
	
	/**
	 * Sets the position of a turtle.
	 * 
	 * @param position new position
	 */
	public void setPosition(Vector2D position) {
		this.position = position;
	}
	
	/**
	 * Returns direction of a turtle's next move.
	 * 
	 * @return direction of a turtle's next move
	 */
	public Vector2D getDirection() {
		return direction;
	}

	/**
	 * Sets the direction of turtle.
	 * 
	 * @param direction new direction
	 */
	public void setDirection(Vector2D direction) {
		this.direction = direction;
	}
	
	/**
	 * Returns color of next line that will be drawn.
	 * 
	 * @return color of next line that will be drawn
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Return effective length of a next turtle's move.
	 * 
	 * @return effective length of a turtle's next move
	 */
	public double getMovementLength() {
		return movementLength;
	}
	
	/**
	 * Sets the effective length of a turtle's next move.
	 * 
	 * @param movementLength effective length of a turtle's next move
	 */
	public void setMovementLength(double movementLength) {
		this.movementLength = movementLength;
	}
	
	/**
	 * Sets the color of a next line that will be drawn.
	 * 
	 * @param color color of a next line that will be drawn
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
}
