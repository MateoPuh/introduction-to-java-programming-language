package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * This class represents {@link Command} that is used to 
 * scale length of next line that will be drawn.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ScaleCommand implements Command {

	/** Factor of scaling. */
	private double factor;
	
	/**
	 * Creates new {@link ScaleCommand} with given factor.
	 * 
	 * @param factor factor of scaling
	 */
	public ScaleCommand(double factor) {
		this.factor = factor;
	}
	
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().setMovementLength(ctx.getCurrentState().getMovementLength() * factor);
	}

}
