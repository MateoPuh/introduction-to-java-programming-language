package hr.fer.zemris.lsystems.impl.lexer;

/**
 * Exception that is thrown in {@link Lexer} class whenever
 * error occurs.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class LexerException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates new {@link LexerException}.
	 */
	public LexerException() {
		
	}
	
	/**
	 * Creates new {@link LexerException} with given message.
	 * 
	 * @param message message of exception
	 */
	public LexerException(String message) {
		super(message);
	}
	
	/**
	 * Creates new {@link LexerException} with given cause.
	 * 
	 * @param cause cause of exception
	 */
	public LexerException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates new {@link LexerException} with given 
	 * message and cause.
	 * 
	 * @param message message of exception
	 * @param cause cause exception
	 */
	public LexerException(String message, Throwable cause) {
		super(message, cause);
	}

}
