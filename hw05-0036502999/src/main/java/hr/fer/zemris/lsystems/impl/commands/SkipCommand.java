package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;

/**
 * This class represents {@link Command} that is used to 
 * skip drawing next line but still moving turtle.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class SkipCommand implements Command {

	/** Step of length of the next line that will not be drawn. */
	private double step;
	
	/**
	 * Creates new {@link SkipCommand} with given step.
	 * 
	 * @param step step of length of next line that will not be drawn
	 */
	public SkipCommand(double step) {
		this.step = step;
	}
	
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState current = ctx.getCurrentState();
		TurtleState next = current.copy();
		next.getPosition().translate(next.getDirection().scaled(step * next.getMovementLength()));

		current.setPosition(next.getPosition());
	}

}
