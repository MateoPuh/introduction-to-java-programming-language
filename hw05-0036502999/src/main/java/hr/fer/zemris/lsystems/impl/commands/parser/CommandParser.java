package hr.fer.zemris.lsystems.impl.commands.parser;

import java.awt.Color;

import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.commands.ColorCommand;
import hr.fer.zemris.lsystems.impl.commands.DrawCommand;
import hr.fer.zemris.lsystems.impl.commands.PopCommand;
import hr.fer.zemris.lsystems.impl.commands.PushCommand;
import hr.fer.zemris.lsystems.impl.commands.RotateCommand;
import hr.fer.zemris.lsystems.impl.commands.ScaleCommand;
import hr.fer.zemris.lsystems.impl.commands.SkipCommand;
import hr.fer.zemris.lsystems.impl.lexer.Lexer;
import hr.fer.zemris.lsystems.impl.lexer.LexerException;
import hr.fer.zemris.lsystems.impl.lexer.TokenType;
import hr.fer.zemris.lsystems.impl.lexer.Token;
 
 /**
  * This class represents Parser for commands in {@link LSystemBuilder}.
  * It uses {@link Lexer} to generate tokens with {@link TokenType} and value.
  * 
  * @author Mateo Puhalović
  * @version 1.0
  *
  */
public class CommandParser {

	/** Lexer used for generating tokens. */
	private Lexer lexer;
	
	/**
	 * Creates new {@link CommandParser} with given command.
	 * 
	 * @param command command to parse
	 * @throws CommandParserException if command is null
	 */
	public CommandParser(String command) {
		if(command == null) {
			throw new CommandParserException("Given text is null!");
		}
		lexer = new Lexer(command);
	}
	
	/**
	 * Parses and returns {@link Command}.
	 * 
	 * @return parsed command
	 * @throws CommandParserException if error occurs
	 */
	public Command parseCommand() {
		try {
			Token token = lexer.nextToken();

			if(token.getType().equals(TokenType.STRING)) {
				Token next = lexer.nextToken();
				if(next.getType().equals(TokenType.NUMBER)) {
					Token last = lexer.nextToken();
					double number = (Double)next.getValue();
					
					if(last.getType().equals(TokenType.EOF)) {
						switch((String)token.getValue()) {
						case "draw":
							return new DrawCommand(number);
						case "skip":
							return new SkipCommand(number);
						case "scale":
							return new ScaleCommand(number);
						case "rotate":
							return new RotateCommand(number * Math.PI / 180.0);
						}
					}
				} else if(next.getType().equals(TokenType.HEX_NUMBER)) {
					Token last = lexer.nextToken();
					if(((String)next.getValue()).length() != 6) {
						throw new CommandParserException("Wrong size of number in hex format.");
					}
					
					try {				
						int r = Integer.parseInt(((String)next.getValue()).substring(0, 2), 16);
						int g = Integer.parseInt(((String)next.getValue()).substring(2, 4), 16);
						int b = Integer.parseInt(((String)next.getValue()).substring(4, 6), 16);
						
						if(last.getType().equals(TokenType.EOF)) {
							switch((String)token.getValue()) {
							case "color":
								return new ColorCommand(new Color(r, g, b));
							}
						}
					} catch(NumberFormatException e) {
						throw new CommandParserException("Error while parsing hex number.");
					}

				} else if(next.getType().equals(TokenType.EOF)) {
					switch((String)token.getValue()) {
					case "push":
						return new PushCommand();
					case "pop":
						return new PopCommand();
					}
				} else if(next.getType().equals(TokenType.STRING)) {
					Token last = lexer.nextToken();
					if(((String)next.getValue()).length() != 6) {
						throw new CommandParserException("Cannot parse hex number.");
					}
					try {
						int r = Integer.parseInt(((String)next.getValue()).substring(0, 2), 16);
						int g = Integer.parseInt(((String)next.getValue()).substring(2, 4), 16);
						int b = Integer.parseInt(((String)next.getValue()).substring(4, 6), 16);
	
	
						if(last.getType().equals(TokenType.EOF)) {
							switch((String)token.getValue()) {
							case "color":
								return new ColorCommand(new Color(r, g, b));
							}
						}
					} catch(NumberFormatException e) {
						throw new CommandParserException("Error while parsing hex number.");
					}
				}
			} 

			throw new CommandParserException("Invalid command.");
			
		} catch(LexerException e) {
			throw new CommandParserException(e.getMessage());
		}
	}
	
}
