package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.math.Vector2D;

/**
 * This class represents {@link Command} that is used to 
 * draw line.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class DrawCommand implements Command {

	/** Step of length of line. */
	private double step;
	
	/**
	 * Creates new {@link DrawCommand} with given step.
	 * 
	 * @param step new step of line
	 */
	public DrawCommand(double step) {
		this.step = step;
	}
	
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState current = ctx.getCurrentState();
		TurtleState next = current.copy();
		next.getPosition().translate(next.getDirection().scaled(next.getMovementLength()));
		
		painter.drawLine(current.getPosition().getX(), current.getPosition().getY(),
				next.getPosition().getX(), next.getPosition().getY(), 
				next.getColor(), (float)step);
		
		current.setPosition(next.getPosition());
	}

}
