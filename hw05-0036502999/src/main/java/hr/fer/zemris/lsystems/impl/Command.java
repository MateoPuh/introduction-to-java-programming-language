package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.Painter;

/**
 * This interface represents command used in L-systems.
 * It defines one method: execute(Context, Painter) which can 
 * use {@link Context} to manipulate {@link TurtleState} objects 
 * and {@link Painter} to draw on screen.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
@FunctionalInterface
public interface Command {

	/**
	 * This method executes the command. It can use {@link Context} 
	 * to manipulate {@link TurtleState} objects and {@link Painter}
	 * to draw on screen.
	 * 
	 * @param ctx context which contains states of a turtle
	 * @param painter painter used to draw on screen
	 */
	void execute(Context ctx, Painter painter);
	
}
