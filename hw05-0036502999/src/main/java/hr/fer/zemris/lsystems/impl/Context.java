package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * This class represents stack of {@link TurtleState} objects
 * used in a process of drawing fractals made by particular 
 * L-system.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Context {

	/** Stack of TurtleState objects. */
	private ObjectStack<TurtleState> stack;
	
	/**
	 * Creates new {@link Context}.
	 */
	public Context() {
		stack = new ObjectStack<>();
	}
	
	/**
	 * Returns {@link TurtleState} from top of a
	 * {@link Context} without removing it for which it uses
	 * peek() method.
	 * 
	 * @return {@link TurtleState} from top of a stack
	 * @throws EmptyStackException if context is empty
	 */
	public TurtleState getCurrentState() {
		return stack.peek();
	}
	
	/**
	 * Puts given {@link TurtleState} to the top
	 * of the {@link Context}.
	 * 
	 * @param state {@link TurtleState} to put on top of 
	 * 	{@link Context}
	 */
	public void pushState(TurtleState state) {
		stack.push(state);
	}
	
	/**
	 * Removes {@link TurtleState} from top of the {@link Context}.
	 */
	public void popState() {
		stack.pop();
	}
	
}
