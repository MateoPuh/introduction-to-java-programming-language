package hr.fer.zemris.lsystems.impl.commands.parser;

/**
 * Exception thrown whenever error occurs in {@link CommandParser}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class CommandParserException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates new {@link CommandParserException}.
	 */
	public CommandParserException() {
		
	}
	
	/**
	 * Creates new {@link CommandParserException} with given message.
	 * 
	 * @param message exception message
	 */
	public CommandParserException(String message) {
		super(message);
	}
	
	/**
	 * Creates new {@link CommandParserException} with given cause.
	 * 
	 * @param cause exception cause
	 */
	public CommandParserException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates new {@link CommandParserException} with given message and cause.
	 * 
	 * @param message exception message
	 * @param cause exception cause
	 */
	public CommandParserException(String message, Throwable cause) {
		super(message, cause);
	}

}
