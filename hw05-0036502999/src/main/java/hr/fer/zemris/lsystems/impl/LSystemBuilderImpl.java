package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

import hr.fer.zemris.java.custom.collections.Dictionary;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.commands.parser.CommandParser;
import hr.fer.zemris.lsystems.impl.parser.ConfigurationParser;
import hr.fer.zemris.math.Vector2D;

/**
 * This class represents builder of L-system. It offers
 * method for defining commands and productions and methods
 * for defining parameters such as axiom, angle, origin, unit
 * length and scaler for unit length. If those parameters are 
 * not defined, default values will be used.
 * 
 * <p>This class offers methods for explicit defining of 
 * parameters, commands and production and also provides 
 * method configureFromText(String[]) which extract this 
 * data from text.</p>
 * 
 * <p>This class also offers {@link LSystemImpl} which represents
 * L-system and provides method for generating and drawing given
 * L-system. This class can be obtained by calling build() method.</p>
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class LSystemBuilderImpl implements LSystemBuilder {

	/** Default axiom for L-system. */
	private static final String DEFAULT_AXIOM = "";
	
	/** Default angle in which turtle is looking. */
	private static final double DEFAULT_ANGLE = 0;
	
	/** Default origin for turtle. */
	private static final Vector2D DEFAULT_ORIGIN = new Vector2D(0, 0);
	
	/** Default scaler of unit length. */
	private static final double DEFAULT_UNIT_LENGTH_DEGREE_SCALER = 1;
	
	/** Default unit length. */
	private static final double DEFAULT_UNIT_LENGTH = 1;
	
	/** 
	 * Dictionary in which keys represent left side of a production 
	 * and values represent right side of a production. 
	 */
	private Dictionary<String, String> productions;
	
	/**
	 *  Dictionary in which values are command that have to be executed
	 *  when character (key) is read.
	 */
	private Dictionary<Character, Command> commands;
	
	/** Left side of first production. */
	private String axiom;
	
	/** Angle of turtle's direction. */ 
	private double angle;
	
	/** Origin point of turtle's movement. */
	private Vector2D origin;
	
	/** 
	 * Initial effective movement length should be set to 
	 * unitLength * (unitLengthDegreeScaler ^d)
	 * where d is depth.
	 */
	private double unitLengthDegreeScaler;
	
	/** Length of move unit of movement. */
	private double unitLength;
	
	/**
	 * This class represents L-system. It provides methods for
	 * drawing L-system's curve with a given depth and generating
	 * String for a given depth.
	 * 
	 * @author Mateo Puhalović
	 * @version 1.0
	 *
	 */
	private class LSystemImpl implements LSystem {

		/**
		 * Draws L-system's curve for given depth. 
		 * 
		 * @param arg0 depth 
		 * @param arg1 painter used for drawing 
		 */
		@Override
		public void draw(int arg0, Painter arg1) {
			Context context = new Context();
			String generated = generate(arg0);
			
			TurtleState state = new TurtleState(
					origin, new Vector2D(1, 0).rotated(angle), Color.BLACK, 
					unitLength * Math.pow(unitLengthDegreeScaler, arg0));
			context.pushState(state);
			for(char ch : generated.toCharArray()) {
				Command command = commands.get(ch);
								
				if(command != null){
					try {
						command.execute(context, arg1);
					} catch(Exception e) {
						System.out.println(e.getMessage());
					}
				} 
			}
		}

		/**
		 * Generates string for L-system at given iteration (also
		 * called depth).
		 * 
		 * @param arg0 depth
		 * @return string at given iteration
		 */
		@Override
		public String generate(int arg0) {
			String current = axiom;
			
			// in each iteration
			for(int i = 0; i < arg0; i++) {
				String nextState = "";
				
				// find production for each character
				for(char c : current.toCharArray()) {
					String s = productions.get(String.valueOf(c));
					
					if(s != null) {
						nextState+=s;
					} else {
						// if there is no production, put the same character in a string
						nextState += String.valueOf(c);
					}
				}
				current = nextState;
			}
			return current;
		}
		
	}
	
	/**
	 * Creates new {@link LSystemBuilderImpl} with default values
	 * for axiom, angle, origin, unit length and degree scaler for unit
	 * length.
	 */
	public LSystemBuilderImpl() {
		axiom = DEFAULT_AXIOM;
		angle = DEFAULT_ANGLE;
		origin = DEFAULT_ORIGIN;
		unitLengthDegreeScaler = DEFAULT_UNIT_LENGTH_DEGREE_SCALER;
		unitLength = DEFAULT_UNIT_LENGTH;

		productions = new Dictionary<>();
		commands = new Dictionary<>();
	}
	
	@Override
	public LSystem build() {
		return new LSystemImpl();
	}

	@Override
	public LSystemBuilder configureFromText(String[] arg0) {
		ConfigurationParser parser = new ConfigurationParser(arg0, this);
		parser.parse();
		return this;
	}

	@Override
	public LSystemBuilder registerCommand(char arg0, String arg1) {
		CommandParser parser = new CommandParser(arg1);
		
		commands.put(arg0, parser.parseCommand());
		return this;
	}

	@Override
	public LSystemBuilder registerProduction(char arg0, String arg1) {
		productions.put(String.valueOf(arg0), arg1);
		return this;
	}

	@Override
	public LSystemBuilder setAngle(double arg0) {
		this.angle = Math.toRadians(arg0);
		return this;
	}

	@Override
	public LSystemBuilder setAxiom(String arg0) {
		this.axiom = arg0;
		return this;
	}

	@Override
	public LSystemBuilder setOrigin(double arg0, double arg1) {
		this.origin = new Vector2D(arg0, arg1);
		return this;
	}

	@Override
	public LSystemBuilder setUnitLength(double arg0) {
		this.unitLength = arg0;
		return this;
	}

	@Override
	public LSystemBuilder setUnitLengthDegreeScaler(double arg0) {
		this.unitLengthDegreeScaler = arg0;
		return this;
	}

}
