package hr.fer.zemris.lsystems.impl.commands;

import java.awt.Color;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * This class represents {@link Command} that is used to 
 * change color of line that is being drawn.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ColorCommand implements Command {

	/** Color with which line will be drawn. */
	private Color color;
	
	/**
	 * Creates new {@link ColorCommand} with given color.
	 * 
	 * @param color new color with which line will be drawn
	 */
	public ColorCommand(Color color) {
		this.color = color;
	}
	
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().setColor(color);		
	}

}
