package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * This class represents {@link Command} that is used to 
 * rotate direction of next line that will be drawn.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class RotateCommand implements Command {

	/** Angle of rotation of direction. */
	private double angle;
	
	/**
	 * Creates new {@link RotateCommand} with given
	 * angle.
	 * 
	 * @param angle angle of rotation
	 */
	public RotateCommand(double angle) {
		this.angle = angle;
	}
	
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().getDirection().rotate(angle);
	}

}
