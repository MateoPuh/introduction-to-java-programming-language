package hr.fer.zemris.lsystems.impl.parser;

/**
 * Exception used in {@link ConfigurationParser}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class ConfigurationParserException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates new {@link ConfigurationParserException}.
	 */
	public ConfigurationParserException() {
		
	}
	
	/**
	 * Creates new {@link ConfigurationParserException} with given message.
	 * 
	 * @param message message od exception
	 */
	public ConfigurationParserException(String message) {
		super(message);
	}
	
	/**
	 * Creates new {@link ConfigurationParserException} with given cause.
	 * 
	 * @param cause
	 */
	public ConfigurationParserException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates new {@link ConfigurationParserException} with given message
	 * and cause.
	 * 
	 * @param message
	 * @param cause
	 */
	public ConfigurationParserException(String message, Throwable cause) {
		super(message, cause);
	}

}
