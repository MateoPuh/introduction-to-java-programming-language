package hr.fer.zemris.lsystems.impl.lexer;

import java.util.Arrays;

import hr.fer.zemris.lsystems.impl.commands.parser.CommandParser;
import hr.fer.zemris.lsystems.impl.parser.ConfigurationParser;

/**
 * This class represents Lexer that generates {@link Token} objects
 * for {@link CommandParser} and {@link ConfigurationParser}.
 * 
 * @author Mateo Puhalović
 * @version 1.0
 *
 */
public class Lexer {

	/** Minus character. */
	private static final char MINUS = '-';
	
	/** Dot character. */
	private static final char DOT = '.';
	
	/** Letter a. */
	private static final char HEX_A = 'a';
	
	/** Letter F */
	private static final char HEX_F = 'f';

	
	/** Command in form of character array. */
	private char[] command;
	
	/** Last generated token. */
	private Token token;
	
	/** Index of currently analyzed character. */
	private int currentIndex;
	
	/**
	 * Creates new {@link Lexer} with given command from
	 * which the tokens will be generated.
	 * 
	 * @param text command from which tokens will be generated
	 * @throws LexerException if given text is null
	 */
	public Lexer(String text) {
		if(text == null) {
			throw new LexerException("Error: Given text is null!");
		}
		this.command = text.toCharArray();
	}

	/**
	 * Returns last generated {@link Token}.
	 * 
	 * @return last generated token
	 */
	public Token getToken() {
		return token;
	}
	
	/**
	 * Generates and returns next token. 
	 * 
	 * @return next generated token
	 * @throws LexerException if error occurs
	 */
	public Token nextToken() {
		removeBlanks();
		
		// if whole command is analyzed, 
		// then return EOF or throw exception if EOF is already generated
		if(currentIndex >= command.length) {
			if(token != null && token.getType().equals(TokenType.EOF)) {
				throw new LexerException("Error: Attempting to generate token after EOF!");
			} else {
				token = new Token(TokenType.EOF, null);
				return token;
			}
		}
		
		// if current character is number or -, generate number
		if(Character.isDigit(command[currentIndex]) 
				|| (command[currentIndex] == MINUS && (currentIndex + 1 < command.length && Character.isDigit(command[currentIndex + 1])))) {
			Token t = generateNumber();
			
			if(t == null) {
				throw new LexerException("Cannot generate number.");
			} else {
				return t;
			}
		}
		
		Token t = generateString();
		
		if(t == null) {
			throw new LexerException("Cannot generate string.");
		} else {
			return t;
		}
	}

	/**
	 * Generetes {@link Token} with {@link TokenType} NUMBER.
	 * 
	 * @return number token
	 */
	private Token generateNumber() {
		removeBlanks();
		
		int startIndex = currentIndex; //index of starting position
		int numOfDots = 0; // number of dots in number
		boolean isNegative = false; 
		boolean isHex = false;
		
		// set is negative
		if(currentIndex < command.length && command[currentIndex] == MINUS) {
			currentIndex++;
			isNegative = true;
		}
		
		while(currentIndex < command.length) { 
			// if current character is dot check if there already was another dot
			if(command[currentIndex] == DOT) {
				numOfDots++;
				if(numOfDots > 1 || isHex) {
					throw new LexerException("Too many dots in number");
				}
				currentIndex++;
				continue;
			}
			
			// check for characters of hex nubmer
			if(Character.toLowerCase(command[currentIndex]) >= HEX_A 
					&& Character.toLowerCase(command[currentIndex]) <= HEX_F) {
				isHex = true;
				if(isNegative || numOfDots > 0) {
					throw new LexerException("Wrong hex format.");
				}
				currentIndex++;
				continue;
			}
			
			if(!Character.isDigit(command[currentIndex])) {
				break;
			}
			
			currentIndex++;
		}
		
		if(startIndex < currentIndex) {
			String s = String.valueOf(Arrays.copyOfRange(command, startIndex, currentIndex));
			
			// dot should not be on beginning nor the end of a number
			if(s.endsWith(String.valueOf(DOT)) 
					|| s.startsWith(String.valueOf(MINUS + DOT)) 
					|| s.startsWith(String.valueOf(DOT))) {
				throw new LexerException("Wrong number format");
			};
			
			if(isHex) {
				// if number contains letter a - f parse as hex number
				token = new Token(TokenType.HEX_NUMBER, s);
			} else {
				try {
					// try parsing nubmer if everything is okay and rethrow exception if not
					double d = Double.parseDouble(s);
					token = new Token(TokenType.NUMBER, d);
					return token;
				} catch(NumberFormatException e) {
					throw new LexerException(e.getMessage());
				}
			}
			
			return token;
		} else {
			return null;
		}
	}
	
	/**
	 * Generates String token.
	 * 
	 * @return string token
	 */
	private Token generateString() {
		removeBlanks();
		int startIndex = currentIndex;
		
		while(currentIndex < command.length 
				&& !Character.isWhitespace(command[currentIndex])
				&& !Character.isDigit(command[currentIndex])) {
			currentIndex++;
		}
		
		if(startIndex < currentIndex) {
			if(startIndex + 1 == currentIndex) {
				// if there is only one character generate character token
				token = new Token(TokenType.CHARACTER, Character.valueOf(command[startIndex]));
			} else {
				token = new Token(TokenType.STRING, 
					String.valueOf(Arrays.copyOfRange(command, startIndex, currentIndex)));
			}
			return token;
		} else {
			return null;
		}
	}
	
	/**
	 * Removes all blanks until end of command or first non-blank character.
	 */
	private void removeBlanks() {
		while(currentIndex < command.length && Character.isWhitespace(command[currentIndex])) {
			currentIndex++;
		}
	}
}
