package hr.fer.zemris.lsystems.impl.lexer;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class LexerTest {
	
	@Test
	public void testNull() {
		assertThrows(LexerException.class, () -> new Lexer(null));
	}

	@Test
	public void testEmpty() {
		Lexer l = new Lexer("");
		
		assertEquals(TokenType.EOF, l.nextToken().getType());
		assertEquals(TokenType.EOF, l.getToken().getType());
		assertEquals(TokenType.EOF, l.getToken().getType());

		assertThrows(LexerException.class, () -> l.nextToken());
	}
	
	@Test
	public void testBlanks() {
		Lexer l = new Lexer(" \t\n");
		
		assertEquals(TokenType.EOF, l.nextToken().getType());
		assertThrows(LexerException.class, () -> l.nextToken());
	}
	
	@Test
	public void testString() {
		Lexer l = new Lexer(" \t\n Mateo   ");
		
		assertEquals(TokenType.STRING, l.nextToken().getType());
		assertEquals("Mateo", (String)l.getToken().getValue());
		assertEquals(TokenType.EOF, l.nextToken().getType());
		assertThrows(LexerException.class, () -> l.nextToken());
	}
	
	@Test
	public void testString2() {
		Lexer l = new Lexer(" \t\n Mateo123   ");
		
		assertEquals(TokenType.STRING, l.nextToken().getType());
		assertEquals("Mateo123", (String)l.getToken().getValue());
		assertEquals(TokenType.EOF, l.nextToken().getType());
		assertThrows(LexerException.class, () -> l.nextToken());
	}
	
	@Test
	public void testCharacter() {
		Lexer l = new Lexer(" \t\n Mateo  F + ");
		
		assertEquals(TokenType.STRING, l.nextToken().getType());
		assertEquals("Mateo", (String)l.getToken().getValue());
		assertEquals(TokenType.CHARACTER, l.nextToken().getType());
		assertEquals('F', (Character)l.getToken().getValue());
		assertEquals(TokenType.CHARACTER, l.nextToken().getType());
		assertEquals('+', (Character)l.getToken().getValue());
		assertEquals(TokenType.EOF, l.nextToken().getType());
		assertThrows(LexerException.class, () -> l.nextToken());
	}
	
	@Test
	public void testNumber() {
		Lexer l = new Lexer(" \t\n rotate 60 ");
		
		assertEquals(TokenType.STRING, l.nextToken().getType());
		assertEquals("rotate", (String)l.getToken().getValue());
		assertEquals(TokenType.NUMBER, l.nextToken().getType());
		assertEquals(60, (Double)l.getToken().getValue());
		assertEquals(TokenType.EOF, l.nextToken().getType());
		assertThrows(LexerException.class, () -> l.nextToken());
	}
	
	@Test
	public void testNegativeNumber() {
		Lexer l = new Lexer(" \t\n rotate -60 ");
		
		assertEquals(TokenType.STRING, l.nextToken().getType());
		assertEquals("rotate", (String)l.getToken().getValue());
		assertEquals(TokenType.NUMBER, l.nextToken().getType());
		assertEquals(-60, (Double)l.getToken().getValue());
		assertEquals(TokenType.EOF, l.nextToken().getType());
		assertThrows(LexerException.class, () -> l.nextToken());
	}
	
	@Test
	public void testNegativeDecimalNumber() {
		Lexer l = new Lexer(" \t\n rotate -6.0 ");
		
		assertEquals(TokenType.STRING, l.nextToken().getType());
		assertEquals("rotate", (String)l.getToken().getValue());
		assertEquals(TokenType.NUMBER, l.nextToken().getType());
		assertEquals(-6.0, (Double)l.getToken().getValue());
		assertEquals(TokenType.EOF, l.nextToken().getType());
		assertThrows(LexerException.class, () -> l.nextToken());
	}
	
	@Test
	public void testNegativeDecimalNumberTwoDots() {
		Lexer l = new Lexer("-6..0");
		assertThrows(LexerException.class, ()-> l.nextToken());
	}
	
	@Test
	public void testNegativeDecimalNumberDotOnEnd() {
		Lexer l = new Lexer("-.60");
		assertThrows(LexerException.class, ()-> l.nextToken());
	}
	
	@Test
	public void testHexNumber() {
		Lexer l = new Lexer(" \t\n rotate 60f ");
		
		assertEquals(TokenType.STRING, l.nextToken().getType());
		assertEquals("rotate", (String)l.getToken().getValue());
		assertEquals(TokenType.HEX_NUMBER, l.nextToken().getType());
		assertEquals("60f", (String)l.getToken().getValue());
		assertEquals(TokenType.EOF, l.nextToken().getType());
		assertThrows(LexerException.class, () -> l.nextToken());
	}
	
	@Test
	public void testNegativeHexNumber() {
		Lexer l = new Lexer(" \t\n rotate -60f ");
		
		assertEquals(TokenType.STRING, l.nextToken().getType());
		assertEquals("rotate", (String)l.getToken().getValue());
		
		assertThrows(LexerException.class, ()-> l.nextToken());
	}
	
}
