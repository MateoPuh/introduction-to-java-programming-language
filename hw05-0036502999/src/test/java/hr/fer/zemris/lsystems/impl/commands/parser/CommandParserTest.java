package hr.fer.zemris.lsystems.impl.commands.parser;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.lsystems.impl.commands.ColorCommand;
import hr.fer.zemris.lsystems.impl.commands.DrawCommand;
import hr.fer.zemris.lsystems.impl.commands.PopCommand;
import hr.fer.zemris.lsystems.impl.commands.PushCommand;
import hr.fer.zemris.lsystems.impl.commands.RotateCommand;
import hr.fer.zemris.lsystems.impl.commands.ScaleCommand;
import hr.fer.zemris.lsystems.impl.commands.SkipCommand;

public class CommandParserTest {

	@Test
	public void testNull() {		
		assertThrows(CommandParserException.class, () -> new CommandParser(null));
	}
	
	@Test
	public void testParseDraw() {
		CommandParser p = new CommandParser("draw 0.5");
		
		DrawCommand draw = (DrawCommand)p.parseCommand();
	}
	
	@Test
	public void testParseDrawException() {
		CommandParser p = new CommandParser("draw");
		
		assertThrows(CommandParserException.class, ()-> p.parseCommand());
	}
	
	@Test
	public void testParseDrawException2() {
		CommandParser p = new CommandParser("draw .");
		
		assertThrows(CommandParserException.class, ()-> p.parseCommand());
	}
	
	@Test
	public void testParseDrawException3() {
		CommandParser p = new CommandParser("draw 00ff");
		
		assertThrows(CommandParserException.class, ()-> p.parseCommand());
	}
	
	@Test
	public void testParseDrawException4() {
		CommandParser p = new CommandParser("draw 1 2");
		
		assertThrows(CommandParserException.class, ()-> p.parseCommand());
	}
	
	@Test
	public void testParseSkip() {
		CommandParser p = new CommandParser(" skip 0.5");
		
		SkipCommand skip = (SkipCommand)p.parseCommand();
	}
	
	@Test
	public void testParseScale() {
		CommandParser p = new CommandParser(" scale 0.5");
		
		ScaleCommand scale = (ScaleCommand)p.parseCommand();
	}
	
	@Test
	public void testParseRotate() {
		CommandParser p = new CommandParser(" rotate -90");
		
		RotateCommand rotate = (RotateCommand)p.parseCommand();
	}
	
	@Test
	public void testParsePush() {
		CommandParser p = new CommandParser("push");
		
		PushCommand push = (PushCommand)p.parseCommand();
	}
	
	@Test
	public void testParsePushException() {
		CommandParser p = new CommandParser("push 15");
		
		assertThrows(CommandParserException.class, ()-> p.parseCommand());
	}
	
	@Test
	public void testParsePop() {
		CommandParser p = new CommandParser("pop");
		
		PopCommand pop = (PopCommand)p.parseCommand();
	}
	
	@Test
	public void testParseColorException() {
		CommandParser p = new CommandParser("color");
		
		assertThrows(CommandParserException.class, ()-> p.parseCommand());
	}
	
	@Test
	public void testParseColorException2() {
		CommandParser p = new CommandParser("color 15");
		
		assertThrows(CommandParserException.class, ()-> p.parseCommand());
	}
	
	@Test
	public void testParseColor() {
		CommandParser p = new CommandParser("color 00ff00");
		
		ColorCommand color = (ColorCommand)p.parseCommand();
	}
	
	@Test
	public void testParseColor2() {
		CommandParser p = new CommandParser("color f0ff00");
		
		ColorCommand color = (ColorCommand)p.parseCommand();
	}
	
}
