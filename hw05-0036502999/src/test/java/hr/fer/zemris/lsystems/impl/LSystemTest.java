package hr.fer.zemris.lsystems.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.LSystemBuilderProvider;
import hr.fer.zemris.lsystems.gui.LSystemViewer;

public class LSystemTest {
	
	private static final String FILENAME = "./src/main/resources/plant2.txt";
	
	@Test
	public void testPlant2() {
		try {
			// load file as a list of lines
			List<String> lines = Files.readAllLines(
				 Paths.get(FILENAME),
				 StandardCharsets.UTF_8
				);
			
			// cast list to array
			String[] data = new String[lines.size()];
			
			for(int i = 0; i < lines.size(); i++) {
				data[i] = lines.get(i);
			}
			
			LSystemBuilderProvider provider = LSystemBuilderImpl::new;
			
			LSystem l = provider.createLSystemBuilder().configureFromText(data).build();
			
			assertEquals("GF", l.generate(0));
			assertEquals("GFF+[+F-F-F]-[-F+F+F]", l.generate(1));
			//assertEquals("F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F", l.generate(2));
		} catch(Exception e) {
			System.out.println(e);
			System.exit(1);
		}
	}
	
	@Test
	public void testPage5() {
		LSystemBuilderImpl lSystemBuilderImpl = new LSystemBuilderImpl();
		
		LSystem lSystem = lSystemBuilderImpl.registerCommand('F', "draw 1")
		.registerCommand('+', "rotate 60")
		.registerCommand('-', "rotate -60")
		.setOrigin(0.05, 0.4)
		.setAngle(0)
		.setUnitLength(0.9)
		.setUnitLengthDegreeScaler(1.0/3.0)
		.registerProduction('F', "F+F--F+F")
		.setAxiom("F")
		.build();
		
		assertEquals("F", lSystem.generate(0));
		assertEquals("F+F--F+F", lSystem.generate(1));
		assertEquals("F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F", lSystem.generate(2));
	}

}
