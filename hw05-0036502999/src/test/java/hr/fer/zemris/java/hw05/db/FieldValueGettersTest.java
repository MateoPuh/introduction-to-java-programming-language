package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class FieldValueGettersTest {

	@Test
	public void testGetters() {
		StudentRecord s = new StudentRecord("0000000001", "Mateo", "Puhalović", 5);
		
		assertEquals("Mateo", FieldValueGetters.FIRST_NAME.get(s));
		assertEquals("Puhalović", FieldValueGetters.LAST_NAME.get(s));
		assertEquals("0000000001", FieldValueGetters.JMBAG.get(s));
	}
	
}
