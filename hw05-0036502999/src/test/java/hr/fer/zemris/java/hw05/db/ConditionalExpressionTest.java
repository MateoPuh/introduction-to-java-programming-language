package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

public class ConditionalExpressionTest {

	@Test
	public void test() {
		QueryParser qp1 = new QueryParser(" jmbag =\"0123456789\" and firstName>=\"Ante\" and lastName LIKE \"*ić\" ");
		List<ConditionalExpression> list = qp1.getQuery();
		
		assertEquals("0123456789", list.get(0).getLiteral());
		assertEquals(FieldValueGetters.JMBAG, list.get(0).getGetter());
		assertEquals(ComparisonOperators.EQUALS, list.get(0).getComparator());
		
		assertEquals("Ante", list.get(1).getLiteral());
		assertEquals(FieldValueGetters.FIRST_NAME, list.get(1).getGetter());
		assertEquals(ComparisonOperators.GREATER_OR_EQUALS, list.get(1).getComparator());

		assertEquals("*ić", list.get(2).getLiteral());
		assertEquals(FieldValueGetters.LAST_NAME, list.get(2).getGetter());
		assertEquals(ComparisonOperators.LIKE, list.get(2).getComparator());
	}
	
}
