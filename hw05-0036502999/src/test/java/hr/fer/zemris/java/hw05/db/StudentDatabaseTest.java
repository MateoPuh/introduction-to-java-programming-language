package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class StudentDatabaseTest {
	
	private static final String FILENAME = "./src/main/resources/database.txt";
	
	private class FalseFilter implements IFilter{

		@Override
		public boolean accepts(StudentRecord record) {
			return false;
		}
		
	}
	
	private class TrueFilter implements IFilter{

		@Override
		public boolean accepts(StudentRecord record) {
			return true;
		}
		
	}
	
	@Test
	public void testForJMBAG() {		
		try{
			// load file as a list of lines
			List<String> lines = Files.readAllLines(
				 Paths.get(FILENAME),
				 StandardCharsets.UTF_8
				);
			
			// cast list to array
			String[] entries = new String[lines.size()];
			
			for(int i = 0; i < lines.size(); i++) {
				entries[i] = lines.get(i);
			}
			
			// create new database
			StudentDatabase db = new StudentDatabase(entries);
			
			assertEquals("0000000003", db.forJMBAG("0000000003").getJmbag());
			assertEquals("Andrea", db.forJMBAG("0000000003").getFirstName());
			assertEquals("Bosnić", db.forJMBAG("0000000003").getLastName());
			assertEquals(4, db.forJMBAG("0000000003").getFinalGrade());
			
		} catch(IOException e) {
			System.out.println(e);
		}
	}
	
	@Test
	public void testAlwaysTrue() {
		String query = "lastName=\"Bosnić\"";
		
		try{
			// load file as a list of lines
			List<String> lines = Files.readAllLines(
				 Paths.get(FILENAME),
				 StandardCharsets.UTF_8
				);
			
			// cast list to array
			String[] entries = new String[lines.size()];
			
			for(int i = 0; i < lines.size(); i++) {
				entries[i] = lines.get(i);
			}
			
			// create new database
			StudentDatabase db = new StudentDatabase(entries);
			
			QueryParser parser = new QueryParser(query);
			
			List<StudentRecord> students = db.filter(new TrueFilter());
			
			assertEquals(63, students.size());
			
		} catch(IOException e) {
			System.out.println(e);
		}
	}
	
	@Test
	public void testAlwaysFalse() {
		String query = "lastName=\"Bosnić\"";
		
		try{
			// load file as a list of lines
			List<String> lines = Files.readAllLines(
				 Paths.get(FILENAME),
				 StandardCharsets.UTF_8
				);
			
			// cast list to array
			String[] entries = new String[lines.size()];
			
			for(int i = 0; i < lines.size(); i++) {
				entries[i] = lines.get(i);
			}
			
			// create new database
			StudentDatabase db = new StudentDatabase(entries);
			
			QueryParser parser = new QueryParser(query);
			
			List<StudentRecord> students = db.filter(new FalseFilter());
			
			assertEquals(0, students.size());
			
		} catch(IOException e) {
			System.out.println(e);
		}
	}
	
	@Test
	public void testAndrea() {
		String query = "lastName=\"Bosnić\"";
		
		try{
			// load file as a list of lines
			List<String> lines = Files.readAllLines(
				 Paths.get(FILENAME),
				 StandardCharsets.UTF_8
				);
			
			// cast list to array
			String[] entries = new String[lines.size()];
			
			for(int i = 0; i < lines.size(); i++) {
				entries[i] = lines.get(i);
			}
			
			// create new database
			StudentDatabase db = new StudentDatabase(entries);
			
			QueryParser parser = new QueryParser(query);
			
			List<StudentRecord> students = db.filter(new QueryFilter(parser.getQuery()));
			
			assertEquals("0000000003", students.get(0).getJmbag());
			assertEquals("Andrea", students.get(0).getFirstName());
			assertEquals("Bosnić", students.get(0).getLastName());
			assertEquals(4, students.get(0).getFinalGrade());

			
		} catch(IOException e) {
			System.out.println(e);
		}
	}
	
	@Test
	public void testJmbag() {
		String query = "jmbag<\"0000000003\"";
		
		try{
			// load file as a list of lines
			List<String> lines = Files.readAllLines(
				 Paths.get(FILENAME),
				 StandardCharsets.UTF_8
				);
			
			// cast list to array
			String[] entries = new String[lines.size()];
			
			for(int i = 0; i < lines.size(); i++) {
				entries[i] = lines.get(i);
			}
			
			// create new database
			StudentDatabase db = new StudentDatabase(entries);
			
			QueryParser parser = new QueryParser(query);
			
			List<StudentRecord> students = db.filter(new QueryFilter(parser.getQuery()));
			
			assertEquals("0000000001", students.get(0).getJmbag());
			assertEquals("Marin", students.get(0).getFirstName());
			assertEquals("Akšamović", students.get(0).getLastName());
			assertEquals(2, students.get(0).getFinalGrade());

			assertEquals("0000000002", students.get(1).getJmbag());
			assertEquals("Petra", students.get(1).getFirstName());
			assertEquals("Bakamović", students.get(1).getLastName());
			assertEquals(3, students.get(1).getFinalGrade());
			
		} catch(IOException e) {
			System.out.println(e);
		}
	}

	@Test
	public void testLike() {
		String query = "firstName LIKE \"K*an\"";
		
		try{
			// load file as a list of lines
			List<String> lines = Files.readAllLines(
				 Paths.get(FILENAME),
				 StandardCharsets.UTF_8
				);
			
			// cast list to array
			String[] entries = new String[lines.size()];
			
			for(int i = 0; i < lines.size(); i++) {
				entries[i] = lines.get(i);
			}
			
			// create new database
			StudentDatabase db = new StudentDatabase(entries);
			
			QueryParser parser = new QueryParser(query);
			
			List<StudentRecord> students = db.filter(new QueryFilter(parser.getQuery()));
			
			assertEquals("0000000015", students.get(0).getJmbag());
			assertEquals("Kristijan", students.get(0).getFirstName());
			assertEquals("Glavinić Pecotić", students.get(0).getLastName());
			assertEquals(4, students.get(0).getFinalGrade());

			assertEquals("0000000062", students.get(1).getJmbag());
			assertEquals("Kristijan", students.get(1).getFirstName());
			assertEquals("Zadro", students.get(1).getLastName());
			assertEquals(3, students.get(1).getFinalGrade());
			
		} catch(IOException e) {
			System.out.println(e);
		}
	}
	
}
