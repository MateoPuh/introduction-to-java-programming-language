package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class QueryFilterTest {
	
	private static final String FILENAME = "./src/main/resources/database.txt";
	
	public void test() {
		String query = "jmbag<\"0000000003\" and firstName LIKE \"*in\"";
		
		try{
			// load file as a list of lines
			List<String> lines = Files.readAllLines(
				 Paths.get(FILENAME),
				 StandardCharsets.UTF_8
				);
			
			// cast list to array
			String[] entries = new String[lines.size()];
			
			for(int i = 0; i < lines.size(); i++) {
				entries[i] = lines.get(i);
			}
			
			// create new database
			StudentDatabase db = new StudentDatabase(entries);
			
			QueryParser parser = new QueryParser(query);
			
			List<StudentRecord> students = db.filter(new QueryFilter(parser.getQuery()));
			
			assertEquals("0000000001", students.get(0).getJmbag());
			assertEquals("Marin", students.get(0).getFirstName());
			assertEquals("Akšamović", students.get(0).getLastName());
			assertEquals(2, students.get(0).getFinalGrade());
			
		} catch(IOException e) {
			System.out.println(e);
		}
	}

}
