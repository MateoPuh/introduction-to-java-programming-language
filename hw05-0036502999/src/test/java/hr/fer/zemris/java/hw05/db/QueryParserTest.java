package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.List;


public class QueryParserTest {

	@Test
	public void testDirectQuery() {
		QueryParser qp1 = new QueryParser(" jmbag =\"0123456789\" ");
		assertTrue(qp1.isDirectQuery()); 
		assertEquals("0123456789", qp1.getQueriedJMBAG()); 
		assertEquals(1, qp1.getQuery().size()); 
		
		QueryParser qp2 = new QueryParser("jmbag=\"0123456789\" and lastName>\"J\"");
		assertFalse(qp2.isDirectQuery()); 
		assertThrows(IllegalStateException.class, () -> qp2.getQueriedJMBAG()); 
		assertEquals(2, qp2.getQuery().size()); 
	}
	
	@Test
	public void testQueryExceptions() {
		QueryParser qp1 = new QueryParser(" jmbag =\"0123456789\" and  ");
		assertThrows(IllegalStateException.class, () -> qp1.getQuery());
		
		QueryParser qp2 = new QueryParser(" \"jmbag\" =\"0123456789\"");
		assertThrows(IllegalStateException.class, () -> qp2.getQuery());

		QueryParser qp3 = new QueryParser(" jmbag2 =\"0123456789\" ");
		assertThrows(IllegalStateException.class, () -> qp3.getQuery());
		
		QueryParser qp4 = new QueryParser(" jmbag =\"0123456789\" and lastName LIKE firstName");
		assertThrows(IllegalStateException.class, () -> qp4.getQuery());
		
		QueryParser qp5 = new QueryParser(" \"0123456789\" = jmbag");
		assertThrows(IllegalStateException.class, () -> qp5.getQuery());
	}
	
	@Test
	public void testGetQuery() {
		QueryParser qp1 = new QueryParser(" jmbag =\"0123456789\" and firstName>=\"Ante\" and lastName LIKE \"*ić\" ");
		List<ConditionalExpression> list = qp1.getQuery();
		
		assertEquals("0123456789", list.get(0).getLiteral());
		assertEquals(FieldValueGetters.JMBAG, list.get(0).getGetter());
		assertEquals(ComparisonOperators.EQUALS, list.get(0).getComparator());
		
		assertEquals("Ante", list.get(1).getLiteral());
		assertEquals(FieldValueGetters.FIRST_NAME, list.get(1).getGetter());
		assertEquals(ComparisonOperators.GREATER_OR_EQUALS, list.get(1).getComparator());

		assertEquals("*ić", list.get(2).getLiteral());
		assertEquals(FieldValueGetters.LAST_NAME, list.get(2).getGetter());
		assertEquals(ComparisonOperators.LIKE, list.get(2).getComparator());
	}
	
}
