package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ComparisonOperatorsTest {
	
	@Test
	public void testEquals() {
		assertFalse(ComparisonOperators.EQUALS.satisfied("Andrija", "Ante"));
		assertTrue(ComparisonOperators.EQUALS.satisfied("Marta", "Marta"));
	}

	@Test
	public void testNotEquals() {
		assertTrue(ComparisonOperators.NOT_EQUALS.satisfied("Andrija", "Ante"));
		assertFalse(ComparisonOperators.NOT_EQUALS.satisfied("Marta", "Marta"));
	}

	@Test
	public void testLess() {
		assertTrue(ComparisonOperators.LESS.satisfied("Ana", "Karlo"));
		assertFalse(ComparisonOperators.LESS.satisfied("Franka", "Bruno"));
		assertFalse(ComparisonOperators.LESS.satisfied("Marta", "Marta"));
	}
	
	@Test
	public void testLessOrEquals() {
		assertTrue(ComparisonOperators.LESS_OR_EQUALS.satisfied("Ana", "Karlo"));
		assertFalse(ComparisonOperators.LESS_OR_EQUALS.satisfied("Franka", "Bruno"));
		assertTrue(ComparisonOperators.LESS_OR_EQUALS.satisfied("Marta", "Marta"));
	}
	
	@Test
	public void testGreater() {
		assertFalse(ComparisonOperators.GREATER.satisfied("Ana", "Karlo"));
		assertTrue(ComparisonOperators.GREATER.satisfied("Franka", "Bruno"));
		assertFalse(ComparisonOperators.GREATER.satisfied("Marta", "Marta"));
	}
	
	@Test
	public void testGreaterOrEquals() {
		assertFalse(ComparisonOperators.GREATER_OR_EQUALS.satisfied("Ana", "Karlo"));
		assertTrue(ComparisonOperators.GREATER_OR_EQUALS.satisfied("Franka", "Bruno"));
		assertTrue(ComparisonOperators.GREATER_OR_EQUALS.satisfied("Marta", "Marta"));
	}
	
	@Test
	public void testLike() {
		assertFalse(ComparisonOperators.LIKE.satisfied("Ana", "*en"));
		assertFalse(ComparisonOperators.LIKE.satisfied("AAA", "AA*AA"));
		assertTrue(ComparisonOperators.LIKE.satisfied("AAAA", "AA*AA"));
		assertTrue(ComparisonOperators.LIKE.satisfied("AAAAAAAA", "AA*AA"));
		assertTrue(ComparisonOperators.LIKE.satisfied("Ana", "A*a"));
	}
	
}
