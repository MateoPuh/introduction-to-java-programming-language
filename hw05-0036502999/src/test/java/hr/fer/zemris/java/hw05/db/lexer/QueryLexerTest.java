package hr.fer.zemris.java.hw05.db.lexer;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class QueryLexerTest {

	@Test
	public void testNullException() {
		assertThrows(NullPointerException.class, () -> new QueryLexer(null));
	}
	
	@Test
	public void testEmptyString() {
		QueryLexer lexer = new QueryLexer("");
		
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.EOF, null)));
	}
	
	@Test
	public void testEOF() {
		QueryLexer lexer = new QueryLexer("");
		
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.EOF, null)));
	}
	
	@Test
	public void testString() {
		QueryLexer lexer = new QueryLexer(" \"testiranje234!\" ");
		
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.STRING, "testiranje234!")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.EOF, null)));

	}
	
	@Test
	public void testAttribute() {
		QueryLexer lexer = new QueryLexer(" attribute ");
		
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.ATTRIBUTE, "attribute")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.EOF, null)));
	}

	@Test
	public void testOperator() {
		QueryLexer lexer = new QueryLexer(" attribute = >= LIKE");
		
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.ATTRIBUTE, "attribute")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.OPERATOR, "=")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.OPERATOR, ">=")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.OPERATOR, "LIKE")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.EOF, null)));
	}
	
	@Test
	public void testAnd() {
		QueryLexer lexer = new QueryLexer(" attribute = and >= anD AND LIKE");
		
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.ATTRIBUTE, "attribute")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.OPERATOR, "=")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.KEY_WORD_AND, "and")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.OPERATOR, ">=")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.KEY_WORD_AND, "anD")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.KEY_WORD_AND, "AND")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.OPERATOR, "LIKE")));
		assertTrue(lexer.nextQueryToken().equals(new QueryToken(QueryTokenType.EOF, null)));
	}
	
}
