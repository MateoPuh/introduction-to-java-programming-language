Korekcije:
==========

-u metodi setAngle u LSystemBuilderImpl sam promijenio:
		this.angle = arg0;
	u:
		this.angle = Math.toRadians(arg0);
		
-u DrawCommand i SkipCommand sam maknuo:
		ctx.pushState(next);
	i dodao:
		current.setPosition(next.getPosition());

Recenzije:
==========

1) Bago, Marko (0036493964)

1. zadatak
-plantove ti ne iscrtava dobro pa ti padaju minimalni uvjeti
-produkcije ti generira dobro, ali problem je u DrawCommandu, 
dok stavim svoj kod iz DrawCommand ti program radi kako treba, 
trebaš ažurirati poziciju sa currentState.setPosition(next), 
a ti si pushao stanje na stack.
-dok stvaraš početni turtle state si promijeni da ti kut pretvara u radijane

2. zadatak
-sve radi kako treba, nema primjedbi
-pohvale za dokumentaciju

Zadaća je solidno riješena, ali nažalost zbog 1. zadatka je za 1

2) Bakula, Silvana (0036511066)

Ocjena za ovu zadacu je nazalost 1 jer se plant2 ne iscrtava dobro,
pa nisu zadovoljeni min.uvjeti za taj zadatak, a inace bi ocjena bila 5.

1. zadatak:
Greska je u implementaciji draw i skip commanda,jer je trebalo izravno 
modificirat trenutno stanje na stogu. Kad sam to namjestila,sve je radilo.
Jos treba pretvoriti stupnjeve u radijane u rotate command i metodi setAngle.

-Nije nikakva greska sto si napravio lexer i parser,ali u ovom slucaju 
mozda bespotrebna komplikacija,s obzirom da redak uvijek zapocinje s 
nekom od kljucnih rijeci,ako razdvojis po prazninama(to zapravo u lekseru 
i radis,a moze biti jedna linija koda),onda prvi clan mora biti neka od njih 
i dalje samo brojevi koje treba parsirati(iznimka kod unitLengthDegreeScaler,
ali nista puno kompliciranije). 

2.zadatak
-getQuery se mogla razbit,a inace jako dobro napisano, nisam dobila nigdje stack trace.